Chapter 4 - Brute
=================

Brute is a measurement of your beefiness, your ability to stand and take punishment, and the strength you put behind your actions. The brute is strong, difficult to take down, will fight the longest and push the hardest. If you’re the brute amongst your fellow adventurers, odds are you’ll be meleeing opponents while your friends stand back. You’ll be the one taking hits and dealing them in kind.

What can you do with your Brute attribute?

Breath Holding c

Whether it be diving into a lake to grab treasure from the bottom, or attempting to not breathe in a poisonous gas, you’re doing your best to keep clean air inside your lungs for as long as possible.

Last Breath: When you decide that you’re going to hold your breath, you immediately take a gulp of air and hold it. From that point on, you can hold your breath based on your Brute roll:

?tierTable
        15 turns (about a minute and a half)

        30 turns (about three minutes)

        75 turns (about seven and a half minutes)

        200 turns (about twenty minutes)

No Last Breath: However, if you were caught by surprise by the sudden lack of air and didn’t have enough time to take a breath, that’s going to drastically lower the amount of time you can hold your breath. Instead, your brute roll will use this chart:

?tierTable
        5 turns (about half a minute)

        10 turns (about a minute)

        20 turns (about two minutes)

        40 turns (about four minutes)

Difficult Lifting c

You can use your bodily strength to attempt to pick heavy things up to move them. Lifting can also be applied when using your strength to move something attempting to crush your party, such as a boulder falling on the party or a lowering ceiling trap.

?tierTable
        You can barely lift it. After 3 turns, you’ll drop it, and you cannot move while holding it.

        You can lift it and move, but it costs 2 extra action points to do so. After 10 turns, you’ll drop it.

        You can lift it and move around fairly easily. It costs 1 extra action point to move, and you can hold it for 30 turns.

        The object is proving to be no problem. You can move normally and put it down at your leisure.

Forceful Intimidation c

Resist: Brute or Spirit (at their discretion, tiers down)

Attempt Cost: 1 AP

You’re big, strong, and scary, and they should avoid you at all costs! You can intimidate anyone, regardless of whether or not they can actually understand you. You can intimidate anyone who can see you.

?tierTable
        They are noticeably aware of your physical prowess.

        They feel inclined to avoid you, making them unable to spend their next action point against you.

        They know they should avoid you, making them unable to spend their next two action points against you.

        They want nothing less than to avoid you, making them unable to spend their next three action points against you!

Note: A single target can only be affected by one intimidation at a time.

Hold c

Brute can be used for holding an item in place, a door shut, and keeping a firm grip on a priceless artifact. If you are holding something, and another person is trying to get it, you would simply make opposed Brute rolls. Since you’re acting defensively, if there is a tie, they win.

Pulling c

Instead of attempting to lift an object, you choose to pull it on the ground. While easier, pulling an object requires extra effort to move.

?tierTable
        Move +3 AP (not possible if you only have 3 AP per turn)

        Move +2 AP

        Move +1 AP

        As a Move

Narrator: Determining Weight

Obviously certain objects are going to be heavier than others. The bigger the object, the higher the penalty (or bonus) to Brute when attempting to lift or pull. A giant statue should be coupled with a high penalty to Brute. However, lifting should only be used when a large amount of strength would be required. For example, players shouldn’t have to roll on lifting a small box of feathers. Another thing to note is the object’s surface. A wide, completely flat-surfaced stone block may be lifted, but has no hand-holes to be pulled by.

* * *

Brawl Skill
-----------

Brawlers have no respect for such things as personal space, codes of honor, or traditional weaponry. They fight hard, they fight dirty, and they fight to win. Brawlers are often experts with unarmed blows and using their fearsome strength to cripple, grab, and kill their enemies. Yet sometimes too will an enormous man with an even more enormous axe get right in and brawl as well.

Block with a Grab

Brawl Specialty

Cost: 0 AP reflexively

When your opponent throws their punch at you, you don’t just block their fist: you grab hold of it. Whenever you successfully evade an attack by an opponent who is within your reach and you are able to grab them, you may automatically (and for 0 action points) attempt a grab upon their hand (or, if they attacked you with a different body part, the grab will be upon that location).

Dirty Fighting

Brawl Specialty

Stance (costs 1 AP to enter)

You’re an expert at hitting where it hurts and causing the opponent to flinch. While you are in this stance, every time your opponent fails to resist one of your called shots, they open themselves to reflexive attacks from those adjacent to them. The reflexive attacks cost the normal amount of action points from those who decide to take the opportunity. Since you caused it, you cannot make a reflexive attack from this.

Drunken Boxing

Brawl Specialty

Cost: Called Shot +1 AP

When you make a called shot, it’s so wild and unpredictable that it’s difficult to evade. When you make a drunken called shot (whether you’re sober or not), you roll your die to determine the called shot location randomly. By doing this, you give yourself a bonus to your accuracy equal to your skill in Brawl.

Fisticuffs

Brawl Specialty

Stance (costs 1 AP to enter)

Bare-knuckle boxing is your forté. When you enter this stance, you are ready to do some serious damage using just your fists. You can only be in this stance if you have at least one hand not holding anything. While in this stance, your unarmed attacks are one damage class higher, plus an additional damage class per 6 skill points you have in Brawl.

        So, if you have 12 points in Brawl, while you’re in this stance, your unarmed attack would have a damage class of 5 (2 normally, plus 1 for the stance, plus an additional 2 for the 12 points you have in Brawl).

Fluid

Brawl Specialty

Cost: 1 AP reflexively

You control all those who attack you. Any time an adjacent opponent attacks you but you successfully evade, you may spend 1 action point reflexively to move your attacker into any adjacent, unoccupied space that has solid, non-lethal ground for them to stand upon.  

Grapple

Brawl Specialty

Stance (costs 1 AP to enter)

Once you grab somebody, you can turn it into a full on grapple, shutting down their ability to move and making it difficult for them to take any actions. You must be grabbing somebody to enter a grapple stance, and you can only stay in this stance while grabbing that person (thus, if they resist, you are knocked out of stance). Anything that would let them break free of the grab will automatically knock you out of this stance. When you switch your grab to a grapple, you are no longer grabbing a single location - instead, you are now grappling their entire body. (For the purposes of other specialties, however, it still counts as a grab.)

        A person who is grappled by you cannot move, just like being grabbed. If they try to take any action, they suffer penalties on every roll they make. Any time a grappled opponent rolls their die (except for random rolls), roll your Brawl, tier the results, and give them the corresponding penalty to their roll.

?tierTable
        -3 on the roll

        -6 on the roll

        -9 on the roll

        -12 on the roll

Heavy-Handed

Brawl Specialty

Cost: Unarmed Attack +1 AP

The blows from your unarmed attacks are so powerful that they feel like they’re coming from giant hammers. When you make a heavy-handed attack, roll your Brawl before you deal damage but after you’ve succeeded in your accuracy roll in order to increase the damage class that you deal.

?tierTable
        +3 damage class

        +4 damage class

        +5 damage class

        +6 damage class

Hold Steady

Brawl Specialty

You’ll grab the bloke while your friends beat the tar out of him. When you’re grabbing somebody, anybody who attacks that person gains a hefty accuracy bonus. They get +3 on their accuracy roll, +1 for every 4 skill points you have in Brawl (being a +4 at 4 points, a +5 at 8 points, and so forth).

Knock Aside

Brawl Specialty

Cost: Deflect (1 AP reflexively)

With a swift knock, you can push aside an attack with the sheer force of your body. You may reflexively deflect attacks from melee weapons, bows, or thrown weapons with your bare hands. When you use such a deflection, you gain a bonus to your evade of +3 plus an additional +1 per 8 skill points you have in Brawl (for +4 at 8 skill points, +5 at 16 skill points, and +6 at 24 skill points).

Monkey Wrestler

Brawl Specialty

Requires: 5 skill points in Brawl

You wrap your legs around their torso, sink your teeth into their arm, grab their face, wrap your elbow around their eyes, and you still have a spare hand to use your dagger. For every 5 points you have in Brawl, you may grab an additional location on a person. Normally you can only grab two locations (assuming you have two hands). You may choose not to use your hands to initiate a grab if you have enough points to do so.

Reversal

Brawl Specialty

Cost: 2 AP reflexively

The only person who’ll be doing the grabbing in this fight is you. If someone successfully grabs you, you can attempt to reverse the grab and capture your captor. Re-roll your resist for your opponent’s grab, but add your skill in Brawl to the roll. If you succeed, they must roll a resist against your Brawl to avoid getting grabbed on a called-shot location of your choice.

Shrug Away

Brawl Specialty

Let’s just say that you don’t like being touched. If you are successfully grabbed, you may immediately make another resist (at no action point cost) with your Brawl skill added to the attribute you’re using to resist.

Throat Jab

Brawl Specialty

Resist: Brute (negates)

Cost: Called Shot to the Neck reflexively

If anybody adjacent to you begins to speak, you can make a called shot to their neck reflexively in order to shut them up. If they fail on their resist, they cannot talk or use their voice (including using specialties that rely on speaking, like Encouragement) until they either spend 1 action point to clear their throat or they wait until the end of their next turn.

p Bone-Breaking Specialties M

Bone-Breaker

Brawl Specialty

Cost: Unarmed Called Shot +1 AP

You smash a precision blow into the opponent, causing a called shot that’s nearly impossible to resist. Do your called shot normally. If they succeed in resisting, you are able to roll your strike again to make them re-resist. The bone-breaker only does damage based on the original strike.

?tierTable
        1 re-roll

        2 re-rolls

        3 re-rolls

        4 re-rolls

Crippling Blow

Brawl Specialty

Requires: Bone-Breaker specialty, 8 skill points in Brawl

Cost: Bone-Breaking Attack, +1 AP per location

Your bone-breaking attack reverberates through the victim, crippling much of their body. When you make your bone-breaking attack, you may spend an extra action point in order to have the same bone-breaker effect against another called shot location. You may affect as many called shot locations as you’d like - each one costs 1 additional action point.

        So, say you make a bone-breaker attack against a person’s neck. You get a tier 3 on your bone-breaker, so they have to make (a ridiculous) 4 resists in order to not be affected. In addition, you spend 2 extra action points to activate the called shot effects for the torso and the hand, which now also require 4 resists to avoid.

p Combo Specialties M

Combo Flow

Brawl Specialty

Requires: 4 skill points in Brawl

You rage like rapids, following the path of least resistance. Every melee attack that you land during a turn grants you a +2 on accuracy and strike rolls for the rest of your turn, and this bonus accumulates with every successful melee attack until the end of your turn.

        So, if you landed one melee attack, your next attack would have a +2 on accuracy and strike. After your second successful attack, the third one would have a +4 on accuracy and strike. This would continue to grow with every successful attack.

Combo Opener

Brawl Specialty

Requires: Combo Flow specialty

Cost: Unarmed Attack +1 AP

You may perform a combo opener in order to improve the potency of your combo flow. If you successfully land your combo opener (which for most purposes just looks like a normal unarmed attack), it might count as having successfully landed multiple attacks.

?tierTable
        Counts as two attacks (granting a +4 on accuracy and strike for the next attack).

        Counts as three attacks (granting a +6 on accuracy and strike for the next attack).

        Counts as four attacks (granting a +8 on accuracy and strike for the next attack).

        Counts as five attacks (granting a +10 on accuracy and strike for the next attack).

Combo Breaker

Brawl Specialty

Any time an adjacent opponent successfully damages you with two separate attacks in one turn, you may perform a combo breaker - a free unarmed attack against your assailant that uses your skill in Brawl in place of your accuracy.

Finisher

Brawl Specialty

Requires: Heavy-Handed specialty

If you successfully make two unarmed attacks in one turn and still have an additional action point, you can make a finisher. A finisher is just like a heavy-handed attack, except that it does not cost the extra action point to perform.

p Grip Specialties M

Crushing Grip

Brawl Specialty

Requires: +4 Strike

Your grabs are vicious, crushing the opponent. When you make a grab, you also deal damage as if you were attacking with the attack normally.

Twist

Brawl Specialty

Requires: Crushing Grip specialty

Cost: 1 AP

Once you have a person grabbed with a hand, you may twist that location to inflict pain. For 1 action point, you may automatically deal damage as per an unarmed attack (without the need to roll accuracy and evade). You may also, for 1 action point, activate the called shot that you have grabbed, rolling strike solely to determine the necessary resist but otherwise doing no damage. Though this does deal damage, it does not act as an attack.

* * *

Frenzy Skill
------------

Frenzy is an emotionally-charged rage, throwing yourself out there and giving people everything they don’t want. Frenzy’s not for the weak - you lose control easily and take haphazard movements all over the battlefield. When you’ve broken your limits, you are literally uncontrollable. Yet your opponents will never know this weakness. They’ll be dead long before that.

Adrenaline Surge

Frenzy Specialty

The deaths of your opponents sends a reinvigorating rush through your body that pushes you to continue fighting. Whenever you deal a fatal effect or kill an opponent, you regain some of your hit points.

?tierTable
        Immediately regain 3 hit points

        Immediately regain 6 hit points

        Immediately regain 9 hit points

        Immediately regain 12 hit points

Backlash

Frenzy Specialty

Requires: 7 skill points in Frenzy

Cost: 1 AP reflexively

Large amounts of damage don’t stop you - rather, they enrage you! Any time you take 10 or more damage (after damage soak), you can make a reflexive attack against your assailant using your skill in frenzy in place of your accuracy. This reflexive attack only costs you 1 action point to make.

Burning Revenge

Frenzy Specialty

Cost: 1 AP reflexively

Your foe drives you to greater strength. If an opponent damages you, you may “collect” that damage in order to add it as a bonus on your next strike roll against that opponent. When you are hit, you may spend 1 action point reflexively to savor the damage (after damage soak). The next time you attack that opponent, you gain the damage as a bonus on the strike roll.

Carry Through

Frenzy Specialty

Cost: Melee Attack +1 AP per adjacent opponent

You carry your blow through one opponent and into another. Once you’ve successfully attacked one opponent within your melee reach, you may continue the attack, cleaving through more opponents within your reach. For every additional opponent you choose, you must spend 1 more action point. You may not select the same opponent twice. You must still roll accuracy to determine if you hit, but you deal the same amount of damage that you dealt with the first attack.

        If you were using any other modifying specialties (that would make it an “Attack +1 AP”), they only apply to the first target.

Crimson Weapon

Frenzy Specialty

Resist: Brute (tiers down)

Cost: Melee Attack +1 AP

You learn to make your weapon strike slice deep into the flesh of your foes. If an opponent takes damage from your crimson weapon, they will begin bleeding at the start of their next turn (and ever turn thereafter). Bleeding damage is unsoakable, but the opponent can roll their Brute to lower the tier result. A person can stop 5 points worth of bleeding by spending 1 action point patching the wound.

?tierTable
        Bleed for 2 damage per turn

        Bleed for 4 damage per turn

        Bleed for 6 damage per turn

        Bleed for 8 damage per turn

Fray Fighter

Frenzy Specialty

Cost: 3 AP

An angry mob of slobbering beasts stare you down, looking to rip you apart piece by piece. For you, this is just another day at work. Using a heavy or smaller melee weapon, you can engage the fray. Roll your tier to determine how many opponents adjacent to you that you hit. They are allowed to roll defense against the attacks.

?tierTable
        Tier 1 damage to 2 adjacent opponents

        Tier 1 damage to 3 adjacent opponents

        Tier 1 damage to 4 adjacent opponents

        Tier 1 damage to all adjacent opponents

Hundred Strikes

Frenzy Specialty

Cost: All of the AP you can spend in a single turn

When you choose to use your hundred strikes ability, you must still have your maximum amount of action points for the turn and be wielding a heavy or smaller melee weapon. You designate a single adjacent opponent as the target of your hundred strikes. For one action point apiece, you can make a melee attack against that opponent. After every attack, you and the target move a single space. The target chooses which space he will move into, and you must either follow or forego the rest of your turn.

Liberator

Frenzy Specialty

Cost: 1 AP reflexively

You don’t like being touched, held, grabbed, or grappled. Whenever you are grabbed, you can make a melee attack called shot against the person grabbing you in whatever way is going to get them off (often by making a called shot against their hand, but you can also make a called shot against their torso to push them away, or a called shot against whatever else they’re using to grab you). You can do this reflexively when they first grab you, and, if you fail, you can continue to make these melee called shots against your assailant for just 1 action point until they let go.  

Merciless

Frenzy Specialty

Stance (costs 1 AP to enter)

Once you go down this path, there’s no going back. Once you enter your merciless stance, you cannot voluntarily exit it. Upon entering, you choose a target of the stance. As long as you are aware of the target or believe the target to be within a couple hundred feet, you cannot exit your stance. While in this stance, you can only attack your target and people who are directly preventing you from getting to your target. If you are not engaged with your target, you must spend at least 1 action point every turn moving toward your target.

        While this stance is active, once per turn you may use your skill in frenzy as a bonus to any one of your combat rolls, or divide it among several. For example, you may add your skill in frenzy to one strike roll, or you may give half of your skill in frenzy to one accuracy roll and the other half to one evade roll. These combat rolls must be made in opposition of the target of your merciless stance.

Neverending Bloodbath

Frenzy Specialty

For every enemy you kill during your turn, you gain 1 action point that can only be used for running toward another enemy.

No Escape

Frenzy Specialty

Cost: As Moving, reflexively

Though your enemies may attempt to retreat, you’re prepared to give chase. Any time a foe that you’re engaged with attempts to move away from you using their land speed, you reflexively follow them. If an opponent’s speed is greater than yours (but within 20 feet), you rise to the challenge and match their speed.

Raging

Frenzy Specialty

Stance (costs 1 AP to enter)

Nothing can stop you. You will destroy everything in your path. When you make an attack while raging, you pull your bonuses from your accuracy, evade, and defense in order to add them to your strike roll. Add your accuracy, evade, and defense bonuses together and apply that number as a bonus to your strike. While in this stance, however, you do not gain any bonuses on your accuracy, evade, or defense rolls. If you leave this stance (either voluntarily or by being forced out of it), you do not gain your bonuses to evade and defense back until the end of your next turn.

Straining Blow

Frenzy Specialty

Cost: as a Melee Attack

You push yourself to your maximum, destroying yourself in order to lay waste to your opponents. When you make a straining blow, you may deal 5 unsoakable hit point damage to yourself in order to add a +1 damage class to your attack. You may deal as much damage to yourself as you’d like in order to gain additional damage classes, but you cannot deal more damage to yourself than you have hit points. Once you are out of hit points, you cannot deal straining blows.

Soulless Blade

Frenzy Specialty

Cost: 2 AP reflexively

When you mean to finish an opponent, you do so mercilessly. After you’ve made a melee attack that deals wounds damage to the target, you may spend 2 action points reflexively in order to convert it into a soulless blade. Now, instead of rolling on the wounds chart, they roll for a fatal effect.

Walking Destruction

Frenzy Specialty

Requires: 15 skill points in Frenzy

Cost: Move + Melee Attack + 2 AP

You make a single move. During this move, you may attack anybody that you become adjacent to. You may not target the same person more than once.

        Other specialties cannot be applied to this attack.

p Bloodlust Specialties M

Berserker

Frenzy Specialty

Stance (costs 1 AP to enter)

The sight of blood excites you. After successfully dealing damage to an opponent, you a gain +1 damage class against that opponent with all melee weapons. This bonus increases by +1 for every 6 skill points you have in frenzy. This bonus can be used against multiple opponents.

Bloodlust

Frenzy Specialty

Requires: Berserker specialty & 6 skill points in Frenzy

Cost: 1 AP

In the whirling chaos of battle you are a singular force of destruction - relentless and unstoppable. When you enter into berserker stance, you may spend an additional action point to upgrade your stance and start bloodlusting. While bloodlusting, your damage class with melee weapons increases by 1 for every enemy within 25 feet of you.

        This bonus replaces the normal bonus you would get from your berserker stance. It is automatically added to all melee weapon attacks, regardless of whether you have attacked the foe before or not.

        If you are knocked out of berserker stance, you must re-enter the stance and spend the extra action point to begin bloodlusting again.

Unquenchable Thirst

Frenzy Specialty

Requires: Berserker & Bloodlust specialties & 12 skill points in Frenzy

Cost: 1 AP

Your unquenchable thirst for blood has become a full-fledged obsession. After you have entered your berserker stance and started bloodlusting, you may spend another action point to upgrade to unquenchable thirst. Now, everybody within 25 feet of you acts as an enemy (including allies) for the purpose of determining your bloodlust bonus. You may now make normal melee attacks (with a heavy weapon or smaller) for just 1 action point, but if anybody comes near you, enemy or ally, you are forced to make a reflexive melee attack against them, if at all possible.

        If you are knocked out of berserker stance, you must re-enter the stance and spend the extra actions point to begin bloodlusting and enter unquenchable thirst again.

p Masochistic Specialties M

Laugh Like You’re Crazy

Frenzy Specialty

Resist: Spirit (negates)

You’re a manic, psychotic, laughing vision of evil on the battlefield. For every 10 points of hit point damage you’ve taken, choose one opponent within 25 feet. That opponent is now suffering the effects of tier 1 fear. They may resist using their Spirit against your frenzy. If they resist, they cannot be affected again until after their next breather.

Marriage to Suffering

Frenzy Specialty

Requires: Laugh Like You’re Crazy specialty

When fighting on the edge, you fight even harder. When out of hit points, you gain a +1 damage class per point of wounds lost. This damage bonus is lost if your hitpoints are no long zero.  

Seize Your Suffering

Frenzy Specialty

Requires: 8 skill points in Frenzy, Laugh Like You’re Crazy & Marriage to Suffering specialties

Taking punishment has become your nourishment on the battlefield. For every point of wounds damage you take, you gain an immediate action point. The action point only lasts for the turn the damage was taken. You may gain no more than 1 action point from this in a single turn than 1 per 8 skill points you have in Frenzy.

        For example, if you take 3 wounds damage this turn, you gain 3 action points in addition to your normal allotment of action points for this turn.

* * *

Overpower Skill
---------------

The ability to overpower an opponent is not one to be overlooked. Nothing can be scarier than a man whose blows can fell giants, strikes are so powerful that they can send a man high into the air, or swings so atrocious that they can sever the fabric that holds our reality together. Overpower is all about hitting your opponent hard and making sure that they never forget who it was that gave them that scar - the one on their shoulder, where their arm used to be.

Brickbreaker

Overpower Specialty

Cost: as a Melee Attack

Through sheer force you are able to break through cover and bypass walls. Whenever an opponent is on the other side of cover, you may attempt to bypass it with your melee attack (they must still be within range of your melee weapon, however). You can only break through cover that is as strong as brick. A reinforced wall of iron could not be broken with brickbreaker, for instance. The cover is not destroyed from the brickbreaker attack, but it is damaged.

?tierTable
        Bypasses poor cover

        Bypasses light cover

        Bypasses medium cover

        Bypasses heavy cover

Dragging

Overpower Specialty

Stance (costs 1 AP to enter)

You pull your weapon behind you, letting your weapon take your entire body weight upwards when you swing. Anytime that you deal 10 or more damage (after damage soak), the enemy loses their stance.

Follow-Through

Overpower Specialty

Requires: 4 skill points in Overpower

While your first attack can leave an opponent realing, your second attack is a true display of your power. If you make two consectutive and successful medium (or larger) melee attacks on your turn against the same opponent, your second attack’s damage is automatically one tier higher.

Heavy Hitter

Overpower Specialty

Stance (costs 1 AP to enter)

Requires: 4 skill points in Overpower

Whenever in this stance and using a heavy (or larger) melee weapon, add an extra point to your damage class for every 4 skill points you have in overpower.

Keep Them Down

Overpower Specialty

Cost: Heavy (or larger) Melee Attack

When an opponent is next to you and prone, you have no problem finishing them off. Whenever attacking a prone opponent with a heavy weapon, your damage automatically tiers up one.

Monstrous Attacks

Overpower Specialty

Cost: Super-Heavy Melee Attack +1 AP

Other people think that super-heavy melee weapons have a damage class of 10. You’re not sure what that means, but you know you can kill those people in one hit! By spending an extra action point when you make an attack with a super-heavy melee weapon, you deal considerably more damage. Your damage class increases by 2, plus an additional +1 per 6 skill points you have in overpower.

No Quarter

Overpower Specialty

Resist: Dexterity (negates, see below)

Cost: Heavy (or larger) Melee Attack +1 AP

You smash down, not targeting a single person, but their entire area. The only escape is for the target to move. When you make a no quarter attack, you are not attacking the person: you are attacking a single space. Anybody in this space is either automatically hit or must spend 1 action point reflexively in order to try to jump out of the space. If they choose to dodge, they roll their Dexterity. If their Dexterity exceeds your accuracy, they can move to 1 adjacent square. If it fails, you hit them.

        Note: If you are using any abilities that depend on the opponent’s evade, treat their Dexterity as evade. If the opponent chooses not to dodge the attack, assume their evade matches your accuracy.

One-Handing It

Overpower Specialty

You may wield two-handed weapons in one hand. Reloading a marksmanship weapon and using bows of any size still requires an additional free hand. For all purposes beyond how many hands the weapon requires, this specialty changes nothing.

Robust Toss

Overpower Specialty

Cost: Medium (or larger) Thrown Attack

Just because your opponent is out of reach doesn’t mean you can’t smash their face in. Whenever you use a medium or larger thrown weapon, you may deal extra damage with the attack. If it lands, roll your Overpower to determine its extra damage.  

?tierTable
        2 additional damage

        4 additional damage

        6 additional damage

        8 additional damage

Shield Whack

Overpower Specialty

Cost: Melee Attack conversion

Most people use their shields for protection, keeping their shield in between your weapon and their flesh. That’s an advantage that you’ll use. Whenever an opponent attempts to use a shield to deflect one of your attacks, you may instantly convert the attack into a shield whack. Though you’ll deal no damage with the attack, you hit the victim’s shield so hard that it staggers them, causing them to lose their stance, and they are disoriented for their next turn.  

Solid Assault

Overpower Specialty

Cost: Melee Attack +1 AP

You ready your strike and bring it in smoothly to deal just the right amount of damage. If you successfully hit with your solid assault, you deal damage as though it were one tier higher.

Stunning Blow

Overpower Specialty

Resist: Brute (tiers down)

Cost: Melee Attack +1 AP

With a well aimed strike, you stun your opponent. If the opponent fails to resist against your overpower and your receive a tier 2 result or higher, the target is stunned. The target may roll their Brute in order to resist. For every tier over Tier 1 that they receive, they lower the effect of Stunning Blow by one tier.

?tierTable
        No effect

        Stunned for 1 AP

        Stunned for 2 AP

        Stunned for 3 AP

Titanic Strength

Overpower Specialty

Requires: 3 skill points in Overpower

You lift the heaviest of weapons and swing them around as though they were tiny fencing blades. You do not need to enter into a footing stance when you use super-heavy melee weapons.

With Gusto

Overpower Specialty

Requires: +13 to Strike

Resist: Brute (negates)

When you attack with gusto, your attack is so powerful that no armor can stand against it. If your melee attack deals tier 4 damage (or greater), you negate all of your opponent’s damage soak from their armor unless they can make their resist against your overpower.

p Armor-Breaking Specialties M

Chipping Away

Overpower Specialty

Resist: Dexterity (tiers down)

Cost: Heavy (or larger) Melee Attack +1 AP

Your attacks wear on the opponent’s armor, slowly chipping it away until it falls apart. When you make a Chipping Away attack, you lower the soak class on the target’s armor by 1. The penalty can never send their soak class below zero, but chipping away does stack over time. If the armor’s soak class reaches 0, the armor is effectively destroyed (and any augments on it or bonuses that the target receives for wearing armor are negated). The opponent can negate the Chipping Away by making a Dexterity roll opposed by your Overpower roll.

        Note: Somebody with broken or damaged armor can patch it back together during a breather.  

Armor Sunder

Overpower Specialty

Requires: Chipping Away specialty

Resist: Dexterity (tiers down)

Cost: Heavy (or larger) Melee Attack +1 AP

The biggest obstacle between your sword and their heart is their armor, and you’re not beyond destroying that too. An armor sundering attack lowers the soak class on their armor. The penalty can never send their soak class below zero, but multiple armor sundering attacks can stack. If the armor’s soak class reaches 0, the armor is effectively destroyed (and any augments on it or bonuses that the target receives for wearing armor are negated).

?tierTable
        -2 soak class

        -3 soak class

        -4 soak class

        -5 soak class

Note: Somebody with broken or damaged armor can patch it back together during a breather.

p Earth-Shattering Specialties M

Earthquaking Strike

Overpower Specialty

Resist: Cunning (negates)

Cost: As a Heavy (or larger) Melee Attack

Rather than attacking the target, you attack the ground in front of the target, destroying the ground and destabilizing everyone around unless they can make a Cunning resist against your Overpower skill.

?tierTable
        The person standing in the area attacked is disoriented for one turn

        Everyone within 5 feet of the area attacked is disoriented for one turn

        Everyone within 5 feet of the area attacked is disoriented for two turns

        Everyone within 10 feet of the area attacked is disoriented for three turns

Rampant Destruction

Overpower Specialty

Requires: Earthquaking Strike specialty

Cost: Earthquaking Strike +1 AP

With just a bit more effort, you can turn your Earthquaking Strike into rampant destruction. The effect of your Rampant Destruction is exactly the same, except this doesn’t just cause the ground around you to vibrate, it causes the ground to explode outward. Roll only once for the Earthquaking Strike as for the Rampant Destruction.

?tierTable
        In the area struck, the ground is destroyed one foot down.

        In the area effected, the ground is destroyed three feet down.

        In the area effected, the ground is destroyed ten feet down.

        In the area effected, the ground is destroyed twenty feet down.

Note on Collapsing Structures: This is especially effective while standing on a bridge, on the second floor of a building, or while out on the streets with a sewer underneath. Remember, however, that if you are in the area of the rampant destruction’s effect, you too will fall down.

p Push Away Specialties M

Staggering Strike

Overpower Specialty

Resist: Brute (tiers down)

Cost: Melee Attack +1 AP

Throwing yourself completely into the attack, you toss your opponent into the air like a rag doll. You knock them back several feet and potentially prone. The target may roll their Brute in order to resist.

?tierTable
        5 feet

        10 feet

        10 feet and prone

        15 feet and prone

Bullrush

Overpower Specialty

Requires: Staggering Strike specialty

Cost: Move + Melee Attack

When you charge at an opponent, you can throw them backwards. If you run toward an opponent in a straight line (for a minimum of 15 feet) and then make a melee attack, your melee attack is automatically a Staggering Strike.

        If you so choose, you may also move with the target (staying adjacent to them) for the distance that you send them from the Staggering Strike. This extra movement has no cost.

* * *

Resilience Skill
----------------

Resilience is your ability to weather attacks, continue on, and protect your allies. A character with resilience can use their great strength to keep the good fight going and never stand down. Resilience makes a character almost impossible to take down, and so builds the perfect front-line fighter and person that you want between the enemy and the group’s travelling scientist.

Blast Proof

Resilience Specialty

Resist: Cunning (tiers down)

Cost: Shield Deflection +1 AP reflexively

In the face of explosives, blasts, and storms, you raise your shield and carry on, shielding yourself and your allies from the blast. Whenever you are in the midst of an explosion or similar effect that has a blast area, you can negate the effect upon yourself and potentially adjacent spaces unless the originator of the effect resists against your skill in Resilience.

?tierTable
        Negates effect in your space

        Negates effect in your space & 1 space behind you

        Negates effect in your space & 2 spaces behind you

        Negates effect in your space & 4 spaces behind you

Body of Steel

Resilience Specialty

Your armored training allows you to push past called shots. When an opponent attempts a called shot on you, you may add your defense to the resist against the called shot.

Brace for Impact

Resilience Specialty

Cost: as a Shield Deflection

Instead of attempting to evade the attack, you Brace for Impact. You may Brace for Impact at any time that you would normally be able to deflect a blow. Bracing for Impact converts your evade bonus for deflecting into a defense bonus instead.

Bulwark

Resilience Specialty

Stance (costs 1 AP to enter)

You ready yourself for any attack, becoming an untouchable bulwark. While in this stance, roll twice for your defense rolls and take the higher result. You still add your defense bonus to the roll of your choice.

Interposition

Resilience Specialty

Resist: Dexterity (negates)

Cost: Move +1 AP reflexively

You are able to gauge an opponent’s intent to strike a friend, allowing you to Interpose yourself between them and one of your allies. You must decide to Interpose yourself before your ally rolls their evade. For the cost of a move +1 action point, you may make a single move to place yourself in front of the attack.

        If the attack is a melee one, you must end your move adjacent to both the ally being attacked and to the person making the attack.

        If the attack is a ranged one, you must end your move in-between your ally and the person making the attack.

        Furthermore, the person making the attack is allowed to resist against your resilience. If they successfully resist, the attack hits the intended target instead of you. If the resist fails, the attack automatically hits you.

Metal Embrace

Resilience Specialty

Cost: 1 AP reflexively

Any time you are struck in combat, you may, for 1 action point, make a resilience roll, soaking an amount of additional damage as determined below.

?tierTable
        Soak 3 additional damage

        Soak 6 additional damage

        Soak 9 additional damage

        Soak 12 additional damage

Never Off-Guard

Resilience Specialty

You are always ready for an attack. Normally, your hit points go down after combat while you’re resting or socializing. Your hit points are always, at least partially, ready to go. When not in combat, you always have a number of hit points up equal to twice your skill in Resilience, even when you’re not conscious. Of course, you can’t have more hit points up than your maximum.

Press

Resilience Specialty

Stance (costs 1 AP to enter)

Resist: Dexterity (negates)

You choose a target, and as long as you’re adjacent to that target, you can completely block him from attacking anybody but you. When you enter this stance, choose a target of the stance. To choose a different target, you must re-enter the stance. As long as you are adjacent to the target, if the target tries to attack anybody, they must succeed at the resist. If they fail, they do not attack and, instead, lose 1 action point.

Protector

Resilience Specialty

Cost: as a Shield Deflection

You may protect those around you using your shield. Any time an adjacent ally would be the target of an attack, you may use your shield to deflect the blow for them. The ally gains any bonuses that you would gain for your deflection.

Resolute

Resilience Specialty

Requires: 2 or more stances known from the Resilience skill

You are the ultimate sentinel, transforming yourself into an impregnable barrier. When you enter into one of your stances from the Resilience skill, you may simultaneously enter all of your known Resilience stances and keep all of them active (as long as they don’t negate each other for any reason). They all act as one stance, so if you get knocked out of your stance, you get knocked out of all of your stances.

Second Skin

Resilience Specialty

Cost: 1 AP reflexively

Attacks which ignore damage soak still have trouble with you. Whenever you are subject to an attack that is going to ignore your damage soak, you may spend 1 action point to convert it into soakable damage.

?tierTable
        Up to 3 points of unsoakable damage made soakable.

        Up to 6 points of unsoakable damage made soakable.

        Up to 9 points of unsoakable damage made soakable.

        Up to 12 points of unsoakable damage made soakable.

Solid Stances

Resilience Specialty

You have great balance and you understand how to keep your posture. As long as you’re conscious, you cannot be voluntarily knocked out of your stance(s) from being pushed back or knocked prone.

Thick Skin

Resilience Specialty

Your body naturally soaks some damage. Your body has a natural soak class of 1. Furthermore, for every 5 skill points you have in Resilience, you have an additional soak class of 1. So, if you have a 20 in Resilience, you have would a soak class (without armor) of 5. This soak class stacks with armor.

Tough Stuff

Resilience Specialty

You gain bonus hit points depending on your skill in Resilience and how many specialties you have. For every specialty you have (including this one), you gain 1 extra hit point. For every 8 skill points you have in Resilience, that number increases by 1. Thus, if you have 8 skill points in Resilience and 6 specialties, you would have 12 extra hit points.

Unassailable Mountain

Resilience Specialty

Requires: you to be wearing heavy (or heavier) armor

Cost: 3 AP reflexively

For 3 action points, you may greatly increase your damage soak. This can be decided after the damage has been announced. If the attack was a special attack, any other effects from the attack still apply. This ability only works while in heavy (or heavier) armor. To determine how much your soak class increases, roll below:

?tierTable
        +4 soak class

        +5 soak class

        +6 soak class

        +7 soak class

Walking Fortress

Resilience Specialty

Stance (costs 1 AP to enter)

Requires: 3 skill points in Resilience

While in the Walking Fortress stance, your defense skyrockets. You gain a +1 to your defense for every 3 skill points you have in resilience.

Ward

Resilience Specialty

Stance (costs 1 AP to enter)

Cost: 1 AP reflexively

You won’t allow your foes to pass by you unscathed. When an opponent moves into your melee range, you can make a reflexive attack against them for 1 action point. If an opponent moves from one space within melee range to another space within melee range, this also leaves them open to your reflexive attacks. A single opponent can only be the target of this specialty once per turn.

p Armored Movement Specialties M

Armored Ease

Resilience Specialty

When you are wearing armor, you may consider it one degree lighter at your discretion. Therefore, you can treat your medium armor as light armor for determining penalties but still gain all of the benefits of wearing medium armor.

Armored Freedom

Resilience Specialty

Requires: Armored Ease specialty & 7 skill points in Resilience

Now, while wearing armor, you may consider it two degrees lighter for determining penalties (in addition to the one degree gained from Armored Ease). Thus, you could be wearing super-heavy armor, but only have the penalties of light armor.

p Barrier Specialties M

Living Barrier

Resilience Specialty

Stance (costs 1 AP to enter)

Resist: Dexterity (negates)

Nothing bypasses you. When you enter your living barrier stance, you strategically place yourself so that you take up three adjacent spaces (as in, your normal space plus two more) and you may attack anything adjacent to your new size.

        Opponents attempting to move through your new space must make a Dexterity resist against your resilience. If the opponent fails, they may not enter your new space and their movement is stopped.

Living Wall

Resilience Specialty

Requires: Living Barrier specialty

Resist: Dexterity (negates)

Cost: 1 AP reflexively

While in living barrier stance, if an opponent attempts to attack someone through you or inside your newly expanded space, you may spend 1 reflexive action point to intercept this attack. In doing so, you receive no evade roll (acting as if the evade roll was a 1) and are hit with the attack. The opponent may roll a Dexterity resist against your Resilience still in order to attempt to attack his original target.

Living Stronghold

Resilience Specialty

Resist: Dexterity

Requires: Living Barrier & Living Wall specialties

Nothing bypasses you. While in living barrier stance, if an opponent attempts to target somebody through you or inside your expanded space and fails their Dexterity resist against you, the attack is negated. They lose all of the action points spent to make the attack.

* * *
