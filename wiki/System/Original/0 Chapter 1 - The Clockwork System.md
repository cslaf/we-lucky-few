Chapter 1 - The Clockwork System
================================

Entering the Clockwork
----------------------

There’s a lot of text in this book. Don’t be afraid…it won’t take you long to start enjoying your stay.

This is a richly customizable mechanism with a variety of rules, ticking tidbits, and random springs. Inside of this mechanism is a single clockwork movement that makes the rest of the system function. Once you’ve seen how the basic movement works, everything else won’t  seem so daunting.

### Dice & Modifiers

To begin, you’ll need a single die: a unique one with 12 sides. This is the foundation for the Clockwork System, a die that can randomly give you any number from 1 to 12.

With this special die, occasionally called the D12, you’ll be able to determine how successful you are on any given task. You’ll have skills, attributes, and various other traits that all have numbers assigned to them. When you need to determine how well you succeed at something, you’ll roll your die and add on the appropriate number.

Rolling your Dexterity? Roll your D12, add the number in your Dexterity attribute, and you’ve got your result.

### Four Tiers of Success

The game revolves around tiers - four tiers, to be exact. Once you’ve received the result of your die roll, you’ll be able to determine what tier it is.

|Tier | Dice Roll | Result|
|-------------|-----------------------|--------|
|T1 (Tier 1): | Result of 1 through 9 | Passing|
|T2 (Tier 2): | Result of 10 through 19 | A Solid Success|
|T3 (Tier 3): | Result of 20 through 29 | Phenomenal!|
|T4 (Tier 4): | Result of 30 or more | Beyond Human|

As you can see, tier 1 is going to be the most minimal of a success. If you’re trying to jump, all you can say is that you successfully jumped. Nobody takes notice up until you get to tier 2. A tier 3 is the utmost of human ability. If you’re hitting a tier 3, that means you’ve just leapt a chasm.

A tier 4? There are no words to explain a tier 4 success.

### Turns!

Let’s discuss combat. If you’re in a fight with somebody, you’re going to take turns. You’ll begin by rolling priority. The person with the highest priority goes first, the person with the next highest goes second, and so on. When it’s your turn, you’re going to have a number of action points (AP) that you can spend.

Let’s say it’s your turn, and you have 3 action points (the norm for starting characters). You could run (1 AP) and shoot your rifle (2 AP). Or, you could run (1 AP), open a door (1 AP), and then duck behind a barrel (1 AP). Or, you could swing your sword (2 AP) at somebody’s head (another 1 AP for aiming).

 The options are pretty open-ended.

### Attributes & Skills

As we’ve said, your character is going to have a lot of numbers associated with him. When you build your character (found in Chapter 2 - Character Creation), you’ll first distribute some points in your skills. Skills are those areas that you’ve trained in. Swashbuckling, Brawl, Gadgetry, Marksmanship - these are all skills.

Now, your skills are going to determine your attributes. Attributes are the core components of your character. If you have a total of 6 points in your Dexterity skills, your Dexterity will be a +6.

When do you use these numbers? For attributes: all the time. Lifting something? Brute. Picking somebody’s wallet? Dexterity. Trying to figure out a puzzle? Cunning.

### Specialties

The most exciting part of your character is their specialties. Specialties are the defining aspects of your character, things that make them awesome. At first level, you’ll start with three, and you’ll choose them from your skills. When you need to make a roll to determine how well you use your specialty, you’ll roll your skill.

### Hit Points, Damage, and all that stuff

A lot of your statistics will be determined by the specialties that you select, the stories that you gain, and the race that you choose. But, honestly, this is all clockwork. Once you start building a character, everything else will fall into place.

Breaking down the Mechanism
---------------------------

Did the last page not explain things enough for you? Then keep on reading.                                    

### Action Points (AP)

During your turn, you have 3 action points you can spend to move, attack, or do virtually anything. Every action has an action point cost, though some are free (and, often, can be done at any time).

:refresh: **Refresh**: Your action points refresh at the end of your turn. So as soon as your turn ends, you may start using reflexives that drain your next turn’s action point pool.

Reflexives: You can use action points reflexively. When you’ve trained yourself so well that you can act in a split second, that’s when you’re using your AP reflexively.

Reflexives are action points you can use out-of-turn. You might have a specialty, for example, that allows you to jump in front of an incoming bullet for an ally (Interposition, found under the Resilience skill). That specialty costs 2 action points reflexively, which you may do any time an ally is being attacked. Your reflexive action points come out of your next turn’s pool of action points. So if you used interposition, on your next turn you’d only have 1 action point left.

Your action point pool increases at certain levels. You’ll find those in this chart:

|Level | Ap|
|------|-----|
|lvl 1 | 3|
|lvl 4 | 4|
|lvl 8 | 5|
|lvl 12 | 6|


### Turn Order

When combat begins, every combatant rolls their priority, creating a “turn order.” Everybody starts the combat with their action points full, so they can start making reflexive actions right away. The person with the highest priority roll goes first. That person could then use any of their action points they have, and, once they’re done, their action points refresh.

 A person can never save action points for their next turn. When your action points refresh, they are hitting a hard reset and going to their normal value. Any action points from your previous turn that you did not use are wasted - so go ahead, use them!

### Attributes

Your attributes are the key building blocks of your character. Attributes reflect your training in your skills and are entirely based off of them. Every skill falls under one of our five key attributes: Brute, Cunning, Dexterity, Spirit, and Sciences. Once you’ve selected your skills, add up the skills under those attributes to get your points in that attribute.

You’ll find yourself using attributes both in and out of combat. Attributes are used to resist special attacks, called shots, poisons, and all other manner of contraptions. If you’re being tricked, you would resist with your Cunning attribute. If you’ve been hit in the head, you’d resist getting a concussion with your Brute attribute.

Likewise, attributes determine how well you interact with people and the setting. How easily you can sneak around depends on your Dexterity. Holding the door closed as enemies try to break in requires Brute. Any time you’re trying to convince somebody of something, you’ll roll your Cunning. Throughout the entire system, these five attributes will be keys to your character and help him or her grow.

### Hit Points & Wounds

Hit points are a reflection of a character’s toughness and willpower to keep on fighting. During combat, every time a character is successfully hit, their hit points will decrease. If all of a character’s hit points are depleted, a character begins to take wounds. Wounds are substantial damage taken to the character’s bodily form, such as deep gashes or wracking blows.

 Your hit points are determined by your specialties, and the amount of wounds you have will start at 12.

 Hit points are only in effect while you’re ready for action. When your character is asleep, in a focused conversation, or for some reason not paying attention, your hit points don’t come into play. Any time you are hit while not prepared, the damage goes straight into your wounds pool.

 Wounds Damage: Every time you take wounds damage, your character suffers a grievous hit. When you take wounds damage, you roll on our called shot chart and take the wound effect from that location. If you rolled “Left Hand,” for example, your hand would be smashed and you’d have a hard time grasping anything with it.

 If you’re hit by an attack and you run out of wounds, you immediately roll randomly on the called shot chart, this time for a fatal effect. If you rolled “Left Hand” again for the fatal effect, your hand would be severed and you could never use that hand again (unless somebody could create you a prosthetic hand). Though that might sound bad, if you’re unlucky enough for the attack to land across your head, your fatal effect is instantaneous death.

 Once you’re out of wounds, every time you take damage, you roll for a fatal effect.

#### Regaining Health

Taking a Breather: Hit points don’t take a long time to return to you. We assume that every blow to your stamina is a small scrape, bruise, or just a representation of you becoming more exhausted.

To replenish your hit points after a battle, just take a breather. If you have time to rest for fifteen to thirty minutes after a battle, all of your hit points will be returned.

Naturally Healing Wounds: Your wounds will return to you at the rate of one wound per day. Your pool of wounds is likely to be much smaller than your hit points, and it takes longer to regenerate them. Be careful when you’re in wounds.

### Priority

Priority determines your turn order. Priority represents a character’s awareness of their current environment and their alertness of the situation. To determine priority, each player involved in combat will roll and add any bonuses to priority that they have.

A player may request to be given a circumstantial bonus by consulting the narrator. With proper justification, the narrator has the right to grant the player a bonus to their priority (normally a +6). This is typically done when a character is ready for combat, has their revolver drawn, and is aching to go. This character is likely to go before the guy sitting in the corner, drinking a glass of wine.

Multiple players may request this benefit, but the narrator is never obligated to anybody. If too many people can justify gaining a priority bonus, then everyone’s starting on the same footing and should probably just go without any bonuses.

If the characters are ready for the combat before the combat truly begins, they can be in their stances and have their weapons already drawn.

### Damage Classes

Weapons and most attacks have damage classes. The damage class of a weapon varies based on the size of the weapon. Light melee weapons, for example, have a damage class of 4, while heavy melee weapons have a damage class of 8.

The damage class determines how much damage it will do for any given tier. If you get a tier 1 result, your damage dealt will be equal to the damage class. If you get a tier 2 result, the damage dealt will be twice the damage class. Tier 3 triples the damage class.

Most easily put, the damage you deal for an attack is equal to your damage class times the tier.


Attacking
---------

Attacking is a simple process. Both combatants roll, the attacker to hit and the defender to evade. If the accuracy meets or exceeds the evade, the attack lands. Then the attacker rolls for strike and the defender rolls for defense. The damage dealt is based on the weapon used and is put against a tiered result. The defense works the same way, soaking an amount of damage based on the armor worn.

Accuracy

When delivering an attack, you must first land it. You make an attack against the target’s evade. If the accuracy meets or exceeds the evade, the attack hits.

Vs

Evade

When receiving an attack, you want to evade it. The attacker makes an accuracy roll against the defender’s evade. If the evade exceeds the accuracy, the attack misses.

Strike

Once you are sure that an attack has landed, how hard the strike hit is decided. Use your roll, add your strike, and then tier the results. The tier will then deal the indicated amount of damage to the defender.

Vs

Defense

In preparation for an incoming blow, the target readies his defenses. Roll again, add your defense, and then tier the results. The tier will then soak damage from the opponent’s attack.

#### Specialty Attacks

Normally, melee attacks require 2 action points (unarmed attacks require 1 AP and ranged attacks vary). Characters can make unique attacks using specialties for extra action points. Many specialties will be labeled as “attack + x AP.” Therefore, when you decide to make a specialty attack, you announce the attack and spend the total action point cost in order to attempt it.

You may stack special attacks if you have enough action points to do so. For example, if you have two specialties that both say “melee attack +1 AP,” you may make your melee attack (for 2 action points) and add both specialties onto it (at 1 action point apiece), which would be a single attack that costs 4 AP to make. Be careful, though: if your opponent manages to evade, you’ve just used 4 action points on a single attack! You cannot add a single attack-modifying specialty twice, however.

How We Roll
-----------

Explaining how to roll a single die may seem asinine, but I’m going to do it anyway (but only because we have two special rules about die rolling).

### Rule #1

1 is 1

This rule is simple. A roll of a 1 results in a 1. Whenever you roll a 1 on your twelve-sided die, you are not allowed to add anything to it. We don’t care if you have a +48 in brute; when you roll that 1, you receive a 1.  

The only exception to this rule regards negatives. If you have some penalty on your roll (such as being blind, which incurs a -4 on accuracy rolls), a 1 is not a 1. Negatives still apply. So your blind accuracy roll was not a 1, it was a -3.  

### Rule #2

Pure 12s Roll Again

It’s true. If you roll a pure 12 (as in, a 12 on the twelve-sided die is showing), you roll again. You then add your new roll to 12 in order to determine the result.

 If you roll 12 again, you keep on going.  

 Just so it’s clear, if you roll a 1 after rolling a 12, the result is a 13. Rule #1 no longer applies.

 If you are rolling a 12 randomly to determine what called shot location you hit, this rule need not apply.

Actions
-------

Now let’s see what you can do.

### Attack

Cost: 2 AP for Most Weapons, 1 AP for Unarmed

Attacking somebody can be anything from stabbing with your rapier to swinging your whip to firing your pistol. You choose a target and you attack.

Your choice of weapon determines how many action points it cost, though most will use 2 action points. Your damage is also determined by your choice of weapon.

Non-Lethal Attacks: You hold your blows just a little bit, aiming for areas that won’t kill the person. You can make a non-lethal attack just like a regular attack; however, if the opponent takes any wounds damage, he does not have to roll on the wounds effect chart. Instead of dying, the opponent falls unconscious upon reaching 0 wounds.

### Called Shot

Cost: Attack +1 AP

Sometimes you don’t just want to hurt them. Sometimes you want to stab them in the eyes, smash their fingers, or kick them in the balls. These options are Called Shots.

 To make a called shot, choose one of the 12 key parts of the body (found on the Called Shot pages, a couple pages over) and make an attack. The attack is just like any other attack, except that it requires one extra action point to do. Thus, a normal melee attack made as a called shot would be three action points, whereas a called shot that was unarmed would be two action points. Called shots can also be made with ranged weapons.

 Every called shot is resistable. The resist is based on your strike (or accuracy with firearms and crossbows), for which the target must roll the appropriate attribute and exceed your strike in order to resist.

 Stacking Called Shot Effects: The effects from called shots do not stack. If somebody is disoriented from a called shot to the brain, another called shot to the brain will only renew the effect, not double it. A called shot to the torso (to knock them back) or stunning them via a called shot to the neck are the only exceptions.

 Called Shot in Wounds & Fatals: A called shot while the target is in wounds or fatal effects will automatically hit that location. There’s no need to roll randomly. (For this reason, many people will do a called shot to a lethal location once a person has reached wounds and fatals, like to the head or chest.)

### Deflect

Cost: 1 AP interruption

Shields are used for deflecting incoming attacks. To make a shield deflection, you must be wielding a shield (or some other item you can make a deflection with) and be in the process of being attacked. Then, you can make a deflect attempt. When you deflect with a shield, it costs you 1 action point and you gain a +4 on your evade roll.

### Draw or Swap an Item

Cost: 1 AP

You may swap weapons or pull out an item (such as a vial or explosive) for one action point.

### Enter a Stance

Cost: 1 AP

You may enter a stance for 1 action point. You can only be in one stance at a time. Oftentimes when combat begins you can simultaneously enter a stance, costing no action points. Any time you take on a specialized stance, the effects of the stance remain active until you are knocked back, knocked prone, or are otherwise knocked out of your stance. You must then re-enter the stance to regain the benefits. At first the only stance your character knows how to enter is the “Footing” stance used to wield Super-Heavy items. You can learn more stances through certain specialties.

### Grab

Cost: as an Unarmed Attack (1 AP) or Whip Attack (2 AP)

Grabs are similar to called shots, but rather than hitting them there, you grab them. Your goal is not to deal damage but to take hold of your opponent and limit their movement.

First choose a called shot location, then roll your accuracy versus their evade just as though you were doing a normal attack. However, you do not deal damage. If you land the attack, the target is grabbed. Once you grab the opponent, a few rules apply:

Neither you nor the target can move without breaking the grab.

The target can attempt to break free for 1 AP by making either a Brute or Dexterity resist against your Brute or Dexterity. You can let go for 0 action points.

If you grab the target’s hand, the target cannot use the weapon, item, or shield in that hand (beyond dropping it).

### Throw

Resist: Brute (tier down)

Cost: 2 AP

After successfully grabbing a person, you may then throw them. Roll your Brute to determine how well you throw them. For every tier over tier 1 that the opponent rolls their Brute, it lowers the effect by 1 tier. Note: small creatures, such as gnomes and most animals, are thrown an additional 5 feet.
| Tier | Effect |
| --- | --- |
|1|Opponent is thrown 5 feet.|
|2|Opponent is thrown 10 feet.|
|3|Opponent is thrown 10 feet and is now prone.|
|4|Opponent is thrown 15 feet and is now prone.|

### Move

Cost: 1 AP

You are able to move 25 feet for a single action point. Certain races (elves, gnomes, and satyrs) have different movement speeds.

### Ready a Firearm

Cost: Varies

Many firearms require you to ready them between each firing. This can be as simple as cocking back the hammer or as complex as muzzle-loading the musket. It all depends on the firearm. You can find out more about readying a firearm in the Gear chapter.

### Stand Up

Cost: 1 AP

Sometimes, after you’ve been knocked prone, you just need to stand back up. Do so for 1 action point.

>**Note** : kneeling does not cost any action points as you are doing that as a combat action. Thus, you could kneel behind a low wall for no action points. If the wall’s only 1 foot tall, though, your narrator might rule that you need that 1 action point to get down into a flat position and stand up from it.

### Sunder

Cost: as an Attack

Resist: Dexterity (tiers down)

Rather than attacking the person, the attack is against the wielded item, be it a weapon, shield, or vial of poison. Most items are instantly destroyed when attacked (such as poisons, complex machines, et cetera). Weapons last a little bit longer.

A melee weapon’s damage class decreases every time it is struck by a sunder. The tier of the attack will indicate how much the weapon’s damage class decreases by. For example, a tier 2 attack would decrease the target’s weapon by 2 damage class. A sunder made against a firearm, crossbow, or bow is twice as effective, lowering the damage class by 2 for every tier of the attack.

The Dexterity resist will tier the effect down against a weapon (so if you get a tier 2 Dexterity resist, it lowers the effect by 1). If used against another item that is instantly destroyed by a sunder, the Dexterity resist can only negate by being a tier over the attack.

A damaged weapon can be repaired during a breather.

### Miscellaneous Actions

Cost: Varies

There are a wide variety of other actions you may want to make while in combat. Here is a list of just some of those actions:

|Actions|AP Cost|
|-|-|
|Breaking a Window | 1 AP|
|Opening a Door | 1 AP|
|Pulling a Lever | 1 AP|

Reflexive Attacks
-----------------

Actions you can do in an instant.

Reflexive attacks are attacks that you can make when an opponent leaves themselves open. They are just like normal attacks - that is, they cost the same amount of action points and you can upgrade them with your specialties - but they can happen outside of your turn. And remember, your action points reset at the end of your turn, so after your turn, you’re back up to full.

For most people, there are two actions that leave a person open to reflexive attacks: leaving cover and using, consuming, or drawing an item.

### Leaving Cover

Can only be made with ranged attacks

If a gunman is hiding behind a crate and leaves cover to take a shot, everyone can take a shot at him, because he is now open.

If somebody is standing behind a tree and walks out from behind the tree, anybody with a gun ready can take a shot.

### Using an Item

Can only be made with melee attacks

If somebody pulls out an accelerator rifle, tries to drink a push potion, or flips on their unusual gadget within melee distance, anybody within melee range can make a reflexive attack.

Fighting out of Combat
----------------------

Fighting out of combat may sound like an oxymoron, but it occurs any time where the defender is not fighting back. If you are stabbing somebody in their sleep, knocking somebody upside the head while they have their back turned, or otherwise dealing damage to a person who doesn’t think they’re in combat, you are fighting out of combat.

No Hit Points

Whenever a person is not at least partially ready for a fight themselves, they take direct wounds damage. Hit points are a reflection of combat stamina, and a person who is not in combat has no hit points. Therefore, if you are stabbed in the back, you take wounds damage.

Coup de Grace

Cost: 3 AP

A coup de grace is a melee attack specifically meant to kill somebody. They can’t be fighting back, moving, or otherwise defending themselves. The attack deals direct damage to their wounds and they must roll on the wounds random effects chart. In a coup de grace, the attacker takes a pure 12 on their strike roll with the defender takes a natural 1 on their defense.

✨ **Knock Out :** A character can make a non-lethal coup de grace that works exactly like a coup de grace, except that the target falls unconscious upon reaching 0 wounds and does receive wounds random effects.

Called Shots
------------

Called shots are all resistable. The defender must roll their resist attribute. If it exceeds the attacker’s strike roll (or accuracy roll for firearms and crossbows), the called shot is resisted.

Wounded and Fatal effects can not be resisted.

1

Head

Resist: Brute

A shot to the head will disorient you (causing you to lose 1 action point per turn) until the end of your next turn.

🩹Wound: Disoriented

A wound to the head causes long-term disorientation. While disoriented, you lose 1 action point per turn, and will be disoriented for 1 turn per 3 points of damage the attack dealt to you. You cannot re-orient yourself from this wound.

🩸Fatal: Beheaded

If your head is dealt a fatal blow, you instantly die.

2

Eyes

Resist: Dexterity

When a successful attack is made against your ocular region, your sight becomes blurry (giving you a -2 on accuracy and evade) until the end of your next turn.

🩹Wound: Blinded

A wound to the eyes leaves you blinded until you can take a breather. You suffer a -4 on accuracy and evade rolls, and you cannot target opponents you can’t locate.  

🩸Fatal: Blind

A fatal to the eyes leaves you permanently blinded. You suffer a -4 on accuracy and evade rolls, and you cannot target opponents you can’t locate. Losing your eyes permanently drops your wounds by 1 point.

3

Ears

Resist: Cunning

When a successful attack is made against your aural region, your hearing becomes fuzzy (giving you a -2 on evade) until the end of your next turn.  

🩹Wound: Deafened

A wound to the ears leaves you deafened until you can take a breather. You suffer a -2 on evade rolls and any roll that requires listening, speaking, or performing a sound-based action.  

🩸Fatal: Deaf

A fatal to the ears leaves you permanently deafened. You suffer a -2 on evade rolls and any roll that requires listening, speaking, or performing a sound-based action. Losing your ears permanently drops your wounds by 1 point.

4

Neck

Resist: Brute

An attack against your neck can leave you gasping for air, unable to take actions. A successful called shot against your neck stuns you for 1 action point.  

🩹Wound: Bleeding

A wound to the neck causes bleeding. Bleeding will cause you 1 point of wounds damage every turn after the one where you take the wound, and will last for 1 turn per 3 points of damage the attack dealt to you.

Any sort of bloodclotting, medical healing, or artificial flesh imbuing will stop the bleeding.  

🩸Fatal: Slit Throat

If your neck is dealt a fatal blow, you will die at the end of your next turn.

5

Torso

Resist: Brute

A good blow to the torso can knock a person backwards. A successful torso called shot sends you backwards 5 feet.

If the person dealing the attack is in melee with you, they may choose to follow you back the 5 feet for 0 action points.

🩹Wound: Broken Ribs

Suffering broken ribs can make it difficult to breath. Any time you want to make any action, you must roll your Brute attribute. If you do not receive a tier 2 result, you fail and lose 1 action point. This wound will stay with you until your next breather.  

🩸Fatal: Slain

You are instantly killed.

6

Groin

Resist: Spirit

A blow to the groin makes you nauseated. You receive a -2 to all rolls until 3 action points have been spent emptying your stomach.

🩹Wound: Purge

Your body begins to purge itself. You can take no actions beyond moving, and you only can go at half your normal speed (rounded down). While purging, if anybody attacks you, you suffer a -4 on your evade roll. The purging lasts for 3 turns.

🩸Fatal: Gutted

Your vital organs are exposed to the world, and you are bleeding out. Unless you can recover 10 points of damage (wounds or hit points) before the end of your next turn, you will die.  

7-8

Arm

Resist: Brute

An attack against your arm makes it difficult for you to use that hand. You take a -2 on any roll that would use that arm and hand, such as accuracy and strike, or evade if you are trying to use a shield with that hand, until the end of your next turn.

🩹Wound: Sprained Arm

Your arm is sprained, making it near impossible for you to wield things with that arm. Anything that you do with the arm suffers a -6 on the roll. The wound will recover during your next breather.  

🩸Fatal: Severed Arm

Your arm is severed. You now have one less arm, and anything that requires you to use two arms cannot be accomplished. Ambiguous actions that would normally use two arms (such as lifting something over head or climbing) take a -6 on the roll. Having a severed arm permanently drops your wounds by 2 points (1 point for the arm and 1 point for the hand).

The bleeding from a severed arm will cause you to die in 3 turns if you do not spend at least 3 AP bandaging it up.

9-10

Hand

Resist: Dexterity

An attack to the hand causes you to drop whatever you’re holding. Picking up a dropped item costs 1 action point.

🩹Wound: Bruised Hand

Your hand is smashed, making it impossible for you to wield anything with that hand. The wound will recover during your next breather.

🩸Fatal: Severed Hand

Your hand is severed. You now have one less hand, and anything that requires you to use two hands cannot be accomplished. Ambiguous actions that would normally use two hands (such as lifting something over head or climbing) take a -6 on the roll. Having a severed hand permanently drops your wounds by 1 point.

The bleeding from a severed hand will cause you to die in 6 turns if you do not spend at least 3 AP bandaging it up.

11-12

Leg

Resist: Dexterity

A called shot to the leg can either trip you or slow you down (attacker’s choice). If you are slowed, it will cost you 1 extra AP to move until the end of your next turn. If you’re tripped, you’re brought prone and lose your stance. While prone, you suffer a -1 on all combat rolls (accuracy, evade, strike, and defense), but can stand up for 1 action point.  

🩹Wound: Sprained Leg

Your leg is sprained. You suffer a -10 on your movement speed (though you cannot be dropped below a minimum of 5 feet) and you’re tripped (bringing you prone). The wound will recover during your next breather.

🩸Fatal: Severed Leg

Part of your leg is severed. You suffer a -20 on your movement speed (though you cannot be dropped below a minimum of 5 feet). Anything that requires two legs, such as swimming, climbing, or kicking, suffers a -6 on the roll. Having a severed leg permanently drops your wounds by 1 point.

The bleeding from a severed leg will cause you to die in 3 turns if you do not spend at least 3 AP bandaging it up.

Attributes
----------

Attributes are the baseline pieces of your character. Your attributes determine how fast you react, how well you push back, how long you survive poisons, how quickly you process information, and a whole plethora of actions that anybody can do.

There are five attributes: Brute, Cunning, Dexterity, Spirit, and Sciences. You will start with a zero in each of these attributes, but as you gain experience in your skills, your attributes will increase as well.

Each attribute has a set of skills that fall underneath it. As you gain points in your skills, add up all of the skills under the attribute to figure out what your attribute score is.

### Using Attributes

Each attribute will include a variety of different options you can use that attribute for.  Dexterity, for example, is used when tripping people, disarming them, trying to be stealthy, attempting to squeeze into a tight spot, or anytime that your hand-eye coordination is going to prove valuable.  

Anybody can do anything listed under attributes. Using attributes requires no training or skill. Most people will simply be average in the attributes (which means they have a zero). Others will excel in them.

### Note to the Narrator: When to Ask for Attribute Rolls

Be sparing when asking for attribute rolls. On one hand, it is completely within your power to ask the players to roll their Dexterity to see if they fall down the stairs, but is it really necessary? Every attribute roll should have a deep impact on the story, otherwise it begins to lose its excitement and meaning.

In Tephra, we utilize a system built on “Four Tiers of Success.” Whenever you ask for an attribute roll, don’t think of it as win or lose. Think, instead, that your player is going to succeed, but the roll will tell you how well. If a failure is going to cause the story to stall, make tier 1 a poor success. It’ll keep the story moving while giving the players some fun consequences.

Resists
-------

Almost all abilities that affect somebody can also be resisted. The most common resist is, of course, your evade and defense - when attacked, your goal is to jump out of the way and take less damage. But other special attacks will often call for attribute-based resists. When you need to resist a tiered effect with an attribute resist, you simply lower the amount that the ability affects you by 1 tier for every tier you receive over tier 1.

So, if you were using your Brute to resist against a marque 3 poison, and you made a tier 2 Brute roll, you would lower the poison’s marque by 1, being affected as though it were a marque 2 poison.
|Tier| Effect|
|--|--|
|Tier 1|         Unaffected|
|Tier 2|        -1 Tier|
|Tier 3|        -2 Tiers|
|Tier 4|        -3 Tiers|

#### Dodging Blasts      (1 AP Dexterity resist)

Any time you are affected by a blast (something that affects a large area), you may attempt to jump out of the blast area. It costs 1 action point reflexively, and you may only move up to your total speed in order to get out of the blast range. Thus, if the edge of the blast area is 40 feet away, and you only have a 30 foot movement speed, there is no way you can jump out of the blast area.

In order to successfully jump out of the blast area, you must roll your Dexterity and receive a tier result equal to the tier or marque of the blast. Thus, a tier 3 blast would require a tier 3 Dexterity result in order to jump out of it. (This is different from normal resists, which require you to receive a tier result one higher than the tier or mark of the ability in order to fully resist it.)

If you fail to get out of the blast area, you move to the edge of the blast area (while still remaining within it) and take the normal damage, which you can still soak, if possible.

Social Tells
------------

When interacting with other characters, it’s nearly impossible to quantify how likely they are to believe your lie or how stubborn they are toward your wild claims. Instead, you can search for their social tells. These are their non-verbal cues, often made unconsciously, that betray a character’s inner thoughts. You might be trying to decide if a man is lying. Successfully reading his social tells might let you notice his sweaty palms, his shifting eyes, and his constant checking of his watch. By this point, you’ll have a pretty good idea that he’s lying.

Social tells are anything but absolute. He may look like he’s lying, but in reality he might just be late for a very important business meeting. The goal of social tells is to guide you toward the answer without outright saying, “The fellow is clearly lying.” The decision on how to act once you’ve observed somebody’s social tells is still up to you.

 A character will use social tells by rolling their Cunning and tiering the results.

Tells

When a character rolls to determine somebody’s tells, the narrator will describe key aspects of the target depending on the success of the roll. For example, you might be attempting to decide if he’s lying. Depending on how well you roll, you might get the following results:

1.  He seems rather sweaty, despite the chill in the room. Pretty much anybody would notice this.
2.  He is very stiff when he talks. He’s not making many gestures, he’s not looking you in the eyes, and his posture seems guarded.
3.  His humor seems forced. He’s scratching behind the ears and looking toward the door, as though he’s about to be rescued or make a quick getaway. He keeps looking at your pistol.
4.  His breathing has changed, and you detect a change in his heart beat due to the cadence in his voice. He has shifted just slightly, so that the table is positioned more between you.

As the result for social tells gets higher, the narrator might give you either more information or more useful information.

Resisting Social Tells

When a person begins looking for social tells, they are testing the air for information. Other characters might be aware of their social tells and attempt to hide them. As such, they can roll a Cunning resist, which will tier down the player’s result.

Alternatively, some characters just won’t have social tells. A man who is known for his stoic demeanor may have few noticeable social tells, and he might need a tier 3 or tier 4 to figure out his motives.

Control Remains with the Character

The social tell system is designed to help a character convince others or manipulate a social situation. However, the ultimate choice of how to act remains with the adventurers and NPCs. The players are not told directly, “This man is lying,” just like the NPC who is being intimidated is not forced to back down because of a good roll. Their decisions should be made in line with their character and how they would react to the information received.

Using Social Tells
------------------

Lie Detecting

Resist: Cunning (tiers down)

When somebody has made a statement that you think isn’t true, you can attempt to detect the lie. You can look for their level of confidence and conviction, how strongly they make the statement, whether they seem nervous, are looking away, or are excessively sweating. When you roll to detect a lie, the target can resist with their own Cunning.

Pacifying & Intimidating

Resist: Spirit (tiers down)

Attempting to pacify or intimidate a target can cause them to back down, calm down, or suddenly fear you. It’s a great way to get information or to get past a potential obstacle. When you are attempting to pacify a target, you’re trying to find just the right things to say in order to make the target less aggressive. Intimidation is similar, but it typically revolves around making the target afraid of you.

 Unlike most social tells, the target will typically be resisting with their Spirit attribute, as they are trying to bolster their own resolve to keep on fighting.

Provoking

Resist: Cunning (tiers down)

When you go to provoke somebody, you are trying to elicit a certain type of response. Perhaps you’re attempting to make them angry, to say something they don’t want to say, or even just to laugh. Sometimes you will provoke somebody so that, in the midst of combat, the target will only attack you.

When you roll for social tells to provoke a person, you’re looking for a way to get your response. If your target is a proper gentleman, you might look for a wedding ring to determine that he’s in a committed marriage and some snide comments about his wife could make him angry. If a guard seems to take great pride in the emblem on his armor, you might make a joke about his organization.

Cover
-----

When an opponent is behind a wall, a thick sheet of rain, or another person, they have cover.

Cover gives you a bonus on your evade rolls, but can also cover called shot locations and provide hiding places. There are four degrees of cover - poor, light, medium, and heavy. Each degree of cover grants greater protection.

Called Shot Locations: When you are behind cover, you can choose certain called shot locations to be covered. For example, if you’re hiding behind a table on its side, the table might count as medium cover and cover all of your called shot locations except your head (which you’re peeking over the table with in order to see your targets), your gun hand, and your arm.

If you need a called shot location to perform an action, then you can’t hide it behind cover (like if you are using a heavy rifle that requires both hands and arms and your head to see, then you’d have to reveal all 5 of those requisite called shot locations). When you take cover, describe what part of you is hiding and allow you and your narrator to make judgment calls as to if the location is hidden or not (but the ultimate decision is left to the narrator). For example, you might have thought only your head and eyes were exposed, but the enemy saw your foot sticking out and took a shot at it. That’s the narrator’s call, which should be making the game that much more fun.

(tier 1)

Poor        A post, side-table, or person

Covers up to 3 called shot locations

Evade: +2 on evade rolls

(tier 2)

Light        A bush, couch, or tree

Covers up to 6 called shot locations

Evade: +4 on evade rolls

(tier 3)

Medium        A large tree, barrel, trench, or around the corner

Covers up to 9 called shot locations

Evade: +6 on evade rolls

(tier 4)

Heavy        Around a full wall, ducked behind furniture

Covers up to 11 called shot locations

Evade: +8 on evade rolls

Total        No portion of the person visible

Covers all called shot locations

Evade: Cannot be targeted

### Soft Cover

Soft cover is anything that obscures an opponent’s vision but can still be attacked through. For instance, if you’re hiding behind dense shrubbery, a canvas screen, or behind a sheet, the opponent can still attack you through that soft cover.

Soft cover causes the opponent to attack you blindly (taking a -4 on the accuracy roll). If the opponent can barely see you (like a shadow of you through the screen), they only take a -2 on the accuracy roll.

This is a double-edged sword: if you can’t accurately tell how they’re attacking you, you suffer the same penalty on your evade roll.

### Taking Total Cover

Cost: 1 AP (or at the end of a movement)

For 1 action point or at the end of a move, you can take full cover behind an object that would normally only grant medium or heavy cover. This is typically the act of ducking down behind the cover or pressing yourself against it.

Once you’ve taken total cover, you can peek out from behind that cover and return for 0 action points (such as poking your head out from around a corner). Doing so, however, does count as “leaving cover” and allows enemies with ranged weapons to take reflexive attacks against you.

### Melee Attacks versus Cover

Though cover is a constant concern for ranged attackers, it can be less so for people carrying melee weapons. While a wall can be great for peeking one’s head around, once that person closes in with a saber, that wall isn’t going to be doing much.

Small objects (such as furniture and vegetation) can provide cover as per normal, but characters will often just move around larger objects (such as larger trees, walls, et cetera).

### Firing Blindly

While taking total cover, you can always choose to fire blindly at your opponents. When you fire blindly, you are only revealing your hand (the one firing the gun, crossbow, or throwing the weapon) or hands (if you’re using two-handed ranged weapons or a bow). This prevents enemies from targeting your vital locations, as they can now only target your hand(s).

 However, you have difficulty aiming at the target while firing blindly. You take a -4 on your accuracy roll against anybody that you shoot (exactly as though you were blind). If you do not know where the enemy is but shoot in the correct direction (at your narrator’s discretion), you’ll suffer a -8 on the accuracy roll.

### Hiding Spots

Hiding spots are locations where you can keep yourself hidden from on-lookers. You can use medium, heavy, or total cover as hiding spots (the action of hiding in a hiding spot counts as taking total cover). Once you’ve taken your hiding spot, people can roll to notice you or they must search for you.

:eye: **Noticing:** Attempting to “notice” somebody is a passive action. When a character notices something, they did so without any real effort, without actively searching for what they noticed. When a person passes near another person’s hiding spot, they both roll Cunning (with the hiding person gaining a bonus based on their degree of cover). If the person walking by meets or exceeds the person hiding, they “notice” that there is somebody hiding there.

Medium Cover        -4 on the Cunning roll to hide, as this degree of cover is difficult to hide behind

Heavy Cover        No bonus on the Cunning roll to hide, as this degree of cover is a typical hiding spot

Total Cover        +4 on the Cunning roll to hide, as this degree of cover is ideal for hiding

Searching: Searching is a much more involved process. If someone says “I search behind the rock” and there somebody is hiding behind the rock, they will automatically find that person. The area must be specific (saying that one “searches the room” is not specific enough: they must say that they search behind the curtains and inside the closet).

If somebody is found hiding, both parties may roll priority. The winner will get to act first, often times taking the chance to immediately run or get off a surprise attack.

Precipitation & Wind

Precipitation and wind can provide cover, as both make it more difficult to aim at your target. Your narrator will use his discretion when determining a cover bonus provided from precipitation and wind. They can also increase cover; for example, if you’re taking cover behind a barrel (say, medium cover), but it’s also hailing outside, that medium cover can suddenly become heavy cover.

Status Effects
--------------

Bleeding

Any time you’re bleeding, you’re taking hit point damage until your hit points are exhausted, at which point you start taking wounds damage. Normally, bleeding will occur when the victim’s action points refresh and last until stopped. This damage cannot be soaked through normal defenses. Bleeding effects stack with other bleeding effects.

For every 1 action point spent to stop bleeding, 5 bleed damage is negated. These action points must be spent by either the victim or a character adjacent to the victim.

Blinded & Poor Vision

When you’re blinded, in total darkness, or just plain can’t see your opponent, you take a -4 on all accuracy and evade rolls against him. If you’re in low levels of light, fog, or just have something in your eye, you take a -2 on accuracy and evade rolls.

Burning

When you are on fire, you will suffer unsoakable damage every turn, and the fire will damage or destroy your equipment. The fire can be put out for a couple action points, depending on the size of the fire.


?tierTable
Tier 1        This is a light flame such as as stepping in a campfire or a explosion’s aftermath. You take 2 unsoakable damage per turn and items in your possession are singed but still functional. 2 action points will put the fire out.

Tier 2        This is a large fire, like from standing in a campfire or being lit by an outside source such as a torch. You take 4 unsoakable damage per turn and all wooden, organic, and cloth items in your possession are destroyed or unuseable. 4 action points will put the fire out.

Tier 3        This is a fire fueled by outside means, such as oil or an alchemical source. You take 8 unsoakable damage per turn and all leather, cloth, and wooden items in your possession are destroyed or unusable. 8 action points will put out the fire.

Tier 4        This a highly intensive fire caused by lava or a full inferno. You take 16 unsoakable damage per turn. All items in your possession, including metal ones, are damaged to a point of being unusable until repaired. 16 action points will put out the fire.

Burnt

Burns can be painful, debilitating wounds that heal very slowly. General burns make armor uncomfortable to wear and hits harder to soak. For specific call shot burns, consider giving equal penalties to abilities when using the burned body located.

As an example, burned hands might make climbing or crafting intricate trinkets very difficult, whereas burned feet could cause a player trouble when attempting fancy footwork or long treks across the country.

?tierTable
Tier 1        You have minor burns on his body that make all incoming attack sting just a little bit more. You suffer a -1 to all defense rolls.

Tier 2        You have several 1st degree burns that make defending difficult. You suffer a -3 to all defense rolls.

Tier 3        You have major 2nd degree burns that hurt when you even move. You suffer a -5 to all defense rolls.

Tier 4        You’ve been burned incredibly badly. Several are 3rd degree burns, making it almost impossible to take a hit. You suffer a -7 to all defense rolls.

Deafened

While deafened, you suffer a -2 on evade rolls. You also take that -2 on any roll that requires listening, speaking, or performing a sound-based action.  

Disoriented

When you’re disoriented, you have a hard time getting about. You lose one action point per turn that you are disoriented. You can re-orientate yourself by spending 3 action points.

Drowning

When you run out of air, make a Brute roll for each turn until you once again have air. The target tier for this roll begins at tier 2 and increases each turn until you fail, at which case you’re knocked out. You die within three turns of falling unconscious if you’re not rescued.  

Enraged

Rage drives a person to act with one goal in mind: to destroy whatever is making them angry. As such, when you are enraged you suffer a -2 to all rolls when doing anything other than attacking whatever enraged you. You gain a +2 accuracy and strike when attacking the object of your rage. You may spend 2 action points at any time to clear your head and calm down, removing your rage.

Fatigued

When you’re fatigued, your maximum hit points are reduced by one half, rounded down. Thus, if you normally have 17 hit points but start a battle fatigued, you’ll start the fight with 8 hit points.

Fear

Fear comes in many forms and is caused by many things. It is an emotion of power that causes nations and heroes to both rise and fall. Fear is one of the most basic and powerful emotions in the universe, as there are few who truly ever overcome it and even fewer who are without it. At any point you may spend 1 reflexive action point to attempt to overcome fear. A strong willpower always overcomes fear and as such you may always use a spirit to resist or overcome fear. Lastly, fear only lasts until your next downtime.  

?tierTable
Tier 1        Being scared by someone or of something is the most basic form of fear. This is the form of fear that comes from being intimidated by the object of fear. When you are scared you receive a -2 to all resist rolls. This penalty increases to a -4 when rolling against the source of your fear.

Tier 2        Fright has the ability to cause doubt in even the strongest and most skilled people. It consumes them and makes them act irrationally which opens them for mistakes. When Frightened you suffer a -2 to all rolls. This penalty increases to a -4 when rolling against the source of your fear.

Tier 3        When you are terrified you body has only one response: put as much distance between you and the object of your fear as possible. When you are terrified, you suffer a -2 to all rolls (-4 against your fear’s source) and must spend at least 1 action point per turn moving away from the object of your fear. In addition, you may not move toward the object of your fear until you are no longer terrified.

Tier 4        True dread is sometimes referred to as crippling fear. It gets this reputation because those unfortunate enough to have experienced true dread had their basic rights as a sentient creature stripped from them: the ability to act. When you are experiencing true dread, you suffer a -4 to all rolls (-6 against your fear’s source) and may not do anything except when attempting to overcome fear (and you can spend one action point at a time to re-roll your resist against this effect).  

Nausea

You’ve become sick to your stomach and cannot focus until it’s been dealt with. You receive a -2 to all rolls until 3 action points have been spent emptying your stomach.

Paralyzed

A paralyzed person is helpless for several turns, minutes, hours, or days. If paralyzed, a person cannot fight back. Any damage dealt to them goes straight into wounds. They cannot move, talk, or take any other actions (unless specified otherwise).

Prone

If you’re prone, you’re low to the ground. It requires 1 action point to stand from prone, which causes you to flinch (and thus draw reflexes). While prone, your move speed is cut down to 5 feet. You take a -1 on all combat rolls (accuracy, evade, strike, and defense) while prone. If you’re grabbed while prone, you cannot stand up until you break the grab.

Stunned

When somebody is stunned, they lose an indicated amount of action points from their immediate pool. If somebody is stunned for 1 action point, they lose the first available action point they have. If somebody is stunned for more action points than they have per turn, they cannot act until they have action points again.

 A stunned character is still aware of their environment and can evade and resist attacks, and is therefore not helpless.

Battlefield Modifier
--------------------

Falling

You take 1 wounds damage per 20 feet that you fall. For every tier result over tier 1 you receive on a Dexterity roll, you may ignore 2 wounds damage. (Thus, a tier 2 Dexterity roll would result in ignoring the first 40 feet of falling damage, and a tier 3 result would ignore the first 80 feet.) For every 2 wounds damage that you take from falling, you roll once on the wounds random effects chart.

Rough Terrain

Rough terrain can be found anywhere, Sometimes it is a mere inconvenience, slowing you down a notch or giving you uneven footing. Other times it can be virtually impassable, making you nearly crawl to get anywhere.

ADD TABLE MANUAL

Minor        You take a -5 to your speed.

Examples: A rocking boat, a light forest

Unsteady        You take a -10 to your speed.

Examples: A forest, rocky terrain, snow

Difficult        You take a -15 to your speed.

Examples: A swamp, a snowy mountain

Impossible        You take a -20 to your speed.

Examples: A dense jungle, ancient rubble

Note: No matter how dense or how many penalties you have to speed, you can always crawl at a 5 feet movement.

* * *
