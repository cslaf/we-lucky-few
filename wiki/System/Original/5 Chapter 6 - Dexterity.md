Chapter 6 - Dexterity
=====================

Dexterity is a representation of your speed, coordination, and reflexes. With dexterity, you can wiggle into tight areas and turn just the right screws without being entirely sure where your hands are. A person high in Dexterity is always going to catch the ball thrown at them, will have no problem walking across a tightrope, and might even known how to pick a lock.

What can you do with your Dexterity attribute?

Balance c

Attempt Cost: Movement +1 AP

Want to walk that tightrope or simply just cross that rickety catwalk over the lake of acid? You need to balance. Balancing comes in four flavors, and your tier roll depends on what obstacle you are able to cross. Your narrator will identify for you the difficulty of the balancing check before you make your attempt.        

?tierTable
        You can barely keep your balance. Taking an action other than moving or getting hit by something will cause you to fall.

        You may wobble from time to time, but you hold your balance. However, if the battlefield conditions change or you perform any action other than moving, roll again.

        You can confidently balance yourself. You won’t fall unless the condition of what you are balancing on changes, in which case roll again.

        You move as if you were on solid ground. Nothing can shake you from your position, save someone using an attack on you that moves you any distance.

Jumping c

Attempt Cost: as a move (normally 1 AP)

Long Jump: Jumping is tiered, based on the difficulty of the jump. You must get a moving start to jump your optimum distance. If you do not move at least 20 feet before the jump (as a separate action), you will jump half the distance. Any movement penalties apply to the jump distance.

?tierTable
        10 feet forward

        20 feet forward

        30 feet forward

        40 feet forward

Vertical Jump: You may attempt to jump vertically for 1 action point as well. When you jump vertically, you can jump 1 foot up for every 5 feet you’d be able to move forward.

Catching a Ledge: If you just barely fail, your narrator might allow you to roll your Dexterity to catch a ledge and pull yourself up. Normally, catching a ledge while jumping will require a tier 2 or higher Dexterity result.

Sneaking c

Attempt Cost: Move +1 AP

Moving silently will almost always be little more than an opposed roll against a listener’s Cunning. If you succeed in beating their Cunning, you may determine how well you move silently.

?tierTable
        The listener is still paranoid and thinks somebody is in your general vicinity.

        The listener believes that something rustled where you are, but it was probably a mouse or some other innocent thing..

        The listener failed to hear anything at all.

        The listener has no inkling that anybody is there, and will probably not bother listening for anybody anytime soon.

Pickpocket c

Resist: Cunning (tiers down)

Attempt Cost: 2 AP

Someone in your party won’t share the map of the dungeon, or you just saw a really flashy purse on someone’s belt. Roll your Dexterity to determine how stealthily you pickpocket the target. The target resists with their Cunning, tiering down your result. If your result is less than tier 1, you fail to pickpocket the item and they are aware of your failure. You don’t have to know what items they have concealed in order to attempt to steal something, but in such a case the item you would receive is the narrator’s choice. You can only pickpocket items not currently equipped to your target. In other words, you can’t steal the sword they are wielding or the jetpack they’re wearing, but you can steal the sword out of their sheath and the keys to their clanker from their pocket. You can only steal one item at a time.

?tierTable
        Your victim feels paranoid and may begin to check their belongings for anything missing.

        Your victim thinks someone must have bumped into them.

        Your victim failed to realize anything had happened.

        Your victim has no idea anything of theirs is missing and won’t bother to check anytime soon.

* * *

Ace Skill
---------

You are the ace. Whether you’re seated in a world-class flying machine or mounted on an overgrown weasel, you know how to guide your ride and use it to its very best. You can do crazy flyer tricks, a barrel roll with a tank, and you’ll never be kicked off your horse prematurely.

Backseat Driver

Tactical Specialty

Stance (costs 1 AP to enter)

While in the Backseat Driver stance, you utilize your ability as a pilot or rider to provide the person who is driving with useful information, allowing them to react more quickly to situations. While you are in this stance, the pilot of a vehicle that you are riding (or the rider of the animal you are on) gets a bonus action point per turn that can be used for maneuvering the vehicle or mount or using Ace specialties.

Co-Pilot

Ace Specialty

You make a remarkably handy co-pilot when the need arises. If adjacent to a ship’s pilot, you may give them any number of your action points, reflexively, for the pilot to use while maneuvering, accelerating, or slowing the vehicle.

Crash Maneuver

Ace Specialty

Cost: 3 AP reflexively

When you are driving an airborne vehicle that is crashing to the ground, you can maintain your cool and attempt to prevent yourself from getting hurt in the crash. When the vehicle hits the ground, you can wrest enough control of the vehicle to keep it on a steady path. If you do so, everyone in the vehicle takes less falling damage. The amount depends on the tier of your roll.

?tierTable
        -3 wounds damage

        -6 wounds damage

        -9 wounds damage

        -12 wounds damage

Denial Maneuver

Ace Specialty

Cost: 2 AP reflexively

You twist the vehicle’s controls at the last minute, making it difficult to target specific sections of your vehicle. When someone makes your vehicle the target of their attack, you can spend 2 action points in order to add your Ace skill to the ship’s evade roll.

Driving with Knees

Ace Specialty

Stance (costs 1 AP to enter)

You are able to control your vehicle or animal mount using your knees. If you are on an animal mount or driving a clanker, you get one free movement per turn. It also requires no hands to pilot your vehicle or animal mount.

Flying Fortress

Ace Specialty

Requires: 2 skill points in Ace

Cost: 2 AP to begin, 1 AP to continue during subsequent turns

When piloting a vehicle, you are able to maneuver it so that incoming attacks won’t hit anything vital to its continued functioning, ensuring the ultimate defense. Your vehicle gains +1 defense for every 2 points you have in Ace.

Hold Together

Ace Specialty

Requires: 7 skill points in Ace

During its final moments, even if everything is falling apart around you, you push your vehicle forward one last time. If a vehicle you’re piloting loses all of its wounds, you can still move it normally for one more turn. Autos are forced to remain on their previous speed setting during this final turn.

Horseman’s Cut

Ace Specialty

Cost: Mounted Melee Attack +1 AP

When attacking a non-mounted opponent atop your mount or vehicle, you are able to make a horsemen’s cut that deals extra damage.

?tierTable
        +1 damage class

        +2 damage class

        +3 damage class

        +4 damage class

Hostile Maneuvers

Ace Specialty

Requires: 5 skill points in Ace

Cost: 2 AP to begin, 1 AP to continue during subsequent turns

You move recklessly close to an opponent’s vehicle in an all out siege. While using Hostile Maneuvers, your vehicle suffers -4 defense; however, all attacks made by allies on your vehicle hit for 1 damage class higher for every 5 points you have in Ace.

Level Flying

Ace Specialty

While you’re at the helm, nobody on board takes accuracy penalties for shooting from a fast moving vehicle.

Quick-Mount

Ace Specialty

You can now mount or dismount any auto, clanker, or animal mount for no action point cost during your turn, whether you intend to pilot the vehicle or not. You can only make one such quick mounting or dismounting per turn.

Vehicular Teamwork

Ace Specialty

Cost: 2 AP reflexively

When anything flies toward your vehicle, your gut instinct is to move out of the way. When anyone on your piloted vehicle is the target of an attack, you can attempt to move your entire vehicle out of the way first. Before they roll evade, you may roll your vehicle’s evade against the attack. The attacker only rolls accuracy once. If either your vehicle or your passenger roll higher than the accuracy of the attack, the attack misses.

p Auto Piloting Specialties M

Strafe

Ace Specialty

You know how to quickly move your auto side-to-side. Once per turn, you can Strafe for no action point cost. This does not change the direction your auto is travelling in.

?tierTable
        5 feet

        10 feet

        15 feet

        25 feet

Evasive Strafe

Ace Specialty

Requires: Strafe specialty

The quick movements of your auto allow you to shake off incoming attacks. When you Strafe, your vehicle gains a bonus to evade until the beginning of your next turn. This bonus is taken away if you stop piloting the vehicle.

?tierTable
        +1 evade

        +2 evade

        +3 evade

        +4 evade

p Clanker Piloting Specialties M

Extension of Self

Ace Specialty

The movements of your clanker are as smooth as the movements of your body. When piloting a clanker you can move it using any specialty you have related to moving yourself. When doing so, use your skill in Ace for the specialty instead of your skill in the skill the specialty comes from. This does not allow you to move your clanker in ways it can’t on its own, such as jumping or flying.

Piston-Spring

Ace Specialty

Cost: Jump

Whenever piloting a clanker, you have the ability to use its momentum to propel it into the air. This allows you to jump your clanker just like a normal person jumps.

p Focused Flying Specialties M

Focused Flying

Ace Specialty

Stance (costs 1 AP to enter)

With both hands on the wheel and every ounce of your being put into your piloting, your control of your vehicle greatly increases. While in this stance, you cannot take any action other than controlling your vehicle. You also must use two of your hands to hold onto the controls of your vehicle. Clankers gain 5 feet of speed for every point you have in Ace. Autos gain 10 feet of maximum speed for every point you have in Ace.

Fine-Tuned Flying

Ace Specialty

Requires: Focused Flying specialty & 2 skill points in Ace

You put your all into ensuring any attacks made on your vehicle will not penetrate its armor. While in your Focused Flying stance, your vehicle gains a +1 to defense for every 2 skill points you have in Ace.

Fully Focused Flying

Ace Specialty

Requires: Focused Flying specialty & 3 skill points in Ace

By making the continued survival of your vehicle and its crew your main priority, you make yourself more aware of incoming attacks. While in your Focused Flying stance, your vehicle gains a +1 to evade for every 3 skill points you have in Ace.

p Mounted Cavalry Specialties M

Fearless Mount

Ace Specialty

Requires: 3 skill points in Ace

You let your mountable animal know you’re the one in charge. When riding an animal mount, the animal is immune to fear effects. In addition, the animal will be willing to do anything you ask of it (as long as it is capable).

One with the Beast

Ace Specialty

Stance (costs 1 AP to enter)

When mounted, you and your animal mount are perfectly in sync. You may add its strike and defense bonuses to yours. In addition, your mount may not be directly attacked; you take all incoming attacks, be they aimed at you or your mount.

p Ramming Specialties M

Ram

Ace Specialty

Cost: Move + 1 AP

You know how to ram your vehicle into things without damaging yourself. For an extra action point during a move, you can slam the vehicle you’re piloting into a target. This has a damage class of 10. For autos, use your accuracy to tier damage. Clankers will use your strike to tier damage. Ramming anything immediately ends your move, even if you miss.

Puncture

Ace Specialty

Cost: Ram +1 AP (typically the same as Move +2 AP)

When you ram a target providing cover, you break it apart. Whether attacking a solid wall or an armored vehicle, you can lower the degree of cover your target provides while still dealing damage. The cover is lowered in degree until someone repairs it during a period of downtime.

?tierTable
        -1 degree of cover

        -2 degrees of cover

        -3 degrees of cover

        -4 degrees of cover

Agility Skill
-------------

Speed, movement, lightning-fast reflexes, and the quick wits to get out of dodge: these are the focuses of a character with agility. Specialties from agility will get you an insane movement speed, free actions during combat, and numerous ways of clearing a battlefield - either to get to your target, or to get far, far away from him.

Battlefield Flow

Agility Specialty

Cost: 0 AP

You’re constantly ready to move to a more strategic (or just plain safer) location. You can move 5 feet for free at any point during your turn, or you can do so at the end of any other person’s turn (when their action points refresh). You can only make one such 5-foot movement per your turn (that is, between the refreshing periods of your action points).

Bounding Lunge

Agility Specialty

Cost: Move + Attack

You move and make a swipe at an opponent. By using your bounding lunge, you can begin a normal move, attack an opponent during the move, and then finish the same move.

Charging Ram

Agility Specialty

Cost: Move + Melee Attack

When you rush toward an opponent, you mean to take them down with one swing. When you move toward an opponent, you gain a bonus on your strike roll equal to +1 for every 5 feet you spend moving toward them. This movement must all be in a straight line, can only come from a single move action, and must be your movement (it cannot be from a vehicle or mount).

Free Movement

Agility Specialty

Movement is becoming a way of life. Once per turn (and only during your turn), you can make a single free movement. You must be in medium or lighter armor to do this.

?tierTable
        10 feet

        20 feet

        30 feet

        40 feet

Groundfighting

Agility Specialty

You’re an old hand at hugging the ground and attacking like an erupting geyser. You may switch from standing to prone and back again for no action point cost. You take no penalties for fighting while prone. You take no penalties for fighting in tight areas.

Instant Draw

Agility Specialty

In your hand or not, it doesn’t matter. You can draw your weapon and other items without using any action points and at any point (even during another person’s turn). You draw items and weapons so fast that you do not leave yourself open to reflexives. (Activating items still leaves you open to reflexives.)

Slow Falling

Agility Specialty

Cost: 2 AP reflexively

When you fall, you are adept at finding ways to slow your descent. You may ignore however much falling distance as your tier result allows. You must be adjacent to something when you’re falling (such as a wall, or through a canopy of trees). You cannot slow your fall in open air. In addition, you can make attacks against enemies along the line of descent or upon landing for no penalty.

?tierTable
        30 feet

        60 feet

        120 feet

        200 feet

Side-Swipe

Agility Specialty

Cost: As a melee attack reflexively

When an opponent over-extends their attack, your dodge allows you to take advantage of their failure. When you successfully evade a melee attack, you can make a side-swipe with any one-handed melee weapon against your attacker. You can optionally move 5 feet around your opponent (staying adjacent to the attacking opponent) at no additional action point cost. You gain a bonus on your accuracy roll for every point that the attacker’s original accuracy roll missed you by.  

Slipstreaming

Agility Specialty

Cost: Move + 1 AP

Requires: 4 skill points in Agility

Your incredible speed sets the pace for your allies. For an additional action point while moving, you can designate your exact path as a slipstream line. If any of your allies follow the path of your slipstream for at least 15 feet within the next turn (before the next time your action points refresh), they can make that move at no cost.

        Note: If the ally would normally require multiple action points to move, they may use your slipstream to move for 1 less action point than they otherwise would.

Snake Bite        

Agility Specialty

Cost: 1 AP

You lash out at an opponent like a coiled snake before following through with the rest of your attack. You may use your first action point of your turn to make a normal, unaltered melee attack with a medium or smaller weapon. No specialties may alter this attack.

Step Back

Agility Specialty

Cost: 1 AP reflexively

Just before you’re about to take a pounding, you skip back a couple feet and barely dodge the attack. After failing to evade, you may attempt to step back. Roll your agility. If your agility result is a tier higher than the opponent’s damage tier, then you can ignore the blow and move to any adjacent square that is not adjacent to the attacking opponent.

        If the opponent rolls tier 4 damage, there is no way to step back from it.

Terrain Mastery

Agility Specialty

You excel at crossing a wide variety of landscapes. You ignore penalties for moving through rough terrain.

Wall Runner

Agility Specialty

Requires: 6 skill points in Agility

Cost: Move +1 AP

Your feet move so quickly that you can bound up walls. When wall running, if you end your turn (though not necessarily your move) on a vertical surface, you will slip and fall off of it.

        Note: You may also use this specialty in conjunction with Free Movement for just 1 action point.

Walk Over

Agility Specialty

A row of enemies form little blockade against you. You can pass through a space occupied by an enemy as though the enemy wasn’t even there. If for some reason this would allow the enemy to attack you, they are denied that privilege. You cannot end your movement in their space - you must pass through.  

p Explosion Dodging Specialties M

Blast Dodger

Agility Specialty

Perhaps it’s your nerves, perhaps you’ve been caught up in explosions too many times. Regardless, you’re always ready to get out of dodge. You may jump out of the way of a blast, once per turn, for 0 action points.

Soaring Dodge

Agility Specialty

Requires: 5 skill points in Agility

Cost: 1 AP reflexively

Large blast radiuses? It’s not anything you can’t get out of. While you are dodging a blast, you may spend an additional action point reflexively per 5 skill points you have in agility in order to make extra moves and escape the blast radius.

p Phasing Specialties M

Phase Step

Agility Specialty

Resist: Cunning (negates)

Cost: Move (normally 1 AP)

When you move, you may instead phase step. When you phase step, you move quickly and in a way hard to notice. In order to hit you while you move, an opponent must make a Cunning resist against your agility to even notice your movement.

Fleeting Shade

Agility Specialty

Requires: Phase Step specialty

Cost: Move

Face it, it’s a whole lot harder to hide yourself in broad daylight. When phase-stepping from the open to a hiding place, you may roll below to give yourself a bonus to your Dexterity against opponents attempting to notice you so you can get hidden fast.

?tierTable
        +3 against enemies’ resists in attempt to see you.

        +6 against enemies’ resists in attempt to see you.

        +9 against enemies’ resists in attempt to see you.

        +12 against enemies’ resists in attempt to see you.

Leave No Trace

Agility Specialty

Requires: Phase Step specialty

Cost: Move (reflexively)

You constantly move through the shadows so that enemies will never be able to keep track of you or where you’ve been. You may Phase Step reflexively when enemies attempt to find you in your hiding place.

p Stance-Shifting Specialties M

Shifting

Agility Specialty

Requires: 2 stances known

Cost: 1 AP reflexively

You have learned how to switch between your stances using only slight movements, keeping your enemy in the dark about your next strike. When you are in one of your stances, you can shift to another stance at any time, during anybody’s turn.

Freeform Shifting

Agility Specialty

Requires: 12 skill points in Agility, Shifting specialty, & 3 stances known

Your feet are constantly moving, guaranteeing your opponent never has a clue. You may now use Shifting at no action point cost once per combat turn (between the times when your action points refresh).

* * *

Marksmanship Skill
------------------

Whether you’re one for pulling out your pistol and getting off a warning shot at the start of combat, or you like to stand behind a pillar with your bow and pick off enemies as your warrior-friend distracts them, marksmanship is the right skill for you. Encompassing all aspects of ranged weaponry, your skill at a distance will make you a frightening foe.

Aim

Marksmanship Specialty

Requires: 4 skill points in Marksmanship

Cost: 1 AP

Before you take your shot, you can spend some time lining up your sights and aiming. You can spend any number of action points preparing yourself, and, for every action point you spend aiming, you may roll on the chart in order to gain a bonus to your accuracy. You can start aiming one turn and then fire in a subsequent turn, but if you are attacked after you start aiming (but before you fire), you lose any bonus you would’ve gained from aiming. You cannot gain any more accuracy to an attack than you have skill in marksmanship.

?tierTable
        +1 to the accuracy roll

        +2 to the accuracy roll

        +3 to the accuracy roll

        +4 to the accuracy roll

Cover Fire

Marksmanship Specialty

Cost: as a Ranged Attack

While you don’t mind hitting the target, the real goal is to prevent the target from firing at your friend. When you provide cover fire, you take a -6 on your accuracy rolls but also provoke the target into attacking you (see Social Tells in the Chapter 1). You gain a bonus on the cunning roll equal to your skill in marksmanship.

Follow Up

Marksmanship Specialty

When you figure out how to nail that bastard, it’s not hard to do it again. If you land a ranged attack on a target and they do not move from their space before you attack again, you gain a bonus to accuracy and strike for you second attack. As long as the opponent does not move, the bonus continues with every subsequent attack. You do not gain the bonus multiple times; however, you may re-roll with every subsequent attack to gain a higher bonus, at your discretion.

?tierTable
        +2 to accuracy and strike

        +4 to accuracy and strike

        +6 to accuracy and strike

        +8 to accuracy and strike

Head Popper

Marksmanship Specialty

Cost: 1 AP reflexively

When they stick their head out of cover, you pop them right quick. When a foe leaves cover (to any degree), you can instantly make a reflexive ranged attack against them for 1 action point. If there is any dispute as to who gets to make the first attack, you win (unless there are two people with Head Popper, in which case it’ll be decided by a priority roll).

Itchy Trigger Finger

Marksmanship Specialty

Your friends might think you just randomly shot a bush, but you know better. If you have even the inkling that there’s somebody sneaking around within your gun’s range and you’re not aware of them, you can try to shoot them. You take the normal penalties for firing blindly, but they are still targeted. If there are multiple people sneaking around, your shot will target the closest one.

        If you miss, there is no reason the sneaking person will be identified, and you’ll probably just assume that there is nobody there.

Knock-Off

Marksmanship Specialty

Resist: Dexterity (negates)

Cost: Ranged Attack +1 AP

You take aim at a mounted opponent and attempt to shoot them off their mount or personal vehicle. Use your skill in marksmanship in a roll-off against their Dexterity. If you succeed, they are thrown off of their mount or personal vehicle.

Lockdown Gunner

Marksmanship Specialty

When the villain pulls out his doomsday device or the assassin draws a vial of poison, you’ll snipe it right out of their hands. You can reflexively attack from a range whenever anybody draws or activates an item. (And, as per normal, you can always make this a called shot to their hand in order to disarm them of the item.)

Long Shot

Marksmanship Specialty

Cost: Ranged Attack +1 AP

You test the air and adjust accordingly, doubling the range that your weapon is accurate to. (Shooting beyond that range takes accuracy penalties as is normal for your weapon.)

Penetrating Shot

Marksmanship Specialty

Cost: Ranged Attack +1 AP

You use your ranged weapon in such a way that their armor offers little protection. For the purposes of this attack, lower your opponent’s soak class by 1 for every tier that you receive with your marksmanship roll. This does not permanently affect the armor.

?tierTable
        Ignores 1 soak class

        Ignores 2 soak class

        Ignores 3 soak class

        Ignores 4 soak class

Point Blank

Marksmanship Specialty

Cost: Ranged Attack +1 AP

When your opponent’s up close, you don’t lose your calm. You just shoot them. You can make a Point Blank attack when using a ranged weapon against an adjacent opponent. You gain an immediate accuracy bonus for making the point blank attack.

?tierTable
        +3 accuracy

        +6 accuracy

        +9 accuracy

        +12 accuracy

Seeker

Marksmanship Specialty

By focusing in on the opponent, you can ignore their cover bonuses. Any time an opponent is being granted evade bonuses from cover, you may ignore it up to your skill in marksmanship. Thus, if they have light cover (granting them a +4 to evade) and you have 3 points in marksmanship, they only receive a +1 to evade.

Snap Reload

Marksmanship Specialty

You’ve been in enough gunfights that you’re quite proficient at readying your firearms and crossbows. You can ready any firearm or crossbow that you’re wielding for one less action point (to a minimum of 0). If you are wielding two or more crossbows or firearms, each one gains the reduction from snap reload.

Sneaky Seconds

Marksmanship Specialty

Requires: 4 action points per turn

You let off two shots so fast that, though they might evade the first one, they’re going to run straight into the second. Sneaky seconds activates immediately upon two consecutive ranged attacks being made in the same turn. Regardless of whether the first attack hits or not, the second ranged attack gains a bonus to its accuracy equal to your skill in marksmanship for determining if it hits (damage remains the same).

        Note: If you can make a third attack in the turn, it does not gain the sneaky seconds bonus. That said, a fourth attack would!

Stable Shot

Marksmanship Specialty

Requires: 3 points in Brute

You no longer need to be in footing stance to fire a super-heavy bow, crossbow, or firearm. You may do it from a standing, normal position.

Turret

Marksmanship Specialty

Stance (costs 1 AP to enter)

You find the perfect spot on the battlefield. You sweep around the field, raise your rifle, and fire. Nothing’s going to get to you.

        While in this stance, you cannot move. You must be stationary and on your feet (you cannot be mounted). If you move, you fall out of your Turret stance. For 2 action points reflexively, you can make a ranged attack at anything that moves so much as five feet toward you.

        If anybody comes into a space adjacent to you, you can make ranged attacks against them for 1 action point during your turn and with a bonus to your damage class, if you hit.

?tierTable
        +1 damage class

        +2 damage class

        +3 damage class

        +4 damage class

Warning Shot

Marksmanship Specialty

Cost: as a Ranged Attack

You can take a shot designed to miss, but also designed to scare the gods out of the target. When you take a warning shot, no attack rolls are necessary - instead, you may roll your Cunning to intimidate the person from afar (see Social Tells in Chapter 1). You gain a bonus on the intimidation roll equal to your skill in marksmanship.

Wing Clipping

Marksmanship Specialty

Resist: Brute (if the method of flight is physical) or Sciences (if the method of flight is mechanical)

Cost: Ranged Attack +1 AP

You take aim at a foe flying under their own power and shoot them out of the sky. Use your skill in marksmanship in a roll-off against their resist. If you meet or exceed their resist, you successfully disable their ability to fly. They will be able to fly again once they either successfully resist or they hit ground. They can attempt to resist for free at the end of their turn (when their action points refresh) or by spending 1 action point at any time.

        Note: If the target has multiple ways of flying, wing clipping will only disable one at a time.

p Archery Specialties M

Arching Shot

Marksmanship Specialty

Cost: Bow Attack +1 AP

You can now make an arching shot. An arching shot is one in which you launch your arrow higher into the air, planning for it to come down at just the right spot to strike your opponent. When using an arching shot, your bow can shoot accurately another 25 feet per point you have in your marksmanship skill. To make this attack, you cannot have a ceiling within 100 feet,

Efficient Ranger

Marksmanship Specialty

Requires: 3 skill points in Marksmanship

You nock your arrow, pull the string back, and release as though you’ve been doing it since you were a wee babe. You may now make attacks with heavy and super-heavy bows for 2 action points instead of the normal 3.

Flight of Arrows

Marksmanship Specialty

Cost: Bow Attack +1 AP

You release a flight of arrows, having nocked multiple arrows all at once. Make your normal attack. If you hit, instead of doing damage with your strike, you act as if you hit them multiple times with tier 1 damage. Each arrow may be soaked by the opponent’s defense.

?tierTable
        2 arrows that deal tier 1 damage

        3 arrows that deal tier 1 damage

        4 arrows that deal tier 1 damage

        5 arrows that deal tier 1 damage

Note: A flight of arrows acts as multiple attacks for the purposes of determining damage, but other abilities (such as other attack-modifying specialties or bow augments) only affect the target once if they’re applied to the flight of arrows.

p Bleeding Arrow Specialties M

Flesh Biter

Marksmanship Specialty

Cost: Bow Attack +1 AP

You’ve got a knack for using arrows to make the target bleed out. When making a Flesh Biter attack, the arrow causes the target to start bleeding out. They’ll continue to bleed until they spend 1 action point to stop the bleeding (which will stop up to 5 points of bleeding). The damage will incur at the end of their turn (when their action points refresh).

?tierTable
        2 point of bleeding

        4 points of bleeding

        6 points of bleeding

        8 points of bleeding

Flesh Piercing

Marksmanship Specialty

Requires: Flesh Biter specialty

Your flesh biting arrows sink in, dealing excruciating damage every turn until removed. Bleeding done from a Flesh Biter is difficult to stop, and acts as though it is twice as much when attempting to stop. (Thus, if the target has 4 points of bleeding on them from a Flesh Biter, it counts as 8 points for the purposes of stopping the bleeding.)

* * *

Swashbuckling Skill
-------------------

The swashbuckler specializes in fancy flourishes, graceful attacks, and absolutely dominating their opponent. Excelling in one-on-one opponents, a swashbuckler chooses their target and shuts them down. The Swashbuckling skill is designed for fast attacks that weave through an opponent’s defenses and sink in right through the ribcage.

Adaptable

Swashbuckling Specialty

Requires: 2 stances known

Your footwork is solid, your training and familiarity perfect, and your stances have become second nature. You can always keep one stance active and you cannot be forcibly removed from that stance. When you enter a stance, you can designate it as your background stance. By designating one as your background stance, you can have two stances going at the same time, one as your background stance and one as your normal stance.

        If you have two stances active, they cannot be mutually exclusive in any way.

Circle Attack

Swashbuckling Specialty

Cost: 1 AP reflexively (when melee attacking)

When the opponent attempts to deflect your attack, you bring your blade around their protections and into their gut. When an opponent attempts to deflect one of your melee attacks, you can spend 1 action point in order to circle around their deflection. This negates their deflection bonus.

        If the opponent can make multiple deflections against your attack, you may spend 1 action point per deflection to circle around each one.

Counter-Stance

Swashbuckling Specialty

Stance (costs 1 AP to enter)

Cost: 1 AP reflexively

You may declare a Counter-Stance against one opponent. If at any point you are attacked by the marked opponent, you can make a reflexive attack for 1 action point against that opponent, even before they finish their attack.

Efficient Strike

Swashbuckling Specialty

Cost: Melee Attack +1 AP

Focusing on the weakest points of your opponent, you are able to home in on their vitals. When you use an Efficient Strike, you may add the number by which your accuracy roll exceeded your target’s evade roll as a bonus to your strike.

Fight Anywhere

Swashbuckling Specialty

Hanging off railing and fighting with your sword in your teeth? No problem! You no longer take penalties for fighting on uneven surfaces, awkward terrain, in an unusual position, with your sword in your teeth, or while hanging from a wall.

Hilt Bash

Swashbuckling Specialty

Cost: 0 AP reflexively

When somebody attempts to keep you down, you bash them in the face. Any time an adjacent opponent attempts to negate an attack you’re in the process of making, you may instantly make a melee attack against them with the weapon you’re attacking with at no cost. (And no, the hilt bash can’t be negated.)

Opening

Swashbuckling Specialty

Cost: 1 AP reflexively

Your keen senses notice when somebody stumbles and leaves themselves open. If anybody within melee range of you gets a 1 or lower on their evade (when being attacked by another person), you can immediately attack them for 1 action point, using the evade that they rolled to determine if you hit them.

Precise Attack

Swashbuckling Specialty

Cost: Melee Attack +1 AP

A Precise Attack is one that has an improved chance of hitting. You roll your accuracy multiple times and take the highest result to determine if you hit. (If you roll a pure 12, continue rolling to figure out what the final result is. The pure 12 and attached rolls will only count as one roll.)

?tierTable
        You roll two dice

        You roll three dice

        You roll four dice

        You roll five dice

Saluted Opponent

Swashbuckling Specialty

Stance (costs 1 AP to enter)

When you enter into this stance, you select one opponent and you salute him. You gain a bonus on your melee accuracy rolls against that opponent equal to your skill in Swashbuckling. When you are in this stance, you take a -2 on evade rolls against all attacks from people other than your saluted opponent.

        To select a new opponent, you must renew your stance for 1 action point

Sword and Board

Swashbuckling Specialty

Stance (costs 1 AP to enter)

You’re a wiz at fending off incoming attacks. While in this stance, you can make one free deflection per turn.

Wild Slash

Swashbuckling Specialty

Cost: as a Melee Attack

Instead of a normal melee attack, you can make a Wild Slash. This is effectively the same as a melee attack, but you determine your accuracy differently. Instead, roll two dice and don’t add your accuracy. Add the results from the two dice, and that is your accuracy roll.

        If either die shows a 1, the entire roll is a 1. When using a Wild Slash, 12s do not explode (that is, you do not re-roll 12s and add the next number) unless both dice show 12s, in which case you may roll both again and add the result to 24.

p En-Garde Specialties M

En-Garde

Swashbuckling Specialty

Stance (costs 1 AP to enter)

You may only enter this stance when you are wielding a melee weapon in one hand and have the other hand empty, perhaps for balance or perhaps for grabbing things coming your way. While in the En-Garde stance, you can take no penalties on your accuracy rolls with melee attacks, and you gain a +4 on all accuracy rolls for reflexive attacks. If you switch weapons or are disarmed, you exit this stance.

Find the Gap

Swashbuckling Specialty

Requires: En-Garde specialty & 8 skill points in Swashbuckling

Armor? Pah! That stuff stands no chance against your fine blade. While in your En-Garde stance, any time you hit an opponent, their soak class is lowered by 1 point for every 8 skill points you have in Swashbuckling for the purposes of the attack.

Lightning Slash

Swashbuckling Specialty

Requires: En-Garde specialty & 16 skill points in Swashbuckling

Cost: 1 AP

While in your En-Garde stance, you can make Lightning Slashes for 1 action point apiece. A Lightning Slash is similar to a normal melee attack, except that it costs 1 action point and you add your skill in Swashbuckling in place of your strike in order to determine your damage. A Lightning Slash must be using a one-handed melee weapon and does not count as an attack. A Lightning Slash cannot be used in conjunction with any other modifying specialty that allows for “attack +1 AP,” as you are no longer making an attack but a Lightning Slash.

p Flickering Specialties M

Flickering

Swashbuckling Specialty

Cost: One-Handed Melee Attack +1 AP

Sometimes the last thing the opponent ever sees is your hand going for your weapon. Your attacks are like a flicker, getting in multiple strikes all at once. Make your normal accuracy roll. If you hit, instead of doing damage with your strike, you act as if you hit them multiple times with tier 1 damage. Unfortunately, each “attack” may be soaked by the opponent’s defense.

?tierTable
        2 attacks that deal tier 1 damage

        3 attacks that deal tier 1 damage

        5 attacks that deal tier 1 damage

        7 attacks that deal tier 1 damage

Note: A Flickering attack acts as multiple attacks for the purposes of determining damage, but other abilities (such as other attack-modifying specialties or weapon augments) only affect the target once if they’re applied to the Flickering attack.

Torrent of Steel

Swashbuckling Specialty

Requires: Flickering specialty & 25 skill points in Swashbuckling

Cost: Flickering +2 AP

Small scrapes and scratches? No longer: your Flickering attacks blend the opponent into bloodied pieces. Your Flickering attacks each deal tier 2 damage.  

p Footwork Specialties M

Footwork Training

Swashbuckling Specialty

At the end of any turn during which you move, you receive a bonus to your accuracy and evade. Roll your swashbuckling at the end of any turn in which you move, and you gain a bonus to your accuracy and evade until the end of your next turn (when your action points refresh).

?tierTable
        +1

        +2

        +3

        +4

Fancy Footwork

Swashbuckling Specialty

Requires: Footwork Training specialty

You are no longer a trainee: your footwork is that of a master. From now on, when determining your Footwork Training bonus, use the Fancy Footwork chart.

?tierTable
        +3

        +4

        +5

        +6

p Parry & Riposte Specialties M

Parry

Swashbuckling Specialty

Cost: 1 AP reflexively

When you are hit in melee with an attack that deals tier 1 damage, you have the option of parrying as a reflex. Roll accuracy and add your skill in Swashbuckling, and, if you score higher than the accuracy roll that hit you, you negate the blow.

Beat Parry

Swashbuckling Specialty

Requires: Parry specialty

Resist: Dexterity (negates)

Cost: Parry +1 AP

When you Parry, you smash the opponent’s weapon so hard they have a difficult time keeping grasp of it. When you do a Beat Parry, your Parry also acts like a called shot to the hand primarily holding the weapon attacking you.

Distance Parry

Swashbuckling Specialty

Requires: Parry specialty

Cost: 1 AP reflexively

You don’t parry with your blade. Oh no, you parry by simply not being in the place they’re attacking. You can parry by moving. It works exactly as a Parry, except that you may also immediately move.

        Because you’re moving, you cannot Distance Parry and then Riposte.

Experienced Parries

Swashbuckling Specialty

Requires: Parry specialty & 9 skill points in Swashbuckling

Cost: 1 (or more) AP

You’re no longer restricted to parrying the tier 1 attacks - you can push off the best of blows. You can attempt to parry any attack, but you must spend 1 action point per tier of the attack’s damage. (Thus, if an attack is dealing tier 3 damage, you’ll need to use 3 action points in order to attempt the parry.)

Riposte

Swashbuckling Specialty

Requires: Parry specialty

When you successfully Parry an attack, you can then immediately make a reflexive attack against the person you parried for no additional action points cost. You may upgrade the Riposte as per a normal attack, but the upgrades cost action points as per normal.

* * *
