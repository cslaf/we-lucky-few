Chapter 7 - Spirit
==================

Spirit breaks the limits within oneself. You use Spirit when attempting to focus yourself, resisting ethereal forces, or when praying for aid from deities. With Spirit, you can push for the stars, keep on going no matter what, and always believe in your path.

What can you do with your Spirit attribute?

Concentration c

You stay focused on your task. Whether it be staying conscious after being drugged, refusing to let a silver-tongued charmer manipulate you, or focusing on an elusive target in a loud and crowded space, Concentration allows you to weed out what you don’t need to be paying attention to and puts all your mind towards achieving your goal.

?tierTable
        You lose track of what you were doing.

        You are able to shut out some of the extraneous noise. You can see your objective, but you’re not fully able to keep your eyes on them.

        You have your target locked in your sights but your focus could still be broken by external stimuli.

        Everything else in the world fades away. All you see is your objective and how to grasp it.  

Narrator: Determining Distractions

If the player is in a gray room with no decorations and only one other person, concentrating on them should be rather easy. However, if the player finds him- or herself in the middle of a crowded, noisy market and the target is wearing the uniform of a town guard, keeping track of them will be a lot harder. Keep this in mind when determining bonuses and penalties.

Heroics c

Attempt Cost: Any resist +1 AP (reflexively)

Those who use Spirit have little that they can do with it alone, but a strong Spirit can save a person in times of duress. Any time you make a resist roll (or are making an attribute roll at a critical time, as per your narrator’s discretion), you can attempt to do so heroically. This requires an extra action point, but will give you either a bonus or a penalty, depending on how well your Spirit is rolled. Once you’ve rolled your Spirit, you can roll the other attribute normally - but now you get the following modifier on that roll:

?tierTable
        -4

        +2

        +6

        +12

* * *


Faith Skill
-----------

Faith is a powerful tool, and it’s one you can utilize to make your foes wish they had half as much faith as you. Regardless of where your faith comes from or toward what goals you put it, your devotion to a cause manifests itself in your actions. A person of faith can reinvigorate allies, move through obstacles as though they don’t exist, and survive far longer than what the faithless think possible.

Blind Faith

Faith Specialty

Requires: 6 skill points in Faith

Cost: same as an Attack

You lash out without hesitation, believing that your next attack is going to land true. When making an unaltered attack (as in, no other specialties are applied to it, including those that do not use action points, such as Critical Hits in Espionage), you may use your skill in Faith in place of your accuracy bonus for the accuracy roll.

Conviction

Faith Specialty

Resist: Spirit (negates)

Cost: Attack +1 AP

Any time you are attacking a corrupted creatures, such as those who have risen from the dead, automatons, or horrible abominations, you can attack with Conviction. If the target fails to resist, all of their damage soak is negated.

Divine Guidance

Faith Specialty

Cost: 2 AP reflexively

You close your eyes and guide your injured ally through faith alone. You can eliminate all status penalties on a person for one turn by giving them Divine Guidance at the beginning of their turn. Divine Guidance negates penalties from wounds, status effects, stuns, et cetera. (It does not negate fatal effects.) Any penalties incurred during their turn begin when their action points refresh, and all of their normal penalties return to them.

Flowing Vigor

Faith Specialty

Cost: 2 AP to begin and 1 AP to channel

You channel life back into an ally within 25 feet, replenishing their hit points. When you begin Flowing Vigor, you spend 2 action points and roll on the tier chart. Thereafter, you may heal that amount of hit points again for only 1 action point. You may continue the channel through your turn and into consecutive turns, but if you take any action other than channeling and moving, Flowing Vigor is broken and must be begun anew.

?tierTable
        2 hit points

        4 hit points

        6 hit points

        8 hit points

Grief & Hope

Faith Specialty

Cost: 3 AP (1 AP sacrifices)

The healing power of Grief and Hope can be an awesome sight to behold. You can channel that energy to reinvigorate ailing allies. For three action points, you can call upon your allies within 25 feet to give you their Grief and Hope in order to save another. When you do this, you can roll your Faith to heal 3 hit points per tier you reach. The target of the healing can be any ally within 25 feet.

?tierTable
        Restores 3 hit points

        Restores 6 hit points

        Restores 9 hit points

        Restores 12 hit points

For each action point an ally gives you, the amount of hit points the target heals increases by 2 per tier. Thus, if two people sacrifice an action point each for your channeled healing, the target will heal 7 hit points per tier your Faith roll receives.

        Each ally may only sacrifice 1 action point for any given usage of Grief & Hope.

Healing Halo

Faith Specialty

Stance (costs 1 AP to enter)

Everyone around you draws upon your divine energies. At the end of every turn that you are in your Healing Halo stance, you and your living allies within 25 feet regain a small amount of hit points. You and your allies gain 1 hit point, plus 1 for every 5 skill points you have in faith, at the end of every turn that you spent in Healing Halo (when your action points refresh).

Infallible Faith

Faith Specialty

Cost: 1 AP reflexively (1 AP sacrifices)

Your allies’ belief in you is infallible, their faith ensuring that you never fail. You may use Infallible Faith whenever you must roll a resist. You immediately gain a +2 bonus on the resist. Every ally within 25 feet may also sacrifice a single action point to you, increasing the bonus by another +3 per sacrificing ally.

Moral Support

Faith Specialty

Requires: a Spirit of 8

Cost: 2 AP reflexively

When a friend is in need, you can push them forward to greater feats of heroism. Any time you know of an ally within 25 feet being forced to use an attribute as a resist or in some momentary peril (such as having to jump a chasm or wrestle free a powerful device from a crazed maniac), you may roll your heroics (found under the Spirit attribute) and give them the bonus. If you roll poorly and receive a penalty as per your heroics result, that penalty is applied on your ally’s attribute roll, regardless of whether they want it there or not.  

Prayer

Faith Specialty

Cost: 1 AP (1 AP sacrifices)

With your allies on each side of you, you know you’re able to push onward with only their prayers. When you begin the Prayer, you roll on the chart, granting yourself temporary hit points. For each action point an ally gives you, roll again on the chart. Each ally may only sacrifice 1 action point for your Prayer and must be within 25 feet to do so. Prayer may not be used again until these temporary hit points are gone. If you were damaged at the time the Prayer begins, these hit points instead act as healing.

?tierTable
        Gain 2 temporary hit point

        Gain 3 temporary hit points

        Gain 4 temporary hit points

        Gain 5 temporary hit points

Purify

Faith Specialty

Cost: 2 AP

By laying your hands on yourself or an ally, your devotion allows your target to ignore the penalties from an ongoing poison, disease, parasite, called shot, wound, or other effect for a number of turns equal to your skill in faith. The target can not be Purified again until the effects of the first Purification wear off. If the effect of the penalty would have naturally worn off during the time the Purification was occurring, it does not resume once the Purification ends.

?tierTable
        Ignore 1 effect

        Ignore up to 2 effects

        Ignore up to 3 effects

        Ignore all effects

Shock of Life

Faith Specialty

Requires: 10 skill points in Faith

Cost: 2 AP

When times seem dire, you know how to shock somebody back into existence. You instantly heal, through touch, a number of hit points equal to twice your skill in Faith. This burst of energy, however, has some negative effects on the target. You roll your faith to determine how minor the negative effects are. A target may, at their discretion, ignore the Shock of Life altogether, though this must be decided prior to the Faith roll.

?tierTable
        Target is stunned for 3 AP

        Target is stunned for 2 AP

        Target is stunned for 1 AP

        Target suffers no ill effect

p AP Sacrifice Upgrade Specialties M

Devoted Peers

Faith Specialty

Requires: any specialty that calls upon sacrificial action points

Your friends are loyal and devoted like few others. When you call upon them to sacrifice their action points for your specialties, they may sacrifice up to two action points each.  

Self-Sacrifice

Faith Specialty

Requires: 4 action points per turn & any specialty that calls upon sacrificial action points

Your faith is so strong that it alone can guide your attacks. When you call upon others to sacrifice action points to your specialties, you may sacrifice your own action points as well. You, however, are not limited, and may sacrifice as many action points as you have available to you.

Silent Devotion

Faith Specialty

Requires: any specialty that calls upon sacrificial action points

Your beliefs do not need to be yelled, exclaimed, or shouted. When you call upon others to sacrifice action points to you, you do so by your willpower alone. You may now ask for your allies’ sacrifices with complete silence and no outward visual or audible  cues to your foes.  

p Champion Specialties M

Appointed Champion

Faith Specialty

Stance (costs 1 AP to enter)

You mark a nearby ally as your Champion and channel your Faith into him. While in this stance, any time said Champion would need to make a Spirit roll, he may choose to let you roll for him and apply your Spirit attribute’s bonus.

Conduit of Faith

Faith Specialty

Requires: Appointed Champion specialty

You draw the breath from nearby allies and empower your Champion. Anyone within 25 feet may sacrifice their last action point they have for the turn (at the end of their turn, when their action points refresh) to your Appointed Champion. It must be their last action point, and the action point is immediately added to the Appointed Champion’s pool of action points.

p Inquisition Specialties M

Proclaim the Heretic

Faith Specialty

Stance (costs 1 AP to enter)

Cost: 1 AP reflexively

You proclaim an enemy to be a heretic, a faithless vagabond, and now no one will lose their courage against him. Choose a single opponent when you enter this stance. When that heretic attacks you or one of your allies, you or the ally can spend 1 action point in order to roll the Spirit attribute instead of defense in order to determine damage soaked.

        To Proclaim a different Hheretic, you will need to change stance (costing 1 action point).

Light in the Dark

Faith Specialty

Requires: Proclaim the Heretic specialty

Your allies are protected by your faith when confronting the heretic. You and all of your allies may choose to use your Spirit in place of their defense bonus when soaking damage dealt by the Proclaimed Heretic. Making this choice does not cost anybody any action points.

p Smiting Specialties M

Smite

Faith Specialty

Cost: Melee Attack +1 AP (1 AP sacrifices)

Your allies’ belief in your attack guides your hand against the unfaithful. You call for your allies’ faith, and their prayers give your attack strength. When you begin to make a Smite, you call for your allies within 25 feet to sacrifice 1 action point in order to empower your attack. Any ally who can hear you (or knows that you called for their belief) and is within range can, reflexively, sacrifice the action point. If your attack lands, your attack deals damage as if it were one damage class higher for every action point an ally sacrificed to you. If nobody sacrificed an action point for the smite, it is treated like a normal attack.

        An ally can only sacrifice one action point per smite, and if the smite misses, the sacrificed action points are simply lost.

Assured Success

Faith Specialty

Requires: Smite specialty

When you Smite your foes, your blade is guided by faith into the enemy. For every action point sacrificed for your Smite, the attack gains a +1 on the accuracy roll.  

Impassioned Victory

Faith Specialty

Requires: 17 skill points in Faith & Smite specialty

When your Smite fells an evil foe, the attack is inspiring and leads your allies on to greater deeds. When your Smite kills or incapacitates the person being attacked, all of those who sacrificed action points to empower your Smite instantly regain those sacrificed action points.

Smiting Shot

Faith Specialty

Requires: Smite specialty

Your allies’ faith steadies your shot and flies with the bullet. You may now make ranged Smites, be it with a bow, firearm, or other ranged weapon.

Zealous Smite

Faith Specialty

Requires: Smite specialty & 7 skill points in Faith

Your smite does not merely deal great destruction but is also a weapon of finesse and a tool of your faith. Any action point sacrificed to your Smite can, instead of increasing your attack’s damage class, be used like a normal action point for modifying the attack with another specialty that you know.

        For example, if you have the Conviction specialty (normally made as an attack +1 AP) and you make a Smite, you can convert one of the action points sacrificed to you in order to make your Smite into a Smite with Conviction.  

* * *

Grace Skill
-----------

Grace is understanding yourself and bypassing your inner limits. Those fully serene are often known for their seemingly super-human abilities, their ability to ignore pain, ignore barriers, and do things that simply aren’t considered possible for other people. Is Grace magic? No, at least that’s what the practitioners of Grace will say. Grace is simply the wisdom of knowing your limits and how to bypass them.

Bloodsoak
Grace Specialty  
Requires: 4 skill points in Grace  

Your blood does not flow freely at another person’s convenience. Through unearthly bodily training, you can keep yourself from bleeding from deep gashes. When you would suffer bleeding damage, you may ignore 1 point per 4 skill points you have in Grace.

  
Connection  
Grace Specialty  
Stance (costs 1 AP to enter)  
Resist: Spirit (negates)  

When you lock eyes with an opponent, they cannot easily break the gaze. To make this connection, an opponent within 100 feet makes a Spirit roll opposed by your grace. If you succeed, their gaze is locked with yours. They cannot move behind any cover that would break the gaze or attack anybody else. If somebody or something walks between the two of you, that will give the opponent an automatic and free resist to break the gaze. The opponent can back away (moving with a -10 feet move penalty), and the connection is automatically broken at 100 feet.  

        If the opponent is actively avoiding your gaze (potentially because they know you have this ability), you might need to be creative in order to force them to look toward you. If a connection is made through a mirror, the connected people must stay at that angle or the connection is lost.

  
Danger Sense  
Grace Specialty  
Requires: 5 skill points in Grace  
Resist: Cunning (negates)  
Cost: 1 AP reflexively (to warn others)  

When a sniper locks onto you from a thousand feet away, you can feel it in your bones and through the chill on your neck. When you or an adjacent ally are about to be attacked, roll your Grace against the first attacker’s Cunning. If your Grace meets or exceeds their Cunning, you may have your hit points up and make a full evade roll against the attack.  

        If you spend 1 action point reflexively, you may warn others in your vicinity so that they have the same bonus. This warning is done as the attack is launched, so an opponent cannot stop his first attack from going off after the warning is given.

  
Destabilize  
Grace Specialty  
Resist: Dexterity or Spirit (target’s discretion, tiers down)  
Cost: Attack +1 AP  

Your attack disrupts their center of gravity, ensuring that they won’t be able to enter a stance for several moments. If the opponent is in a stance, they are knocked out of it unless they can resist against your skill in grace.

        If the target is not in a stance, however, the target cannot enter a stance until they spend some action points to stabilize again.
         
?tierTable
        Destabilized until 1 action point is spent to stabilize  
        
        Destabilized until 2 action points are spent to stabilize  
        
        Destabilized until 3 action points are spent to stabilize  
        
        Destabilized until 4 action points are spent to stabilize  


Dispel Pain  
Grace Specialty  
Cost: 1 AP reflexively  

When an opponent successful lands a called shot on you, you can dispel the pain. Doing so allows you to re-roll the resist, and you add your grace to the roll.

  
Force of Self  
Grace Specialty  
Stance (costs 1 AP to enter)  
Resist: Spirit (negates)  

Your aura is so powerful that nobody can even come close to you. When you enter this stance, anybody attempting to step into an adjacent space to you must make a successful resist. If they cannot make the resist, they can spend another action point in order to try again. If somebody is already standing next to you, they are unaffected. However, if you and anybody adjacent to you is separated, and they try to move next to you again, they must again succeed in rolling the resist.  You can allow allies to stand next to you.

  
Inner Calm  
Grace Specialty  
Stance (costs 1 AP to enter)  

You center yourself, calming your emotions and focusing your mind. While you are in your Inner Calm stance, you cannot be disoriented. If you become disoriented while outside of this stance, you may enter this stance to end the disorientation.

  
Iron Palm  
Grace Specialty  
Cost: Unarmed Called Shot +1 AP  

When you hit the opponent, your attack sends ripples through their body, activating multiple called shot effects as if you had hit each one separately. When you make a called shot with your Iron Palm, your called shot affects multiple locations.

?tierTable
        Affects called shot and an adjacent location of your choice
        
        Affects called shot and two adjacent locations
        
        Affects called shot and any two called shot locations
        
        Affects called shot and any three called shot locations
 
  
Master of Forms  
Grace Specialty  
Requires: 6 skill points in Grace & 2 stances known  

You can enter multiple stances, gaining all of their effects. You can have one stance active per 3 skill points you have in Grace. You must enter each one separately (spending 1 action point for each stance). Of course, you cannot enter two mutually exclusive stances. For instance, if a stance says that you cannot move while in that stance, you cannot be in another stance that requires you to move every turn.

  
Parting Waves  
Grace Specialty  
Resist: Dexterity (negates)  
Cost: 1 AP reflexively  

Any time you successfully evade an attack from a melee weapon, you can reflexively spend 1 action points in order to disarm the opponent of the weapon they attacked you with. They must make a Dexterity resist against your skill in grace in order to keep their weapon. If they fail, the weapon clatters to their feet.

  
Shocking Soul  
Grace Specialty  
Requires: 9 skill points in Grace  
Cost: 2 AP reflexively  

If you are successfully struck by a melee attack, you can immediately channel the power of your soul through their weapon and into your assailant. The enemy takes 1 unsoakable damage per point you have in Grace.

  
Spirit Break  
Grace Specialty  
Cost: 1 AP reflexively  

You focus your mind and your thoughts, reaching out to the people around you. Every time someone rolls their Spirit attribute within 25 feet of you, you may lower the result of the roll by your Grace.  
  
Spiritual Seal  
Grace Specialty  
Cost: Attack +1 AP  

Your attack can seal the Spirit on another person, causing them to be unable to tap into their strength of will. The target of your attack takes a penalty on all of their Spirit skills (Faith, Grace, Luck, and Shamanism) and the Spirit attribute until the end of your next turn.  

?tierTable
        -3  
        
        -6  
        
        -9   
        
        -12   
        
  
Void Strike  
Grace Specialty  
Resist: Spirit (as evade; see below)  
Cost: Melee Attack +1 AP  

You can guide your ki along the path of your strike, creating a sharp wave that rends through the target. Your melee attack can target those an additional 5 feet away from you per point you have in Grace. Because of the nature of this attack, the target may choose to use his Spirit in place of his evade in order to avoid the attack. If they have a poor Spirit, they may use their evade as per normal.  
  
  
p Ki-Unleashing Specialties M
Ki Flow  
Grace Specialty  
Resist: Brute or Spirit (negates, target’s discretion)  
Cost: 2 AP  

Through meditation and will, you have refined the control of your internal energy. Your Spirit strains against your flesh, manifesting itself in moments of duress. Enemies near you are pushed away from you when you manifest your ki flow.  

?tierTable
        Enemies within 5 feet of you must resist being pushed away 5 feet.   
        
        Enemies within 10 feet of you must resist being pushed away 5 feet.   
        
        Enemies within 15 feet of you must make resist being pushed away 5 feet.   
        
        Enemies within 20 feet of you must resist being pushed away 5 feet.   
        
  
Ki Rage  
Grace Specialty  
Requires: Ki Flow specialty  
Cost: Ki Flow +1 AP (3 AP total)  

The ki pressure that surrounds you has taken on a life of its own. When you release your ki flow on the battlefield, it damages all enemies that are affected by it. In addition to being pushed back, they also take a few points of unsoakable damage.  

?tierTable
        3 damage   
        
        6 damage   
        
        9 damage   
        
        12 damage   
  
  
p Light-as-Air Specialties M
Feather in the Wind  
Grace Specialty  
Cost: 1 AP (as a normal move)  

You may move across the air, over water, and skip across lava as though you were completely weightless. When you move like this, you can only move your normal speed weightlessly, and it must be a horizontal direction (you cannot move upwards). You may spend your next action point to continue moving weightlessly, but during that brief second between moving weightlessly, your weight returns. If you are falling when you activate weightlessness, you remain falling, but can move your speed horizontally.

        Any penalties you have to speed (such as from armor or crippling attacks) affect your weightless speed.

  
Weightless  
Grace Specialty  
Stance (costs 1 AP to enter)  
Requires: Feather in the Wind specialty  

When you are standing in a single spot, you can control your body’s weight so that it is at equilibrium with its surroundings. If you are on water in your Weightless stance, you will not sink. If you are in the air in your Weightless stance, you will not fall. If you stop moving weightlessly while in this stance (with your Feather in the Wind specialty), your weight does not return.  

        If you are trying to enter this stance under duressed conditions (such as while falling), you may do so for 2 action points, and may do so reflexively.  
        
        You cannot jump from a point that would not normally support your weight, such as from the surface of a pond or on the edge of a palm leaf.  
  
  
p Paralyzing Specialties M
Touch of Paralysis  
Grace Specialty  
Resist: Brute (negates)  
Cost: Unarmed Attack +2 AP  

You strike out at major nerve clusters and pressure points, twisting the target into a statue of agony. When you hit the target with Touch of Paralysis, you also roll to determine the tier. The target is allowed to resist against a roll of your Grace every time they would take damage from the paralysis, and, once successfully resisted, the effect ends. A person can be effected by only one Touch of Paralysis at any given time, with the greater result overtaking the previous. 

?tierTable
        The target takes 1 point of damage every time they try to move.  
        
        The target takes 3 points of damage every time they try to move.   
        
        The target takes 5 points of damage every time they try to move.   
         
        The target takes 7 points of damage every time they try to move.  
        
  
Blocked Ki  
Grace Specialty  
Requires: Touch of Paralysis specialty  
Resist: Brute or Spirit (target’s discretion, tiers down)  
Cost: Unarmed Attack +1 AP  

You strike one of your opponent’s chakra points, disrupting their natural energy flow. This causes a massive blockage that explodes in pain when the opponent makes even the slightest of movements. The opponent receives 1 point of unsoakable damage with each action point they spend. The opponent may spend the indicated amount of action point meditating, trying to unblock their blocked ki.   

?tierTable 
        1 AP to unblock the ki channel   
        
        2 AP to unblock the ki channel   
        
        3 AP to unblock the ki channel   
        
        4 AP to unblock the ki channel   
        
  

* * *

Luck Skill
----------

Luck is everywhere. Everyone has it. Some people have phenomenal luck, others are commonly out on their luck. You, however, know how to make your own luck. Now, it’s often a gamble, but when you win, you win big. Lucky people manipulate their own fate, sticking their necks on the line and hoping to persevere. The winners become the best, and the losers, well, they often die.

Confident in your Luck

Luck Specialty

Cost: 1 AP reflexively

You know the odds. You know that you’re oozing out good luck. When you so choose, you may use your skill in Luck in place of any other resist. Is somebody trying to push you around and you need a Brute resist to get out of it? Just spend 1 action point to use your skill in Luck in place of your Brute attribute.

Don’t Tell Me the Odds

Luck Specialty

Cost: 1 AP reflexively

You refuse to accept a bad hand dealt by fate and push on to succeed. You can spend 1 action point reflexively to get a +1 on any roll, but you can only use it if the bonus will raise your roll high enough that it will reach a higher tier result. This bonus increases by +1 for every 10 points you have in Luck.

Cheat Fate

Luck Specialty

Stance (costs 1 AP to enter)

Using your natural luck, you are able to prevent certain outcomes from being rolled. When you enter this stance, choose a single number from 2 to 11. If anybody near you rolls the number you chose, they must re-roll it. If the same number is rolled on the re-roll, it is kept.

Equalizing Force

Luck Specialty

Cost: 2 AP reflexively

Luck is the great balancer, bringing good luck to the misfortunate and bad luck to the fortunate. You can use Equalizing Force whenever you see somebody roll a die. For 2 action points, you change the die rolled to a 6.

        If the target had rolled a 12, the 12 is now a 6 but they roll again and add the results. If they had rolled a 1, it is now a 6, but they can’t add any of their bonuses to the 6. Now, a 6 is a 6 is a 6. This specialty cannot be used to alter wound effect or fatal effect rolls or any roll that requires a random die roll.

Hex

Luck Specialty

Resist: Dexterity or Spirit (target’s discretion, negates)

Cost: 2 AP reflexively

When your enemies are moving toward you, there’s always some loose piece of rubble or a stray twig that trips them up. For 2 action points, when anybody is moving directly toward you and is within 25 feet, you can cause them to trip. They make an opposed resist against your luck roll. If you meet or exceed their resist, they fall to the ground and are prone (normally costing an action point to stand up).  

Jackpot

Luck Specialty

Requires: 4 skill points in Luck

Lucky hits are rare to find, so you squeeze them for all their worth. When rolling strike or accuracy to determine damage, increase your damage class by 2 every time you roll a pure 12. This damage class increase only affects the one attack.

Jinx

Luck Specialty

Any time you are attacked, you can jinx that opponent. To do so, you willingly take a 1 on the evade and defense rolls of the incoming attack, letting them land a full blow against you. You can only choose to do this if you would have gained the full bonuses to your evade and defense in the first place. Your attacker is now jinxed. The next time they are attacked (be it from you or an ally), you may add your skill in Luck to either the accuracy or strike roll.  

Roll of the Dice

Luck Specialty

Requires: Any 1 other specialty from the Luck skill

The fates of fortune seem drawn to you, giving you every opportunity to take a gamble. At the beginning of your turn you can make one free roll for the sole purpose of activating one of your other luck specialties. Dice rolled this way can be stored without spending action points.

Roulette

Luck Specialty

Cost: Attack +1 AP

The more you’re willing to risk, the greater the rewards. When you announce your attack choose a number ranging from 1 through 12. Now roll your die without adding anything to it. If you get that number or higher, you get the number you chose as a bonus on your accuracy roll. If you get under that number, you resolve the attack as per normal.

Spot of Misfortune

Luck Specialty

Requires: 3 skill points in Luck

Resist: Spirit (negates)

Cost: 2 AP to create, 1 AP reflexively to enact

You know when an area is filled with bad luck. Choose a 5 foot spot within 25 feet of you. Any time somebody makes a combat roll (accuracy, evade, strike, or defense) while in that spot, you may spend 1 action point to cause them to take a 1 on that roll. They can resist with their Spirit opposed by your skill in Luck.

        When you use Spot of Misfortune on an opponent, they get a feeling that they’re standing in an unlucky location. You can make one such location for every 3 points you have in Luck.

        You can create a Spot of Misfortune inside a vehicle only if the vehicle has a cockpit larger than 10 feet by 10 feet. Otherwise, the vehicle is simply inside the spot of misfortune and can move out of it.

p Failure Avoidance Specialties M

Free from Failure

Luck Specialty

Stance (costs 1 AP to enter)

Requires: 6 skill points in Luck

While you might still do poorly, you have confidence that you’ll never fail completely. While in this stance, your natural 1s are not 1s. You may still add appropriate bonuses to rolls of 1. You may only do this if you rolled the 1 - having a specialty or effect that causes you to take an effective 1 can’t be affected by Free from Failure.

Steady Friends

Luck Specialty

Requires: 10 skill points in Luck & Free from Failure specialty

Cost: 1 AP reflexively

Your confidence extends to your friends and allies. When an ally within 25 feet rolls a natural 1 and you are in your Free from Failure stance, you may allow your ally to add their normal bonuses to the natural 1.

p Foul Luck Specialties M

Curse

Luck Specialty

Resist: Spirit (negates)

Cost: 1 AP to store, 1 AP reflexively to use

Your streaks of bad luck cause people to avoid you for fear of your bad vibes rubbing off onto them. When you roll a 1 in combat, you may spend 1 action point to save that roll. Your roll remains a natural 1.

        You may spend one action point to give another person within 50 feet the same result. They roll their Spirit against your skill in Luck to resist this effect. You may store a number of 1s equal to 1 plus 1 per 3 skill points you have in Luck. These 1s are stored until your next breather and may only be used on any dice rolled during combat.

Fumble

Luck Specialty

Requires: Curse specialty

Resist: Spirit (negates)

Cost: 1 AP

Your foe gets overly excited and drops their weapon before they’re even able to deliver an attack. When you’ve stored natural 1s with Curse, you can spend an action point and one of your stored natural 1s to roll your Luck against a target within 25 feet. If they fail to resist, they drop their weapon as if they had been disarmed.

p Luck Holder Specialties M

Ace Up My Sleeve

Luck Specialty

Cost: 1 AP reflexively to grab the die, 1 AP to use

You’ve become strategic in your use of luck. You’re able to store your successes and use them at more opportune times. Any time you roll a pure 12 on a combat roll, you can store it. To do this you must leave the die where it landed with the 12 showing and announce to the table that you are storing that pure 12. You then re-roll for the roll that you saved the die on.

        You can use a saved pure 12 on any combat roll.

        You must use the saved pure 12 before your next breather. You can save one pure 12, plus an additional pure 12 per 4 skill points you have in luck.

Leading the Lucky Life

Luck Specialty

Requires: Ace Up My Sleeve specialty

Cost: 1 AP

You cash in on your saved fortune for a rush of revitalizing energy. When using Ace Up My Sleeve to store pure 12s, you can instead use 1 action point to remove one of your saved 12s and roll your Luck to restore your hit points.

?tierTable
        You restore 7 hit points

        You restore 14 hit points

        You restore 21 hit points

        You restore 28 hit points

Second Chance

Luck Specialty

Requires: Ace Up My Sleeve specialty

Resist: Spirit (negates)

Cost: 1 AP reflexively

When things look their darkest you always seem to luck out of the deadly blows. When hit by an attack which would normally give you a fatal effect and you’ve stored a pure 12 with Ace Up My Sleeve, you can spend 1 reflexive action point and a stored 12 to receive a wound effect instead of receiving the fatal effect. The attacker may roll a Spirit resist to negate this effect.

p Lucky #7 Specialties M

Lucky Number 7

Luck Specialty

Stance (costs 1 AP to enter)

You have a habit of getting twice as many pure rolls as anyone else. Any time your die rolls a 7, it becomes a “pure 7,” and you may roll again and add the results. Fancy that!

Luckier Number 7

Luck Specialty

Requires: Lucky Number 7 specialty & 16 skill points in Luck

Not only are natural 7s lucky for you, they are overwhelmingly lucky. Now, when you are in your Lucky Number 7 stance and roll a 7, you can pick up the 7 and put it down as a 12. 7s equal 12s. And then you can re-roll them.

p Ranged Evading Specialties M

Feeling Lucky

Luck Specialty

Resist: Spirit (tiers down)

Cost: 1 AP reflexively

Ranged marksmanship and archery weapons have a habit of not working when they’re used to kill you. Hopefully that luck continues. When being fired upon by a ranged weapon, the opponent is allowed to resist. If they fail, roll your tier to determine the ill effect that happens to them.

?tierTable
        You roll twice on your evade and take the highest result.

        The weapon fails to fire.

        The weapon fails to fire and the ammo is destroyed. The weapon must be readied again, if applicable.

        The weapon backfires, dealing tier 1 damage to the user.

Unfriendly Fire

Luck Specialty

Requires: Feeling Lucky specialty

Resist: Dexterity (negates)

Cost: 2 AP reflexively

Sometimes your enemies miss you. Sometimes their guns misfire. Sometimes they explode in their hands. And sometimes your enemies accidentally shoot each other. This specialty makes the latter happen more often.

        When you are shot at with a firearm, crossbow, or bow, you can attempt to make the attack Unfriendly Fire for 2 action points. Choose a target within 10 feet of you. They are the new target of the shot.

        The original attacker still determines if they hit against the new target’s evade, but you roll your Luck in order to determine what tier of damage is done. For every tier that the original attacker rolls their resist above tier 1, they lower the damage by 1 tier. The new target can attempt to soak damage, as per normal.

        If the attack was special (that is, had specialties modifying it), all of the specialty modifiers are lost and the attack becomes a normal attack. The original attacker keeps the extra action points required to make the attack special.

Unfriendly Artillery

Luck Specialty

Requires: Feeling Lucky specialty & Unfriendly Fire specialty

When you divert an attack into a new target through sheer luck alone, it has the potential to be very powerful. Your Unfriendly Fire no longer loses the specialty modifiers from the original attack, and the original attacker does not regain their action points from adding those specialty modifiers.

* * *

Shamanism Skill
---------------

Shamanism is related to nature, the earth, and everything organic. The shaman stands out among the modern, industrial world as a return to archaic times. Shamanism sometimes represents a manipulation of the elements. Other times it is a use of animals to help further their causes. Regardless, those with skill in Shamanism will make a powerful and distinctive mark on any game.

Control Beast

Shamanism Specialty

Cost: 3 AP

You are able to calm an animal and gain its loyalty. You may even be able to redirect its anger.

        If you attempt to use this ability on an animal under another shaman’s control, the owner and you must make opposed Shamanism rolls. If you succeed, you can attempt to control it. If you fail, nothing occurs.

?tierTable
        The animal becomes cautious, only attacking if forced

        The animal becomes passive and will not attack

        The animal becomes passive and willing to help you

        The animal becomes your ally, and will not attack you. You may direct it to attack another, at your discretion.

Druidic

Shamanism Specialty

Stance (costs 1 AP to enter)

Warped metal feels unnatural in your hand, and you’ve always felt more at home wielding your ancestral, tribal weaponry. When in this stance, wood and organic weapons (not including unarmed attacks) deal 2 damage classes higher.

Fire Resistance

Shamanism Specialty

Your natural body heat increases. Your love of the flame has begun to manifest as you become more and more resistant to fire. Any time you take damage from fire or a fire-based attack, you soak a fair deal of the damage.

?tierTable
        3 damage from heat or fire soaked

        6 damage from heat or fire soaked

        9 damage from heat or fire soaked

        12 damage from heat or fire soaked

Geomancer

Shamanism Specialty

Requires: Topographer specialty

Choose an extreme terrain: jungle, desert, swamp, tundra, high atmosphere, or the abyss (deep underwater); or an extreme weather condition: raging thunderstorm, hurricane, tornado, blizzard, meteor shower, sand-storm, or heat wave. When fighting in these chosen conditions, you may use your skill in Shamanism in place of any tiered skill roll.

Hardened Trainer

Shamanism Specialty

Through years of dealing with animals, you are keen at fighting against them. When you are fending off an animal, you gain a bonus to your defense equal to your skill in Shamanism.

Lion’s Roar

Shamanism Specialty

Resist: Spirit (negates)

Cost: 2 AP

You breathe deeply and let out a mighty roar that strikes fear into the heart of all nearby enemies. When making such a warcry, all opponents within 50 feet who can hear you must roll a Spirit resist against your Shamanism. If they do not resist, they become frightened, suffering the effects of Tier 2 fear.

Naturalist

Shamanism Specialty

Requires: 5 skill points in Shamanism

Coating yourself in metal makes you uneasy. You gain an additional soak class for every 5 skill points you have in Shamanism while wearing organic or wooden armor.

Parasite

Shamanism Specialty

Resist: Brute (tiers down)

Cost: 2 AP to begin, 1 AP to continue during subsequent turns

By stomping in constant rhythm, you draw out nearby insect colonies to assault an opponent within 25 feet. On the turn the swarm attacks, and every turn thereafter, roll on the called shot chart. The victim must roll to resist against your Shamanism or suffer the effects of this called shot.

        Anything that would affect an area disrupts the swarm and it must be reformed (for 2 action points). If at any point you lose the ability to move your legs or are knocked prone, the swarm is disrupted.

Still as Stone

Shamanism Specialty

Stance (costs 1 AP to enter)

Stalking your prey like a crouched cougar, you wait for the exact correct moment to pounce, still as the world around you and blending in as if you were a part of the scene itself. Whether you’re in the quiet meadows and copses of the forest or a back-alley in the night, you can blend in with your surroundings perfectly while standing still. When you enter this stance, you gain a bonus equal to your skill in Shamanism to hide when you have cover.

Tactics of the Wolf

Shamanism Specialty

Cost: Melee Attack reflexively

You’ve learned to approach combat like a pack of wolves attacking their prey: all at once. When somebody attacks an adjacent opponent, you can make a reflexive attack against that opponent. If you hit, you gain a bonus on your strike.

?tierTable
        You gain +3 on the strike roll

        You gain +6 on the strike roll

        You gain +9 on the strike roll

        You gain +12 on the strike roll

Topographer

Shamanism Specialty

Be it hurricane, heatwave, blizzard, or sleet, you are able to function in all weather conditions without penalty. In addition, you take no penalties for moving through rough or unsafe terrain.

p Bird Calling Specialties M

Avian Wrath

Shamanism Specialty

Cost: 2 AP to begin, 1 AP to continue during subsequent turns

Raising your hands above you and whistling, you call down death from above. Native animals dive down and attack your victim, a victim who can be up to 50 feet away. Depending on your surroundings, these creatures might be fish, bats, birds, or even insects, though they’re never larger than a human’s fist. When you first call the animals, you must make an accuracy roll against the target’s evade (just like a normal attack). If you hit them with your summoned avians, they deal damage as per the tiers below. This damage can be soaked.

        Anything that would affect an area (such as an explosion or gas) disrupts the swarm (forcing you to start over, if you so desire). If at any point you lose the ability to speak, the swarm is disrupted. You may not talk while continuing an Avian Wrath or use any specialties requiring the use of your voice.

?tierTable
        Animals deal 6 initial damage, 3 damage when continued

        Animals deal 12 initial damage, 6 damage when continued

        Animals deal 18 initial damage, 9 damage when continued

        Animals deal 24 initial damage, 12 damage when continued

Blacken the Sky

Shamanism Specialty

Requires: Avian Wrath specialty

Resist: Cunning (negates)

When using Avian Wrath, the swarm of squaking animals becomes so dense that it distracts your foe. Every turn an opponent is hit by your Avian Wrath, they must resist against your Shamanism or be disoriented (losing 1 action point). Once your Avian Wrath stops, they become re-oriented.

Pitch Black

Shamanism Specialty

Requires: Avian Wrath & Blacken the Sky specialties

When using Avian Wrath, your swarm becomes so thick and fast that it blocks out all light around your victim. The victim is now blinded while they remain the target of the Avian Wrath.

p Chemical Immunity Specialties M

Venom Immunity

Shamanism Specialty

Being stung, being bitten, having venom injected into your body: these things used to be a big deal, but they’re less worrisome nowadays. When you are attempting to resist a poison, add your skill in Shamanism to the resist roll.

Alchemical Resistance

Shamanism Specialty

Requires: Venom Immunity specialty

You’ve trained your body to ward off venoms, both natural and unnatural. When resisting any alchemical substances (gases, acids, poisons, et cetera), add your skill in Shamanism to the roll.

p Protective Swarm Specialties M

Protect the Monarch

Shamanism Specialty

Cost: 2 AP to begin, 1 AP to continue during subsequent turns

By vibrating your vocal cords, you trigger a defense mechanism in nearby creatures that swarm you, protecting you from harm. As with all swarms, these are creatures native to the terrain ranging from fishes to small woodland creatures and even insects, though they are never larger than a fist.

        Anything that would affect an area disrupts the swarm and it must be reformed again (costing 2 action points). If at any point you lose the ability to speak, the swarm is disrupted. You may not talk while continuing a Protect the Monarch or use any specialties requiring you to use your voice. You may only be covered in one swarm at a time.

?tierTable
        +1 soak class

        +2 soak class

        +3 soak class

        +4 soak class

Devoted Drones

Shamanism Specialty

Requires: Protect the Monarch specialty

The swarm covering your body is willing to die in order to protect you. When using Protect the Monarch, the swarm is only disrupted when dismissed (such as when you stop spending action points on it) or until your next breather.

Hive Exodus

Shamanism Specialty

Requires: Protect the Monarch specialty

Be it from lifting you up into the air or dragging you across the battlefield, each turn you spend with Protect the Monarch active, your swarm may move you in any direction. If moved into the air the swarm maintains you there each turn until disturbed.

?tierTable
        You are moved 5 feet

        You are moved 10 feet

        You are moved 15 feet

        You are moved 20 feet

p Swarming Insect Specialties M

Colony of One

Shamanism Specialty

Resist: Brute or Dexterity (negates, as per a grab)

Cost: 2 AP to begin, 1 AP to continue during subsequent turns

By humming at a very low frequency, you are able to call a swarm of native insects or animals to overwhelm your victim and hold them in place. These creatures can be anything smaller than your fist and native to the area. The swarm uses your accuracy when making a grab against its target no farther than 50 feet away. In order for the victim to break free they must roll resist against your skill in shamanism (using their brute or dexterity).

        Anything that would affect an area (such as an explosion or gas) disrupts the swarm (forcing you to start over, if you so desire). If at any point you lose the ability to speak, the swarm is disrupted. You may not talk while continuing a colony of one or use any specialties requiring the use of your voice.

?tierTable
        The swarm grabs one called shot location

        The swarm grabs one called shot location

        The swarm grabs two called shot locations

        The swarm grabs two called shot locations

Drag Down

Shamanism Specialty

Requires: Colony of One specialty

Resist: Brute (negates)

The vermin swarm pulls its opponents to the ground. Any foe successfully grabbed by your Colony of One is also brought prone unless they can make a brute resist against your Shamanism.

Hive Mind

Shamanism Specialty

Requires: Colony of One specialty

When continuing a swarm, it is no longer disrupted when you are knocked prone or rendered unable to speak. This means you may now freely speak and use specialties requiring speech without having to call out your swarms again.  

Pressure Cooker

Shamanism Specialty

Requires: Colony of One specialty

The swarm of creatures holding down the victim begins to move rapidly, cooking them with sheer friction. Each turn after grabbing a target with colony of one, a point or more of unsoakable damage is dealt to the target.

?tierTable
        Takes 1 unsoakable damage

        Takes 2 unsoakable damage

        Takes 3 unsoakable damage

        Takes 4 unsoakable damage

* * *