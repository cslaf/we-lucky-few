import re
import os
specialtyCheck = re.compile(r'(((\b\w+\s)(\+\d+))($|,))')

h3 = re.compile(r'(^p\s([^\n]*)M\s*$)|((^c\s|)([^\n]*)[^i][c]$)')
h4 = re.compile(r'([^\n]*)\s[z]$')
skillCheck = re.compile(r'(Skill$)')
augmentCheck = re.compile(r'(Augment(\s*|\s*\(setting\))$)')
craftingCheck = re.compile(r'(^Crafting)')

resistCheck = re.compile(r'^Resist:')
requiresCheck = re.compile(r'^Requires:')
costCheck = re.compile(r'^(Activation\s|Attempt\s|)Cost:')
#combine these to a single blockquote
#add specialty

#create a tier table creation based off a ?tierTable
tierTableCheck = re.compile(r'^\?tierTable')
marqueTableCheck = re.compile(r'^\?marqueTable')

stanceCheck = re.compile(r'^Stance \(costs 1 AP to enter\)')
skillSpecialtyCheck = re.compile(r'^(\w+?(\s|-))+(Specialty)$')


noteCheck = re.compile(r'^\s*Note:')

def removeLeadingSpaces(i, dat) :
    if not dat[i-1].strip("\n") :
        dat[i-1] = dat[i-1].strip("\n")

def toTierTableLine(string, indx) :
    if not string.strip("\n").strip() :
        return ""
    split = re.split('\s{8}', string)
    first = split[0]
    second = split[1]
    if first.startswith("Tier") :
        second = split[1]
        return "|" + first.replace("Tier", "") + "|" + second.strip("\n") +"|\n"
    return "|" + str(int((indx+1)/2)) + "|" + second.strip("\n") +"|\n"


# Make a reference
data = []
for root, dirs, files in os.walk("./Original") :
    files.sort()
    for file in files :
        if re.search("^\d\s", file) :
            with open("./Original/" + file, 'r', encoding="utf8") as f:
                data.extend(f.readlines())

matched_lines = { re.split("\xa0|s{8}", line)[0] : line for line in data if specialtyCheck.search(line)}
matchstring = re.compile("(" + "|".join([match + "\s*\n" for match in matched_lines.keys()]) + ")")
i = 0
for line in data :
    mat = matchstring.search(line)
    if mat :
        thismatch = matched_lines[mat.group(0).strip("\n").strip()]
        matchsplit = re.split('\s{8}',thismatch)
        data[i] = "#### " + matchsplit[0] + "\n> " + matchsplit[1]

    if specialtyCheck.search(line) :
        if not line.startswith("#") :
            data[i] = "#### " + line 

    m = re.search(h3, line)
    if m :
        data[i] = "### " + line.strip('\n').rstrip('M').strip('p').strip('c').strip('z').strip() + "\n"
    if h4.search(line) :
        data[i] = "#### " + line.strip('\n').rstrip('z') + "\n"
    
    if re.search(skillCheck, line) :
        #if ends in skill, make h2
        if not line.startswith("##") :
            data[i] = "## " + line
    if re.search(augmentCheck, line) :
        #if it doesn't start with aug or requires but ends in Augment, make previous h4
        if not line.startswith(("Aug", "Requires")) :
        #do some fun stuff with skill emoji here later
            data[i] = ">   " + line
            if not data[i-1].strip("\n").strip() :
                data[i-1] = data[i-1].strip("\n").strip()
            if not data[i-2].strip("\n").strip() :
                data[i-2] = data[i-2].strip("\n").strip()
            if data[i-1] and data[i-1].strip("\n").strip() :
                data[i -1] = "#### " + data[i - 1]
            if data[i-2] and data[i-2].strip("\n").strip() :
                data[i -2] = "#### " + data[i - 2]
    if craftingCheck.search( line) :
        #if it starts with crafting and doesn't end with .
        if not line.endswith(".\n") :
            data[i] = "### " + line
    if resistCheck.search(line) :
        data[i] = "> ⬇️ " + line
    if costCheck.search(line) :
        data[i] = "> 🪙 " + line 
    if requiresCheck.search(line) :
        data[i] = "> ❓ " + line
    if stanceCheck.search(line) :
        data[i] = "> 🧘 " + line
    if noteCheck.search(line) :
        data[i] = "> 🗒️️ " + line

    if skillSpecialtyCheck.search(line) :
        #do some fun stuff with skill emoji here later
        data[i] = "> " + line

    if tierTableCheck.search(line) :
        data[i] = "|Tier| Effect|\n|--|--|\n"
        for x in range(1,8) :
            data[i+x] =  toTierTableLine(data[i+x], x)
    if marqueTableCheck.search(line) :
        data[i] = "|Marque| Effect|\n|--|--|\n"
        for x in range(1,8) :
            data[i+x] =  toTierTableLine(data[i+x], x)

    i +=1





# split into chapters here
chapters = []
toc = []
i = 0
lastchapter = 0

for line in data :
    if line.startswith("# ") or line.startswith("Chapter") :
        toc.append( "[[" + line.strip("#").strip() + "]]\n\n")
        if i != 0 :
            chapters.append(data[lastchapter: i - 1])
        lastchapter = i
    i += 1
chapters.append(data[lastchapter: i - 1])


#print("".join(toc))

with open(r'Revised Clockwork Gaming System.md', 'w', encoding="utf8") as file:
    file.writelines(toc)

i = 0

for name in toc :
    n = name.strip("\n").strip("[").strip("]")
    chapter_path = n.split(" ")[0]
    if chapter_path != "Appendix" :
        chapter_path += "s/"
    else : 
        chapter_path += "/"
    with open(chapter_path + n + ".md", 'w', encoding="utf8") as file :
        file.writelines(chapters[i])
    i += 1