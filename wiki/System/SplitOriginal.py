with open(r'Original/Clockwork Gaming system_old.md', 'r', encoding="utf8") as file:
    data = file.readlines()
 
# split into chapters here
chapters = []
toc = []
i = 0
lastchapter = 0

for line in data :
    if line.startswith("# ") or line.startswith("Chapter") :
        toc.append( "[[" + line.strip("#").strip() + "]]\n")
        if lastchapter != 0 :
            chapters.append(data[lastchapter: i - 1])
        lastchapter = i
    i += 1
chapters.append(data[lastchapter: i - 1])


print("".join(toc))

with open(r'Original/Clockwork System Ref.md', 'w', encoding="utf8") as file:
    file.writelines(toc)

i = 0

for name in toc :
    n = name.strip("\n").strip("[").strip("]")
    chapter_path = str(i) + " "
    with open("Original/" + chapter_path + n + ".md", 'w', encoding="utf8") as file :
        print(n)
        file.writelines(chapters[i])
    i += 1