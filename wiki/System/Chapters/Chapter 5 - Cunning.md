Chapter 5 - Cunning
===================

Also this will have [[Glamour]] as a skill.

Knowledge is power, and using it to your advantage can move worlds. At its most basic level, cunning is the ability to process and retain information. As you grow, you can train yourself to be more keen, see into things that other people would quickly disregard, and learn the skills to survive no matter where you go. The attribute of Cunning also increases you ability to see the whole picture and interact with others.

What can you do with your Cunning attribute?

### Gather Intel

Memory and research can be very valuable tools for finding information. Intelligence can be found through talking to the right people, reading, or simply having heard about it in the past. To determine if you know the information, roll your Cunning and tier the result. The narrator will then tell you the appropriate amount of information you know.

|Tier| Effect|
|--|--|
|1|Tier 1 You know nothing more than common knowledge, such as the name of a road or the king’s name.|
|2|Tier 2 You’ve heard some tidbits, a smidge of esoteric knowledge, like the year an old temple was built or the nickname for the local baron’s favorite hunting dog.|
|3|Tier 3 You know rather obscure information, such as where the merchant lord of a large shipping company buys his illegal firearms from or how a famous doctor prevents people from bleeding during his surgeries.|
|4|Tier 4 You know the rarest of information, from the coordinates of the tree a bartender in the next town over shared his first kiss under, to the secret meaning behind the king’s royal title.|

Research and intelligence gathering can allow you to roll again, and potentially with bonuses. If you’ve spent quite a bit of time in a library, looking up the reasons behind the current plague, you may roll again. In addition, the advanced medical section in the library may grant you a +3 on the roll. Likewise, if the local librarian who you’ve been talking to also has some knowledge of the plague, that +3 might jump up to a +6 on the roll.

### Lockpicking

> 🪙 Attempt Cost: 3 AP

The gunfire makes it hard to concentrate. Your friends have your back, picking off the ravenous arachnid automatons while you have your toolset in your hand, trying desperately to get through this lock. There’s too many of them to fight off. If you can’t get this done before the full swarm arrives, you’re all dead.

        Trying to pick a lock costs 3 action points, just to get into position and starts the process. After that, you’re going to roll Cunning. For any given lock, you may only roll once. The tier that you receive will tell you how well you pick the lock.

|Tier| Effect|
|--|--|
|1|Tier 1 Requires several minutes and is not something that can be done during combat|
|2|Tier 2 Requires 9 more action points|
|3|Tier 3 Requires 3 more action points|
|4|Tier 4 You got it on the first try.|

Difficulty of Locks: The basic door lock will rarely hamper a person truly trying to open the lock. However, advanced locks will decrease your tier result. For example, a solid, well-built lock might lower the lockpicker’s result by 1 tier. The duke’s custom-made and advanced locking mechanism might lower the lockpicker’s tier by 3. If the lockpicking roll is brought below tier 1, it simply cannot be accomplished (unless mitigating factors might allow it - and the narrator might even say, “It seems possible, but will require several hours of focus”).

        A lock that lowers the result by 4 tiers should probably not even exist, as the lock is virtually unpickable.

### Notice

Notice is an immensely useful little trait, from noticing that the building is on fire to noticing that the king is wearing the wrong ring. Notice comes in two categories: noticing people and noticing objects. If you are looking for a person, you would roll your Cunning against their Cunning (if they’re hiding) or their Dexterity (if they’re sneaking). If you are attempting to notice an object, you would roll your Cunning and tier the result to see if you can notice it.

|Tier| Effect|
|--|--|
|1|Tier 1 You can only notice readily visible items.|
|2|Tier 2 You notice items that are partially hidden.|
|3|Tier 3 You notice items that are almost entirely hidden, or something else is betraying their whereabouts.|
|4|Tier 4 You notice items that nobody else would, like a faint outline under a curtain or that the airflow in the room is slightly off.|

* * *
## TODO
- Add in temporal skill, maybe shared with agility?
- Change showmanship skill to glamor and and convert tactical skills to it

## Espionage Skill
---------------

Let’s face it: you just want to stab people when nobody’s looking. It may be due to personal, unresolved social problems. It could be an emotional dependency on other people’s pain. Or maybe you just like delivering surprises. Regardless, you take people off-guard, play with them in a fight, and then trash them them like a rotting corpse in a trench. In fact, that’s exactly what they are.

#### Destabilizing Strike
> Accuracy +1, Strike +2, Hit Points +7

> Espionage Specialty

> 🪙 Cost: Attack +1 AP

When your opponent is disoriented, you take advantage of their weakened mind and can make a Destabilizing Strike. A Destabilizing Strike, if it deals damage, causes the opponent to become open to a reflexive attack from every character within melee range of the target. These reflexive attacks can be made either as unarmed attacks or by using super-heavy or smaller melee weapons, which will only cost 1 action point to perform instead of the normal 2. As the person who made the destabilizing strike, you cannot make a reflexive attack.

#### Feign Fatal Wounds
> Accuracy +1, Evade +2, Hit Points +7

> Espionage Specialty

> ⬇️ Resist: Cunning (negates)

> 🪙 Cost: 1 AP reflexively

When you’re struck in combat, you can overplay the success of the attack, making your enemy think you’ve been taken out of the battle or seriously injured. When damage is dealt to you, you may spend 1 action point reflexively in order to trick the opponent into thinking that they have wounded, dealt you a fatal attack, or outright killed you. They may resist with a successful Cunning roll (opposed by your espionage roll). If they fail, you may choose to what degree you appear to have been injured (or falling down and feigning death entirely).

        Do note that if you fall down as a result of feigning death or a serious wound, standing up costs an action point.

#### First Strike
> Accuracy +1, Priority +4, Hit Points +5

> Espionage Specialty

You make it your business knowing that you’re in a fight before your enemies do. If you make the first strike of a combat - either before priority is rolled or by being the first person to act in the combat - your attack gains a bonus on your accuracy roll equal to your skill in Espionage.

#### Flowing Shadow
> Accuracy +1, Evade +1, Hit Points +6

> Espionage Specialty

> ❓ Requires: 4 skill points in Espionage

You flow within the shadows, keeping your opponents guessing as you weave and dart through the darkness. Any time the person attacking you is blinded or in poor lighting, you can roll your evade two times and take the higher result. In addition, you gain a +1 on your evade rolls per 4 skill points you have in Espionage while in darkness. If the opponent attacking you in unaffected by darkness or relies on a way of finding you that is not based on sight, you do not gain the evade bonus or the ability to roll twice and take the higher result.

#### Heartseeker
> Accuracy +1, Strike +2, Hit Points +6

> Espionage Specialty

> 🪙 Cost: Melee Attack with a Light Weapon +1 AP

When wielding a light weapon, you know the best way to get through your enemy’s pesky armor. When you make a Heartseeking attack, your opponent has more difficulty soaking it. For the purposes of this attack, lower your opponent’s soak class by 1 for every tier that you receive with your Espionage roll. This does not permanently affect the armor.

|Tier| Effect|
|--|--|
|1|-1 soak class|
|2|-2 soak class|
|3|-3 soak class|
|4|-4 soak class|

#### Invisible Blade
> Accuracy +1, Strike +1, Hit Points +7

> Espionage Specialty

> 🧘 Stance (costs 1 AP to enter)

You fight with your weapons palmed, keeping your attacks so tight that your blades are little more than an extension of your fists. When using a weapon that is both light and concealable, you fight as if unarmed: you cannot be disarmed or have your weapon sundered, and your attacks only cost 1 action point to make. For all other purposes, your weapon still counts as a light weapon.

#### Master Lockpick
> Evade +1, Priority +3, Hit Points +7

> Espionage Specialty

Doors are of little bother to you; your skill in lockpicking turns solid barriers into tiny inconveniences. This specialty improves your ability to understand a lock and pick it. When you begin picking a lock, roll for your Master Lockpick to further reduce the action points that you’ll need in order to pick the lock. The AP cost of the lock can never drop below marque I.

|Tier| Effect|
|--|--|
|1|No luck. Your normal Cunning will have to deal with the lock.|
|2|You can kind of see it. The marque of the lock is reduced by 1 for the purpose of this lockpicking.|
|3|It’s an average challenge. The marque of the lock is reduced by 2 for the purpose of this lockpicking.|
|4|Aha! The marque of the lock is reduced by 3 for the purpose of this lockpicking.|

#### Pierce the Darkness
> Accuracy +2, Priority +1, Hit Points +7

> Espionage Specialty

> ❓ Requires: 4 skill points in Espionage

You launch your attack from the victim’s blindspot, ensuring your success. When you are attacking from perfect darkness, poor lighting, fog, or anything else that hampers vision, you gain a +1 on your accuracy roll for every 4 skill points you have in Espionage. If the opponent has some way of seeing through the poor visibility you’re hiding in, you do not gain the bonus.

#### Silent Kill
> Accuracy +1, Evade +2, Hit Points +6

> Espionage Specialty

> ⬇️ Resist: Cunning (see below)

> 🪙 Cost: Melee Attack +1 AP

When you attack somebody, you may attempt to make the attack absolutely silent. This is normally done when going for a killing blow that the target is unaware of, in order to not get caught in the act.

        You may be stifling their scream or trying to make your blade not chink against their armor. If the target does not try to make a noise, you simply roll your skill in espionage against anybody listening.

        If, however, the target screams, yells out, or calls for help, you must attempt to cover their mouth or stab them in the throat: anything to keep the sound from carrying. If this happens, anybody listening can attempt a Cunning resist to hear the cry.

#### Sinister Strike
> Accuracy +1, Strike +3, Hit Points +7

> Espionage Specialty

> ❓ Requires: 18 skill points in Espionage

> 🪙 Cost: Reflexive Melee Strike +3 AP

Your ally has created an opening, and it’s time to go in for the kill. After an ally hits an adjacent opponent, you can reflexively make a sinister strike. This attack deals damage directly to wounds. If it exhausts the target’s wounds, they suffer a fatal effect.

### Nightwalker Specialties

#### Fighting Blind
> Accuracy +2, Strike +1, Hit Points +6

> Espionage Specialty

You take no penalty for melee fighting while in poor lighting or in absolute darkness.

        If you do not know where your opponent is, this specialty does not give you a way of locating them, and this Fighting Blind does not negate the ranged penalty for blindness.

#### Deep Blind Senses
> Accuracy +1, Evade +1, Hit Points +6

> Espionage Specialty

> ❓ Requires: Fighting Blind specialty & 5 skill points in Espionage

Your hearing, sense of the air vibration, or ability to locate your opponents in the deepest of darknesses allows you to fire accurately upon distant targets even when blind. You may use ranged attacks against opponents at no penalty when blind or in poor lighting up to 5 feet away per skill point you have in Espionage.  

### Cover User Specialties

#### Cover Expert
> Evade +2, Priority +1, Hit Points +5

> Espionage Specialty

> 🧘 Stance (costs 1 AP to enter)

You’re well practiced at taking advantage of whatever cover is available. While you are in this stance, any cover that you take is treated as though it were one level greater. If you exceed heavy cover, you cannot be targeted by a ranged attack.

#### Contort
> Accuracy +1, Evade +2, Hit Points +6

> Espionage Specialty

> ❓ Requires: Cover Expert specialty

> 🪙 Cost: 1 AP

Small barriers can fully eclipse your body as you contort yourself to fit behind them. While in your Cover Expert stance and behind cover, you may contort yourself so that the cover becomes complete cover, and you cannot be targeted by ranged attacks while behind it. You may leave your cover for no action point cost.

        Additionally, when you are in a hiding spot but somebody is actively searching that spot, they do not automatically find you. Instead, they must still roll their Cunning to notice you. If they fail the roll, they do not notice you, even though they were searching your exact area.

### Critical Specialties

#### Critical Hits
> Accuracy +2, Strike +1, Hit Points +5

> Espionage Specialty

Light weapons, while normally less damaging than other weapons, do seem to have a knack for finding soft spots in a target’s defense. When you attack with a light weapon in melee, for every 5 points that your accuracy roll exceeds your target’s evade roll, your attack’s damage class increases by 1. This extra damage class cannot exceed your skill in espionage.

        Thus, if your opponent rolled a 2 on their evade and you rolled a 12, your attack’s damage class would be 2 higher than normal.

#### Hairsplitter
> Accuracy +2, Strike +1, Hit Points +5

> Espionage Specialty

> ❓ Requires: Critical Hits specialty & +6 Accuracy (from specialties)

Your precision and accuracy is not to be out done. When you are making a melee attack with a light weapon, you gain an additional damage class for every 3 points that your accuracy roll exceeds your target’s evade roll. This bonus replaces your bonus from your Critical Hits specialty. This extra damage class cannot exceed your skill in espionage.

#### Pinpoint Shot
> Accuracy +2, Priority +1, Hit Points +6

> Espionage Specialty

> ❓ Requires: Either the Critical Hits or Heartseeker specialty

You’re just as accurate from a range as you are when you’re standing right next to your victim. You may use any specialty that calls for a melee attack with a light weapon when using a light ranged weapon.

### Dirt in the Eyes Specialties

#### Dirt in the Eyes
> Evade +2, Priority +1, Hit Points +7

> Espionage Specialty

> 🪙 Cost: 2 AP

Throwing dirt, blood, water, and other foul substances is an art that has been perfected by dirty fighters for centuries. It’s perfect for catching people off-guard and momentarily blinding them. If you’d like to throw dirt in somebody’s eyes, they must be adjacent to you, and you must succeed at making an accuracy roll against their evade roll. (For all intents and purposes, this is similar to a called shot against the head.) If they have anything protecting their eyes, such as goggles, the target gains a +3 on their evade roll. If you’re successful, the opponent suffers a penalty to their next evade roll equal to your skill in Espionage.

        The target may, if he so chooses, spend 1 action point to rub the substance out before taking the penalty on his next evade.

#### Blind & Swing
> Accuracy +1, Strike +2, Hit Points +6

> Espionage Specialty

> ❓ Requires: Dirt in the Eyes specialty

> 🪙 Cost: Melee Attack +1 AP

You may combine your Dirt in the Eyes attack with a melee attack, more efficiently catching your foes in their crucial moment of weakness. For the cost of a melee attack +1 action point, you may throw dirt in your opponent’s eye and make a melee attack. You resolve your Dirt in the Eyes attack first (which, if successful, gives them a large penalty on their evade roll) and then your attack second.

### Disorienting Specialties

#### Distracting Attack
> Accuracy +2, Priority +1, Hit Points +7

> Espionage Specialty

> ⬇️ Resist: Cunning (tiers down)

> 🪙 Cost: Melee Attack +1 AP

Your attacks bewilder and confuse your opponent. You can make a melee attack that, if successful, disorients the target (causing them to lose 1 action point per turn) for a handful of turns.

|Tier| Effect|
|--|--|
|1|1 turn|
|2|2 turns|
|3|3 turns|
|4|4 turns|

#### Taking Advantage
> Accuracy +1, Strike +2, Hit Points +8

> Espionage Specialty

> ❓ Requires: Distracting Attack specialty

When your opponent is disoriented, you know all the tricks for taking advantage of their momentary weakness. When fighting somebody who is disoriented, it does not cost you the extra action point in order to make called shots against the person.

#### Brain-Blowing Attack
> Accuracy +1, Strike +2, Hit Points +9

> Espionage Specialty

> ❓ Requires: 12 skill points in Espionage & Distracting Attack specialty

Your distracting attacks land beautifully, leaving the target fully disoriented for a considerably longer period of time. Whenever you make a distracting attack, you disorient them for twice as long as usual.

* * *

## Expertise Skill
---------------

Your intelligence stems beyond book smarts into the realm of practical use. Your keen observations coupled with raw brainpower and experience have given you the skills you use on a daily basis. You are a thinker, combining thought with action. Whether it be determining the exact distance of an enemy force or doctoring the wounded, expertise gives you the know-how to do what needs to be done.

#### Appraisal
> Accuracy +1, Evade +1, Hit Points +9

> Expertise Specialty

You’ve got a keen eye for exactly how much something is worth. Though you can figure out the price of most items by looking to the Trust, your skill lies in figuring out the price for ancient prized relics, valuable gemstones, and precious works of art. You may only appraise an item once per downtime.

|Tier| Effect|
|--|--|
|1|You are able to determine the going market value of an item within 10%.|
|2|You are able to determine the market value of an item, almost to the duke.|
|3|Not only can you pinpoint the market value for an item, you can decipher any history or lore about it.|
|4|You have a knack for knowing the exact price for an item, obscure information about it, and whether it contains any ancient secrets or certain people are searching for it|

#### Concentrated Focus
> Evade +1, Defense +3, Hit Points +10

> Expertise Specialty

> ❓ Requires: 3 skill points in Expertise

Your concentration is so intense that it is difficult to stun you. When making a resist against being stunned, you may add your expertise onto the resist. (If the resist is a Cunning roll, add your expertise in addition to the Cunning total.)

#### Deep Breath
> Evade +1, Priority +3, Hit Points +8

> Expertise Specialty

> 🪙 Cost: 1 AP

> ❓ Requires: 5 skill points in Expertise

Taking one deep breath, you look around the battlefield, analyzing your environment. As you exhale, time itself grinds to a halt. You receive 1 extra action point when your action points refresh. You may only perform this once per turn. (Simply put, this specialty lets you spend 1 action point in order to gain an extra action point on your next turn, exceeding your normal maximum pool.)

#### Demoman
> Accuracy +1, Strike +3, Hit Points +8

> Expertise Specialty

> ❓ Requires: 3 skill points in Expertise

You don’t know how they work, but you sure know how to blow them up! For every 3 skill points you have in expertise, you gain a +1 damage class when attacking automatons and vehicles. This applies to anything you use that has a damage class, including explosives.

#### Efficiency Expert
> Priority +3, DIY +3, Hit Points +8

> Expertise Specialty

> ❓ Requires: 3 skill points in Expertise

When you or your friends are crafting items, you’re key to ensuring that no funds are wasted, that no screw is left behind. Not only does your DIY (do-it-yourself) score increase (based on the bonuses granted by this specialty), you improve your allies’ DIY score. When you spend your downtime with your fellow adventurers working on equipment, their DIY score becomes 3 points higher. This bonus cannot increase their effective DIY score beyond 12.

        If you have multiple efficiency experts, the bonus do not stack. Instead, for every additional efficiency expert you have in your party, you gain an additional +1 DIY.

#### Fire Fighter
> Defense +2, Priority +2, Hit Points +12

> Expertise Specialty

You must’ve had a pyromaniac as a friend growing up - you’re able to make battling back blazing infernos seem like child’s play. If you are putting out fires, you gain 2 extra action points per turn for putting out fires. You cannot catch on fire when extinguishing a fire on another creature.

#### Hurl
> Accuracy +1, Strike +3, Hit Points +9

> Expertise Specialty

You can throw just about anything. Even if an item or weapon is not classified as a throwing weapon, you may throw it as if it were. You do not take penalties for impromptu weapons, and it can go the distance it would go for its size categories (light: 25 feet; medium: 75 feet; and, heavy: 50 feet).

#### Improv Fighter
> Accuracy +1, Strike +2, Hit Points +10

> Expertise Specialty

> ❓ Requires: 3 skill points in Expertise

You’re a master of picking up whatever is nearby and using it to its most destructive potential. Barstool, glass, painting, or ladder, you’ve killed with them all. When using an impromptu weapon, your penalties are eliminated. When fighting with such unconventional melee objects, you gain a +1 to accuracy for every 3 points in expertise you have because opponents never expect you to be so talented with them.

#### Mechanic
> Evade +1, Defense +2, Hit Points +8

> Expertise Specialty

> 🪙 Cost: 3 AP

Though you may not be the original inventor, you have a knack for seeing problems and figuring out how to fix them. You may repair an adjacent automaton, vehicle, or any mechanical contraption with wounds or hit points.

|Tier| Effect|
|--|--|
|1|repairs 6 wounds or hit points|
|2|repairs 12 wounds or hit points|
|3|repairs 18 wounds or hit points|
|4|repairs 24 wounds or hit points|

#### Patch the Bleeding
> Defense +2, Priority +2, Hit Points +11

> Expertise Specialty

> 🪙 Cost: 1 AP

You’ve dealt with enough wounds to know how to make them stop bleeding very quickly. You can spend 1 action point to stop 10 bleeding damage (as opposed to the normal 5). In addition, if somebody is bleeding out (such as from losing a limb), for every 1 action point you spend it counts as 3 action points for the purposes of preventing bleeding out.

#### Observance
> Evade +1, Defense +2, Hit Points +10

> Expertise Specialty

> ⬇️ Resist: Cunning (negates)

> 🪙 Cost: 2 AP

You watch the enemy closely, learning his movements and predicting his actions. Once you have “observed” somebody (an act that takes 2 action points), the next attack they make on you is converted into a normal attack unless they can make the resist against your Expertise. If they had applied any specialties to the attack or were making a called shot, all of these additions are negated. They still expend however many action points they would have, however.

        For example, an opponent is about to deliver an attack with a heavy weapon (2 AP), but they upgrade it to a solid strike (+1 AP) and called shot to the head (+1 AP). If they’ve been observed, their solid attack to the head becomes a regular attack with their heavy weapon, but still costs the 4 action points they would have used. If the observed opponent made a normal attack with their heavy weapon as their next attack, the observation would be wasted.

#### Weak Point
> Accuracy +1, Evade +1, Hit Points +8

> Expertise Specialty

> 🪙 Cost: 2 AP

After carefully studying your enemy, you are able to locate its weak point and exploit it. You may spend 2 action points studying a single foe, attempting to locate a weakness.

|Tier| Effect|
|--|--|
|1|You are able to detect any specific weaknesses to called shots the enemy has.|
|2|You are able to determine what called shots the opponent would have the most trouble resisting or called shot weaknesses it has.|
|3|You are able to determine any called shot weaknesses it has, which called shots it would have the most trouble resisting, and, when striking a called shot weakness, the effect of the called shot is doubled.|
|4|You are able to determine any called shot weaknesses it has, which called shots it would have the most trouble resisting, and when striking a called shot weakness, the effect of the called shot is doubled. Furthermore, you are able to determine an immortal creature’s death trigger.|

#### Trick Counter
> Accuracy +1, Evade +1, Hit Points +9

> Expertise Specialty

> ⬇️ Resist: Cunning (negates)

> 🪙 Cost: 1 (or more) AP reflexively

Your enemies think they are upgrading their attacks and coming at you with unique tricks. You see through them. Whenever you are being attacked with a specialized attack or called shot (one that would have a cost of attack +1 AP, or attack +2 AP, et cetera), you may spend an equal amount of action points to the upgrade in order to attempt to negate it. The opponent could then resist with a Cunning roll against your expertise. If they fail, their attack is converted to normal, though the opponent still spends the same amount of action points.

        For example, if you are being attacked by a solid strike (attack +1 AP), you can spend 1 action point in order to attempt to make it a normal attack. If they have added multiple specialties on to the attack, you can choose to negate just a portion of it, or negate all specialties, and the opponent resists against losing each specialty separately.

### Anti-Poison Specialties

#### Poison Finder
> Accuracy +1, Evade +1, Hit Points +11

> Expertise Specialty

Any time a poison comes within 10 feet of you, you may automatically roll your cunning to perceive it (unless you are not conscious or otherwise have your senses dulled), and you add your skill in expertise to the roll.

#### Remove Poison
> Defense +3, Priority +2, Hit Points +10

> Expertise Specialty

> 🪙 Cost: 2 AP

You have a knack for drawing poison out of a victim. Any time that you or an adjacent ally has been poisoned, you may attempt to remove it. If the poison has not activated yet, you can attempt to remove all effects. If it has activated, however, you can only remove lingering effects. Thus, if the poison dealt damage upon activation, you would have to remove the poison prior to activation in order to prevent the damage from being dealt.

        When you remove poison, you are able to negate effects. Once you attempt to remove the poison, you’re able to see what effects the poison is going to have.

|Tier| Effect|
|--|--|
|1|remove 1 effect|
|2|remove 2 effects|
|3|remove 3 effects|
|4|remove 4 effects|

> 🗒️️ Note: Poisons that have the same augment multiple times in order to increase the potency of that augment only count as one effect.

### Combat Insights Specialties

#### Combat Insight
> Evade +1, Priority +2, Hit Points +11

> Expertise Specialty

While others are forced to rely on their brute strength or reflexes to avoid called shots, you see them all coming and react accordingly. You may use your cunning for all called shot resists instead of the normal attribute.

#### Combat Analytics
> Accuracy +1, Evade +1, Hit Points +10

> Expertise Specialty

> ❓ Requires: 11 skill points in Expertise & Combat Insight specialty

> 🪙 Cost: 1 AP reflexively

You’ve been in combat enough times that nothing surprises you anymore. Gone are the days of relying on Brute, Dexterity, or Spirit. Whenever a resist is required of you during combat, you may always use your Cunning by spending 1 action point.

### Item Appropriation Specialties

#### Weapon Appropriations
> Accuracy +1, Evade +1, Hit Points +9

> Expertise Specialty

Though you might not have an armsmith handy, your knack for bargaining and making contacts allows you to get some higher quality weapons than normal. You may add a single weapon augment on to your weapon of choice (be it firearm, crossbow, melee weapon, throwing weapon, or bow). The augment must take up a single slot. The augment begins at marque 1, but increases by marque as though your expertise was determining its marque. (Thus, at 5 skill points in Expertise, you’ll have a marque II augment, 15 skill points will give you a marque III, and 25 skill points a marque IV.)

        You may only have one such augment on a weapon that you carry, though you may select a different augment during every downtime that you have. (The downtime must occur in a location that has commerce and trade, otherwise you wouldn’t be able to procure the augment.) If the augment does not have a marque, you cannot appropriate it until you reach the equivalent cost for the augment.

#### Quality Weapon
> Accuracy +1, Strike +3, Hit Points +7

> Expertise Specialty

> ❓ Requires: Weapon Appropriations specialty

You’re quite good at getting handouts. Your appropriated weapon gains an additional augment (of the same marque). Once you reach 10 skill points in Expertise, you gain a third augment on the weapon. All augments must be on the same weapon.

### Surgery Specialties

#### Field Surgeon
> Priority +2, Defense +2, Hit Points +10

> Expertise Specialty

> ❓ Requires: 4 skill points in Expertise

> 🪙 Cost: 3 AP

The field surgeon patches up major wounds to decrease their allied mortality rate throughout the battlefield. You are that surgeon. You can restore wounds damage, but once you make your attempt, you cannot restore any more wounds damage on a person unless they take more damage to their wounds. Furthermore, you cannot restore more wounds damage than they’ve taken from their most recent attack. Thus, if somebody cut your ally for 3 wounds damage, you could not heal 4 of their wounds.

        The patient must spend 3 action points reflexively in order to be treated. The patient can do this over multiple turns, pulling action points from their next turn if need be. If you are engaged with a melee attacker, this leaves both you and the patient open to reflexive attacks (which they can make for the normal cost of their attack).

|Tier| Effect|
|--|--|
|1|restores 1 wound|
|2|restores 2 wounds|
|3|restores 3 wounds|
|4|restores 4 wounds|

#### First Aid
> Defense +2, Speed +5, Hit Points +9

> Expertise Specialty

> ❓ Requires: Field Surgeon specialty

> 🪙 Cost: Move + Field Surgeon reflexively

When somebody near you takes damage, you’re able to leap to the rescue and help them out. When somebody has taken wounds damage that is within one move’s distance of you, you may reflexively move to them and heal them using your Field Surgeon specialties.

#### Self-Surgery
> Priority +3, Wounds +1, Hit Points +9

> Expertise Specialty

> ❓ Requires: Field Surgeon specialty

Though its difficulty is beyond most people, you’re able to grit through self-surgery. You may now use Field Surgeon on yourself.

* * *

RENAME TO GLAMOUR

## Showmanship Skill
-----------------

You are a performer, the master of showmanship, the troubadour of the world, the man who was born on a stage. You control people’s emotions through your performances, stopping people dead in their tracks with your bravado and encouraging them to do impossible feats through your inspiration. The person with a knack in showmanship is going to be useful no matter where they go, and their allies will never regret having a performer around.

#### Blindside
> Evade +1, Priority +3, Hit Points +8

> Showmanship Specialty

> 🪙 Cost: 2 AP to begin, 1 AP to continue during subsequent turns

Large hand gestures and loud noises keep your opponent focused in a direction of your choosing, allowing you to make them blind to everything in the opposite direction. The target must be within 25 feet of you. The opponent acts blind and deaf toward anything from that direction until something attacks them from that direction, at which time your Blindside is cancelled and must be restarted. You may have blindside activated on multiple people at once, but a single opponent can only be the target of one blindside.

#### Captive Audience
> Accuracy +1, Evade +1, Hit Points +9

> Showmanship Specialty

> 🧘 Stance (costs 1 AP to enter)

> ⬇️ Resist: Cunning (negates)

Once you’ve got someone close to you, you never let them go. When entering this stance, choose a single adjacent target. You cannot move away from the target, but you also prevent the target from moving away from you unless they resist. They can attempt to resist immediately when you enter the stance, and then at the end of their turn, when their action points refresh. If they successfully resist, you exit your stance.

#### Catchphrase
> Accuracy +1, Evade +1, Hit Points +8

> Showmanship Specialty

> 🪙 Cost: 1 AP

You reveal your hidden catchphrase and jump into your next action. Saying your catchphrase requires 1 action point, but the next time you roll a tier result for a specialty, you use your showmanship skill in place of the skill the specialty normally requires.

#### Chime In
> Evade +1, Priority +3, Hit Points +8

> Showmanship Specialty

> ❓ Requires: 2 skill points in Showmanship

By adding in little snippets of information to help an argument, you add a bonus to another player’s cunning roll whenever they are attempting diplomacy, bluff, or any social interaction. The bonus is a +1 for every 2 skill points you have in showmanship.

#### Conveyor
> Evade +1, Priority +2, Hit Points +9

> Showmanship Specialty

> 🪙 Cost: 1 AP reflexively

You can duplicate effects that you see, extending an ally’s buffing range. When an ally creates an effect that affects all allies within a certain range, you can spend 1 action point reflexively in order to also affect all allies within the same range of you.  

#### Deafening Roar
> Strike +2, Defense +2, Hit Points +10

> Showmanship Specialty

> ⬇️ Resist: Brute (tiers down)

> 🪙 Cost: 1 AP

You choose one adjacent opponent and roar so loudly in his ear that his eardrum bursts. The opponent is deafened (suffering a -2 to evade) for a number of turns.

|Tier| Effect|
|--|--|
|1|1 turns|
|2|2 turns|
|3|3 turns|
|4|4 turns|

#### Distract
> Evade +1, Priority +3, Hit Points +7

> Showmanship Specialty

> ⬇️ Resist: Cunning (negates)

> 🪙 Cost: 2 AP reflexively

Sometimes you’re right in front of them. Other times, they’re not so sure. You can distract an opponent during their turn, momentarily thinking that you’re somewhere else. Whenever an opponent takes an action against you, you may attempt to distract them. Choose another location within 10 feet to create your distraction. If the opponent fails their Cunning resist against your showmanship, they - for the purpose of this one action - believe that you are in the location where you caused the distraction. If there is something else there (like a wall or a person), they gain a +10 on the resist.

#### Jester
> Accuracy +1, Evade +1, Hit Points +7

> Showmanship Specialty

> ⬇️ Resist: Cunning (tiers down)

> 🪙 Cost: 2 AP

You can throw your voice, yell, or dance to bewilder opponents. On-looking opponents within 25 feet may become disoriented simply from laughing at you. Jester can affect multiple people, and all affected may attempt to resist. For every tier that they receive above tier 1, they decrease the amount of turns they are disoriented by 1.

|Tier| Effect|
|--|--|
|1|1 target is disoriented for 1 turn|
|2|2 targets are disoriented for 2 turns|
|3|3 targets are disoriented for 3 turns|
|4|4 targets are disoriented for 4 turns|

#### Marionette Strings
> Accuracy +2, Priority +2, Hit Points +6

> Showmanship Specialty

> 🪙 Cost: 1 AP reflexively

You guide the attack of a nearby ally, ensuring their success. Any time an ally makes an attack within 25 feet, you may use your marionette strings to give them a bonus on their accuracy. This can be decided even after the roll has been made.

|Tier| Effect|
|--|--|
|1|+2 on their accuracy roll|
|2|+4 on their accuracy roll|
|3|+6 on their accuracy roll|
|4|+8 on their accuracy roll|

#### Praise
> Accuracy +1, Evade +1, Hit Points +7

> Showmanship Specialty

> 🪙 Cost: 1 AP reflexively

You sing the praises of your fellow adventurers. You may spend 1 action point reflexively to allow a party member to re-roll any resist, as long as they can hear you. You can only do this once per resist.

#### Sleight of Hand
> Evade +2, Priority +2, Hit Points +6

> Showmanship Specialty

When you use an item, one second it’s there, the next it’s gone. Your opponents won’t be able to take advantage of you while you use items. Activating and using items does not leave you open to reflexive attacks.

#### Smoke & Mirrors
> Evade +1, Priority +3, Hit Points +7

> Showmanship Specialty

> ❓ Requires: 3 skill points in Showmanship

> ⬇️ Resist: Dexterity (negates)

> 🪙 Cost: 3 AP reflexively

With the flick of your wrist, you can make an ally within 25 feet appear in one location when they thought they were elsewhere. When an ally has failed an evade roll, you may reflexively move your ally to an adjacent space. The assailant may resist against your showmanship with their Dexterity. If you succeed, your ally may immediately move to an adjacent space, and the assailant’s attack automatically misses.

#### Throw Off Balance
> Evade +1, Priority +2, Hit Points +8

> Showmanship Specialty

> ⬇️ Resist: Cunning (tiers down)

> 🪙 Cost: 1 AP reflexively

Just as a melee attack hits one of your allies within 25 feet, you distract the opponent and cause them to do minimal damage. The attacker must roll their strike multiple times and take the lowest roll, but they can use their Cunning resist in order to lower the result.  

|Tier| Effect|
|--|--|
|1|The attacker rolls two times and takes the lowest|
|2|The attacker rolls three times and takes the lowest|
|3|The attacker rolls four times and takes the lowest|
|4|The attacker rolls five times and takes the lowest|

#### Unmarred Perfection
> Evade +1, Priority +2, Hit Points +8

> Showmanship Specialty

> 🧘 Stance (costs 1 AP to enter)

> ⬇️ Resist: Cunning or Spirit (negates)

Opponents regret attacking you, for fear of harming your perfect image. While in this stance, anybody that attempts to attack you in melee must resist or suffer a penalty to their accuracy equal to your skill in Showmanship. However, while in this stance, you can make only graceful and non-alarming moves, and thus must roll all strike rolls twice and take the lower result.

### Choreographed Specialties

#### Epic Dance
> Evade +2, Priority +1, Hit Points +6

> Showmanship Specialty

> 🪙 Cost: 2 AP to begin, 1 AP to continue during subsequent turns

You bust out your best dance moves, chaining them together in a beautifully unpredictable fashion. While dancing your epic dance, you gain a +3 to evade. You gain an additional point of evade for every 5 skill points you have in Showmanship. You can only have one epic dance going at any given time.

        If tripped, your Epic Dance is cancelled and must be restarted.

#### Never Stop the Dance
> Evade +1, Defense +2, Hit Points +9

> Showmanship Specialty

> ❓ Requires: Epic Dance specialty

While performing your epic dance, you use your choreography to  block and brace your body against incoming blows. You gain an identical bonus to your defense in addition to the evade bonus you receive from your epic dance.

### Epic Music Specialties

#### Battle Theme
> Accuracy +1, Evade +1, Hit Points +7

> Showmanship Specialty

> 🪙 Cost: 2 AP to begin, 1 AP to continue during subsequent turns

Don’t worry, you’ve got this. The epic music in the air says that you can’t fail. While performing your battle theme, you gain a +3 to accuracy rolls. You gain an additional point of accuracy for every 5 skill points you have in showmanship. You can only be singing one battle theme at any given time.

        If you are singing, a successful called shot to your neck will cancel your battle theme. Alternatively, if you are using a musical instrument, a successful sunder or disarm will also cancel your battle theme.

#### Heavenly Serenade
> Evade +1, Priority +2, Hit Points +8

> Showmanship Specialty

> ❓ Requires: Battle Theme specialty

> ⬇️ Resist: Spirit (negates)

> 🪙 Cost: 2 AP reflexively

Your Battle Theme is so beautiful that it digs deep into people’s souls and prevents them from acting. While performing your Battle Theme, any time anybody within 25 feet wants to make an action, you can reflexively spend 2 action points to make them roll a resist against your Showmanship in order to take that action. If they fail the resist, they instead lose 1 action point.

#### Spotlight
> Accuracy +1, Evade +1, Hit Points +9

> Showmanship Specialty

> ❓ Requires: Battle Theme specialty

> 🪙 Cost: 1 AP reflexively

You point to an ally within 25 feet, signaling to them that it’s their time to shine. While performing your Battle Theme, you may spend 1 action point reflexively to grant your Battle Theme accuracy bonus to an ally’s accuracy, evade, strike, or defense roll.

#### Unified Chorus
> Accuracy +1, Strike +3, Hit Points +10

> Showmanship Specialty

> ❓ Requires: Battle Theme specialty & 16 skill points in Showmanship

Your beautiful song causes your allies to lend you their voices! Any bonuses to accuracy or strike you gain from your Battle Theme now apply to all allies within 25 feet of you. If multiple allies have this specialty, everyone only receives bonuses from the one with the highest amount of skill points in Showmanship.

#### Victory Theme
> Accuracy +1, Strike +2, Hit Points +9

> Showmanship Specialty

> ❓ Requires: Battle Theme specialty

You begin to weave a power ballad which drives you forward; your music crying for the utter destruction of your foes. Any bonus to accuracy you receive from your battle theme also acts as a bonus to your strike.

### Smokescreen Specialties

#### Smokescreen
> Evade +1, Priority +3, Hit Points +8

> Showmanship Specialty

> 🪙 Cost: 1 AP

When you need to, you’ve always got a way to throw enemies off for just a second or two. You may toss down a Smokescreen during your turn that lasts until the end of your turn (when your action points refresh). This Smokescreen only envelopes you, but it hides all of your actions while in the Smokescreen. If you move, the Smokescreen does not move with you and instead disperses.

#### Walking Darkness
> Evade +1, Speed +5, Hit Points +7

> Showmanship Specialty

> 🧘 Stance (costs 1 AP to enter)

> ❓ Requires: Smokescreen specialty

More than a simple disappearing act, you walk across the battlefield shrouded by a magician’s smoke cloud. While in this stance, you gain a +4 to evade and all of your actions are considered hidden. If an opponent has some way of knowing where you are that does not rely on sight or can see through perfect darkness, you do not gain the bonus to evade against them.

* * *

## Tactical Skill
--------------

Tactical is the skill of battlefield commanders, great generals, and military leaders. As your party’s tactician, you’re going to keep people safe by making timely decisions to affect the overall flow of the battlefield. The enemies are your pawns, and you move everyone with equal ease. Tactical is about knowing your enemy, focusing your best attacks, and using all of your best advantages to drive your pike into their disadvantages.

#### Ally of the Machine
> Accuracy +1, Evade +1, Hit Points +9

> Tactical Specialty

> ❓ Requires: 6 skill points in Automata

Using your keen sense of machinery and the way automatons function, you can now treat automatons as your allies. When using a specialty that allows you to affect your allies, automatons may now act as allies.  

#### Armistice
> Evade +2, Priority +1, Hit Points +6

> Tactical Specialty

> 🧘 Stance (costs 1 AP to enter)

> ⬇️ Resist: Cunning (negates)

Sometimes the best part of a battle is when you’re not fighting in it. When you enter your armistice stance, opponents who attack you are stunned for 1 action point unless they resist against your Tactical. However, you must exit your Armistice stance (for 0 action points) before attacking, else you will be stunned for 2 action points.

#### Blitzkreig
> Accuracy +1, Speed +5, Hit Points +8

> Tactical Specialty

> 🧘 Stance (costs 1 AP to enter)

You select the next foe and your allies attack. When you enter into this stance, you select a single target within 50 feet. All of your allies gain a speed bonus when moving toward that target. The bonus is 10 feet, plus 5 feet for every 5 skill points you have in Tactical (thus being +15 feet at 5 skill points, +20 feet at 10 skill points, et cetera).

#### Call in a Favor
> Accuracy +1, Evade +1, Hit Points +7

> Tactical Specialty

> ❓ Requires: 4 skill points in Tactical

When you’re cornered, you call in the guy with the sword to get you out of your dangerous situation. You can give an ally 3 of your action points in order for the ally to run over and attack an opponent adjacent to you. The ally must be willing to make the move and the attack. They may only use these action points for moving toward the designated opponent and attacking, though they may use the action points to upgrade the attack as they see fit. In addition, if the ally has any action points of their own, they may choose to use them reflexively to continue attacking or bolster their attack on the opponent adjacent to you.  

#### Change Formation
> Evade +1, Priority +2, Hit Points +7

> Tactical Specialty

> 🪙 Cost: 2 AP

You call for an immediate change of formation. All of your allies within 50 feet are automatically allowed to change their stance, free of cost, at their discretion.

#### Change Places
> Evade +1, Speed +5, Hit Points +6

> Tactical Specialty

> 🪙 Cost: 1 AP

You know where the forces that threaten your team members lie. You can move your ally (as long as your ally is willing).

|Tier| Effect|
|--|--|
|1|Your ally moves 5 feet|
|2|Your ally moves 10 feet|
|3|Your ally moves 15 feet|
|4|Your ally moves 20 feet|

#### Crippling Formation
> Accuracy +1, Strike +2, Hit Points +7

> Tactical Specialty

> ❓ Requires: 3 skill points in Tactical

When an ally makes a called shot within 25 feet of you, you make the shot more difficult to resist. The called shot’s strike roll is effectively 1 point higher for every 3 skill points you have in Tactical. This only changes the resist and does not increase the damage or the effect.

#### Crossfire
> Accuracy +1, Strike +2, Hit Points +8

> Tactical Specialty

> 🧘 Stance (costs 1 AP to enter)

When you enter this stance, choose a single location (a 5 foot square). If anybody enters into that space, you and all of your allies can make reflexive attacks against that person for only 1 action point.  

#### Forewarned
> Accuracy +1, Priority +4, Hit Points +6

> Tactical Specialty

> 🪙 Cost: 1 AP reflexively

When priority is called for at the beginning of a battle, you may give a +3 to an ally’s priority roll within 25 feet, plus an additional +1 per 3 skill points you have in Tactical. Doing so costs 1 action point reflexively (which comes from your first turn’s pool of action points).

#### Focused Support
> Accuracy +1, Strike +2, Hit Points +6

> Tactical Specialty

> ❓ Requires: 7 skill points in Tactical

> 🪙 Cost: 2 AP to begin, 1 AP to continue during subsequent turns

You can build a bond with another that pushes them to greatness. When you begin your focused support, you choose an ally within 25 feet. That ally gains 1 additional action point per turn for as long as you are within 25 feet and continue focusing your support. If you want to change the beneficiary of your focused support, you must begin anew. You can only have one focused support active at any given time.

#### Lead the March
> Speed +5, Priority +2, Hit Points +6

> Tactical Specialty

> 🪙 Cost: 1 AP

When you lead the march, you give your allies within 25 feet a bonus to their speed. This bonus lasts until the end of your next turn (when your action points refresh). This bonus is granted to you as well. Multiple bonuses from Lead the March do not stack.

|Tier| Effect|
|--|--|
|1|Speed increases by 10 feet|
|2|Speed increases by 20 feet|
|3|Speed increases by 30 feet|
|4|Speed increases by 40 feet|

#### Malleable Formation
> Accuracy +1, Speed +5, Hit Points +7

> Tactical Specialty

Your allies can shift and move around the battlefield with ease, keeping your enemies guessing. All allies within 25 feet of you can make a 5 foot movement during their turn for 0 action points.

#### Master Tactician
> Accuracy +1, Priority +3, Hit Points +8

> Tactical Specialty

> ❓ Requires: 15 skill points in Tactical

You’re at your best when thinking on your feet. You receive 1 extra action point per turn which may be used only for reflexes. 

#### Stand-Off
> Evade +1, Priority +2, Hit Points +6

> Tactical Specialty

> 🪙 Cost: 1 AP reflexively

Before anybody takes their first turn in a combat, right as priority is about to be determined, you may call for a stand-off. This costs 1 action point, but puts the entire combat in a stand-off. Anybody may voluntarily take the first action, but - if they do so - every roll they make during their first turn suffers a penalty equal to your skill in tactical.

        If multiple people attempt to act first in a stand-off, they roll priority between themselves. After the first person in a standoff acts, everyone determines priority normally and moves after the stand-off breaker.

### Encouraging Specialties

#### Encouragement
> Evade +1, Speed +5, Hit Points +6

> Tactical Specialty

> 🪙 Cost: 1 AP reflexively

You call out encouragement, urging your allies to a more assured victory. For 1 action point reflexively, you may give an ally within 50 feet a bonus on any one of the following rolls: accuracy, evade, strike, or defense. This bonus must be determined before the roll is made.

|Tier| Effect|
|--|--|
|1|+2|
|2|+4|
|3|+6|
|4|+8|

#### Inspiring Words
> Evade +1, Priority +2, Hit Points +8

> Tactical Specialty

> ❓ Requires: 5 skill points in Tactical & Encouragement specialty

Your words of encouragement invoke a deep drive within your allies that brings about confidence, allowing them to continue battling. When using Encouragement, all of your within 50 feet  recover 1 hit point plus 1 for every 10 skill points you have in Tactical.

### Flow of Battle Specialties

#### Direct the Battle
> Accuracy +1, Evade +1, Hit Points +8

> Tactical Specialty

> 🧘 Stance (costs 1 AP to enter)

You set the course of battle, proclaiming the next victim of the tide. When you enter this stance, you choose one enemy within 50 feet to be the victim. While you are in this stance, you can allow a single ally per turn to make a special attack or called shot that costs “Attack +1 AP,” without spending the extra action point, so long as it is directed at the target of the stance.  

#### Concentrated Barrage
> Accuracy +1, Strike +2, Hit Points +7

> Tactical Specialty

> ❓ Requires: Direct the Battle specialty

When you enter into your direct the battle stance, you point at a single target and exclaim, “Get that fool!” Every ally within 25 feet can make a special attack or called shot that costs “Attack +1 AP” against the target of your direct the battle stance without spending that extra action point, though each ally only gets this benefit once per turn.  

#### Overwhelm
> Accuracy +1, Evade +1, Hit Points +9

> Tactical Specialty

> ❓ Requires: Direct the Battle specialty

> 🪙 Cost: 1 AP reflexively

Any time the target of your direct the battle stance takes damage from an ally, you may use 1 action point to encourage your ally’s attack along.

|Tier| Effect|
|--|--|
|1|Ally’s attack does an additional 3 damage|
|2|Ally’s attack does an additional 6 damage|
|3|Ally’s attack does an additional 9 damage|
|4|Ally’s attack does an additional 12 damage|

### Order Specialties

#### Issue Orders
> Accuracy +1, Evade +1, Hit Points +7

> Tactical Specialty

> 🪙 Cost: 3 AP

You yell an order across the battlefield, and one of your allies answers the call. The ally must be within 50 feet and be able to hear you. Your order allows an ally to make a called shot using their bonuses but without spending any of their action points.

|Tier| Effect|
|--|--|
|1|Ally attempts the called shot|
|2|Ally attempts the called shot with a +2 on the strike|
|3|Ally attempts the called shot with a +4 on the strike|
|4|Ally attempts the called shot with a +6 on the strike|

#### Complex Orders
> Accuracy +1, Priority +3, Hit Points +8

> Tactical Specialty

> ❓ Requires: Issue Orders specialty & 5 skill points in Tactical

When giving an ally a called shot order, you may spend the cost of one of your ally’s attack-modifying specialties to allow that ally to use that attack modifier for free.

        For instance, if your ally knows Solid Assault (under Overpower) you may spend an extra action point in addition to the Issue Orders cost in order to allow your ally to use their attack modifier for free. This additional action point expenditure can utilize the action point bonus from Direct the Battle.

#### Improved Orders
> Accuracy +1, Evade +1, Hit Points +8

> Tactical Specialty

> ❓ Requires: Issue Orders specialty, Complex Orders specialty, & 8 skill points in Tactical

You are becoming more adept at giving orders to your allies, allowing you 1 free action point per turn for use with the effects of Complex Orders.

