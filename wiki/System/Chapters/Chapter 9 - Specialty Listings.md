# Chapter 9 - Specialty Listings
==========

Specialty Listings
------------------

Brawl (Brute)

#### Block with a Grab        Accuracy +1, Evade +1, Hit Points +10

#### Dirty Fighting        Accuracy +1, Strike +2, Hit Points +9

#### Drunken Boxing        Accuracy +1, Evade +1, Hit Points +9

#### Fisticuffs        Strike +2, Priority +1, Hit Points +10

#### Fluid        Evade +2, Strike +1, Hit Points +7

#### Grapple        Accuracy +2, Strike +1, Hit Points +10

#### Heavy-Handed        Strike +2, Priority +2, Hit Points +9

#### Hold Steady        Accuracy +1, Evade +1, Hit Points +10

#### Knock Aside        Accuracy +1, Evade +1, Hit Points +9

#### Monkey Wrestler        Accuracy +1, Strike +1, Hit Points +10

#### Reversal        Accuracy +1, Evade +1, Hit Points +10

#### Shrug Away        Evade +1, Priority +2, Hit Points +10

#### Throat Jab        Strike +2, Priority +2, Hit Points +11

Bone-Breaking Specialties

#### Bone-Breaker        Strike +2, Priority +1, Hit Points +11

#### Crippling Blow        Strike +3, Priority +1, Hit Points +9

Combo Specialties

#### Combo Flow        Accuracy +1, Strike +2, Hit Points +10

#### Combo Opener        Accuracy +1, Strike +2, Hit Points +10

#### Combo Breaker        Accuracy +1, Evade +1, Hit Points +9

#### Finisher        Strike +3, Priority +1, Hit Points +9

Grip Specialties

#### Crushing Grip        Strike +3, Priority +1, Hit Points +10

#### Twist        Accuracy +1, Strike +2, Hit Points +10

Frenzy (Brute)

#### Adrenaline Surge        Evade +1, Strike +1, Hit Points +14

#### Backlash        Accuracy +1, Strike +2, Hit Points +11

#### Burning Revenge        Strike +3, Priority +1, Hit Points +11

#### Carry Through        Accuracy +1, Strike +2, Hit Points +10

#### Crimson Weapon        Strike +3, Priority +1, Hit Points +10

#### Fray Fighter        Evade +1, Strike +2, Hit Points +10

#### Hundred Strikes        Accuracy +1, Strike +2, Hit Points +10

#### Liberator        Evade +2, Strike +2, Hit Points +10

#### Merciless        Accuracy +1, Strike +2, Hit Points +8

#### Neverending Bloodbath        Strike +2, Speed +5, Hit Points +11

#### No Escape        Strike +2, Speed +5, Hit Points +10

#### Raging        Strike +2, Priority +2, Hit Points +9

#### Straining Blow        Accuracy +1, Strike +2, Hit Points +13

#### Soulless Blade        Accuracy +1, Strike +2, Hit Points +9

#### Walking Destruction        Strike +3, Speed +5, Hit Points +10

Bloodlust Specialties

#### Berserker        Accuracy +1, Strike +2, Hit Points +12

#### Bloodlust        Accuracy +1, Strike +2, Hit Points +10

#### Unquenchable Thirst        Accuracy +1, Strike +3, Hit Points +11

Masochistic Specialties

#### Laugh Like You’re Crazy        Strike +2, Priority +2, Hit Points +10

#### Marriage to Suffering        Strike +2, Wounds +1, Hit Points +11

#### Seize Your Suffering        Strike +2, Wounds +2, Hit Points +11

Overpower (Brute)

#### Brickbreaker        Strike +3, Priority +1, Hit Points +10

#### Dragging        Accuracy +1, Strike +2, Hit Points +11

#### Follow-Through        Accuracy +1, Strike +2, Hit Points +10

#### Heavy Hitter        Accuracy +1, Strike +3, Hit Points +9

#### Keep Them Down        Accuracy +1, Strike +2, Hit Points +11

#### Monstrous Attacks        Strike +3, Defense +1, Hit Points +10

#### No Quarter        Strike +3, Priority +1, Hit Points +11

#### One-Handing It        Accuracy +1, Strike +2, Hit Points +10

#### Robust Toss        Accuracy +1, Strike +2, Hit Points +11

#### Shield Whack        Accuracy +1, Strike +3, Hit Points +7

#### Solid Assault        Strike +3, Priority +1, Hit Points +11

#### Stunning Blow        Accuracy +1, Strike +2, Hit Points +9

#### Titanic Strength        Accuracy +1, Strike +3, Hit Points +8

#### With Gusto        Strike +3, Priority +1, Hit Points +10

Armor-Breaking Specialties

#### Chipping Away        Accuracy +1, Strike +2, Hit Points +10

#### Armor Sunder        Accuracy +1, Strike +2, Hit Points +9

Earth-Shattering Specialties

#### Earthquaking Strike        Accuracy +1, Strike +2, Hit Points +11

#### Rampant Destruction        Strike +3, Priority +1, Hit Points +11

Push Away Specialties

#### Staggering Strike        Accuracy +1, Strike +2, Hit Points +9

#### Bullrush        Strike +2, Speed +5, Hit Points +11

Resilience (Brute)

#### Blast Proof        Evade +1, Defense +2, Hit Points +14

#### Body of Steel        Defense +3, Wounds +1, Hit Points +14

#### Brace for Impact        Accuracy +1, Defense +3, Hit Points +15

#### Bulwark        Evade +1, Defense +2, Hit Points +13

#### Interposition        Defense +2, Speed +5, Hit Points +13

#### Metal Embrace        Evade +1, Defense +3, Hit Points +15

#### Never Off-Guard        Evade +1, Priority +3, Hit Points +17

#### Press        Strike +2, Defense +2, Hit Points +11

#### Protector        Evade +1, Defense +3, Hit Points +14

#### Resolute        Defense +1, Wounds +1, Hit Points +13

#### Second Skin        Evade +1, Defense +2, Hit Points +14

#### Solid Stances        Evade +1, Defense +3, Hit Points +13

#### Thick Skin        Defense +3, Wounds +1, Hit Points +11

#### Tough Stuff        Defense +2, Wounds +1, Hit Points +19

#### Unassailable Mountain        Accuracy +1, Defense +3, Hit Points +12

#### Walking Fortress        Defense +2, Wounds +1, Hit Points +13

#### Ward        Strike +2, Defense +2, Hit Points +11

Armored Movement Specialties

#### Armored Ease        Evade +1, Defense +3, Hit Points +12

#### Armored Freedom        Defense +2, Speed +5, Hit Points +14

Barrier Specialties

#### Living Barrier        Defense +3, Priority +1, Hit Points +12

#### Living Wall        Defense +2, Wounds +1, Hit Points +14

#### Living Stronghold        Defense +2, Wounds +1, Hit Points +15

Espionage (Cunning)

#### Destabilizing Strike        Accuracy +1, Strike +2, Hit Points +7

#### Feign Fatal Wounds        Accuracy +1, Evade +2, Hit Points +7

#### First Strike        Accuracy +1, Priority +4, Hit Points +5

#### Flowing Shadow        Accuracy +1, Evade +1, Hit Points +6

#### Heartseeker        Accuracy +1, Strike +2, Hit Points +6

#### Invisible Blade        Accuracy +1, Strike +1, Hit Points +7

#### Master Lockpick        Evade +1, Priority +3, Hit Points +7

#### Pierce the Darkness        Accuracy +2, Priority +1, Hit Points +7

#### Silent Kill        Accuracy +1, Evade +2, Hit Points +6

#### Sinister Strike        Accuracy +1, Strike +3, Hit Points +7

Nightwalker Specialties

#### Fighting Blind        Accuracy +2, Strike +1, Hit Points +6

#### Deep Blind Senses        Accuracy +1, Evade +1, Hit Points +6

Cover User Specialties

#### Cover Expert        Evade +2, Priority +1, Hit Points +5

#### Contort        Accuracy +1, Evade +2, Hit Points +6

Critical Specialties

#### Critical Hits        Accuracy +2, Strike +1, Hit Points +5

#### Hairsplitter        Accuracy +2, Strike +1, Hit Points +5

#### Pinpoint Shot        Accuracy +2, Priority +1, Hit Points +6

Dirt in the Eyes Specialties

#### Dirt in the Eyes        Evade +2, Priority +1, Hit Points +7

#### Blind & Swing        Accuracy +1, Strike +2, Hit Points +6

Disorienting Specialties

#### Distracting Attack        Accuracy +2, Priority +1, Hit Points +7

#### Taking Advantage        Accuracy +1, Strike +2, Hit Points +8

#### Brain-Blowing Attack        Accuracy +1, Strike +2, Hit Points +9

Expertise (Cunning)

#### Appraisal        Accuracy +1, Evade +1, Hit Points +9

#### Concentrated Focus        Evade +1, Defense +3, Hit Points +10

#### Deep Breath        Evade +1, Priority +3, Hit Points +8

#### Demoman        Accuracy +1, Strike +3, Hit Points +8

#### Efficiency Expert        Priority +3, DIY +3, Hit Points +8

#### Fire Fighter        Defense +2, Priority +2, Hit Points +12

#### Hurl        Accuracy +1, Strike +3, Hit Points +9

#### Improv Fighter        Accuracy +1, Strike +2, Hit Points +10

#### Mechanic        Evade +1, Defense +2, Hit Points +8

#### Patch the Bleeding        Defense +2, Priority +2, Hit Points +11

#### Observance        Evade +1, Defense +2, Hit Points +10

#### Weak Point        Accuracy +1, Evade +1, Hit Points +8

#### Trick Counter        Accuracy +1, Evade +1, Hit Points +9

Anti-Poison Specialties

#### Poison Finder        Accuracy +1, Evade +1, Hit Points +11

#### Remove Poison        Defense +3, Priority +2, Hit Points +10

Combat Insights Specialties

#### Combat Insight        Evade +1, Priority +2, Hit Points +11

#### Combat Analytics        Accuracy +1, Evade +1, Hit Points +10

Item Appropriation Specialties

#### Weapon Appropriations        Accuracy +1, Evade +1, Hit Points +9

#### Quality Weapon        Accuracy +1, Strike +3, Hit Points +7

Surgery Specialties

#### Field Surgeon        Priority +2, Defense +2, Hit Points +10

#### First Aid        Defense +2, Speed +5, Hit Points +9

#### Self-Surgery        Priority +3, Wounds +1, Hit Points +9

Showmanship (Cunning)

#### Blindside        Evade +1, Priority +3, Hit Points +8

#### Captive Audience        Accuracy +1, Evade +1, Hit Points +9

#### Catchphrase        Accuracy +1, Evade +1, Hit Points +8

#### Chime In        Evade +1, Priority +3, Hit Points +8

#### Conveyor        Evade +1, Priority +2, Hit Points +9

#### Deafening Roar        Strike +2, Defense +2, Hit Points +10

#### Distract        Evade +1, Priority +3, Hit Points +7

#### Jester        Accuracy +1, Evade +1, Hit Points +7

#### Marionette Strings        Accuracy +2, Priority +2, Hit Points +6

#### Praise        Accuracy +1, Evade +1, Hit Points +7

#### Sleight of Hand        Evade +2, Priority +2, Hit Points +6

#### Smoke & Mirrors        Evade +1, Priority +3, Hit Points +7

#### Throw Off Balance        Evade +1, Priority +2, Hit Points +8

#### Unmarred Perfection        Evade +1, Priority +2, Hit Points +8

Choreographed Specialties

#### Epic Dance        Evade +2, Priority +1, Hit Points +6

#### Never Stop the Dance        Evade +1, Defense +2, Hit Points +9

Epic Music Specialties

#### Battle Theme        Accuracy +1, Evade +1, Hit Points +7

#### Heavenly Serenade        Evade +1, Priority +2, Hit Points +8

#### Spotlight        Accuracy +1, Evade +1, Hit Points +9

#### Unified Chorus        Accuracy +1, Strike +3, Hit Points +10

#### Victory Theme        Accuracy +1, Strike +2, Hit Points +9

Smokescreen Specialties

#### Smokescreen        Evade +1, Priority +3, Hit Points +8

#### Walking Darkness        Evade +1, Speed +5, Hit Points +7

Tactical (Cunning)

#### Ally of the Machine        Accuracy +1, Evade +1, Hit Points +9

#### Armistice        Evade +2, Priority +1, Hit Points +6

#### Blitzkreig        Accuracy +1, Speed +5, Hit Points +8

#### Call in a Favor        Accuracy +1, Evade +1, Hit Points +7

#### Change Formation        Evade +1, Priority +2, Hit Points +7

#### Change Places        Evade +1, Speed +5, Hit Points +6

#### Crippling Formation        Accuracy +1, Strike +2, Hit Points +7

#### Crossfire        Accuracy +1, Strike +2, Hit Points +8

#### Forewarned        Accuracy +1, Priority +4, Hit Points +6

#### Focused Support        Accuracy +1, Strike +2, Hit Points +6

#### Lead the March        Speed +5, Priority +2, Hit Points +6

#### Malleable Formation        Accuracy +1, Speed +5, Hit Points +7

#### Master Tactician        Accuracy +1, Priority +3, Hit Points +8

#### Stand-Off        Evade +1, Priority +2, Hit Points +6

Encouraging Specialties

#### Encouragement        Evade +1, Speed +5, Hit Points +6

#### Inspiring Words        Evade +1, Priority +2, Hit Points +8

Flow of Battle Specialties

#### Direct the Battle        Accuracy +1, Evade +1, Hit Points +8

#### Concentrated Barrage        Accuracy +1, Strike +2, Hit Points +7

#### Overwhelm        Accuracy +1, Evade +1, Hit Points +9

Order Specialties

#### Issue Orders        Accuracy +1, Evade +1, Hit Points +7

#### Complex Orders        Accuracy +1, Priority +3, Hit Points +8

#### Improved Orders        Accuracy +1, Evade +1, Hit Points +8

Ace (Dexterity)

#### Backseat Driver        Accuracy +1, Evade +1, Hit Points +9

#### Co-Pilot        Accuracy +1, Evade +3, Hit Points +8

#### Crash Maneuver        Accuracy +1, Defense +3, Hit Points +9

#### Denial Maneuver        Evade +2, Priority +1, Hit Points +7

#### Driving with Knees        Accuracy +1, Evade +1, Hit Points +8

#### Flying Fortress        Accuracy +1, Defense +3, Hit Points +7

#### Hold Together        Evade +1, Defense +3, Hit Points +8

#### Horseman’s Cut        Accuracy +1, Strike +3, Hit Points +7

#### Hostile Maneuvers        Accuracy +1, Strike +2, Hit Points +9

#### Level Flying        Accuracy +1, Priority +3, Hit Points +8

#### Quick-Mount        Accuracy +2, Priority +1, Hit Points +6

#### Vehicular Teamwork        Accuracy +1, Evade +1, Hit Points +10

Auto Piloting Specialties

#### Strafe        Accuracy +1, Evade +1, Hit Points +9

#### Evasive Strafe        Evade +1, Defense +2, Hit Points +7

Clanker Piloting Specialties

#### Extension of Self        Accuracy +1, Evade +1, Hit Points +9

#### Piston-Spring        Evade +1, Defense +2, Hit Points +10

Focusing Flying Specialties

#### Focused Flying        Accuracy +1, Defense +3, Hit Points +8

#### Fine-Tuned Flying        Defense +3, Priority +2, Hit Points +7

#### Fully Focused Flying        Accuracy +1, Evade +1, Hit Points +8

Mounted Cavalry Specialties

#### Fearless Mount        Accuracy +1, Priority +3, Hit Points +8

#### One with the Beast        Strike +2, Defense +2, Hit Points +10

Ramming Specialties

#### Ram        Accuracy +1, Strike +3, Hit Points +7

#### Puncture        Accuracy +1, Defense +3, Hit Points +9

Agility (Dexterity)

#### Battlefield Flow        Evade +1, Priority +2, Hit Points +7

#### Bounding Lunge        Accuracy +1, Speed +5, Hit Points +7

#### Charging Ram        Strike +2, Speed +5, Hit Points +6

#### Free Movement        Priority +2, Speed +5, Hit Points +6

#### Groundfighting        Evade +2, Priority +2, Hit Points +7

#### Instant Draw        Accuracy +1, Priority +3, Hit Points +7

#### Slow Falling        Accuracy +1, Evade +2, Hit Points +6

#### Side-Swipe        Accuracy +1, Evade +1, Hit Points +9

#### Slipstreaming        Accuracy +1, Speed +5, Hit Points +8

#### Snake Bite        Accuracy +1, Priority +3, Hit Points +7

#### Step Back        Accuracy +1, Evade +1, Hit Points +7

#### Terrain Mastery        Priority +4, Speed +5, Hit Points +9

#### Wall Runner        Accuracy +1, Speed +5, Hit Points +8

#### Walk Over        Evade +1, Speed +5, Hit Points +7

Explosion Dodging Specialties

#### Blast Dodger        Evade +1, Speed +5, Hit Points +8

#### Soaring Dodge        Evade +1, Priority +3, Hit Points +9

Phasing Specialties

#### Phase Step        Evade +1, Speed +5, Hit Points +7

#### Fleeting Shade        Evade +1, Speed +5, Hit Points +6

#### Leave No Trace        Evade +1, Speed +5, Hit Points +6

Stance-Shifting Specialties

#### Shifting        Accuracy +1, Evade +1, Hit Points +7

#### Freeform Shifting        Accuracy +1, Evade +1, Hit Points +8

Marksmanship (Dexterity)

#### Aim        Accuracy +2, Evade +1, Hit Points +5

#### Cover Fire        Accuracy +1, Evade +2, Hit Points +6

#### Follow Up        Accuracy +1, Evade +1, Hit Points +7

#### Head Popper        Accuracy +1, Evade +1, Hit Points +7

#### Itchy Trigger Finger        Evade +2, Priority +3, Hit Points +8

#### Knock-Off        Accuracy +2, Priority +2, Hit Points +6

#### Lockdown Gunner        Accuracy +1, Priority +3, Hit Points +8

#### Long Shot        Accuracy +2, Priority +1, Hit Points +7

#### Penetrating Shot        Accuracy +1, Evade +1, Hit Points +8

#### Point Blank        Accuracy +1, Evade +1, Hit Points +7

#### Seeker        Accuracy +2, Priority +1, Hit Points +7

#### Snap Reload        Accuracy +1, Priority +4, Hit Points +6

#### Sneaky Seconds        Accuracy +1, Evade +1, Hit Points +7

#### Stable Shot        Accuracy +1, Defense +2, Hit Points +8

#### Turret        Accuracy +1, Evade +1, Hit Points +5

#### Warning Shot        Accuracy +2, Priority +1, Hit Points +7

#### Wing Clipping        Accuracy +2, Evade +1, Hit Points +6

Archery Specialties

#### Arching Shot        Accuracy +1, Strike +3, Hit Points +7

#### Efficient Ranger        Accuracy +1, Strike +2, Hit Points +7

#### Flight of Arrows        Accuracy +1, Strike +3, Hit Points +8

Bleeding Arrow Specialties

#### Flesh Biter        Accuracy +1, Strike +2, Hit Points +7

#### Flesh Piercing        Accuracy +1, Strike +3, Hit Points +6

Swashbuckling (Dexterity)

#### Adaptable        Accuracy +1, Evade +1, Hit Points +7

#### Circle Attack        Accuracy +1, Strike +2, Hit Points +8

#### Counter-Stance        Accuracy +2, Strike +1, Hit Points +6

#### Efficient Strike        Accuracy +1, Priority +3, Hit Points +8

#### Fight Anywhere        Evade +2, Speed +5, Hit Points +6

#### Hilt Bash        Accuracy +1, Strike +2, Hit Points +8

#### Opening        Accuracy +1, Priority +2, Hit Points +6

#### Precise Attack        Accuracy +1, Strike +2, Hit Points +8

#### Saluted Opponent        Accuracy +1, Evade +1, Hit Points +6

#### Sword and Board        Accuracy +1, Evade +1 , Hit Points +8

#### Wild Slash        Strike +2, Priority +3, Hit Points +8

En-Garde Specialties

#### En-Garde        Accuracy +1, Evade +1, Hit Points +8

#### Find the Gap        Accuracy +1, Strike +2, Hit Points +8

#### Lightning Slash        Accuracy +2, Priority +2, Hit Points +8

Flickering Specialties

#### Flickering        Accuracy +1, Evade +1, Hit Points +7

#### Torrent of Steel        Accuracy +1, Priority +2, Hit Points +7

Footwork Specialties

#### Footwork Training        Priority +3, Speed +5, Hit Points +8

#### Fancy Footwork        Evade +1, Speed +5, Hit Points +9

Parry & Riposte Specialties

#### Parry        Accuracy +1, Strike +2, Hit Points +9

#### Beat Parry        Accuracy +1, Strike +3, Hit Points +9

#### Distance Parry        Accuracy +1, Speed +5, Hit Points +8

#### Experienced Parries        Accuracy +1, Strike +3, Hit Points +8

#### Riposte        Accuracy +1, Strike +2, Hit Points +8

Faith (Spirit)

#### Blind Faith        Evade +1, Priority +2, Hit Points +8

#### Conviction        Accuracy +1, Strike +3, Hit Points +8

#### Divine Guidance        Strike +2, Defense +2, Hit Points +9

#### Flowing Vigor        Evade +1, Defense +2, Hit Points +9

#### Grief & Hope        Evade +1, Priority +2, Hit Points +8

#### Healing Halo        Evade +1, Defense +2, Hit Points +10

#### Infallible Faith        Evade +1, Defense +2, Hit Points +8

#### Moral Support        Accuracy +1, Evade +1, Hit Points +7

#### Prayer        Accuracy +1, Defense +2, Hit Points +9

#### Purify        Evade +1, Priority +2, Hit Points +8

#### Shock of Life        Evade +1, Defense +3, Hit Points +11

AP Sacrifice Upgrade Specialties

#### Devoted Peers        Evade +1, Defense +2, Hit Points +10

#### Self-Sacrifice        Evade +1, Strike +2, Hit Points +8

#### Silent Devotion        Evade +2, Priority +1, Hit Points +7

Champion Specialties

#### Appointed Champion        Evade +1, Defense +2, Hit Points +9

#### Conduit of Faith        Accuracy +1, Evade +1, Hit Points +8

Inquisition Specialties

#### Proclaim the Heretic        Evade +1, Strike +2, Hit Points +8

#### Light in the Dark        Accuracy +1, Evade +1, Hit Points +8

Smiting Specialties

#### Smite        Accuracy +1, Strike +1, Hit Points +8

#### Assured Success        Accuracy +2, Priority +1, Hit Points +6

#### Impassioned Victory        Accuracy +1, Strike +3, Hit Points +7

#### Smiting Shot        Accuracy +1, Priority +2, Hit Points +8

#### Zealous Smite        Accuracy +1, Strike +2, Hit Points +7

Grace (Spirit)

#### Bloodsoak        Evade +1, Defense +2, Hit Points +9

#### Connection        Accuracy +2, Priority +1, Hit Points +7

#### Danger Sense        Evade +1, Priority +5, Hit Points +9

#### Destabilize        Accuracy +1, Strike +2, Hit Points +8

#### Dispel Pain        Evade +1, Defense +3, Hit Points +9

#### Force of Self        Accuracy +1, Evade +1, Hit Points +7

#### Inner Calm        Evade +1, Priority +2, Hit Points +10

#### Iron Palm        Accuracy +1, Strike +2, Hit Points +10

#### Master of Forms        Accuracy +1, Evade +1, Hit Points +8

#### Parting Waves        Evade +1, Priority +2, Hit Points +8

#### Shocking Soul        Accuracy +1, Strike +2, Hit Points +8

#### Spirit Break        Accuracy +1, Evade +1, Hit Points +8

#### Spiritual Seal        Accuracy +1, Evade +1, Hit Points +9

#### Void Strike        Accuracy +1, Strike +2, Hit Points +10

Ki-Unleashing Specialties

#### Ki Flow        Evade +1, Strike +1, Hit Points +10

#### Ki Rage        Evade +1, Strike +2, Hit Points +8

Light-as-Air Specialties

#### Feather in the Wind        Evade +1, Speed +5, Hit Points +6

#### Weightless        Accuracy +1, Evade +1, Hit Points +8

Paralyzing Specialties

#### Touch of Paralysis        Accuracy +1, Strike +2, Hit Points +7

#### Blocked Ki        Accuracy +1, Strike +2, Hit Points +8

Luck (Spirit)

#### Confident in your Luck        Accuracy +1, Evade +1, Hit Points +7

#### Don’t Tell Me the Odds        Evade +2, Priority +2, Hit Points +6

#### Cheat Fate        Accuracy +2, Evade +1, Hit Points +6

#### Equalizing Force        Accuracy +1, Evade +1, Hit Points +6

#### Hex        Evade +1, Priority +3, Hit Points +7

#### Jackpot        Accuracy +1, Strike +3, Hit Points +8

#### Jinx        Accuracy +1, Strike +2, Hit Points +9

#### Roll of the Dice        Accuracy +1, Evade +2, Hit Points +8

#### Roulette        Accuracy +2, Priority +1, Hit Points +6

#### Spot of Misfortune        Accuracy +1, Evade +1, Hit Points +7

Failure Avoidance Specialties

#### Free from Failure        Evade +1, Priority +2, Hit Points +7

#### Steady Friends        Accuracy +1, Evade +1, Hit Points +8

Foul Luck Specialties

#### Curse        Evade +1, Defense +2, Hit Points +9

#### Fumble        Accuracy +1, Evade +1, Hit Points +8

Luck Holder Specialties

#### Ace Up My Sleeve        Evade +1, Priority +2, Hit Points +6

#### Leading the Lucky Life        Accuracy +1, Evade +1, Hit Points +9

#### Second Chance        Evade +1, Wounds +1, Hit Points +10

Lucky #7 Specialties

#### Lucky Number 7        Evade +1, Priority +2, Hit Points +6

#### Luckier Number 7        Evade +1, Priority +2, Hit Points +7

Ranged Evading Specialties

#### Feeling Lucky        Evade +2, Priority +1, Hit Points +6

#### Unfriendly Fire        Accuracy +1, Evade +2, Hit Points +5

#### Unfriendly Artillery        Accuracy +1, Evade +1, Hit Points +7

Shamanism (Spirit)

#### Control Beast        Accuracy +1, Evade +1, Hit Points +8

#### Druidic        Accuracy +1, Strike +2, Hit Points +8

#### Fire Resistance        Evade +1, Defense +3, Hit Points +10

#### Geomancer        Accuracy +1, Evade +1, Hit Points +7

#### Hardened Trainer        Accuracy +1, Defense +2, Hit Points +10

#### Lion’s Roar        Strike +2, Priority +2, Hit Points +9

#### Naturalist        Evade +1, Defense +2, Hit Points +9

#### Parasite        Strike +2, Priority +2, Hit Points +7

#### Still as Stone        Accuracy +1, Evade +1, Hit Points +6

#### Tactics of the Wolf        Accuracy +1, Strike +3, Hit Points +8

#### Topographer        Evade +1, Priority +2, Hit Points +7

Bird Calling Specialties

#### Avian Wrath        Accuracy +1, Evade +1, Hit Points +7

#### Blacken the Sky        Accuracy +1, Evade +1, Hit Points +8

#### Pitch Black        Accuracy +1, Evade +1, Hit Points +8

Chemical Immunity Specialties

#### Venom Immunity        Evade +1, Defense +2, Hit Points +10

#### Alchemical Resistance        Evade +1, Defense +3, Hit Points +9

Protective Swarm Specialties

#### Protect the Monarch        Defense +2, Priority +1, Hit Points +9

#### Devoted Drones        Defense +3, Priority +1, Hit Points +10

#### Hive Exodus        Defense +2, Speed +5, Hit Points +7

Swarming Insect Specialties

#### Colony of One        Accuracy +1, Strike +1, Hit Points +8

#### Drag Down        Accuracy +1, Strike +1, Hit Points +8

#### Hive Mind        Accuracy +1, Evade +1, Hit Points +9

#### Pressure Cooker        Accuracy +1, Strike +2, Hit Points +9

General Science Specialties

#### Learn Augments        Augments +4, DIY +1, Hit Points +4

#### Nothing up my Sleeve        Accuracy +1, Augments +2, Hit Points +4

Alchemy (Sciences)

Immunity Specialties

#### Self-Made Immunity        Defense +2, Augments +1, Hit Points +8

#### Immunizations        Defense +2, Augments +1, Hit Points +6

On-the-Fly Specialties

#### On-the-Fly Brewer        Evade +1, Augments +2, Hit Points +4

#### Expiration        Evade +1, Augments +2, Hit Points +4

#### Herbalist        Augments +2, DIY +1, Hit Points +5

#### Rapid Mixer        Evade +1, Augments +1, Hit Points +5

#### Walking Chemical Plant        Evade +1, Augments +1, Hit Points +5

### Crafting Acids

#### Acid Brewer        Augments +2, DIY +1, Hit Points +5

#### Beta Acids        Augments +2, DIY +1, Hit Points +5

#### Prototype Acids        Augments +1, DIY +1, Hit Points +6

### Crafting Gases

#### Gas Brewer        Augments +2, DIY +1, Augments +4

#### Beta Gases        Augments +2, DIY +1, Hit Points +4

#### Prototype Gases        Augments +1, DIY +1, Hit Points +4

### Crafting Medicines

#### Medicine Brewer        Augments +2, DIY +1, Hit Points +5

#### Beta Medicines        Augments +2, DIY +1, Hit Points +6

#### Prototype Medicines        Augments +1, DIY +1, Hit Points +7

### Crafting Poisons

#### Poison Brewer        Augments +2, DIY +1, Hit Points +4

#### Beta Poisons        Augments +2, DIY +1, Hit Points +4

#### Prototype Poisons        Augments +1, DIY +1, Hit Points +5

Armsmith (Sciences)

#### Belt Feeder        Evade +2, Augments +1, Hit Points +7

#### Interchangeable Parts        Priority +2, Augments +1, Hit Points +5

#### Rapid Replacements        Evade +1, DIY +1, Hit Points +6

#### Temporary Attachments        Augments +2, DIY +1, Hit Points +5

#### Weapon Support        Accuracy +1, Augments +1, Hit Points +6

### Crafting Firearms & Crossbows

#### Gunsmith        Augments +2, DIY +1, Hit Points +4

#### Beta Firearms        Augments +2, DIY +1, Hit Points +4

#### Prototype Firearms        Augments +1, DIY +1, Hit Points +6

#### Crossbow Craftsman        Augments +2, DIY +1, Hit Points +4

#### Beta Crossbows        Augments +2, DIY +1, Hit Points +4

#### Prototype Crossbows        Augments +1, DIY +1, Hit Points +6

### Crafting Melee Weapons & Throwing Weapons

#### Weapon Smith        Augments +2, DIY +1, Hit Points +4

#### Beta Weapons        Augments +2, DIY +1, Hit Points +4

#### Prototype Weapons        Augments +1, DIY +1, Hit Points +5

#### Interchangeable Parts        Priority +2, Augments +1, Hit Points +5

### Crafting Bows

#### Bowyer        Augments +2, DIY +1, Hit Points +4

#### Beta Bows        Augments +2, DIY +1, Hit Points +4

#### Prototype Bows        Augments +1, DIY +1, Hit Points +4

### Crafting Armor

#### Armor Smith        Augments +2, DIY +1, Hit Points +5

#### Beta Armor        Augments +2, DIY +1, Hit Points +5

#### Prototype Armor        Augments +1, DIY +1, Hit Points +6

Automata (Sciences)

#### Automaton Repairs        Defense +2, Augments +2, Hit Points +7

#### Interchangeable Parts        Augments +2, DIY +1, Hit Points +6

### Crafting Steamers

#### Steam-Powered Crafter        Augments +2, DIY +1, Hit Points +4

#### Beta Boilers        Augments +2, DIY +1, Hit Points +4

#### Prototype Boilers        Augments +1, DIY +1, Hit Points +5

Steamer Operator Specialties

#### Steamer Operator        Accuracy +1, Evade +1, Hit Points +6

#### Steam Poser        Evade +1, Augments +1, Hit Points +7

#### Steam Specialist        Accuracy +1, Augments +1, Hit Points +6

### Crafting Fuse Boxes

#### Fuse Box Builder        Augments +2, DIY +1, Hit Points +4

#### Advanced Brainworks        Augments +2, DIY +1, Hit Points +4

#### Epic Brainworks        Augments +2, DIY +1, Hit Points +6

#### Heroic Brainworks        Augments +2, DIY +1, Hit Points +5

#### Personality        Augments +2, DIY +1, Hit Points +5

### Crafting Clockworks

#### Clockwork Crafter        Augments +2, DIY +1, Hit Points +4

#### Advanced Analytics        Augments +2, DIY +1, Hit Points +5

### Crafting Prosthetics

#### Prosthetician        Augments +2, DIY +1, Hit Points +6

#### Beta Prosthetics        Augments +2, DIY +1, Hit Points +6

#### Prototype Prosthetics        Augments +1, DIY +1, Hit Points +7

#### Automata Tinkerer        Augments +2, DIY +1, Hit Points +4

#### Automata Upgrader        Augments +2, DIY +1, Hit Points +4

#### Nerve Crafting        Augments +1, DIY +1, Hit Points +7

#### Sensory Builder        Augments +2, DIY +1, Hit Points +7

Bio-Flux (Sciences)

Invigoration Specialties

#### Bio-Invigoration        Evade +1, Defense +3, Hit Points +8

#### Bio-Invigoration Expert        Evade +1, Priority +3, Hit Points +9

#### Quickshot Bio-Invigoration        Evade +1, Speed +5, Hit Points +8

#### Bio-Invigoration Expert        Evade +1, Wounds +1, Hit Points +11

#### Self-Administer        Accuracy +1, Evade +1, Hit Points +11

Essence Manipulation

#### Manipulate Essence        Augments +2, DIY +1, Hit Points +6

#### Beta Essence        Augments +2, DIY +1, Hit Points +6

#### Prototype Essence        Augments +2, DIY +1, Hit Points +6

#### Fast Manipulation        Evade +1, Augments +2, Hit Points +7

#### Body Renewal        Defense +2, Augments +1, Hit Points +9

#### Gene Therapy        Augments +2, Wounds +1, Hit Points +9

### Crafting Bio-Zappers

#### Bio-Zapper Developer        Augments +2, DIY +1, Hit Points +4

#### Beta Bio-Zappers        Augments +2, DIY +1, Hit Points +4

#### Prototype Bio-Zappers        Augments +1, DIY +1, Hit Points +4

#### Concentrated Steam        Accuracy +1, Augments +2, Hit Points +6

#### Extra Settings        Augments +2, DIY +1, Hit Points +4

#### Multi-Ray        Accuracy +1, Augments +1, Hit Points +6

#### Splicer        Accuracy +1, Augments +1, Hit Points +6

#### Tracing        Accuracy +1, Augments +1, Hit Points +7

Engineer (Sciences)

#### Maintenance        Augments +2, DIY +1, Hit Points +6

#### Power Surge        Augments +1, Priority +3, Hit Points +6

#### Quick Upgrades        Augments +2, DIY +1, Hit Points +5

#### Vehicle Repairs        Defense +2, Augments +1, Hit Points +7

Grease Monkey Specialties

#### Gearhead        Augments +1, DIY +1, Hit Points +5

#### Gearjunkie        Augments +1, Evade +1, Hit Points +6

### Crafting Vehicles

#### Auto-Wright        Augments +2, DIY +1, Hit Points +4

#### Beta Autos        Augments +2, DIY +1, Hit Points +5

#### Prototype Autos        Augments +1, DIY +1, Hit Points +6

#### Manual-Wright        Augments +2, DIY +1, Hit Points +4

#### Beta Clankers        Augments +2, DIY +1, Hit Points +5

#### Prototype Clankers        Augments +1, DIY +1, Hit Points +5

Armoring Vehicles

#### Vehicle Armorer        Augments +2, DIY +1, Hit Points +6

#### Beta Armoring        Augments +2, DIY +1, Hit Points +6

#### Prototype Armoring        Augments +1, DIY +1, Hit Points +7

Gadgetry (Sciences)

#### Beta Hacker        Evade +1, Augments +1, Hit Points +8

#### Dud        Accuracy +1, Evade +1, Hit Points +7

#### Item Breaker        Accuracy +1, Strike +3, Hit Points +9

#### Reverse Engineer        Augments +2, DIY +1, Hit Points +5

#### Saboteur        Accuracy +1, Priority +3, Hit Points +8

#### Improvised Improvements        Evade +1, Augments +1, Hit Points +7

### Crafting Explosives

#### Pyrotechnician        Augments +2, DIY +1, Hit Points +4

#### Beta Explosives        Augments +2, DIY +1, Hit Points +5

#### Prototype Explosives        Augments +2, DIY +1, Hit Points +6

#### Major Explosion        Augments +2, DIY +1, Hit Points +4

### Crafting Eyewear

#### Optician        Augments +2, DIY +1, Hit Points +4

#### Beta Eyewear        Augments +2, DIY +1, Hit Points +4

#### Prototype Eyewear        Augments +2, DIY +1, Hit Points +5

### Crafting Trinkets

