Chapter 2 - Character Creation
==============================

Making a Character
------------------

Many consider making the character to be almost more fun than actually playing the game. The Clockwork System uses a fast and easy way of character creation with the hardest part left entirely up to you: figuring out exactly what you want your character to be.

I've made a moderately edited version of the [[Character Sheet]] for our campaign. I may edit it more, and I'll definately have some additional documents for tracking your specific goods.


## Skills                

Skills represent your training and talents. Each skill is grouped under an attribute. Choose one skill: that is your primary skill at first level, and it gets 3 points (which means that you’ll get a +3 when rolling that skill). Then, choose two more skills. These are your secondary skills, and each of them gets 2 points. Finally, select three more skills, giving each 1 point.

So the rule is simple: 3 points in one skill, 2 point in two skills, and 1 point in three skills. You cannot place these points in the same skill - therefore, at first level, you’ll be placing points in a total of six skills.

        You can choose skills from one attribute or multiple attributes. The more points you have in one attribute, the greater that attribute is going to be. On the other hand, if you’re more spread out, you’ll be better at doing multiple things.

## Attributes

Where skills are the sprinkles on your sundae, attributes are the ice cream. You can determine your attributes very easily - simply add up all of the skills under an attribute, and that’s your attribute score. Done!

## Specialties                        

Now that you’ve placed your skills and attributes, you can choose your specialties. You choose three specialties at first level, and you will gain one specialty every level thereafter.

        Specialties are character defining abilities, allowing you to do amazing things. Specialties are found under skills. If you want an agility specialty, for example, you flip to the Agility page and peruse those specialties.

        You can only choose a specialty if you have at least one point in the skill that the specialty falls under, so your best bet to find fitting specialties is to look under the skills that you have points in - the more points you have in a skill, the better you’re going to be at using your specialties that come from that skill.

        Specialty Bonuses: Each specialty has a couple bonuses, or “combat statistics,” that it rewards the player with. When you take a specialty, mark down on the back of your character sheet any bonuses that the specialty grants you.

Acc:        Accuracy

Eva:        Evade

Stk:        Strike

Def:        Defense

Pri:        Priority

Spd:        Speed

Aug:        Augment

DIY:        Do-it-Yourself 

Wnd:        Wound

HP:        Hit Points

## Augments

If you have any augments from your specialties, go ahead and select them now.

        Every time a specialty grants you an augment, you get to choose an augment under a Sciences skill (such as Alchemy, Armsmith, Engineer, and Gadgetry) that you have points in and learn it. Once you’ve learned the augment, you can craft it into an item at any time. You may choose an augment regardless of the amount of slots it takes up. To learn more about crafting and augments, turn to Chapter 10 - Sciences.

## Weapons & Armors

You now get to choose your weapons. Weapons are largely chosen based on their size and damage class. If you’d like a one-handed sword, go for a medium melee weapon. If you’d like an elephant gun, go for a heavy rifle. Each size deals a set amount of damage. Weapons, found in Chapter 3 - Gear, fully explains your options.

        Your accuracy and strike are determined by bonuses from your specialties. If the weapon has anything special about it, feel free to put that in the Notes section. You can name your weapon whatever you want, from “sword” to “meat cleaver” to “Lord Hazard’s Razor-Edged Lance of Destruction,” if that’s what you get kicks out of.

        Now select your armor. Just like with weapons, armor comes in light, medium, and heavy categories. Your evade and defense are determined by bonuses from your specialties. There’s a page for armor and shields right after where you chose your weapons.

## Gear    

You’re almost finished, so now you get a reward: you get to go shopping! For a beginning character, you may select anything from the Starting Gear list in Chapter 3 - Gear. There is no stiff limit on how much gear you can have when you begin. Instead, just select what you feel your character would be carrying on them. Of course, we assume that you’ll be logical and not try to carry ten tents on your person.

## Derived Statistics    

Finish filling in the statistics for your character sheet.

        You have 3 action points (AP) at first level.

        Your speed is  25 feet plus any bonuses from your specialties.

        Your priority is based on bonuses granted from specialties.

        Unless you are missing a limb, or have extra limbs, you’ll start off with 12 wounds.

        Your hit points are entirely determined from your specialty-granted combat statistics.

Leveling Up
-----------

There are twelve levels a character can progress through, and it takes twelve experience points to level. Once a person has that twelfth experience point, the character goes to the next level. It’s just like a clock.

        At every level after first, your skills go up and you gain a specialty. Leveling up is pretty easy - just use this list:

*   Place 2 points in any 1 skill
*   Place 1 point in any 2 other skills
*   Add the skills grouped under an attribute to determine your new attribute score
*   Learn 1 new specialty
*   Apply that specialty’s granted bonuses
*   Action Point Increase: If you’re reaching 4th, 8th, or 12th level, your action points per turn increase by 1

Level 1                3 AP

Level 4                4 AP

Level 8                5 AP

Level 12        6 AP

Retrofitting
------------

Sometimes you’ll build a character but later decide that a specialty you chose just isn’t something you use. You may also run into that problem with augments or other aspects of your character. When this happens, you feel the need to retrofit your character.

        Retrofitting Specialties: You may retrofit one specialty every time you gain an action point (thus, at levels 4, 8, and 12). You may delete one specialty and gain a new one. However, that new specialty must fall in the same attribute as the previous specialty.

        Retrofitting Augments: You may retrofit one augment every time you learn a new augment. However, the replacement must come from the same skill as the one being replaced.

#### Note to the Narrator: Further Retrofitting

While we generally don’t encourage it, players may sometimes ask to retrofit a character outside of our listed rules. To what extent that they do so is up to you, and you have full control over any retrofitting they do. If they can make a valid case for the retrofitting, let them - let’s face it, nobody wants to play a character they don’t like.

        Just try not to let them completely destroy or rearrange their character concept.
