Chapter 3 - Gear
===============

Materials
---------

Every item you have will be composed of something, oftentimes metal, wood, or fabric. Materials will be most important for your weapons and armor, and you will need to decide what an item is made of when you select that item.

### Metal

Weapons and armor made of metal are the modus operandi. Metal items are magnetic and they can get hot, but they’re not likely to catch on fire. Metal weapons also have the most variety in things that they can do, so you’ll be able to upgrade them a lot with augments.

### Organic
> -2 Augment Slot

Organic items will often be made up of bone, though sometimes they’ll also consist of animal hides or scales. Organic items are very difficult to work with, and will always have 2 less slots for augments than their metal counterparts.

### Textile
> -1 Augment Slot

Textiles include basic fabrics and, more commonly, leathers. Generally, the only thing you can make using textiles are light armor, cloaks, and whips. Textile items have 1 less slot than metal items to hold augments and will burn away easily, if lit on fire.

### Wood
> -1 Augment Slot

Wood weapons and items are pretty common, and often your polearms and shields will include wood, though it may not be the primary material. Wood will give you 1 less slot on items to use for augments. Wood is also more likely to burn than other items.

[[Material Variants]]

Materials Available for Weapons
|Type| Valid Materials|
|-----|----|
|Melee & Throwing Weapons|Any|
|Firearms|Metal, Wood|
|Bows|Metal, Organic, Wood|
|Crossbows|Metal, Organic, Wood|

## Currency 

This is setting specific.


Melee Weaponry
--------------

Melee attacks are those that target adjacent foes. When making a melee attack, you can either be using a close melee weapon, be fighting unarmed, or using a polearm or flexible weapon (like a chain or whip). There is a wide variety of melee weapons available to you, and they have the highest damage potential.

#### Unarmed 

> 🪙 Cost: 1 AP

Damage Class: 2

Target: Adjacent Foe

An unarmed attack is any attack that does not use a weapon. Instead, unarmed attacks can be anything from your fists to your elbows to your legs and knees. Unarmed attacks are the fastest possible attacks at 1 action point apiece, but they do significantly less damage than attacks with weapons.

        Unarmed attacks utilize no equipment and thus cannot be upgraded by an armsmith.

#### Weapons Sizes 

There are four sizes of weapons: light, medium, heavy, and (the more rare) super-heavy, classified

|Weapon Size|Definition|
|-----------|----------|
|Unarmed|Some part of your body|
|Light|Something small enough to fit in your pocket|
|Medium|Something too large to fit in your pocket, but easily used with one hand|
|Heavy|Something so large you couldn't effectively use it without two hands|
|Super-Heavy|Something so large you need two hands to even use it, and you need to brace your self to swing it|

#### Light Weapons 

> 🪙 Cost: 2 AP

Damage Class: 4

Wielded With: One Hand

Target: Adjacent Foe

Light weapons are defined as any small, one-handed weapon that is easily concealable. All small items can count as light weapons. If you’re thinking about getting really good with light weapons, you might want to check out the Espionage skill, one of the Cunning skills.

Medium Weapons

> 🪙 Cost: 2 AP

Damage Class: 6

Wielded With: One Hand

Target: Adjacent Foe

Medium weapons are one-handed weapons that are not easily concealable. Medium weapons are the bulk of one-handed weapons - pretty much anything that looks large enough to deal some real damage. If you want to get great with medium weapons, you should look at the Swashbuckling skill under Dexterity.

#### Heavy Weapons 

> 🪙 Cost: 2 AP

Damage Class: 8

Wielded With: Two Hands

Target: Adjacent Foe

A heavy weapon is loosely defined as any weapon that is used in two hands. A heavy weapon is virtually impossible to conceal on your person. If you want to master heavy weapons, you should look into the Overpower skill, located under the Brute attribute.

#### Super-Heavy Weapons 

> 🪙 Cost: 2 AP

Damage Class: 10

Wielded With: Two Hands

Target: Adjacent Foe

Super-heavy weapons are immense, building-destroying tools of destruction. Super-heavy weapons require two hands to wield and two hands to carry. These do a lot of damage but require the wielder to take up footing in order to swing them. Entering into footing stance requires 1 action point, and you cannot normally enter into another stance while you’re in footing.

        You may use a super-heavy weapon without taking footing, but doing so gives you a -3 on your accuracy and strike rolls, as if it were an impromptu weapon.

### Weapon Variations

#### Flexible 

> ability to grab, -1 damage class

You can make any weapon into a non-rigid one, a weapon that has flexible parts like a whip, chain, or flail. Flexible weapons can be used to make a grab, just like a hand can. Because the weapon loses some of its impact power, its damage class is 1 lower.

#### Polearm 
> +5 feet of reach, -1 damage class

Medium and heavier weapons can be polearms. The longer handle allows polearms to attack any foe within 10 feet, but also makes the weapon more difficult to swing, decreasing its damage class by 1.

#### Throwing 
> can be thrown, -1 damage class

Throwing weapons can be anything from throwing knives to javelins to gigantic hurling spheres. Weapons designed to be thrown have 1 less damage class, but can hit people at a distance. Each thrown weapon has a different distance, depending on its size. For every 10 feet you throw a weapon beyond its distance, it takes a -1 on the accuracy roll. Weapons not designed to be thrown can go no farther than 25 feet.

Light: 25 feet

Medium: 75 feet

Heavy: 50 feet

> 🗒️️ Note: A thrown weapon is no longer a melee weapon for the purposes of determining how specialties work.

### Combination Weapons

While you can make any melee weapon into a flexible weapon, a polearm, or a throwing weapon, you can also combine those aspects. Want to make a flexible polearm? Go for it! You’ll just have to apply the damage class penalty both times, but now you have a weapon that’ll hit people 10 feet away and you can make grabs.

        If you want to create a bolas, you can combine a throwing weapon with a flexible weapon. Now you can throw a weapon that can make grabs. Want to tie up an opponent? Throw a light bolas at their legs, making a grab, and you’ll nab them.

### Impromptu Weapons
>(-3 on Accuracy and Strike)

Anything that is not designed to be used as a weapon is impromptu weaponry. Impromptu weaponry is assigned a weapon class by the narrator and has a -3 to accuracy and strike rolls. Once a weapon is assigned a weapon class, it cannot change weapon classes (unless it breaks in two!).

        For example, if some characters pick up their chairs, they are heavy melee weapons (because they’re using them two-handed), so they have a damage class of 8 and they take a -3 to their accuracy and strike rolls. If they later break off the chair legs, they can use them as impromptu light weapons, so they’d have a damage class of 4.


Firearms
--------

Blasting people with double-barreled shotguns, unholstering your revolver in an instant and loading your target with lead, or scoping down your rifle’s sights and shooting a man at two hundreds yards away - it doesn’t matter who you’re trying to kill, firearms will do you right.

        Firearms come in several varieties, but they’ll be largely covered under four different categories: light, medium, heavy, and super-heavy. Each of those categories will have a basic damage class and range. Once you’ve chosen the size of your firearm, you will also choose your ammunition type. Most will choose the standard cartridge, the normal damaging round. Some might choose “shot” to turn their weapon into a shotgun and deal more damage up close. Others will use the sniper cartridge, a long range round with slightly lower damage, or the high damage jacketed rounds. Once you’ve chosen your choice of ammunition, you may change it for 1 action point, assuming you are carrying another ammunition type.

        Unlike other weapons, firearms use accuracy to determine your damage instead of your strike. You only roll once for firearms - your accuracy will determine if you hit, and then you will tier the result to get damage. As you can imagine, accuracy is twice as important for a gunslinger as most others.

        Firearms have to be readied between every shot. This can represent rechambering, reloading, or cocking the hammer of the firearm. When combat begins, you can often assume that the firearm is readied, but after firing, you must ready it again.

#### Light Firearms 

Cost to Fire: 2 AP

Damage Class: 2

Wielded With: One Hand

Readying: 0 AP

Target: Any foe within 50 feet. For every 10 feet beyond that the target stands, the shot takes a -1 on the accuracy roll.

Light firearms are peashooters, normally the type that could fit in your palm or in your pocket, and a gun that somebody searching you would often entirely overlook. On the flipside, though, they deal very minimal damage and are more likely to be an irritant than a killer.

#### Medium Firearms 

Cost to Fire: 2 AP

Damage Class: 4

Wielded With: One Hand

Readying: 1 AP if you’re using one hand, or 0 AP if you’re using two hands

Target: Any foe within 100 feet. For every 25 feet beyond that the target stands, the shot takes a -1 on the accuracy roll.

Medium firearms are all of your revolvers, sawed-off shotguns, and small rifles. These guns are handguns - they can be held in one hand, though many will use two hands to help cock the hammer, rechamber the bullet, and lend support to absorb the recoil of the firearm. Note that reloading them normally requires 1 action point, but if you dedicate a second hand to the gun, it costs 0 action points to reload.

#### Heavy Firearms 

> 🪙 Cost: 2 AP

Damage Class: 6

Wielded With: Two Hands

Readying: 1 AP

Target: Any foe within 200 feet. For every 50 feet beyond that the target stands, the shot takes a -1 on the accuracy roll.

Heavy firearms are your rifles and shotguns - your two-handed long rifles. They have a pretty good range, but require 2 hands to use and rechambering costs 1 action point.

#### Super-Heavy Firearms 

> 🪙 Cost: 2 AP

Damage Class: 8

Wielded With: Two Hands

Readying: 2 AP

Target: Any foe within 300 feet. For every 100 feet beyond that the target stands, the shot takes a -1 on the accuracy roll.

Super-heavy firearms are very large guns, meant to take down everything from elephants to raging, steam-powered automatons. These do a lot of damage, but require the wielder to take up footing in order to fire them. Entering into footing stance requires 1 action point, and you cannot normally enter into another stance while you’re in footing.

        You may use a super-heavy firearm without taking footing, but doing so gives you a -3 on your accuracy roll, as if it were an impromptu weapon.

### Firearm Modifications

#### Double-Barreled 

Firearms can be made double-barreled for no additional cost. By making a weapon into a double-barreled firearm, you can fire it twice before having to ready it. However, reloading the double-barreled firearm takes 1 more action point per round loaded.

### Firearm Ammunitions

Firearms can use a wide range of ammunitions, and each type of ammunition is going to give you some different bonuses and penalties. You may change your firearms ammunition for 1 action point.

#### Cartridge 

The cartridge is the most common type of ammunition used. Normally a cartridge is a bullet packaged with the gunpowder and primer to most easily blast holes in nearby villains. The cartridge uses the basic statistics for the firearm.

#### Blank 

Blanks are cartridges that do not fire anything - they produce all of the effects of having fired the firearm, but do not blast forward any projectile. Somebody using blanks is going to have an easier time clubbing somebody to death with their firearm than they will shooting them with it.

#### High Damage Cartridge 

High damage cartridges, often jacketed or armor-piercing rounds or solid shot, increase the damage class by 2 but also increase the readying time by 1 action point. High damage cartridges are very powerful and deadly, but much more difficult to use.

#### Shot 

Shot, commonly known as lead shot or buckshot, is composed of numerous small projectiles that are all shot out of the firearm at the same time. At a distance, shot has a higher likelihood of hitting. However, the further the shot is from the firearm, the less penetrating power it has. Rather than losing accuracy at a range, shot decreases its damage class by 1 for every range increment past the basic distance.

#### Sniper Cartridge 

Sniper cartridges are specifically designed to go far and go fast. A sniper cartridge only loses 1 accuracy for every 2 range increments it travels. However, sniper shots have 1 less damage class than normal.

## Bows

Bows are difficult weapons to use, requiring great amounts of training and skill. For those who master them, however, they can easily rival firearms and deal a great amount of damage. As per normal, bows use accuracy to determine if they hit and strike to determine how much damage they deal. Though you can get a bow of any size, they always require two hands to use.

#### Light Bows 

> 🪙 Cost: 2 AP

Damage Class: 3

Wielded With: Two Hands

Target: Any foe within 25 feet. For every 10 feet beyond that the target stands, the shot takes a -1 on the accuracy roll.

Light bows are most commonly slightshots, though very small bows do exist. They don’t deal much damage, but are faster than their larger counterparts.

#### Medium Bows 

> 🪙 Cost: 2 AP

Damage Class: 5

Wielded With: Two Hands

Target: Any foe within 50 feet. For every 10 feet beyond that the target stands, the shot takes a -1 on the accuracy roll.

Medium bows are between 2 and 4 feet tall, have decent pull, and can deal fair amounts of damage.

#### Heavy Bows 

> 🪙 Cost: 3 AP

Damage Class: 7

Wielded With: Two Hands

Target: Any foe within 75 feet. For every 25 feet beyond that the target stands, the shot takes a -1 on the accuracy roll.

Heavy bows are typically about 4-5 feet tall. They have a decent range, and deal some pretty solid damage when they strike.

#### Super-Heavy Bows 

> 🪙 Cost: 3 AP

Damage Class: 9 (wielded as a heavy weapon)

Wielded With: Two Hands

Target: Any foe within 200 feet. For every 75 feet beyond that the target stands, the shot takes a -1 on the accuracy roll.

Super-heavy bows are designed to hit opponents at a great distance and their long arrows can deal excruciating amounts of damage. Super-heavy bows require the wielder to take up footing in order to fire them. Entering into footing stance requires 1 action point, and you cannot normally enter into another stance while you’re in footing.

        You may use a super-heavy bow without taking footing, but doing so gives you a -3 on your accuracy roll, as if it were an impromptu weapon.

## Crossbows

Crossbows are weapons that in many ways are similar to firearms, but they are more difficult to load and can deal more damage. Bows and crossbows use the same types of ammunitions, listed below under “Arrows & Bolts.”

        Crossbows use accuracy to determine your damage instead of your strike. You only roll once for crossbows - your accuracy will determine if you hit, and then you will tier the result to get damage.

#### Light Crossbows 

Cost to Fire: 2 AP

Damage Class: 3

Wielded With: One Hand

Readying: 1 AP

Target: Any foe within 25 feet. For every 10 feet beyond that the target stands, the shot takes a -1 on the accuracy roll.

Light crossbows are often small, concealable crossbows that shoot a small metal spike or dart. Many people will attach them to their wrists.

#### Medium Crossbows 

Cost to Fire: 2 AP

Damage Class: 5

Wielded With: One Hand

Readying: 1 AP

Target: Any foe within 50 feet. For every 10 feet beyond that the target stands, the shot takes a -1 on the accuracy roll.

Medium crossbows are often called pistol crossbows, as they can be held with one hand and fired like a pistol.

#### Heavy Crossbows 

> 🪙 Cost: 2 AP

Damage Class: 7

Wielded With: Two Hands

Readying: 2 AP

Target: Any foe within 100 feet. For every 25 feet beyond that the target stands, the shot takes a -1 on the accuracy roll.

Heavy crossbows require two hands to use and are difficult to conceal. They’re good at a range, but have a hefty reload time.

#### Super-Heavy Crossbows 

> 🪙 Cost: 2 AP

Damage Class: 9

Wielded With: Two Hands

Readying: 3 AP

Target: Any foe within 150 feet. For every 50 feet beyond that the target stands, the shot takes a -1 on the accuracy roll.

Super-heavy crossbows are almost ballistas - they do a great deal of damage but are very difficult to use in the heat of battle. These do a lot of damage, but require the wielder to take up footing in order to fire them. Entering into footing stance requires 1 action point, and you cannot normally enter into another stance while you’re in footing.

        You may use a super-heavy bow without taking footing, but doing so gives you a -3 on your accuracy roll, as if it were an impromptu weapon.

### Arrows & Bolts

Bows and crossbows shoot arrows and bolts, respectively. There are numerous ways to craft arrows and bolts, though they are often made of wood with a stone or metal tip. You may change your choice of crossbow bolt for 1 action point, but changing your choice of arrow costs nothing.

#### Standard 

Most crossbows and bows will use the standard arrows and bolts. These can refer to broadhead arrows or some other normal version.

#### Bladed 

Bladed bolts and arrows have barbs near the tip that cause bleeding from the target, though they fly less true. Using a bladed bolt or arrow gives a -2 on the accuracy roll, but causes 1 point of bleeding per tier of damage dealt.

#### Hooked 

These arrows and bolts are designed to stick in a target and not come out. Though they do no less damage, removing them requires 2 action points (whereas most arrows and bolts can be removed for one action point, if it is even necessary to do so).

        Hooked arrows and bolts are often used for latching onto a wall. You can attach rope to a hooked arrow or bolt for 1 action point and, when it strikes a wall, you’ll be able to climb up or zip-line down the rope to where the arrow or bolt struck.

#### Signal 

Signal arrows and bolts are known for their whistling qualities. They are extremely loud and will let everyone in a large area know that they were shot. Signal bolts and arrows, however, have a damage class 1 lower than normal.

## Armor

Armor comes in many shapes and sizes, types and qualities. Armor can be made out of leather, bone, iron, steel, bronze, or numerous other materials. Armor was for many years a requirement for warriors, but recently has gone down in popularity with the rise in magnetech weaponry. Nonetheless, it remains a common sight on the battlefield.

        Using Armor: There are five degrees of armor - minimal, light, medium, heavy, and super-heavy. Armor negatively affects a person’s evade roll and speed, but gives them a great amount of damage soak. The “soak class” works the exact same way as damage class - for every tier you receive on your defense roll, you soak that damage. For example, if you received a tier 2 soak in heavy armor (soak class of 4), you would soak 8 damage from the attack.

        Penalties: Armor negatively affects your evade, speed, and ability to move about. You’ll suffer a penalty to evade, a penalty to your land-based movement, and a penalty to your climbing and swimming speeds while wearing armor.

        Describing your Armor: How your armor looks is up to you and your narrator. Thick robes could be defined as light armor, but so could a breastplate. Chainmail covered in fish bones and a suit of field plate could both be heavy armor. How you describe your armor is your choice.

|Type|Soak Class|Evade Penalty|Speed Penalty|Climbing & Swimming Penalty|Material Options|
|----|----------|-------------|-------------|---------------------------|----------------|
|Unarmored|0|-|-0 ft.|-0 ft.|None, Organic, or Textile|
|Minimal|1|-|-0 ft.|-0 ft.|Metal, Organic, or Textile|
|Light|2|-1|-5 ft.|-5 ft.|Metal, Organic, or Textile|
|Medium|3|-2|-5 ft.|-10 ft.|Metal or Organic|
|Heavy|4|-3|-10 ft.|-15 ft.|Metal or Organic|
|Super-Heavy|5|-4|-10 ft.|-20 ft.|Metal|

#### Donning your Armor 

For thick suits, putting on your armor can be quite an ordeal, and when you’re awoken in the middle of the night in nothing but your skivvies, you might need to put some armor on pronto.

|Type|Time to Don|With Help|
|----|-----------|---------|
|Unarmored|-|-|
|Minimal |3 AP|-|
|Light|6 AP|-|
|Medium|24 AP|12 AP|
|Heavy|3 minutes|1 minute|
|Super-Heavy|10 minutes|3 minutes|

## Deflection Items

A shield is used for deflecting incoming attacks. While the term “shield” indeed provokes in the mind a large, bulwark strapped on one arm and used to protect the body, this is not the only defensive item used. Companion weapons, such as the parrying dagger and cloak are also common, and each has its own merits.

        You use shields and companion weapons to make deflections. A deflection costs 1 action point, and may be used whenever you are attacked.

#### Cloak 
>+3 to evade against melee attacks

>Material Options: Organic or Textile

A cloak is often wrapped around one arm or, at the very least, guided by one arm, to blind opponents, throw off their weapons, and mislead them. A cloak can only be used against melee attacks, granting a +3 to evade for the deflection. The cloak, however, allows you to use your hand freely and make grabs.


#### Parrying Dagger 
>+3 to evade against melee attacks

>Material Options: Metal, Organic, or Wood

The parrying dagger is used to deflect attacks while the primary weapon makes them. A dagger can only be used against melee attacks, granting a +3 to evade for the deflection roll. Furthermore, a dagger counts as a light weapon.


#### Shield 
>+4 to evade against melee and ranged attacks

>Material Options: Metal, Organic, or Wood

A shield is strapped onto the forearm and held with the hand. You can hold things with your shield hand, but - while doing so - you cannot make deflections. The shield grants you a +4 to evade for deflections, and can be used both against melee and ranged attacks.



Animals (Reference)
-------

### Movement

You only have to spend 1 action point a turn to guide your animal. It will spend as much of its own action points as it takes to get to the destination you designate. Your animal shares your turn, so this will happen instantly.

### Attacking & Other Options

To make your animal perform an action outside of moving, both you and it have to spend the number of action points it would take any normal character without specialties to perform the action.

### Upkeep

Now, most high-class establishments won’t allow animals inside, so remember to bring something to secure your animal outside of the building before going in. Also be prepared for paying for animal upkeep, which generally amounts to a duke a day for feed. Veterinarians aren’t commonplace, so be sure to keep your animal in good health.

### Horses                      

The quintessential mount, fast horses and durable warhorses will transport any gentleman adventurer across the battlefield with ease. Simple enough to control, these noble beasts may not be strong in combat, but they will get you into or out of the fray.

AP: 3                Cost: 100 princes

HP: 16                Wounds: 9

Priority: +0        Speed: 50 feet

> 🗒️️ Note: If the horse spends all of its action points moving in a turn, it moves at twice its speed. A horse can be mounted.

Guard                                                      

Hide (natural organic armor)

Eva: -1                Def: +1

Soak:  3  |  6  |  9  |  12  

> 🗒️️ Note: Horses have two additional called shot locations for their legs, but have no arms or hands.

Attacks                                                            

Hoof (2 AP)

Natural Medium Weapon

Acc: +0                Stk: +1

Damage:  6  |  12  |  18  |  24

Skills                        Attributes                                  

Agility: +2                 Brute: +3

Brawl: +1                Cunning: +0

Overpower: +1                Dexterity: +2

Resilience: +2                Spirit: +1

Shamanism: +1                Sciences: +0

Reference                                                                                

Specialties: Gallop, Large

Stories: Four-Legged, Natural Armor

### Canines                      

From wolves to coyotes to domesticated dogs, canines can be a gentleman adventurer’s best friend. Stronger while in large groups, canines are always a worthy ally.

AP: 3                Cost: 35 princes

HP: 12                Wounds: 8

Priority: +0        Speed: 40 feet

> 🗒️️ Note: A canine can be mounted by a gnome (and other similarly small people). A canine can track by scent alone.

Guard                                                      

Hide (natural organic armor)

Eva: +0                Def: +0

Soak:  2  |  4  |  6  |  8  

> 🗒️️ Note: Canines have two additional called shot locations for their legs, but have no arms or hands.

Attacks                                                            

Bite (2 AP)

Natural Medium Weapon

Acc: +1                Stk: +3

Damage:  6  |  12  |  18  |  24

> 🗒️️ Note: The canine gains a +1 to strike for each ally within 25 feet that also has the Pack Instincts story. The canine can begin a move, attack, and then complete the move.

Skills                        Attributes                                  

Espionage: +1                 Brute: +2

Frenzy: +1                Cunning: +4 (+2 when tracking by scent

Resilience: +1                and another +2 when on guard)

Showmanship: +2        Dexterity: +1

Tactical: +2                Spirit: +0

                        Sciences: +0

Special Actions                                                                                

Howl (2 AP)

The canine gives a (Showmanship +2: +1 | +2 | +3 | +4 ) bonus to strike to the next attack roll made by any allies within 25 feet.

Reference                                                                                

Specialties: Bounding Lunge, Howl

Stories: Four-Legged, Guard, Natural Armor, Pack Instincts, Scent

### Birds of Prey            

Trained birds are not only worthy companions, they’re also amazing messengers, guides, and scouts. With perfect spatial memory, they’ll always know how to return to previous locations.  

AP: 3                Cost: 45 princes

HP: 11                Wounds: 8

Priority: +0        Speed: 25 feet, 45 feet flight

Guard                                                      

Unarmored

Eva: +1                Def: +0

Soak:  0  |  0  |  0  |  0  

> 🗒️️ Note: Birds of prey have two additional called shot locations for their wings (which are treated as legs), but have no arms or hands.

Attacks                                                            

Talon (2 AP)

Natural Light Weapon

Acc: +3                Stk: +1

Damage:  4  |  8  |  12  |  16

> 🗒️️ Note: When the bird of prey has dived 10 or more feet before making an attack, the target must make a Brute resist against the bird’s agility (+2) or fall prone.

Skills                        Attributes                                  

Ace: +1                 Brute: +1

Agility: +2                Cunning: +3 (+10 when noticing)

Espionage: +2                Dexterity: +3

Resilience: +1                Spirit: +0

Tactical: +1                Sciences: +0

Stances                                                                                

Thermal Gliding

While in this stance, the bird of prey moves at double its flight speed.

Reference                                                                                

Specialties: Death from Above, Thermal Gliding

