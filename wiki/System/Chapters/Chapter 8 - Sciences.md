Chapter 8 - Sciences
---------------------


I will add 
[[Chronology]] and [[Gemcutting]] in here.

The Sciences attribute represents a character’s proficiency to make items, invent new items, and utilize their creations in ways better than anyone else can. Knowledge in the sciences will give you information of the world, allowing you to deduct what are the elements in a serum, take apart and reassemble an advanced firearm, and use general book knowledge to solve a number of riddles.

What can you do with your Sciences attribute?

Disassemble

Through your experience crafting, it’s also taught you how to break things down. Disassembling will often be used for traps and mines, to prevent their activation.

|Tier| Effect|
|--|--|
|1|You can disassemble small, practical things, such as a clock or door.|
|2|You can disassemble a simple trap or item (Marque I or II).|
|3|You can disassemble a complicated trap or item (Marque III).|
|4|You can disassemble a clever trap or item (Marque IV).|

Understand

Because of your knowledge of science, you have a knack for comprehending what an item does and the purpose it serves. If you see an item, you can attempt to ascertain what it does.

|Tier| Effect|
|--|--|
|1|You can understand what its basic function is, but none of its augments.|
|2|You can determine its basic functions and Marque I augments.|
|3|You can determine its basic functions and up to Marque II augments.|
|4|You can determine its basic functions and all augments up to Marque III.|

### Crafting
--------

Though each of the six crafting science skills are certainly different, they each utilize a similar system. This crafting system works on a few key principles: downtime, augments, and marques. All crafting occurs during downtime, when the game is otherwise not running and the character is left to his own devices. If the game goes a couple days without any major plot unfolding, that’s downtime.

        Let’s say that your party’s armsmith, Isabella, wants to craft herself a nice sword. She has 8 points in her Armsmith skill and the Weapon Smith specialty, so she can craft a sword with three slots. She applies the augments accurate and reach to the weapon (each of which being one slot). Because she has 8 points in the skill, they are all Marque II. That means her accurate augment gives the sword a +2 on accuracy rolls, and her reach augment extends the reach of the sword out by five feet (making it one long sword!).

### Learning Augments

You gain augments by taking craft-oriented specialties. When you learn a specialty in crafting, sometimes you will gain a number of augments from the specialty. For each augment you gain in this way, choose one augment from any list that you have access to from a specialty. (Weaponsmith, for instance, gives you access to all of the Weaponsmith augments that you are eligible to learn.) Once you have learned an augment, you may place it on any item of its type that you craft. So, if you create a weapon, and you know the “Accurate” augment, any weapon you craft that has enough open slots may contain the “Accurate” augment. You can only place an augment on an item once (so a sword could not be augmented with “Accurate” multiple times).

        The fastest way to gain augments is to take the Learn Augments specialty on the next page.

### Do-It-Yourself

Once you start taking crafting specialties, you’ll gain a DIY score. This “Do-It-Yourself” score tells you how many of your hand-crafted items you can keep on yourself at any given time without having to pay for the base materials.

        The items that you craft from your DIY score are shoddy, somewhat rugged items that you have pieced together over time. They’re not composed of the finest rifle-stock, the best wood, or the clockmaker’s finest gears. These are the items that you’ve simply figured out how to work and they’re the ones you keep with you.

        You can lend out items that you’ve crafted from your DIY score to your fellow adventurers and friends. However, because of the rickshaw nature of DIY-made items, they require constant upkeep. If one of your items is away from you for longer than a single period of downtime (that is, a couple days), it’ll stop working properly and become little more than useless scrap materials.

        You may craft items of a higher quality by purchasing them at the total cost of the item if it were one marque less than your current marque. The resulting item requires little maintenance and is of high quality. It has your marque, even though it cost less for you to craft. Items bought in this way are of selling quality. This kind of purchase can only be made in a place that can provide materials - consult your narrator.

### Augments & Marques

Almost every item you can craft has a number of slots on it that you can place augments into. Most augments will only take up one slot, but some augments, called complex augments, are very powerful and take up multiple slots. Each augment will tell you how many slots it is worth just beneath its name.

        Augments often have four marques available to them - upgrades that make the augment more powerful as you gain skill in your craft. Marques are similar to tiers, except, instead of rolling, you simply look at your skill. If your skill falls within a certain range, that’ll be your marque. Consult the chart below.

### Crafting in your Downtime

Crafting is something that takes time - you can’t just write it down on your character sheet and suddenly have a dozen poisonous chemicals. Unfortunately, real life (and that’s what we’re going for here, obviously) doesn’t work that way. Instead, crafting is done in your downtime.

        Downtime is loosely defined as all of the time you aren’t actively roleplaying your character or forced to do something. If your character has a couple days off, that’s downtime. If your character is on a voyage at sea for a couple months, that’s downtime. Setting up camp in a dangerous dungeon complex is not downtime - your character has no time to work on things and replenish his supplies.

        During downtime you can make new items, repair old ones, and refill your supplies. If you’re an alchemist that can have five potions at any given time, downtime will allow you to replenish those five potions.

        Downtime is subject to the narrator’s discretion and is a way for narrators to control the flow of time and the game. If you’re out of supplies and the warrior’s equipment is all broken, it might be time for some downtime to recuperate. Yet maybe you have a very brief amount of downtime and your narrator decides that you have enough time to replenish two of your potions, but not all of them. That’s at your narrator’s discretion.

Specialties                              

#### Learn Augments
> Augments +4, DIY +1, Hit Points +4

> General Crafting Specialty

You learn the granted augments. You can take this specialty multiple times.

#### Nothing up my Sleeve
> Accuracy +1, Augments +2, Hit Points +4

> General Crafting Specialty

You are known for always having the right item for the right job. At any given moment, you can craft an item on the fly, acting as though you already had it. It must be concealable (thus, you can’t just pull a tank or heavy rifle out of your pocket). You can only do this once between downtimes, and the item must be something you know how to craft (as in, you have taken the basic crafting specialty for the item and any augments you are going to put on it).

The Cost of Prototyping

Many inventors can produce beta objects: creations that exceed the normal augment limit of 3. These beta designs, however, are so difficult to use that only the original inventor can typically control them. But while many crafters can build betas, very few can build prototypes. Prototypes are objects that exceed the typical augment limit of 3 but that anybody can use.

        The materials cost for creating the extra augments for prototypes is the same as it always is. However, because prototypes are so rare, those who create them will typically inflate the price for the extra effort and skill required to craft the items. When buying off the market, the extra 2 augments that prototypes allow will often be double the price of the normal 3 augment item.

### Augments with the Same Name

Sometimes, you’ll find an “Accurate” augment under bow augments and an “Accurate” augment under weapon augments. They might have slightly different flavor text, but they do the exact same thing. Well, if you learn one, you’ve learned the other.

        Anytime you learn an augment, you know that augment regardless of which section of the book it falls under. If you learn how to craft firearms, crossbows, bows, and melee weapons, there is no need to learn the “Damaging” augment 4 different times. Just learn it once, and you’re good to go!

* * *
## TODO :
- Gem Crafting
	- Material
	- Augments (Refinements)

## Alchemy Skill
=============

Alchemy allows a person to create everything from basic acid to chemical weaponry. Alchemy uses chemicals to create a variety of potent solutions. Chemical weapons have gone into rapid development over the past fifty years, yet the usage of poisons, medicinal potions, and numerous other alchemic substances has been around for centuries. An alchemist can bring great diversity to any group, being most noteworthy for his or her healing abilities. Beyond medicinal, alchemist have a wide repertoire of explosives, corrosives, gases, and poisons. This is not a person you want to see on a bad day.

Using Potions        

Potions find themselves being thrown around combat in all sorts of ways: a friendly toss to an ally, a deadly throw at an opponent, jabbed down someone’s throat, smashed into the ground at your feet, or poured over a wound. Here are some basic rules for using potions.

### Drawing & Drinking

> 🪙 Cost: 1 AP

Drawing and drinking a potion can be one smooth action that costs one action point. It does leave you open to melee reflexes: reflexive attack, reflexive called shot to the lower arm (disarm), or a reflexive called shot to the wielded item (sunder, this time to the potion). Be careful of drinking in combat!

### Throwing

> 🪙 Cost: 2 AP

You may throw a potion at an enemy. You must have already drawn the weapon in order to do so. Attacking somebody with it requires 2 AP, and it deals damage as an unarmed weapon. Potions have a range of 25 feet.

        If you miss with a potion, it lands nearby and breaks. The narrator will determine if it lands on the space the enemy was in or near them.

        You can also toss a potion to an ally. This works in much the same way, except you need not roll accuracy and evade - unless, of course, the ally doesn’t want it! (For the ally to catch the toss, they must have one hand free, but it doesn’t cost any action points. They can drink it for 1 action point, as per normal, but may do so reflexively when they catch it.)

### Applying Poison to a Weapon

> 🪙 Cost: 3 AP

You can apply a poison to a weapon for 3 action points. Doing so coats the weapon. The first time that the weapon deals damage (after damage soak), the poison is used. You can’t coat a bullet with a poison, and a weapon can only be coated in one poison at a time.

### Immunity Specialties

#### Self-Made Immunity
> Defense +2, Augments +1, Hit Points +8

> Alchemy Specialty

> ❓ Requires: Either Acid Brewer, Gas Brewer, or Poison Brewer specialty

Due to prolonged exposures with your chemicals, your body has become immune to all of your harmful concoctions. Any harmful potions that you create, including contact poisons, can no longer harm you.

#### Immunizations
> Defense +2, Augments +1, Hit Points +6

> Alchemy Specialty

> ❓ Requires: Self-Made Immunity specialty

You can brew your potions based on your friend’s chemical makeup, ensuring that they are also immune to your harmful concoctions. During any breather (a period of 15 minutes or more), you can immunize an ally against your harmful potions. From that point on, they are immune to any potions that you create until you specify that they are not. You can also designate potions against which they are not immune.

### On-the-Fly Specialties

#### On-the-Fly Brewer
> Evade +1, Augments +2, Hit Points +4

> Alchemy Specialty

> 🪙 Cost: 3 AP per augment slot

You carry many of your basic chemicals on you and can mix them during the heat of battle to create that all-so-necessary potion. You can create any potion On-the-Fly as long as you have some basic chemicals on you. You can never have any more of your own potions brewed than your DIY score would allow. You may divide the action points spent brewing your potions over as many turns as you would like. Augments that do not have a slot cost require only 1 action point to be put in the potion.

#### Expiration
> Evade +1, Augments +2, Hit Points +4

> Alchemy Specialty

> ❓ Requires: On-the-Fly Brewer specialty

Any of the chemicals that you brew On-the-Fly can be brewed with an expiration time. The potion may either activate upon expiring or become a dud. If you set the potion to activate, acids will eat through the vial, and gases will be automatically released. Once the dud potion expires, it’s little more useful than water (and fails to even provide the hydrating benefits of water).

        You may set the potion to expire within up to 5 turns. The potion will expire at the end of your turn, based on the number of turns set.

#### Herbalist
> Augments +2, DIY +1, Hit Points +5

> Alchemy Specialty

> ❓ Requires: On-the-Fly Brewer specialty

The world is your chemistry set. You have no need for urban chemicals, and can create your potions with only those things found in the wild. Lost in the forest? The herbs will provide everything you need. Stuck in a dank dungeon? Look for an underground stream and some moss. You can use On-the-Fly Brewer without any need for basic chemicals, and can do so almost anywhere.

#### Rapid Mixer
> Evade +1, Augments +1, Hit Points +5

> Alchemy Specialty

> ❓ Requires: On-the-Fly Brewer specialty

> 🪙 Cost: 1 AP per augment slot

You have little need for pre-prepared potions. You can now brew potions On-the-Fly for only 1 action point per augment slot used in the potion. Augments that cost 0 slots on the potion may be placed on the potion for no action point cost.

#### Walking Chemical Plant
> Evade +1, Augments +1, Hit Points +5

> Alchemy Specialty

> ❓ Requires: On-the-Fly Brewer & Expiration specialty

You can brew potions well past your normal DIY score. You may brew as many potions as you’d like; however, all potions you brew past your DIY limit expire and become duds within a number of turns after you brew them. (At your discretion, you can cause them to expire earlier.)

|Tier| Effect|
|--|--|
|1|Expires at the end of your next turn|
|2|Expires up to 2 turns later|
|3|Expires up to 3 turns later|
|4|Expires up to 4 turns later|

### General Alchemy Augments

These are augments that, once learned, can be placed on almost any acid, gas, medicine, or poison.

#### Contact
>   General Alchemical Augment

The potion seeps through skin and armor, going straight into the bloodstream. You can now simply throw the vial at an ally in order for the potion to affect them. They do not need to catch it and drink it themselves.

> 🗒️️         Note: This augment always acts as marque I for the purposes of determining cost, though you can learn this augment despite your skill in alchemy.

#### Solid
>   General Alchemical Augment

You develop your alchemical substances as a pill or small glob with a protective coating. The substance takes on a solid state, so that it can exist underwater or in other liquids. It will stay that way until broken.

> 🗒️️         Note: This augment always acts as marque I for the purposes of determining cost, though you can learn this augment despite your skill in alchemy.

### Crafting Acids

#### Acid Brewer
> Augments +2, DIY +1, Hit Points +5

> Alchemy Specialty

When we say acids, we’re not talking about citrus-based cleaning supplies. We’re talking about acids that eat through metals within seconds, acids that burn skin, and acids that reduce the iron of a sword to its original form. You can now brew acids.

        Acids are often used to attack a single person.

        Without spending any money, you can brew and maintain several acids based on your current Do-It-Yourself (DIY) score. These acids can then be upgraded with augments. You’ll learn 2 augments from this specialty which can be selected under “acid augments” below. These augments have marques. At lower levels, you’ll start with Marque I augments. As your skill in Alchemy improves, your marques will increase. See the “Crafting” page at the beginning of this chapter for more information.

        Each acid can be upgraded with 3 augments. Sometimes an augment will take up multiple augment slots. For example, the “flesh burner” augment is worth 2 slots, so an acid only has 1 more available slot for an augment after “flesh burner” has been applied.

#### Resisting Acids 

All acids can be resisted with a Brute roll. For every tier of Brute rolled above Tier 1, the marque of the acid is reduced by 1.

#### Number of Acids you can Maintain  

Without needing to buy chemicals, you can brew some acids entirely out of scraps. These acids must be constantly maintained by you and stop working soon after leaving your care. You can build and maintain a number of acids based on your DIY score. You can brew new acids or augment old ones during any period of downtime you have.

diy :    1 2 3 4 5 6 7 8 9 10 11 12
build:  3 4 4 4 5 5 5 6 6 7   7   7 

#### The Cost of Acids 

If you need to brew an acid that you can’t build for free from your DIY score, you will need to buy the materials for it.

        Every augment will increase the price. The higher the marque, the greater the price. The market price for an augment can be found in the chart below.

If you are brewing the augment, you pay 1/5th the price, which is the same as if you were buying an augment one marque lower. (As in, the material cost for a Marque III augment is the market price fo a Marque II augment.) The material cost for a Marque 1 augment is 6 dukes.

#### Beta Acids
> Augments +2, DIY +1, Hit Points +5

> Alchemy Specialty

> ❓ Requires: 4 skill points in Alchemy & Acid Brewer specialty

Your acids go far beyond what most other chemists dream of, yet they are difficult for most people to use. Such acids have two more slots for you to place augments into.

        If anybody other than you attempts to use one of your beta acids, they must succeed in rolling a science result one tier higher than the highest level marque you have on your acid. If your acid has a Marque IV augment, it is impossible for them to use it (unless they can somehow obtain a tier result of 5 with their science attribute).

#### Prototype Acids
> Augments +1, DIY +1, Hit Points +6

> Alchemy Specialty

> ❓ Requires: 16 skill points in Alchemy, Acid Brewer, & Beta Acids specialties

You’ve perfected your beta acids and made them user-friendly. Now anybody can use an acid that you designate as being a prototype.

### Acid Augments

#### Burns
>   Acid Augment

This acid burns the skin of the person it is thrown on. This is resistable and goes away after taking a breather.

|Marque| Effect|
|--|--|
|1|Tier 1 burns (giving you a -1 on defense rolls)|
|2| Tier 2 burns (giving you a -3 on defense rolls)|
|3|Tier 3 burns (giving you a -5 on defense rolls)|
|4|Tier 4 burns (giving you a -7 on defense rolls)|

#### Flesh Burner
>   Acid Augment

Takes up 2 Augment Slots on the Acid

Flesh burning acid damages living people and organic tissue. If used on a person, it deals damage to them. If simply thrown on somebody, it will lower the soak class of the person’s armor (and cannot be resisted). (If thrown on a person wearing organic armor, it will affect the armor first.) If a called shot is made to the person’s organic weapon (or another wielded item), it will lower the damage class of the weapon. If used on an organic trinket or gizmo of any sorts, it will render the item unusable until the item can be hammered out. Normally a weapon or armor can be banged back to its original form during a breather (15 minutes or more).

|Marque| Effect|
|--|--|
|1|6 damage or a -1 to damage or soak class|
|2| 12 damage or a -2 to damage or soak class|
|3|18 damage or a -3 to damage or soak class|
|4|24 damage or a -4 to damage or soak class|

#### Metal Melter
>   Acid Augment

Takes up 2 Augment Slots on the Acid

This melts away metals, causing them to lose their form and substance. If used on an automaton, metal vehicle, or somebody inexplicably made of metal, it deals damage to them. If simply thrown on somebody wearing armor, it will lower the soak class of the person’s armor (and cannot be resisted). (If thrown on a metal creation wearing armor, it will affect the armor first until the armor is gone.) If a called shot is made to the person’s weapon (or another wielded item), it will lower the damage class of the weapon. If used on a trinket or gizmo of any sorts, it will render the item unusable until the item can be hammered out. Normally a weapon or armor can be banged back to its original form during a breather (15 minutes or more).

|Marque| Effect|
|--|--|
|1|6 damage or a -1 to damage or soak class|
|2| 12 damage or a -2 to damage or soak class|
|3|18 damage or a -3 to damage or soak class|
|4|24 damage or a -4 to damage or soak class|

#### Rusting
>   Acid Augment

Takes up 2 Augment Slots on the Acid

The acid rapidly begins to rust armor and washes out important lubrication, causing the armor to lock up. When an opponent is in medium or heavier metal armor, and is splashed with the acid, it begins to grind on itself, decreasing movement speed.

|Marque| Effect|
|--|--|
|1|-5 speed, -10 swim and climb speeds|
|2| -10 speed, -10 swim and climb speeds|
|3|-10 speed, -15 swim and climb speeds|
|4|-15 speed, -15 swim and climb speeds|

Automatons and vehicles affected by the rusting augment take the penalty to regular speed only.

#### Splash
>   Acid Augment

Your acids splash against multiple spots. The marque of this augment determines how many adjacent spaces the acid will splash into an effect.

|Marque| Effect|
|--|--|
|1|Affects 1 adjacent space|
|2| Affects 2 adjacent spaces|
|3|Affects 3 adjacent spaces|
|4|Affects 4 adjacent spaces|

#### Thick Rust
>   Acid Augment

> ❓ Requires: Rusting Augment

The acid gets into the finer pieces of the armor, beginning to rust in even the smallest movements. The opponent takes a penalty to Dexterity and also must spend twice as many action points to get out of their armor. The armor can be cleaned out during the next breather.

|Marque| Effect|
|--|--|
|1|-3 Dexterity|
|2| -6 Dexterity|
|3|-9 Dexterity|
|4|-12 Dexterity|

#### Wood Wrecker
>   Acid Augment

Takes up 2 Augment Slots on the Acid

This melts away wood, causing it to lose its form and substance. If used on a wooden automaton, vehicle, or somebody inexplicably made of wood, it deals damage to them. If simply thrown on somebody wearing wooden armor, it will lower the soak class of the person’s armor (and cannot be resisted). (If thrown on a wooden creation wearing wooden armor, it will affect the armor first, until the armor is gone.) If a called shot is made to the person’s wooden weapon (or another wielded item), it will lower the damage class of the weapon. If used on a wooden trinket or gizmo of any sorts, it will render the item unusable until the item can be hammered out. Normally a weapon or armor can be banged back to its original form during a breather (15 minutes or more).  

|Marque| Effect|
|--|--|
|1|6 damage or a -1 to damage or soak class|
|2| 12 damage or a -2 to damage or soak class|
|3|18 damage or a -3 to damage or soak class|
|4|24 damage or a -4 to damage or soak class|

### Crafting Gasses

#### Gas Brewer
> Augments +2, DIY +1, Augments +4

> Alchemy Specialty

You can whip up the most eye-bleeding, mouth-gagging, toxic gases around. You can now brew gases.

        Gases affect the space that the gas was released, and every adjacent space. Every turn, the gas has a chance of leaving, which the narrator will roll for at the end of the every turn after they throw it. The likelihood of the gas dissipating is based on how windy it is, with Tier 1 being absolutely no wind (such as a small, airtight room) and Tier 4 being a very windy area (such as during a storm or aboard a fast-flying ironbird). See the sidebar: Dispersing Gases.

        Without spending any money, you can brew and maintain several gases based on your current Do-It-Yourself (DIY) score. These gases can then be upgraded with augments. You’ll learn 2 augments from this specialty, which can be selected under “gas augments” below. These augments have marques. At lower levels, you’ll start with Marque I augments. As your skill in Alchemy improves, your marques will increase. See the “Crafting” page at the beginning of this chapter for more information.

        Each gas can be upgraded with 3 augments. Sometimes an augment will take up multiple augment slots. For example, the “blinding” augment is worth 2 slots, so a gas only has 1 more available slot for an augment after “blinding” has been applied.

#### Resisting Gases 

All gases can be resisted with a Brute roll. For every tier of Brute rolled above Tier 1, the marque of the gas is reduced by 1.

#### Number of Gases you can Maintain  

Without needing to buy chemicals, you can brew some gases entirely out of scraps. These gases must be constantly maintained by you and stop working soon after leaving your care. You can build and maintain a number of gases based on your DIY score. You can brew new gases or augment old ones during any period of downtime you have.

#### The Cost of Gases 

If you need to brew a gas that you can’t create for free from your DIY score, you will need to buy the materials for it.

        Every augment will increase the price. The higher the marque, the greater the price. The market price for an augment can be found in the chart below.

If you are brewing the augment, you pay 1/5th the price, which is the same as if you were buying an augment one marque lower. (As in, the material cost for a Marque III augment is the market price fo a Marque II augment.) The material cost for a Marque 1 augment is 8 dukes.

#### Beta Gases
> Augments +2, DIY +1, Hit Points +4

> Alchemy Specialty

> ❓ Requires: 4 skill points in Alchemy & Gas Brewer specialty

Your gases are much more lethal than most, yet they are difficult for most people to use. Such gases have two more slots for you to place augments into.

        If anybody other than you attempts to use one of your beta gases, they must succeed in rolling a science result one tier higher than the highest level marque you have on your gas. If your gas has a Marque IV augment, it is impossible for them to use it (unless they can somehow obtain a tier result of 5 with their Sciences attribute).

Dispersing Gases

The conditions of the battlefield often determine the amount of time a gas can remain on the field without dispersing naturally. When determining if a gas is dispersed, the narrator rolls to determine whether or not the gas is dispersed, giving no special bonuses to the roll unless otherwise stated.

        Gases cannot be deployed underwater, and the conditions of wind have the greatest effect on the dispersing of gases. Each turn, the narrator rolls and must surpass the required roll based on the tier of the wind to determine if the gas remains.
|Tier| Effect|
|--|--|
| 1|The gas will stay on a roll of 2 or higher. Tier 1 wind is stagnant air.|
| 2|The gas will stay on a roll of 5 or higher. Tier 2 wind is a breeze.|
| 3|The gas will stay on a roll of 9 or higher. Tier 3 wind is a gust.|
| 4|The gas will stay on a roll of 12 or higher. Tier 4 wind is a strong gust.|

Gases can also be dispersed by certain other forces. When something moves through the area, it will disperse the gas unless the narrator rolls a 5 or more. If a bomb goes off in the gas, it will disperse the gas unless the narrator rolls a 9 or more. Gases with expirations disperse naturally or cannot be deployed when they expire.

#### Prototype Gases
> Augments +1, DIY +1, Hit Points +4

> Alchemy Specialty

> ❓ Requires: 16 skill points in Alchemy, Gas Brewer, & Beta Gases specialties

You’ve perfected your beta gases and made them user-friendly. Now anybody can use a gas that you designate as being a prototype.

### Gas Augments

#### Area of Effect
>   Gas Augment

Takes up 2 Augment Slots on the Gas

Your gas affects more areas. You can, at your discretion, make the area of effect smaller than your normal marque allows.

|Marque| Effect|
|--|--|
|1|within 10 feet of the original space|
|2|within 15 feet of the original space|
|3|within 20 feet of the original space|
|4|within 25 feet of the original space|

#### Arm Mutation
>   Gas Augment

Your gas causes the nerves in their arm to become unwound, for their flesh to warp, and for their arms to become largely unusable. If effected with an arm mutation, the target may resist with a resilience result one tier higher than the mark of this augment. Any time the target makes a roll that uses the arm, including accuracy and strike (and evade if they attempt to deflect), the target suffers a penalty on the roll. The mutation lasts for a number of turns based on the mark.

|Marque| Effect|
|--|--|
|1|-1 for 3 turns|
|2|-2 for 4 turns|
|3|-3 for 5 turns|
|4|-4 for 6 turns|

#### Blinding
>   Gas Augment

Takes up 2 Augment Slots on the Gas

A blinding chemical burns the eyes of everyone within the area of the gas, to the point where opening their eyes is painful. Those within the blinding area suffer a -4 on accuracy and evade rolls. People may leave the area of the gas, but they remain blinded for a number of turns afterwards based on the marque of this gas (though this is resistable).

|Marque| Effect|
|--|--|
|1|1 turn|
|2| 2 turns|
|3|3 turns|
|4|4 turns|

#### Corrosive
>   Gas Augment

A corrosive chemical weapon deals damage to everything it hits. They can melt skin, destroy lungs, or worse. The corrosive does hit point damage to every person inside, every time they end their turn inside the corrosive gas (though this is resistable).

|Marque| Effect|
|--|--|
|1|3 damage|
|2| 6 damage|
|3|9 damage|
|4|12 damage|

#### Confusing
>   Gas Augment

Confusing gas disorients the target, making them unable to make Cunning rolls. If the target fails to resist, they will automatically take a 1 on any Cunning rolls they make. Furthermore, the confused victim will be unaware of their own confusion, and will naturally default to Cunning if that is their highest attribute. Any person affected by the gas will remain in a confused state for a number of turns after leaving the gas (though this is resistable).

|Marque| Effect|
|--|--|
|1|1 turn|
|2| 2 turns|
|3|3 turns|
|4|4 turns|

#### Fogging
>   Gas Augment

A fogging gas blurs the vision of everyone within the area the weapon effects. Those within the fogging are effectively in poor lighting, suffering a -2 on accuracy and evade rolls. If somebody leaves the area they are no longer affected, or if somebody enters the area, they enter into the poor lighting. If somebody is trying to shoot through the fogging gas, they take the penalty as well.

> 🗒️️         Note: This augment always acts as marque I for the purposes of determining cost, though you can learn this augment despite your skill in alchemy.

#### Internal Burning
>   Gas Augment

This gas burns muscles of the target with every action they take. They take damage with every action point they spend until the internal burning exhausts (though this is resistable).

        They may choose, however, to forgo action points - that is, if they have 3 action points for the turn, the target can wait for those 3 action points, and no damage will be done.

|Marque| Effect|
|--|--|
|1|1 damage for 3 action points|
|2| 2 damage for 4 action points|
|3|3 damage for 5 action points|
|4|4 damage for 6 action points|

#### Lingering
>   Gas Augment

Whatever you used, it stays where it was and continues to work its magic. If the narrator does not roll high enough for the gas to stay in the area, the narrator will re-roll. The narrator will do this a number of times based on the marque of the lingering.

|Marque| Effect|
|--|--|
|1|re-roll one time|
|2| re-roll two times|
|3|re-roll three times|
|4|re-roll four times|

#### Luminescent Gas
>   Gas Augment

The gas coats the skin and clothes of those within its range and glows, causing them to have extreme difficulty hiding. Instead of the normal Brute resist, Luminescent is resisted by dexterity. Opponents who are already hidden are automatically forced to re-roll if they fail to entirely resist the luminescence, this time with the penalty.

        Luminescence will naturally wash off with water, after being in rain for 1 turn, or if the target can spend 12 action points brushing it off.

|Marque| Effect|
|--|--|
|1|-3 on sneaking and hiding rolls|
|2| -6 on sneaking and hiding rolls|
|3|-9 on sneaking and hiding rolls|
|4|-12 on sneaking and hiding rolls|

#### Paranoia
>   Gas Augment

Takes up 3 Augment Slots on the Gas

This chemical distresses the target and causes them to see things that aren’t there. Any affected by the paranoia must spend their next available action point (or two, if their only attack requires two action points) to attack the person closest to them, regardless of whether they are friend or foe. The paranoia makes the attack wild, however, and the targets suffer a penalty on their accuracy and strike rolls during this attack.

        Instead of the normal Brute resist, Cunning or Spirit can be used for the resist (at the target’s discretion).

|Marque| Effect|
|--|--|
|1|-3 on accuracy and strike rolls for the paranoia attack|
|2|-2 on accuracy and strike rolls for the paranoia attack|
|3|-1 on accuracy and strike rolls for the paranoia attack|
|4|no penalty on the accuracy or strike rolls for the paranoia attack|

#### Replicating
>   Gas Augment

Takes up 2 Augment Slots on the Gas

Replicating gases feed on death, spreading farther out with each victim it claims. Any time somebody dies in replicating gas, it feeds on the body to create more of itself, spreading out to affect everything within several adjacent areas of the original victim. It will continue to spread indefinitely.

|Marque| Effect|
|--|--|
|1|spreads to adjacent areas|
|2|spreads to adjacent areas within 10 feet|
|3|spreads to adjacent areas within 15 feet|
|4|spreads to adjacent areas within 20 feet|

#### Slowing
>   Gas Augment

Those affected by your gas get stiff joints, feel frozen, instantly gain arthritis, or for some other reason can’t seem to move as fast as they once did. Their speed is reduced by the specified footage for three turns (though this reduction is resistable). They can never go below 5 feet.

|Marque| Effect|
|--|--|
|1|5 feet speed reduction|
|2| 10 feet speed reduction|
|3|15 feet speed reduction|
|4|20 feet speed reduction|

#### Sticky
>   Gas Augment

This chemical weapon causes the victims to become sticky, slowing down their reflexes. The target suffers a penalty on evade rolls for three turns (though this penalty is resistable).

|Marque| Effect|
|--|--|
|1|-1 on evade rolls|
|2|-2 on evade rolls|
|3|-3 on evade rolls|
|4|-4 on evade rolls|

#### Stunning
>   Gas Augment

Your targets are stunned when they get hit by this gas. They are stunned for the specified action points (though this is resistable).

|Marque| Effect|
|--|--|
|1|stunned for 1 action point|
|2| stunned for 1 action point|
|3|stunned for 2 action points|
|4|stunned for 2 action points|

#### Thick
>   Gas Augment

This gas hugs the ground it’s released at. Winds in the area act as if they’re one tier lower when determining how long the gas lasts. In addition, if used on board a moving vehicle, the gas will sink onto the vehicle’s deck and move with the vehicle.

> 🗒️️         Note: This augment always acts as Marque I for the purposes of determining cost, though you can learn this augment despite your skill in alchemy.

### Crafting Medicine

#### Medicine Brewer
> Augments +2, DIY +1, Hit Points +5

> Alchemy Specialty

Medicinal potions are used to restore hit points, heal wounds, fight off poisons, and a variety of other beneficiary effects. All medicinal potions must be injected or ingested.

        Without spending any money, you can brew and maintain several medicines based on your current Do-It-Yourself (DIY) score. These medicines can then be upgraded with augments. You’ll learn 2 augments from this specialty, which can be selected under “medicine augments” below. These augments have marques. At lower levels, you’ll start with Marque I augments. As your skill in Alchemy improves, your marques will increase. See the “Crafting” page at the beginning of this chapter for more information.

        Each medicine can be upgraded with 3 augments. Sometimes an augment will take up multiple augment slots. For example, the “heavy push” augment is worth 3 slots, so a medicine has 0 available slot for an augment after “heavy push” has been applied.

#### Number of Medicines you can Maintain  

Without needing to buy chemicals, you can brew some medicines entirely out of scraps. These medicines must be constantly maintained by you and stop working soon after leaving your care. You may build and maintain a number of medicines based on your DIY score. You may brew new medicines or augment old ones during any period of downtime you have.

#### The Cost of Medicines 

If you need to brew a medicines that you can’t create for free from your DIY score, you will need to buy the materials for it.

        Every augment will increase the price. The higher the marque, the greater the price. The market price for an augment can be found in the chart below.

If you are brewing the augment, you pay 1/5th the price, which is the same as if you were buying an augment one marque lower. (As in, the material cost for a Marque III augment is the market price for a Marque II augment.) The material cost for a Marque 1 augment is 8 dukes.

#### Beta Medicines
> Augments +2, DIY +1, Hit Points +6

> Alchemy Specialty

> ❓ Requires: 4 skill points in Alchemy & Medicine Brewer specialty

While it is impossible for others to administer your medicines, they’re well worth the hassle. Your beta medicines have two more slots for you to place augments into.

        If anybody other than you attempts to use one of your beta medicines, they must succeed in rolling a science result one tier higher than the highest level marque you have on your medicine. If your medicine has a Marque IV augment, it is impossible for them to use it (unless they can somehow obtain a tier result of 5 with their Sciences attribute).

Prototype Medicine

> Alchemy Specialty

> ❓ Requires: 16 skill points in Alchemy, Medicine Brewer, & Beta Medicine specialties

You’ve perfected your beta medicines and made them user-friendly. Now anybody can use a medicine that you designate as being a prototype.

### Medicine Augments

#### Antitoxin
>   Medicine Augment

When you take an antitoxin, you gain a specified bonus against all poisons for the next hour. In addition, if you are currently poisoned, you may instantly reroll your resist against the poison with the bonus against it.

|Marque| Effect|
|--|--|
|1|+3|
|2|+6|
|3|+9|
|4|+12|

#### Heavy Push
>   Medicine Augment

Takes up 3 Augment Slots on the Medicine

While push stimulates the body to help it get over the beating it’s taken, heavy push takes it to a dangerous level. The muscles rip and the body cannot deal with the stress it’s being put under. This causes the heavy push to deal damage to the administrator, but gains action points instead. You can only use 1 extra action point per turn, and the damage is unsoakable. Any unused extra action points will go away during your next breather.

|Marque| Effect|
|--|--|
|1|10 backlash damage for each additional action point used, up to 2 extra action points|
|2|9 backlash damage for each additional action point used, up to 3 extra action points|
|3|7 backlash damage for each additional action point used, up to 4 extra action points|
|4|6 backlash damage for each additional action point used, up to 5 extra action points|

#### Improved Push
>   Medicine Augment

Takes up 2 Augment Slots on the Medicine

> ❓ Requires: Push augment known

Improved push takes your grandpappy’s old style push potions and gives them a jolt of electricity, making them push that much harder. Improved push potions return your hit points to you, though you cannot go over your maximum.

|Marque| Effect|
|--|--|
|1|restores 10 hit points|
|2|restores 20 hit points|
|3|restores 30 hit points|
|4|restores 40 hit points|

#### Liquid Skin - Wound Healing
>   Medicine Augment

Takes up 2 Augment Slots on the Medicine

Liquid skin instantly heals wounds damage. Healing this quickly, however, does so by infuriating the body’s natural regenerative system, and, made slightly wrong, can be very dangerous. When this is administered, the target rolls their die. If they receive a 1 on the twelve-sided die, they take the healing as damage.

|Marque| Effect|
|--|--|
|1|1 wound healed|
|2|2 wounds healed|
|3|3 wounds healed|
|4|4 wounds healed|

#### Pain Reliever
>   Medicine Augment

Pain relieving medicines are designed to allow you to keep pushing on despite huge amounts of physical pain. When under the effects of painkillers you can ignore the wound effects 6-12 until your next breather.

|Marque| Effect|
|--|--|
|1|Can ignore 1 such wound effect|
|2|Can ignore 2 such wound effects|
|3|Can ignore 3 such wound effects|
|4|Can ignore 4 such wound effects|

#### Push - Hit Point Regain
>   Medicine Augment

Push is a body stimulant that allows a person a boost in stamina, to ignore their wounds and to take a lot more punishment before falling. Push refills a person’s hit points, though it will not go over their maximum.

|Marque| Effect|
|--|--|
|1|Restores 4 hit points|
|2|Restores 8 hit points|
|3|Restores 12 hit points|
|4|Restores 16 hit points|

#### SlowHeart
>   Medicine Augment

This potion slows the target’s heart, releases stress, and allows the person to relax and breath easily. If the target chooses, they may ignore the calming effects of SlowHeart, but if they go with the flow and slow down a little bit, their accuracy will improve. The bonus lasts until their next breather.

|Marque| Effect|
|--|--|
|1|+1|
|2|+2|
|3|+3|
|4|+4|

#### Stimulant
>   Medicine Augment

This stimulant is used to forego fatigue. If you are exhausted, taking a stimulant will push off the fatigue for a number of hours as specified by the potion’s marque. Once a stimulant has been used, the fatigue returns after the designated amount of time and another stimulant will have no effect until you rest.

|Marque| Effect|
|--|--|
|1|2 hours|
|2|4 hours|
|3|8 hours|
|4|24 hours|

#### Styptic
>   Medicine Augment

A styptic is a useful substance that, when poured on a wound, dries the blood and keeps it from bleeding. Any person that this is administered to stops bleeding and will not bleed from any wounds or attacks they have suffered up to this point.

> 🗒️️         Note: This augment always acts as Marque I for the purposes of determining cost, though you can learn this augment despite your skill in alchemy.

### Crafting Poisions

#### Poison Brewer
> Augments +2, DIY +1, Hit Points +4

> Alchemy Specialty

Poisons are rarely seen as the most savory tools of war; nonetheless, they remain ever popular. Poisons kill, disable, and bring great discomfort to those injected with them.

        Poisons are always a little difficult to find, requiring a tier 2 Cunning result to notice. A person can always volunteer to look at something to see if it is poisoned, such as trying to notice poison on a sword or in a drink.

        Poisons must either be consumed by the target or injected (as can happen on the battlefield, when a blade is coated in poison). The basic poison will activate within the target after the target has used 3 action points.

        Without spending any money, you can brew and maintain several poisons based on your current Do-It-Yourself (DIY) score. These poisons can then be upgraded with augments. You’ll learn 2 augments from this specialty, which can be selected under “poison augments” below. These augments have marques. At lower levels, you’ll start with Marque I augments. As your skill in Alchemy improves, your marques will increase. See the “Crafting” page at the beginning of this chapter for more information.

        Each poison can be upgraded with 3 augments. Sometimes an augment will take up multiple augment slots. For example, the “hallucinogenic” augment is worth 2 slots, so a poison only has 1 available slot for an augment after “hallucinogenic” has been applied.

#### Resisting Poisons 

All poisons can be resisted with a Brute roll. For every tier of Brute rolled above Tier 1, the marque of the poison is reduced by 1.

#### Number of Poisons you can Maintain  

Without needing to buy chemicals, you can brew some poisons entirely out of scraps. These poisons must be constantly maintained by you and stop working soon after leaving your care. You can brew and maintain a number of poisons based on your DIY score. You can brew new poisons or augment old ones during any period of downtime you have.

#### The Cost of Poisons 

If you need to brew a poison that you can’t create for free from your DIY score, you will need to buy the materials for it.

        Every augment will increase the price. The higher the marque, the greater the price. The market price for an augment can be found in the chart below.

If you are brewing the augment, you pay 1/5th the price, which is the same as if you were buying an augment one marque lower. (As in, the material cost for a Marque III augment is the market price fo a Marque II augment.) The material cost for a Marque 1 augment is 8 dukes.

#### Beta Poisons
> Augments +2, DIY +1, Hit Points +4

> Alchemy Specialty

> ❓ Requires: 4 skill points in Alchemy & Poison Brewer specialty

Your poisons are difficult to use but oh-so-effective. Your beta poisons have two more slots for you to place augments into.

        If anybody other than you attempts to use one of your beta poisons, they must succeed in rolling a science result one tier higher than the highest level marque you have on your poisons. If your poison has a Marque IV augment, it is impossible for them to use it (unless they can somehow obtain a tier result of 5 with their science attribute).

#### Prototype Poisons
> Augments +1, DIY +1, Hit Points +5

> Alchemy Specialty

> ❓ Requires: 16 skill points in Alchemy, Poison Brewer, & Beta Poisons specialties

You’ve perfected your beta poisons and made them user-friendly. Now anybody can use a poison that you designate as being a prototype.

### Poison Augments

#### Anti-Toxin Suppressor
>   Poison Augment

Anti-toxin suppressors are built into poisons specifically to target anti-toxins. If an anti-toxin is used against a poison that has a suppressor brewed into it, the suppressor will entirely negate the effects of the anti-toxin unless the anti-toxin is one marque higher than the suppressor.

|Marque| Effect|
|--|--|
|1|Negates Marque I anti-toxins|
|2|Negates Marque II anti-toxins|
|3|Negates Marque III anti-toxins|
|4|Negates Marque IV anti-toxins|

#### Blinding
>   Poison Augment

Takes up 2 Augment Slots on the Poison

This poison blinds the person, causing them to temporarily lose their eyesight. Being blind causes the target to take a -4 on accuracy and evade rolls. It lasts for an amount of time based on the marque.

|Marque| Effect|
|--|--|
|1|2 turns|
|2|4 turns|
|3|6 turns|
|4|8 turns|

#### Contortion
>   Poison Augment

Takes up 3 Augment Slots on the Poison

The target’s body seizes up and they fall to the ground, prone. While prone, you suffer a -1 on all combat rolls (accuracy, evade, strike, and defense) and cannot move more than 5 feet per turn. The target cannot willingly stand from prone until the target has used or forgone a specified number of action points, and if somebody lifts them, they immediately fall back down.

|Marque| Effect|
|--|--|
|1|2 action points|
|2|4 action points|
|3|6 action points|
|4|8 action points|

#### Disorienting
>   Poison Augment

A disorienting poison causes the person’s nerves to be shot, for them to swoon and be unable to focus. A disorienting poison disorients the target (causing them to lose 1 action point per turn) until the end of a specified number of turns.

|Marque| Effect|
|--|--|
|1|2 turn|
|2|4 turns|
|3|6 turns|
|4|8 turns|

#### Dizzying
>   Poison Augment

This poison dizzies the target, causing them to waver and fail to walk in a straight line. While dizzied, the target’s speed is cut in half (rounded down). The dizzying effect lasts until the end of a specified number of turns.

|Marque| Effect|
|--|--|
|1|2 turns|
|2|4 turns|
|3|6 turns|
|4|8 turns|

#### Hallucinogenic
>   Poison Augment

Takes up 2 Augment Slots on the Poison

A hallucinogenic poison causes the brain to spaz out and see things that are definitely not there (or so we think). A hallucinogenic poison forces the target to make a cunning roll every time he wants to take an action. A failure causes the target to spend one action point moving aimlessly (at the narrator’s discretion).

|Marque| Effect|
|--|--|
|1|Tier 2 Cunning results required to move for 1 turn|
|2|Tier 2 Cunning results required to make any actions for 2 turns|
|3|Tier 3 Cunning results required to make any actions for 3 turns|
|4|Tier 3 Cunning results required to make any actions for 4 turns|

#### Instant
>   Poison Augment

This poison now blossoms very quickly. Upon delivery being made, it occurs instantaneously. The poison acts as soon as it is delivered.

> 🗒️️         Note: This augment always acts as Marque II for the purposes of determining cost, though you can learn this augment despite your skill in alchemy.

#### Irresistible
>   Poison Augment

When a poison is made irresistible, the Brute result required to resist the poison increases. Characters must act as if their Brute roll to resist the blast was one tier lower.

> 🗒️️         Note: This augment always acts as marque II for the purposes of determining cost, though you can learn this augment despite your skill in alchemy.

#### Painful
>   Poison Augment

Your poison does what poisons do best - it deals damage to their hit points. The painful augment does straight hit point damage (though it is resistable as per normal).

|Marque| Effect|
|--|--|
|1|6 damage|
|2|12 damage|
|3|18 damage|
|4|24 damage|

#### Push-Back
>   Poison Augment

Takes up 2 Augment Slots on the Poison

Push back is a rather devious poison used specifically to counter enemies who rely on push potions (that is, potions that restore their hit points). For the hour after somebody fails to resist a poison with push-back, any push potion they take will deal damage to them instead of restoring their hit points. The damage dealt is equal to the amount of hit points that would have been restored, but this is now resistable with your Brute.

> 🗒️️         Note: This augment always acts as Marque III for the purposes of determining cost, though you can learn this augment despite your skill in alchemy.

#### Slow-Acting
>   Poison Augment

A slow-acting poison takes longer to occur and is therefore harder to trace back to the person who administered it. Within the scope of your marque, you determine when the effect kicks in. You make this decision when you create the poison, and cannot alter it thereafter.

|Marque| Effect|
|--|--|
|1|4 hours - 1 day|
|2|3 hours - 3 days|
|3|2 hours - 1 week|
|4|1 hour - 1 month|

#### Stunning
>   Poison Augment

The poison stuns the opponent, causing the opponent to respond slowly and lose action points. The target is stunned for the specified amount of action points.

|Marque| Effect|
|--|--|
|1|1 action point stunned|
|2|2 action points stunned|
|3|3 action points stunned|
|4|4 action points stunned|

#### Thirst
>   Poison Augment

The poison interacts with the water in the person’s body, making it almost impossible for the water to function normally. This quickly dehydrates the person, causing them to lose focus. Due to this, they take a penalty on accuracy and evade rolls until the end of three turns.

|Marque| Effect|
|--|--|
|1|-1 on accuracy and evade rolls|
|2|-2 on accuracy and evade rolls|
|3|-3 on accuracy and evade rolls|
|4|-4 on accuracy and evade rolls|

#### Undetectable
>   Poison Augment

When a poison is made undetectable, the Cunning tier required to find it is increased.

|Marque| Effect|
|--|--|
|1|Tier 3 Cunning result|
|2|Tier 3 Cunning result|
|3|Tier 4 Cunning result|
|4|Tier 4 Cunning result|

## Armsmith Skill
==============

So you want to craft arms, do you? Do you have what it takes to create weapons, armor, firearms and everything else that you and your fellows will need to slay villains and save the land? Is it in you to sweat away your days creating items of war, giving birth to items of destruction? The Armsmith skill will allow you to do just that, creating all sorts of weapons, firearms, armors, and worse. Much, much worse.

#### Belt Feeder
> Evade +2, Augments +1, Hit Points +7

> Armsmith Specialty

> 🪙 Cost: 2 AP to begin, 1 AP to continue during subsequent turns

You can feed in ammunition, allowing an adjacent gunman to keep firing without interruption. When you begin belt feeding, you select an adjacent ally. For that turn and every subsequent turn that you continue belt feeding (at the cost of 1 action point per turn), the targeted ally does not have to spend action points to ready their firearm. This only works on firearms that have a readying cost of 4 or less. If either you or your adjacent ally become separated, you must re-begin the belt feeding.  

#### Interchangeable Parts
> Augments +2, DIY +1, Hit Points +6

> Armsmith Specialty

> 🪙 Cost: 3 AP

Your weapons are designed so that their parts can be replaced and altered in the middle of battle. This gives the weapons additional slots for augments, but these augments are not always active. At any time, a person can switch out the augments for 3 action points, activating one augment but deactivating another augment. You may replace multiple augments at the same time all for the cost of 3 action points.

        This specialty works within the marque system. The amount of interchangeable slots the weapon has depends on the marque of its creator.
        
|Tier| Effect|
|--|--|
|1|1 interchangeable slot for an augment|
|2|2 interchangeable slots for augments|
|3|3 interchangeable slots for augments|
|4|4 interchangeable slots for augments|

#### Rapid Replacements
> Evade +1, DIY +1, Hit Points +6

> Armsmith Specialty

> 🪙 Cost: 2 AP (to replace) or 1 AP (to fix)

Your sniper’s rifle just got snapped in two, and your brawler just got his chain cut in half. No worries - you’re there to help. For two action points, you can replace any broken weapon that you created (with the same weapon) as long as the current wielder is adjacent to you. Furthermore, if a weapon’s damage class has been lowered for any reason, you can return it to optimum efficiency for one action point.  

#### Temporary Attachments
> Augments +2, DIY +1, Hit Points +5

> Armsmith Specialty

> 🪙 Cost: 1+ AP

Suddenly, in the heat of battle, your friend wants his sword to be sheathed in fire. Normally that would be insane. For you, though, that requires surprisingly little effort. You can apply an augment you know to a weapon you or an adjacent ally are wielding for just 1 action point. If the augment takes up multiple augment slots, it costs that many action points to attach (so an augment that would take up 2 augment slots would cost 2 action points to attach). The temporary attachment doesn’t last long: it will stop functioning at the end of your next turn (when your action points refresh).

        The weapon you are attaching the temporary augment to does not need to have free augment slots: this is a bonus augment that does not fit into a weapon’s normal maximum number of augments.  

#### Weapon Support
> Accuracy +1, Augments +1, Hit Points +6

> Armsmith Specialty

> 🧘 Stance (costs 1 AP to enter)

You provide support repairs and guidance to adjacent allies on the battlefield, tweaking their weapons as they mow down opponents. Any ally using a weapon that you either created or augmented gains a bonus to their accuracy as long as they are adjacent to you while in your Weapon Support stance. The bonus is +1 plus an additional +1 per 4 skill points you have in Armsmith (so +2 at 4 skill points, +3 at 8 skill points, and so forth).

        If you have the Rapid Replacement specialty, an adjacent ally’s weapon cannot be broken so long as you remain adjacent to them and in the Weapon Support stance.

### Crafting Firearms & Crossbows
-----------------------------

#### Gunsmith
> Augments +2, DIY +1, Hit Points +4

> Armsmith Specialty

You can now create new firearms and upgrade them. These firearms can be of any size, type, or material. You can craft any firearm from the light peashooters to the super-heavy rifles.

        Without spending any money, you can build and maintain several firearms based on your current Do-It-Yourself (DIY) score. These firearms can then be upgraded with augments. You’ll learn 2 augments from this specialty, which can be selected under “firearm & crossbow augments” below. These augments have marques. At lower levels, you’ll start with Marque I augments. As your skill in Armsmith improves, your marques will increase. See the “Crafting” page at the beginning of this chapter for more information.

        Each firearm can be upgraded with 3 augments. Sometimes an augment will take up multiple augment slots. For example, the “damaging” augment is worth 2 slots, so a firearm only has 1 more available slot for an augment after “damaging” has been applied.

#### Number of Firearms you can Maintain  

Without needing to buy pieces or parts, you can build some firearms entirely out of scraps. These firearms must be constantly maintained by you and stop working soon after leaving your care. You can build and maintain a number of firearms based on your DIY score. You can build new firearms or augment old ones during any period of downtime you have.

#### The Cost of Firearms 

If you need to build a firearm that you can’t build for free from your DIY score, you will need to buy the materials for it.

        A firearm will have a base materials cost. It is 1/5th the market price.

        Every augment will increase the price. The higher the marque, the greater the price. The market price for an augment can be found in the chart below.

If you are building the augment, you pay 1/5th the price, which is the same as if you were buying an augment one marque lower. (As in, the material cost for a Marque III augment is the market price for a Marque II augment.) The material cost for a Marque 1 augment is 5 princes.

#### Beta Firearms
> Augments +2, DIY +1, Hit Points +4

> Armsmith Specialty

> ❓ Requires: 4 skill points in Armsmith & Gunsmith specialty

Your firearms are overly complex, but they can accomplish quite a bit. Such firearms have two more slots for you to place augments into.

        If anybody other than you attempts to use one of your beta firearms, they must succeed in rolling a science result one tier higher than the highest level marque you have on your firearm. If your firearm has a Marque IV augment, it is impossible for them to use it (unless they can somehow obtain a tier result of 5 with their Sciences attribute).

#### Prototype Firearms
> Augments +1, DIY +1, Hit Points +6

> Armsmith Specialty

> ❓ Requires: 16 skill points in Armsmith, Gunsmith, & Beta Firearms specialties

You’ve perfected your beta firearms and made them user-friendly. Now anybody can use a firearm that you designate as being a prototype.  

#### Crossbow Craftsman
> Augments +2, DIY +1, Hit Points +4

> Armsmith Specialty

You can now create new crossbows and upgrade them. These crossbows can be of any size, type, or material. You can craft any crossbow from the light hand-crossbow to the super-heavy ballistas.

        Without spending any money, you can build and maintain several crossbows based on your current Do-It-Yourself (DIY) score. These crossbows can then be upgraded with augments. You’ll learn 2 augments from this specialty, which can be selected under “firearm & crossbow augments” below. These augments have marques. At lower levels, you’ll start with Marque I augments. As your skill in Armsmith improves, your marques will increase. See the “Crafting” page at the beginning of this chapter for more information.

        Each crossbow can be upgraded with 3 augments. Sometimes an augment will take up multiple augment slots. For example, the “damaging” augment is worth 2 slots, so a crossbow only has 1 more available slot for an augment after “damaging” has been applied.

#### Number of Crossbows you can Maintain  

Without needing to buy pieces or parts, you can build some crossbows entirely out of scraps. These crossbows must be constantly maintained by you and stop working soon after leaving your care. You can build and maintain a number of crossbows based on your DIY score. You can build new crossbows or augment old ones during any period of downtime you have.

#### The Cost of Crossbows 

If you need to build a crossbows that you can’t build for free from your DIY score, you will need to buy the materials for it.

        A crossbow will have a base materials cost. It is 1/5th the market price.

        Every augment will increase the price. The higher the marque, the greater the price. The market price for an augment can be found in the chart below.

If you are building the augment, you pay 1/5th the price, which is the same as if you were buying an augment one marque lower. (As in, the material cost for a Marque III augment is the market price for a Marque II augment.) The material cost for a Marque 1 augment is 5 princes.

#### Beta Crossbows
> Augments +2, DIY +1, Hit Points +4

> Armsmith Specialty

> ❓ Requires: 4 skill points in Armsmith & Crossbow Crafter specialty

Your crossbows are overly complex, but they can accomplish quite a bit. Such crossbows have two more slots for you to place augments into.

        If anybody other than you attempts to use one of your beta crossbows, they must succeed in rolling a science result one tier higher than the highest level marque you have on your crossbow. If your crossbow has a Marque IV augment, it is impossible for them to use it (unless they can somehow obtain a tier result of 5 with their Sciences attribute).

#### Prototype Crossbows
> Augments +1, DIY +1, Hit Points +6

> Armsmith Specialty

> ❓ Requires: 16 skill points in Armsmith, Crossbow Crafter, & Beta Crossbows specialties

You’ve perfected your beta crossbows and made them user-friendly. Now anybody can use a crossbow that you designate as being a prototype.  

### Firearm & Crossbow Augments

#### Accurate
>   Firearm & Crossbow Augment

Fine attention has been placed on the quality of your weapon. The user gains a bonus to accuracy with the weapon.

|Marque| Effect|
|--|--|
|1|+1|
|2|+2|
|3|+3|
|4|+4|

#### Automatic Reload
>   Firearm & Crossbow Augment

The crossbow or firearm has a fast reloading mechanism, allowing it to be reloaded much more quickly than normal.

|Marque| Effect|
|--|--|
|1|1 less AP to reload|
|2|1 less AP to reload|
|3|2 less AP to reload|
|4|2 less AP to reload|

#### Bipod
>   Firearm & Crossbow Augment

You can set up a bipod to level your weapon on. Setting up the bipod requires 2 action points, and then pulling it back up so that you can move again requires 1 action point. While the bipod is set up, however, you gain a bonus on your accuracy.

|Marque| Effect|
|--|--|
|1|+1|
|2|+2|
|3|+3|
|4|+4|

#### Collapsible
>   Firearm & Crossbow Augment

Sometimes discretion is the better part of not having your weaponry confiscated, so you create a clever collapsing mechanism for your weapons which makes them easier to conceal. Any weapon this is applied to can be broken down for 3 action points and re-assembled for 3 action points. It is treated, for purposes of concealment, as being smaller than it is, but only when broken down.

|Marque| Effect|
|--|--|
|1|1 category smaller|
|2|2 categories smaller|
|3|3 categories smaller|
|4|4 categories smaller|

#### Crank-Free
>   Firearm Augment

> ❓ Requires: Augmented with Rotating Barrels

Converting your firearm to be truly automatic, the crank-free firearm no longer increases the need of hands - a one-handed firearm is still one-handed, and a two-handed firearm is still just two-handed.  

> 🗒️️         Note: This augment always acts as Marque II for the purposes of determining cost, though you can learn this augment despite your skill in Armsmith.

#### Custom
>   Firearm & Crossbow Augment

This weapon was designed to be used by one person and one person only. That person must be designated at the time of the weapon’s crafting. If anybody else attempts to use the custom weapon, they suffer a penalty on all accuracy and strike rolls with it.

|Marque| Effect|
|--|--|
|1|-3|
|2|-6|
|3|-9|
|4|-12|

#### Damaging
>   Firearm & Crossbow Augment

Takes up 2 Augment Slots on a Firearm or Crossbow

Your weapon is larger but lighter, dealing extra damage with each shot.

|Marque| Effect|
|--|--|
|1|+1 damage class|
|2|+2 damage class|
|3|+3 damage class|
|4|+4 damage class|

#### Deflecting
>   Firearm & Crossbow Augment

You can use this weapon like a shield, allowing you to deflect incoming attacks (gaining a +4 to evade in exchange for 1 reflexive action point).

> 🗒️️         Note: This augment always acts as Marque II for the purposes of determining cost, though you can learn this augment despite your skill in Armsmith.

#### Delivery
>   Crossbow Augment

You can launch an alchemic potion, explosives, or other items through your firearm. If the item shot is friendly (as in, you’re not trying to hit the target), you may shoot the item next to them, and they may pick it up for 1 action point. Regardless, a delivery weapon’s range is cut be a small portion whenever it is being used to deliver an item.

|Marque| Effect|
|--|--|
|1|-75 feet|
|2|-50 feet|
|3|-25 feet|
|4|no penalty|

#### Easily Altered
>   Firearm & Crossbow Augment

> ❓ Requires: Weapon to be made with Interchangeable Parts

The weapon is easily altered, allowing its interchangeable parts to be used and switched around much more quickly than normal. The easily altered augment is a permanent affixture of the weapon, however, and cannot be activated or deactivated with interchangeable parts.

|Marque| Effect|
|--|--|
|1|2 AP to interchange parts|
|2|2 AP to interchange parts|
|3|1 AP to interchange parts|
|4|1 AP to interchange parts|

#### Gnome-Sized
>   Firearm & Crossbow Augment

> ❓ Requires: placed on light weapon

You’ve been able to shrink the weapon down so that a gnome will be able to conceal it.

> 🗒️️         Note: This augment always acts as Marque I for the purposes of determining cost, though you can learn this augment despite your skill in Armsmith.

#### Horrifying
>   Firearm & Crossbow Augment

Takes up 2 Augment Slots on a Firearm or Crossbow

The weapon is so twisted and wicked-looking that it could strike fear into the heart of even the bravest of warriors. When first seeing the weapon, all enemies must make a spirit resist or suffer from tier 1 fear. At any time a character can spend an action point to attempt another resist roll. A single person can only evoke the effect of one horrifying weapon per combat.

|Marque| Effect|
|--|--|
|1|Tier 2 Spirit result to resist|
|2|Tier 3 Spirit result to resist|
|3|Tier 4 Spirit result to resist|
|4|Irresistable (unless they can get a Tier 5 Spirit resist)|

#### Location Seeking
>   Firearm & Crossbow Augment

When crafting this weapon designate a called shot. The weapon seems to guide itself toward that specific called on your victim’s body with the greatest of ease. Called shots to the designated location do not require an additional action point when made with this weapon. However, attacking any other called shot location with this weapon can be problematic, as it was built with one cause in mind. (This penalty does not apply to normal, unspecified attacks.)

|Marque| Effect|
|--|--|
|1|-8 on accuracy rolls when attacking a different called shot location|
|2|-6 on accuracy rolls when attacking a different called shot location|
|3|-4 on accuracy rolls when attacking a different called shot location|
|4|-2 on accuracy rolls when attacking a different called shot location|

> 🗒️️ Note: When crafting the augment, the word “location” should be replaced with the specified called shot, such as “eye seeking” or “torso seeking.”

#### Luminous
>   Firearm & Crossbow Augment

Your weapon glows. Perhaps you strung lights along its barrel, gave it a glow-in-the-dark coating, or made your weapon transparent with a light set inside. The light extends outwards from your weapon a number of feet determined by the marque of this augment. For 1 action point, you may turn it on, off, or dim it.

|Marque| Effect|
|--|--|
|1|25 feet|
|2|50 feet|
|3|100 feet|
|4|200 feet|

#### Reinforced
>   Firearm & Crossbow Augment

You build your weapon solidly, giving it little room to break on the battlefield. When somebody attempts to sunder the reinforced weapon, it acts as if it is several size categories larger than it is. Once these “reinforced” size categories are gone, then its actual damage begins to decrease.

|Marque| Effect|
|--|--|
|1|This augment can be applied multiple times, its effect stacking.|
|2|1 reinforced size category|
|3|2 reinforced size categories|
|4|3 reinforced size categories|

        4 reinforced size categories

#### Rotating Barrels
>   Firearm Augment

Creating what comes close to being a fully automatic weapon, a firearm with multiple rotating barrels uses a crank to fire. Using rotating barrels increases the need of hands - a one-handed firearm now requires two hands, and a two-handed firearm now requires you to be in a “firing position.” A rotating multi-barrel firearm eliminates the reload time of the firearm, but at a price to accuracy.

        This augment can only be placed on firearms that use ammunition and have a reload time of 2 action points or less.

|Marque| Effect|
|--|--|
|1|-5 accuracy|
|2|-3 accuracy|
|3|-1 accuracy|
|4|No accuracy penalty|

#### Scope
>   Firearm & Crossbow Augment

The range on your firearm increases greatly. This augment can be applied multiple times, with its increases stacking.

|Marque| Effect|
|--|--|
|1|+50 feet|
|2|+100 feet|
|3|+200 feet|
|4|+300 feet|

#### Signature Weapon
>   Firearm & Crossbow Augment

Takes up 2 Augment Slots on a Firearm or Crossbow

> ❓ Requires: placed on weapon with Custom augment

Be it a family heirloom or just your perfectly customized weapon, the very sight of it invigorates you. As long as your weapon is in hand there is still hope. You recover a small amount of hit points when your action points refresh. This augment only affects the person this weapon was customized for.

|Marque| Effect|
|--|--|
|1|recover 1 hit point|
|2|recover 2 hit points|
|3|recover 3 hit points|
|4|recover 4 hit points|

#### Silent
>   Firearm & Crossbow Augment

This weapon is whispering death. It makes almost no sound when shot or reloaded, making it almost impossible for people to figure out where it is by sound alone. Any time anybody is attempting to figure out where the weapon was shot from based on sound must make a tier result with their cunning.

|Marque| Effect|
|--|--|
|1|Tier 2|
|2|Tier 3|
|3|Tier 4|
|4|Impossible (or Tier 5)|

#### Small Choke
>   Firearm & Crossbow Augment

Introducing a small choke to a shotgun increases the effective distance of its shot. A small choke doubles the range of the shot fired from it. Other choices of ammunition fired from a small choke suffer a -3 accuracy.  

> 🗒️️         Note: This augment always acts as Marque I for the purposes of determining cost, though you can learn this augment despite your skill in Armsmith.

### Firearm & Crossbow Accessories

You can learn weapon accessories just like augments. However, firearm & crossbow accessories do not take up any augment slots and can only be applied to a weapon once. Each accessory has its own cost associated with it.

Chained-Grip

Firearm & Crossbow Accessory

The user keeps a chain attached to both their wrist and the weapon, making it difficult - if not impossible - to be disarmed. Whenever the target of a disarm (called shot to the hand), the user gains a bonus to the resist roll.

|Marque| Effect|
|--|--|
|1|+2 to resist being disarmed|
|2|+4 to resist being disarmed|
|3|+6 to resist being disarmed|
|4|+8 to resist being disarmed|

### Crafting Melee Weapons & Throwing Weapons
-----------------------------------------

#### Weapon Smith
> Augments +2, DIY +1, Hit Points +4

> Armsmith Specialty

You can create new melee weapons and throwing weapons and upgrade them. These weapons can be of any size, type, or material. You can craft anything from a metal stiletto to a wooden pike to a sword made from bone.

        Without spending any money, you can build and maintain several weapons based on your current Do-It-Yourself (DIY) score. These weapons can then be upgraded with augments. You’ll learn 2 augments from this specialty, which can be selected under “weapon augments” below. These augments have marques. At lower levels, you’ll start with Marque I augments. As your skill in Armsmith improves, your marques will increase. See the “Crafting” page at the beginning of this chapter for more information.

        Each weapon can be upgraded with 3 augments. Some materials can only be upgraded twice (like wooden weapons) or just once (like organic ones). Sometimes an augment will take up multiple augment slots. For example, the “damaging” augment is worth 2 slots, so a weapon only has 1 more available slot for an augment after “damaging” has been applied.

#### Number of Weapons you can Maintain  

Without needing to buy pieces or parts, you can build some weapons entirely out of scraps. These weapons must be constantly maintained by you and stop working soon after leaving your care. You may build and maintain a number of weapons based on your DIY score. You may build new weapons or augment old ones during any period of downtime you have.

#### The Cost of Weapons 

If you need to build a weapon that you can’t build for free from your DIY score, you will need to buy the materials for it.

        A weapon will have a base materials cost. It is 1/5th the market price.

        Every augment will increase the price. The higher the marque, the greater the price. The market price for an augment can be found in the chart below.

If you are building the augment, you pay 1/5th the price, which is the same as if you were buying an augment one marque lower. (As in, the material cost for a Marque III augment is the market price fo a Marque II augment.) The material cost for a Marque 1 augment is 5 princes.

#### Beta Weapons
> Augments +2, DIY +1, Hit Points +4

> Armsmith Specialty

> ❓ Requires: 4 skill points in Armsmith & Weaponsmith specialty

Your weapons are overly complex, but they can accomplish quite a bit. Such weapons can be upgraded with 2 more augments (bringing the total for metal melee weapons up to 5 augmentable slots).

        If anybody other than you attempts to use one of your beta weapons, they must succeed in rolling a Sciences result one tier higher than the highest marque you have on your weapon. If your weapon has a Marque IV augment, it is impossible for them to use it (unless they can somehow obtain a tier result of 5 with their Sciences attribute).

#### Prototype Weapons
> Augments +1, DIY +1, Hit Points +5

> Armsmith Specialty

> ❓ Requires: 16 skill points in Armsmith, Weapon Smith, & Beta Weapons specialties

You’ve perfected your beta weapons and made them user-friendly. Now anybody can use a weapon that you designate as being a prototype.  

#### Interchangeable Parts
> Augments +2, DIY +1, Hit Points +6

> Armsmith Specialty

> ❓ Requires: Weapon Smith specialty

> 🪙 Cost: 3 AP

Your weapons are designed so that their parts can be replaced and altered in the middle of battle. This gives the weapons additional slots for augments, but these augments are not always active. At any time, a person may switch out the augments for 3 action points, activating one augment but deactivating another augment. You may replace multiple augments at the same time all for the cost of 3 action points.

        This specialty works within the marque system. The amount of interchangeable slots the weapon has depends on the marque of its creator.
        
|Tier| Effect|
|--|--|
|1|1 interchangeable slot for an augment|
|2|2 interchangeable slots for augments|
|3|3 interchangeable slots for augments|
|4|4 interchangeable slots for augments|

### Weapon Augments

#### Accurate
>   Weapon Augment

Fine attention has been placed on the quality of your weapon. The user gains a bonus to accuracy with the weapon.

|Marque| Effect|
|--|--|
|1|+1|
|2|+2|
|3|+3|
|4|+4|

#### Aerodynamic
>   Weapon Augment

You can make your melee weapons into perfectly good throwing weapons. If you apply this augment onto one of your melee weapons, you may throw it like a weapon designed for throwing.

> 🗒️️         Note: This augment always acts as Marque I for the purposes of determining cost, though you can learn this augment despite your skill in Armsmith.

#### Bone-Shattering
>   Weapon Augment

By increasing the weapon’s density, the weapon allows its wielder to make more effective called shots against his victims.

|Marque| Effect|
|--|--|
|1|+1 strike against all called shot locations|
|2|+3 strike against all called shot locations|
|3|+5 strike against all called shot locations|
|4|+7 strike against all called shot locations|

#### Chainsaw
>   Weapon Augment

> ❓ Requires: placed on a melee weapon

> 🪙 Cost: 1 AP (after a successful attack)

Your weapon has a deadly, barbed spinning edge. If you make a successful attack with a chainsaw weapon that deals damage, you may spend 1 action point (as many times as you have AP left in your turn) to deal additional unsoakable damage as the chainsaw continues to rip into the person.

|Marque| Effect|
|--|--|
|1|1 extra damage|
|2|2 extra damage|
|3|3 extra damage|
|4|4 extra damage|

#### Collapsible
>   Weapon Augment

Sometimes discretion is the better part of not having your weaponry confiscated, so you create a clever collapsing mechanism for your weapons which makes them easier to conceal. Any weapon this is applied to can be broken down for 3 acion points and re-assembled for 3 action points. It is treated, for purposes of concealment, as being smaller than it is, but only when broken down.

|Marque| Effect|
|--|--|
|1|1 category smaller|
|2|2 categories smaller|
|3|3 categories smaller|
|4|4 categories smaller|

#### Cryothermal
>   Weapon Augment

> ⬇️ Resist: Brute (marques down)

> 🪙 Cost: 1 AP reflexively during an attack

Your weapon leaks a subzero liquid that freezes the opponent upon contact. When you make an attack with a cryothermal weapon, you may spend 1 action point to make the target frosty. Unless your victim can resist the entire effect, they become chilled, making them shiver when they attack.

|Marque| Effect|
|--|--|
|1|Victim suffers a -2 on strike rolls until your next turn|
|2|Victim suffers a -4 on strike rolls until your next turn|
|3|Victim suffers a -6 on strike rolls until your next turn|
|4|Victim suffers a -8 on strike rolls until your next turn|

#### Custom
>   Weapon Augment

This weapon was designed to be used by one person and one person only. That person must be designated at the time of the weapon’s crafting. If anybody else attempts to use the custom weapon, they suffer a penalty on all accuracy and strike rolls with it.

|Marque| Effect|
|--|--|
|1|-3|
|2|-6|
|3|-9|
|4|-12|

#### Damaging
>   Weapon Augment

Takes up 2 Augment Slots on a Weapon

Your weapon is larger but lighter, dealing extra damage with each blow.

|Marque| Effect|
|--|--|
|1|+1 damage class|
|2|+2 damage class|
|3|+3 damage class|
|4|+4 damage class|

#### Deflecting
>   Weapon Augment

You may use this weapon like a shield, allowing you to deflect incoming attacks (gaining a +4 to evade in exchange for 1 reflexive action point).

> 🗒️️         Note: This augment always acts as Marque I for the purposes of determining cost, though you can learn this augment despite your skill in Armsmith.

#### Easily Altered
>   Weapon Augment

> ❓ Requires: weapon to be made with Interchangeable Parts

The weapon is easily altered, allowing its interchangeable parts to be used and switched around much more quickly than normal. The easily altered augment is a permanent affixture of the weapon, however, and cannot be activated or deactivated with interchangeable parts.

|Marque| Effect|
|--|--|
|1|2 AP to interchange parts|
|2|2 AP to interchange parts|
|3|1 AP to interchange parts|
|4|1 AP to interchange parts|

#### Electrical
>   Weapon Augment

A small metal coil runs down the length of your weapon, leading to a power source that electrifies it. After landing a hit, your victim is shocked for a small amount of unsoakable damage. If the opponent is in metal armor or submersed in water, they take twice the damage. (If they are both in water and metal armor, they take four times the damage.)

|Marque| Effect|
|--|--|
|1|1 unsoakable electrical damage|
|2|2 unsoakable electrical damage|
|3|3 unsoakable electrical damage|
|4|4 unsoakable electrical damage|

#### Electrical Archs
>   Weapon Augment

> ❓ Requires: placed on weapon with the Electrical augment

The electricity that runs down your blade now jumps about, hitting multiple victims. Once you’ve hit an opponent, the electricity will arch to another opponent within 10 feet. Roll your accuracy versus their evade. If you meet or exceed it, the second opponent will be hit by the arch (dealing the normal damage for your electricity). The electricity will arch a number of times based on the marque of the augment. If, at any point, you miss a target, the electricity will not continue to jump.

|Marque| Effect|
|--|--|
|1|Electricity jumps once|
|2|Electricity jumps twice|
|3|Electricity jumps thrice|
|4|Electricity jumps quatrice|

#### Gas Leaks
>   Weapon Augment

Cost to Activate: 1 AP reflexively during an attack

Your weapon is hollow and covered in pores. You can fill your weapon with deadly gases that you release when you hit your opponent. You may choose to release multiple gases at the same time, but each one costs 1 action point. The augment’s marque determines how many gases it can hold. It costs 3 action points to add refill a single canister of gas.

|Marque| Effect|
|--|--|
|1|Holds up to 1 dose|
|2|Holds up to 2 doses|
|3|Holds up to 3 doses|
|4|Holds up to 4 doses|

> 🗒️️ Note: Unless you have doses of alchemical gas loaded into the weapon, this augment grants no bonuses. See Alchemy for the crafting of gases.

#### Gnome-Sized
>   Weapon Augment

> ❓ Requires: placed on light weapon

You’ve been able to shrink the weapon down so that a gnome will be able to conceal it.

> 🗒️️         Note: This augment always acts as Marque I for the purposes of determining cost, though you can learn this augment despite your skill in Armsmith.

#### Horrifying
>   Weapon Augment

Takes up 2 Augment Slots on a Weapon

The weapon is so twisted and wicked-looking that it could strike fear into the heart of even the bravest of warriors. When first seeing the weapon, all enemies must make a spirit resist or suffer from tier 1 fear. At any time, a character may spend an action point to attempt another resist roll. A single person can only evoke the effect of one horrifying weapon per combat.

|Marque| Effect|
|--|--|
|1|Tier 2 Spirit result to resist|
|2|Tier 3 Spirit result to resist|
|3|Tier 4 Spirit result to resist|
|4|Irresistable (unless they can get a Tier 5 spirit resist)|

#### Inspiring
>   Weapon Augment

Takes up 2 Augment Slots on a Weapon

This weapon is symbolic to your allies, inspiring them to fight onwards when it’s in your hand. All allies that can see your weapon recover a small amount of hit points when you’re wielding it and your action points refresh. This does not stack with other inspiring weapons.

|Marque| Effect|
|--|--|
|1|allies recover 1 hit point|
|2|allies recover 2 hit points|
|3|allies recover 2 hit points|
|4|allies recover 3 hit points|

#### Jackhammer
>   Weapon Augment

Takes up 2 Augment Slots on a Weapon

A small, unbalanced sphere spins in the hilt of the weapon, causing it to vibrate wildly. This allows the weapon to deal tremendous damage to machines, vehicles with moving parts, and stationary structures.

|Marque| Effect|
|--|--|
|1|+2 damage class against automatons, vehicles, clockworks, and structures|
|2|+4 damage class against automatons, vehicles, clockworks, and structures|
|3|+6 damage class against automatons, vehicles, clockworks, and structures|
|4|+8 damage class against automatons, vehicles, clockworks, and structures|

#### Lightning Pulsing
>   Weapon Augment

Takes up 2 Augment Slots on a Weapon

Cost to Activate and Deactivate: 1 AP

Your weapon pulses with lightning, just looking for a target to vaporize. Lightning is a powerful force that is hard to control. It causes the user to take a penalty on their accuracy roll, but the attack is unsoakable if the opponent is wet or in metal armor. The lightning pulse can be turned on or turned off for 1 action point. While it is on, the penalty is always in effect.

|Marque| Effect|
|--|--|
|1|-8 on the accuracy roll|
|2|-6 on the accuracy roll|
|3|-4 on the accuracy roll|
|4|-2 on the accuracy roll|

#### Location Seeking
>   Weapon Augment

When crafting this weapon designate a called shot. The weapon seems to guide itself toward that specific called shot on your victim’s body with the greatest of ease. Called shots to the designated location do not require an additional action point when made with this weapon. However, attacking any other called shot location with this weapon can be problematic, as it was built with one cause in mind. (This penalty does not apply to normal, unspecified attacks.)

|Marque| Effect|
|--|--|
|1|-8 on accuracy rolls when attacking a different called shot location|
|2|-6 on accuracy rolls when attacking a different called shot location|
|3|-4 on accuracy rolls when attacking a different called shot location|
|4|-2 on accuracy rolls when attacking a different called shot location|

> 🗒️️ Note: When crafting the augment, the word “location” should be replaced with the specified called shot, such as “eye seeking” or “torso seeking.”

#### Long Archs
>   Weapon Augment

> ❓ Requires: placed on a weapon with Electrical Archs augment

Generally, electrical archs will jump from one target to another at a very small distance. The electricity will simply fizzle out if it goes too far. But with longer archs, your weapon’s electricity will take great leaps across the battlefield.

|Marque| Effect|
|--|--|
|1|Can arch up to 25 feet|
|2|Can arch up to 50 feet|
|3|Can arch up to 75 feet|
|4|Can arch up to 100 feet|

#### Luminous
>   Weapon Augment

Your weapon glows. Perhaps you strung lights along its blade, gave it a glow-in-the-dark coating, or made your weapon transparent with a light set inside. The light extends outwards from your weapon a number of feet determined by the marque of this augment. For 1 action point, you may turn it on, off, or dim it.

|Marque| Effect|
|--|--|
|1|25 feet|
|2|50 feet|
|3|100 feet|
|4|200 feet|

#### Powerful
>   Weapon Augment

Your weapon is stronger than others, be it due to a sharper blade, large striking side, or a heftier head. Regardless, it gives the user a bonus on their strike rolls.

|Marque| Effect|
|--|--|
|1|+2 on strike|
|2|+4 on strike|
|3|+6 on strike|
|4|+8 on strike|

#### Propelled
>   Weapon Augment

> ❓ Requires: placed on throwing weapons

The weapon sails through the air, being easy to throw and going much further than its mundane counterparts. An easily thrown weapon has a farther range.

|Marque| Effect|
|--|--|
|1|+20 feet|
|2|+30 feet|
|3|+40 feet|
|4|+60 feet|

#### Reach
>   Weapon Augment

You extend the weapon so that it can reach farther.

|Marque| Effect|
|--|--|
|1|5 feet farther|
|2|5 feet farther|
|3|10 feet farther|
|4|10 feet farther|

#### Reinforced
>   Weapon Augment

You build your weapon solidly, giving it little room to break on the battlefield. When somebody attempts to sunder the reinforced weapon, it acts as if it is several size categories larger than it is. Once these “reinforced” size categories are gone, then its actual damage begins to decrease.

|Marque| Effect|
|--|--|
|1|1 reinforced size category|
|2|2 reinforced size categories|
|3|3 reinforced size categories|
|4|4 reinforced size categories|

#### Returning
>   Weapon Augment

> ❓ Requires: placed on throwing weapon

You can throw your weapon, and it will return to you. Due to this, the range you can throw it is decreased, but you will always have the weapon on hand.

|Marque| Effect|
|--|--|
|1|-30 feet|
|2|-20 feet|
|3|-10 feet|
|4|no ranged penalty|

#### Signature Weapon
>   Weapon Augment

Takes up 2 Augment Slots on a Weapon

> ❓ Requires: placed on weapon with Custom augment

Be it a family heirloom or just your perfectly customized weapon, the very sight of it invigorates you. As long as your weapon is in hand there is still hope. You recover a small amount of hit points when your action points refresh. This augment only affects the person this blade was customized for.

|Marque| Effect|
|--|--|
|1|recover 1 hit point|
|2|recover 2 hit points|
|3|recover 3 hit points|
|4|recover 4 hit points|

#### Skullsmasher
>   Weapon Augment

This weapon has a specially weighted spike designed to cause severe brain trauma when striking against an opponent’s head or eyes.

|Marque| Effect|
|--|--|
|1|+2 strike when making a called shot to the head or eyes|
|2|+4 strike when making a called shot to the head or eyes|
|3|+6 strike when making a called shot to the head or eyes|
|4|+8 strike when making a called shot to the head or eyes|

#### Specialized
>   Weapon Augment

> ❓ Requires: placed on weapon with Custom augment

Be it from pressurized spikes in its grip or an unearthly balance, when a person attempts to wield the weapon, it causes them harm. The person the weapon was customized for is immune to this effect.  

|Marque| Effect|
|--|--|
|1|The weapon causes its user 2 unsoakable damage with each use|
|2|The weapon causes its user 4 unsoakable damage with each use|
|3|The weapon causes its user 6 unsoakable damage with each use|
|4|The weapon causes its user 8 unsoakable damage with each use|

#### Static
>   Weapon Augment

> ⬇️ Resist: Brute (marques down)

Cost to Activate: 1 AP reflexively

You can make sparks fly off your weapon as you bring it in to decimate your foe. For 1 action point, you can make your attack full of static, causing the target to lose their ability to react as quickly. Meanwhile, their hair also stands on end.

|Marque| Effect|
|--|--|
|1|Victim suffers a -1 on evade rolls until your next turn|
|2|Victim suffers a -2 on evade rolls until your next turn|
|3|Victim suffers a -3 on evade rolls until your next turn|
|4|Victim suffers a -4 on evade rolls until your next turn|

#### Streamlined
>   Weapon Augment

Takes up 3 Augment Slots on a Weapon

> ❓ Requires: placed on light or medium weapon

Made of only the lightest and highest quality parts, this weapon has feels lighter than air. All attacks made with this weapon cost 1 action point.

|Marque| Effect|
|--|--|
|1|-3 damage class|
|2|-2 damage class|
|3|-1 damage class|
|4|no damage class penalty|

#### Tangling
>   Weapon Augment

> ❓ Requires: placed on flexible weapon

The whip is specially designed to make grabs against the opponent. Whenever used, the whip gains a bonus on all rolls to prevent the target from resisting the grab.

|Marque| Effect|
|--|--|
|1|+2|
|2|+4|
|3|+6|
|4|+8|

#### Pyrothermal
>   Weapon Augment

> ⬇️ Resist: Brute (marques down)

Cost to Activate: 1 AP reflexively during an attack

Your weapon heats up, burning the target. When you attack, you may spend 1 action point to make it pyrothermal. If it hits, the attack burns its target should they fail to resist against your melee attack. These burns last until the victim’s next breather, when they can be treated.

|Marque| Effect|
|--|--|
|1|Victim suffers Tier 1 burns (-1 on defense rolls)|
|2|Victim suffers Tier 2 burns (-3 on defense rolls)|
|3|Victim suffers Tier 3 burns (-5 on defense rolls)|
|4|Victim suffers Tier 4 burns (-7 on defense rolls)|

#### Combustion
>   Weapon Augment

Takes up 3 Augment Slots on a Weapon

> ⬇️ Resist: Dexterity (marques down)

Cost to Activate: 1 AP reflexively during an attack

Fire lashes out around your weapon, catching those struck with it on fire. When you hit an opponent, you can spend 1 action point in order to attempt to catch them on fire. The target may resist with their Dexterity (lowering the marques of the combustion).

|Marque| Effect|
|--|--|
|1|The victim catches on Tier 1 fire|
|2|The victim catches on Tier 2 fire|
|3|The victim catches on Tier 2 fire|
|4|The victim catches on Tier 3 fire|

#### Burn Trail
>   Weapon Augment

> ❓ Requires: placed on weapon with the Combustion augment

Any person attempting to physically aid a victim of your Combustion must first make a Dexterity resist that equals the marque of your Combustion augment or be set on fire themselves.

> 🗒️️         Note: This augment always acts as Marque III for the purposes of determining cost, though you can learn this augment despite your skill in Armsmith.

#### Everburning
>   Weapon Augment

> ❓ Requires: placed on weapon with the Combustion augment

Despite your victim’s best efforts to put themselves out, they seem to continue burning. When catching things on fire through the use of a combustion attack, extra action points are required to put out the fire.

|Marque| Effect|
|--|--|
|1|1 extra AP|
|2|2 extra AP|
|3|3 extra AP|
|4|4 extra AP|

### Weapon Accessories

You can learn weapon accessories just like augments. However, weapon accessories do not take up any augment slots and can only be applied to a weapon once. Each weapon accessory has its own cost associated with it.

Alchemic Tube

Weapon Accessory

The weapon has a container attached to it and a rivet or channel that allows the weapon to strike with poison or another alchemical substance. The container can hold one usage of the substance. After its use, it takes some action points to snap a new alchemical substance into.

|Marque| Effect|
|--|--|
|1|3 AP|
|2|2 AP|
|3|1 AP|
|4|1 AP reflexively|

Chained-Grip

Weapon Accessory

The user keeps a chain attached to both their wrist and the weapon, making it difficult - if not impossible - to be disarmed. Whenever the target of a disarm (called shot to the hand), the user gains a bonus to the resist roll.

|Marque| Effect|
|--|--|
|1|+2 to resist being disarmed|
|2|+4 to resist being disarmed|
|3|+6 to resist being disarmed|
|4|+8 to resist being disarmed|

### Crafting Bows
-------------

#### Bowyer
> Augments +2, DIY +1, Hit Points +4

> Armsmith Specialty

You can now create new bows of any size and upgrade them.

        Without spending any money, you can build and maintain several bows based on your current Do-It-Yourself (DIY) score. These bows can then be upgraded with augments. You’ll learn 2 augments from this specialty, which can be selected under “bow augments” below. These augments have marques. At lower levels, you’ll start with Marque I augments. As your skill in Armsmith improves, your marques will increase. See the “Crafting” page at the beginning of this chapter for more information.

        Each bow can be upgraded with 3 augments. Some materials can only be upgraded twice (like wooden bows) or just once (like organic ones). Sometimes an augment will take up multiple augment slots. For example, the “damaging” augment is worth 2 slots, so a metal bow only has 1 more available slot for an augment after “damaging” has been applied.

#### Number of Bows you can Maintain  

Without needing to buy pieces or parts, you can carve some bows entirely out of scraps. These bows must be constantly maintained by you and stop working soon after leaving your care. You may build and maintain a number of bows based on your DIY score. You may build new bows or augment old ones during any period of downtime you have.

#### The Cost of Bows 

If you need to build a bow that you can’t build for free from your DIY score, you will need to buy the materials for it.

        A bow will have a base materials cost. It is 1/5th the market price.

        Every augment will increase the price. The higher the marque, the greater the price. The market price for an augment can be found in the chart below.

If you are building the augment, you pay 1/5th the price, which is the same as if you were buying an augment one marque lower. (As in, the material cost for a Marque III augment is the market price for a Marque II augment.) The material cost for a Marque 1 augment is 5 princes.

#### Beta Bows
> Augments +2, DIY +1, Hit Points +4

> Armsmith Specialty

> ❓ Requires: 4 skill points in Armsmith & Bowyer specialty

Your bows are surprisingly complex, but they can do a lot more than standard bows. Such bows can be upgraded with 2 more augments (bringing the total for metal bow up to 5 augmentable slots).

        If anybody other than you attempts to use one of your beta bows, they must succeed in rolling a sciences result one tier higher than the highest marque you have on your bow. If your bow has a Marque IV augment, it is impossible for them to use it (unless they can somehow obtain a tier result of 5 with their Sciences attribute).

#### Prototype Bows
> Augments +1, DIY +1, Hit Points +4

> Armsmith Specialty

> ❓ Requires: 16 skill points in Armsmith, Bowyer, & Beta Bows specialties

You’ve perfected your beta bows and made them user-friendly. Now anybody can use a bow that you designate as being a prototype.  

### Bow Augments

#### Accurate
>   Bow Augment

Fine attention has been placed on the quality of your bow. The user gains a bonus to accuracy with the bow.

|Marque| Effect|
|--|--|
|1|+1|
|2|+2|
|3|+3|
|4|+4|

#### Collapsible
>   Bow Augment

Sometimes discretion is the better part of not having your weaponry confiscated, so you create a clever collapsing mechanism for your bows which makes them easier to conceal. Any bow this is applied to can be broken down for 3 action points and re-assembled for 3 action points. It is treated, for purposes of concealment, as being smaller than it is, but only when broken down.

|Marque| Effect|
|--|--|
|1|1 category smaller|
|2|2 categories smaller|
|3|3 categories smaller|
|4|4 categories smaller|

#### Custom
>   Bow Augment

This bow was designed to be used by one person and one person only. That person must be designated at the time of the bow’s crafting. If anybody else attempts to use the custom bow, they suffer a penalty on all accuracy and strike rolls with it.

|Marque| Effect|
|--|--|
|1|-3|
|2|-6|
|3|-9|
|4|-12|

#### Damaging
>   Bow Augment

Takes up 2 Augment Slots on a Bow

Your bow is larger but lighter, dealing extra damage with each blow.

|Marque| Effect|
|--|--|
|1|+1 damage class|
|2|+2 damage class|
|3|+3 damage class|
|4|+4 damage class|

#### Ground-Mount
>   Bow Augment

You can set up your bow on the ground, using the earth to steady your shot. Setting up the bow requires 2 action points, and then pulling it back up so that you can move again requires 1 action point. While the bow is set in place, however, you gain a bonus on your accuracy.

|Marque| Effect|
|--|--|
|1|+1|
|2|+2|
|3|+3|
|4|+4|

#### Deflecting
>   Bow Augment

You can use this bow like a shield, allowing you to deflect incoming attacks (gaining a +4 to evade in exchange for 1 reflexive action point).

> 🗒️️         Note: This augment always acts as Marque I for the purposes of determining cost, though you can learn this augment despite your skill in Armsmith.

#### Easily Altered
>   Bow Augment

> ❓ Requires: Weapon to be made with Interchangeable Parts

The weapon is easily altered, allowing its interchangeable parts to be used and switched around much more quickly than normal. The easily altered augment is a permanent affixture of the weapon, however, and cannot be activated or deactivated with interchangeable parts.

|Marque| Effect|
|--|--|
|1|2 AP to interchange parts|
|2|2 AP to interchange parts|
|3|1 AP to interchange parts|
|4|1 AP to interchange parts|

#### Powerful
>   Bow Augment

Your weapon has a greater pull than others, giving the user a bonus on their strike rolls.

|Marque| Effect|
|--|--|
|1|+2 on strike|
|2|+4 on strike|
|3|+6 on strike|
|4|+8 on strike|

#### Quickened Arrows
>   Bow Augment

Your bow fires arrows faster and more forcefully than most, making them have a more powerful knockback effect. Whenever your bow is used to make a called shot against a target’s torso, it gets a bonus to strike just for the called shot effect. The bonus does not affect the damage tier of the attack

|Marque| Effect|
|--|--|
|1|+2 to Strike for Torso called shots|
|2|+4 to Strike for Torso called shots|
|3|+6 to Strike for Torso called shots|
|4|+10 to Strike for Torso called shots|

#### Reinforced
>   Bow Augment

You build your bow solidly, giving it little room to break on the battlefield. Whenever somebody attempts to sunder the reinforced bow, it acts as if it is several size categories larger than it is. Once these “reinforced” size categories are gone, then its actual damage begins to decrease.

|Marque| Effect|
|--|--|
|1|1 reinforced size category|
|2|2 reinforced size categories|
|3|3 reinforced size categories|
|4|4 reinforced size categories|

#### Scope
>   Bow Augment

The range on your bow increases greatly, improving the range that the bow can accurate hit a target within.

|Marque| Effect|
|--|--|
|1|+50 feet|
|2|+100 feet|
|3|+200 feet|
|4|+300 feet|

#### Silent
>   Bow Augment

This bow is whispering death. It makes almost no sound when an arrow is nocked or fired from it, making it almost impossible for people to figure out where it is by sound alone. Any time anybody is attempting to figure out where the bow was shot from based on sound must make a tier result with their cunning.

|Marque| Effect|
|--|--|
|1|Tier 2|
|2|Tier 3|
|3|Tier 4|
|4|Impossible (or Tier 5)|

#### Versatile
>   Bow Augment

Your bow is also an effective melee weapon. You can make attacks against adjacent opponents for 2 action points, although your damage class is at a -1 when doing so.

> 🗒️️         Note: This augment always acts as Marque II for the purposes of determining cost, though you can learn this augment despite your skill in Armsmith.

#### Combustion
>   Bow Augment

Takes up 3 Augment Slots on a Bow

> ⬇️ Resist: Dexterity (marques down)

Cost to Activate: Attack +1 AP

Fire lashes out from your bow, allowing you to set your arrows on fire. You can use this to attempt to set your enemy aflame. The target may resist with their dexterity (lowering the marques of the combustion).

|Marque| Effect|
|--|--|
|1|The victim catches on Tier 1 fire|
|2|The victim catches on Tier 2 fire|
|3|The victim catches on Tier 2 fire|
|4|The victim catches on Tier 3 fire|

#### Burn Trail
>   Bow Augment

> ❓ Requires: placed on bow with the Combustion augment

Any person attempting to physically aid a victim of your Combustion must first make a dexterity resist that equals the marque of your Combustion augment or be set on fire themselves.

> 🗒️️         Note: This augment always acts as Marque III for the purposes of determining cost, though you can learn this augment despite your skill in Armsmith.

#### Everburning
>   Bow Augment

> ❓ Requires: placed on bow with the Combustion augment

Despite your victim’s best efforts to put themselves out, they seem to continue burning. When catching things on fire through the use of a Combustion attack, extra action points are required to put out the fire.

|Marque| Effect|
|--|--|
|1|1 extra AP|
|2|2 extra AP|
|3|3 extra AP|
|4|4 extra AP|

### Crafting Armor
--------------

#### Armor Smith
> Augments +2, DIY +1, Hit Points +5

> Armsmith Specialty

You can now create armor and upgrade it. You can upgrade any type of armor.

        Without spending any money, you can build and maintain several suits of armor based on your current Do-It-Yourself (DIY) score. These armors can then be upgraded with augments. You’ll learn 2 augments from this specialty, which can be selected under “armor augments” below. These augments have marques. At lower levels, you’ll start with Marque I augments. As your skill in Armsmith improves, your marques will increase. See the “Crafting” page at the beginning of this chapter for more information.

        Each armor can be upgraded with 3 augments. Some materials can only be upgraded twice (like wooden armors) or just once (like organic armors). Sometimes an augment will take up multiple augment slots. For example, the “damage soaking” augment is worth 2 slots, so a metal suit of armor only has 1 more available slot for an augment after “damage soaking” has been applied.

#### Number of Armors you can Maintain  

Without needing to buy pieces or parts, you can build some armor entirely out of scraps. These armors must be constantly maintained by you and stop working soon after leaving your care. You may build and maintain a number of armors based on your DIY score. You may build new armors or augment old ones during any period of downtime you have.

#### The Cost of Armors 

If you need to build a suit of armor that you can’t build for free from your DIY score, you will need to buy the materials for it.

        Armor will have a base materials cost. It is 1/5th the market price.

        Every augment will increase the price. The higher the marque, the greater the price. The market price for an augment can be found in the chart below.

If you are building the augment, you pay 1/5th the price, which is the same as if you were buying an augment one marque lower. (As in, the material cost for a Marque III augment is the market price fo a Marque II augment.) The material cost for a Marque 1 augment is 4 princes.

#### Beta Armor
> Augments +2, DIY +1, Hit Points +5

> Armsmith Specialty

> ❓ Requires: 4 skill points in Armsmith & Armor Smith specialty

Your armor comes with a lot of weird straps and oddly placed component, but their efficiency is unbreakable. Such armors have two more slots for you to place augments into.

        If anybody other than you attempts to use one of your beta armors, they must succeed in rolling a Sciences result one tier higher than the highest level marque you have on your armor. If your armor has a Marque IV augment, it is impossible for them to use it (unless they can somehow obtain a tier result of 5 with their science attribute).

#### Prototype Armor
> Augments +1, DIY +1, Hit Points +6

> Armsmith Specialty

> ❓ Requires: 16 skill points in Armsmith, Armor Smith, & Beta Armor specialty

You’ve perfected your beta armor and made them user-friendly. Now, any of your beta armors that you designate as being prototypes, anybody can use.

### Armor & Shield Augments

#### Airmelting
>   Armor Augment

> 🪙 Activation Cost: 1 AP

The armor has numerous exhausts, heat valves, and pipes on it that, when released, super-heat the air around the wearer. If you’re wearing this armor and spend 1 action point to release the exhausts, it heats the air around you. Everyone within 5 feet of you takes heat damage, and you take half that amount (unless you have fireproofing).

|Marque| Effect|
|--|--|
|1|2 damage|
|2|4 damage|
|3|6 damage|
|4|8 damage|

#### Bracings
>   Armor Augment

The armor is braced to prevent you from ever falling prone. Unless you choose to undo the bracings (an action that costs 1 action point), there is no way the wearer of this armor can go prone.

> 🗒️️         Note: This augment always acts as Marque I for the purposes of determining cost, though you can learn this augment despite your skill in Armsmith.

#### Bulletproofing
>   Armor Augment

The armor is designed to take bullets. Whenever being shot by a firearm, the wearer gains a bonus on their defense roll.

|Marque| Effect|
|--|--|
|1|+2 defense|
|2|+4 defense|
|3|+6 defense|
|4|+8 defense|

#### Camouflage
>   Armor Augment

The armor has been painted, ruffled, and tarnishes added to allow it to blend in more efficiently in natural terrains. When in the uncivilized outdoors, this armor grants a bonus to cunning rolls when attempting to hide.

|Marque| Effect|
|--|--|
|1|+2|
|2|+4|
|3|+6|
|4|+8|

#### Crashbreaking
>   Armor Augment

This armor is designed to help cushion the wearer’s fall by absorbing the shock and keeping the person inside from rattling around. It’s not going to keep you alive in a crashing airship from a high altitude, but it might help out when somebody pushes you over the side of the building. While wearing crashbreaking armor, you can fall farther before taking wounds damage.

|Marque| Effect|
|--|--|
|1|1 wounds damage per 30 feet fallen|
|2|1 wounds damage per 40 feet fallen|
|3|1 wounds damage per 50 feet fallen|
|4|1 wounds damage per 60 feet fallen|

#### Damage Soaking
>   Armor Augment

Takes up 2 Augment Slots on Armor

Your armor is so thick that it soaks more damage than normal.

|Marque| Effect|
|--|--|
|1|+1 damage soak class|
|2|+2 damage soak class|
|3|+3 damage soak class|
|4|+4 damage soak class|

#### Defensive
>   Armor Augment

This armor is heavier, with more shock-absorbent plating and fewer spots for enemy blade’s to find cleavage. This armor provides a bonus to the wearer’s defense.

|Marque| Effect|
|--|--|
|1|+2 defense|
|2|+4 defense|
|3|+6 defense|
|4|+8 defense|

#### Electro-Absorption
>   Armor Augment

The armor re-routes electricity through it and into specially created devices that absorb the shock. Anything that deals electricity damage is not entirely soakable, and attacks that deal electricity damage (even if they are only partially electrical, such as attacks with a pulsing weapon) increase the soak class of the armor.

|Marque| Effect|
|--|--|
|1|+2 soak class against electrical attacks|
|2|+4 soak class against electrical attacks|
|3|+6 soak class against electrical attacks|
|4|+8 soak class against electrical attacks|

#### Fireproofing
>   Armor Augment

While wearing armor that has fireproofing, all fire attacks are soakable. Furthermore, the soak class for the armor is improved against fire. (If the wearer would take burns or be set on fire from a fire attack that is entirely soaked, the extra effects are negated.)

|Marque| Effect|
|--|--|
|1|+1 soak class against fire|
|2|+2 soak class against fire|
|3|+3 soak class against fire|
|4|+4 soak class against fire|

#### Flame Retardant
>   Armor Augment

The armor generally cannot be caught on fire, depending on how intense the fire is.

|Marque| Effect|
|--|--|
|1|Cannot be caught on tier 1 fire|
|2|Cannot be caught on tier 2 fire|
|3|Cannot be caught on tier 3 fire|
|4|Cannot be caught on tier 4 fire|

#### Handcrushing
>   Armor Augment

> 🪙 Activation Cost: 1 AP

When people add gears to their armor, it’s not merely steampunk-themed decor. Those gears are designed to tear apart anybody who lays a hand on that armor. If a person is grabbing you while wearing this armor, you may spend 1 action point to activate the spinning gears, the hydraulic spikes, or the built-in flamethrower and deal unsoakable damage to the person touching you.  

|Marque| Effect|
|--|--|
|1|4 damage|
|2|8 damage|
|3|12 damage|
|4|16 damage|

> 🗒️️ Note: As a courtesy, feel free to remind the person grabbing you that they can reflexively let go of you for 0 action points whenever they’d like.  

#### Injector
>   Armor Augment

Through the use of a magnetic trigger system connected to a liquid injector, this augment creates a quick injection system that allows the wearer to, through the push of a button, inject himself with a dose of a chemical. It requires 5 action points to reload a dose of the chemical after it’s been used, but no action points to inject it. At any given time, the armor can only hold so many chemicals.

|Marque| Effect|
|--|--|
|1|1 alchemical potion|
|2|2 alchemical potions|
|3|3 alchemical potions|
|4|4 alchemical potions|

#### Mobile
>   Armor Augment

Light, easy to move in, and with enhanced piston-joints to really get you moving, this armor provides a bonus to your speed.

|Marque| Effect|
|--|--|
|1|+5 feet|
|2|+10 feet|
|3|+10 feet|
|4|+15 feet|

#### Quick Equip
>   Armor Augment

Most armor takes a while to equip - not this stuff! It takes only a turn or two.

|Marque| Effect|
|--|--|
|1|10 AP to equip|
|2|6 AP to equip|
|3|3 AP to equip|
|4|1 AP to equip|

#### Razor-Ridged
>   Armor Augment

Your armor has sharp edges, pointy bits, and moving pieces that keep enemies from grabbing you. Any time an opponent grabs onto you, they automatically take a number of unsoakable damage based on the razor-ridged marque. Every turn that they keep holding on to you, they take that damage again (this damage is dealt at the end of their turn, when their action points refresh).

|Marque| Effect|
|--|--|
|1|3 damage|
|2|6 damage|
|3|9 damage|
|4|12 damage|

#### Reinforced Plating
>   Armor Augment

Through additional plating being added to various body parts, you are able to decrease the chances of called shots affecting you. When you choose this augment, choose one limb or area on the body (either arm, either leg, the torso, or the head). Thereafter, you gain a bonus whenever attempting to resist a called shot there.

|Marque| Effect|
|--|--|
|1|+2|
|2|+4|
|3|+6|
|4|+8|

#### Stabilizing Rods
>   Armor Augment

> 🪙 Activation Cost: 0 AP (see below)

Your armor is built with stabilizing rods in the arms that allow you to automatically enter firing stance for 0 action points. This allows you to fire super-heavy firearms and crossbows immediately. However, you are rooted to the spot you’re in until you spend 1 action point to reset the stabilizing rods back into their original location.

> 🗒️️         Note: This augment always acts as Marque III for the purposes of determining cost, though you can learn this augment despite your skill in Armsmith.

#### Slippery
>   Armor Augment

While wearing this armor, you are very difficult to grab on to. Any time you are attempting to evade a grab or resist a grab, you gain a bonus on your roll.

|Marque| Effect|
|--|--|
|1|+2|
|2|+4|
|3|+6|
|4|+8|

#### Steaming
>   Armor Augment

Your armor lets off steam, enveloping you and everyone around you in a foggy, hard-to-see-through steam. You may turn it off for 1 action point, but otherwise it makes an area around you difficult to see through. Anybody in the area is affected by poor vision, and takes a -2 to accuracy and evade rolls.

|Marque| Effect|
|--|--|
|1|5 feet|
|2|10 feet|
|3|15 feet|
|4|20 feet|

#### Welded Weapon
>   Armor Augment

You weld a weapon to your armor. The weapon cannot be disarmed, dropped, or concealed, but it also does not need to be drawn in order to be used. The maximum size of the weapon depends on the mark.

|Marque| Effect|
|--|--|
|1|Medium|
|2|Medium|
|3|Heavy|
|4|Heavy|

* * *

## Automata Skill
==============

Automata is the complex skill of piecing together machines that act on their own or do things that people cannot do by themselves. If you’re interested in creating a massive, mechanical juggernaut, an intelligent and self-serving automaton, or creating prosthetics that can be attached to your friends in place of their fleshy limbs, automata is your place.

The 3 Types of Automatons

There are three types of automatons that an automaton crafter can choose to develop.

        Steamers: Steam-powered automatons that are remotely controlled by their owner. Steamers are typically the largest and strongest of the three automatons.

        Fuse Boxes: Electric-powered automatons with brainworks that allow them to think on their own. Fuse boxes are rarely very strong, but they move entirely on their own accord.

        Clockworks: Automatons comprised of clockwork and springs that have pre-programmed analytical engines, causing them to only react to their surroundings.

Once you’ve chosen which type of automaton you want to craft, you’ll take the specialty for that automaton. You may learn different types of automatons, and even apply different control styles to the same automaton.

Skill Organization

This skill begins with specialties allowing you to build and customize automatons up front, followed by specialties for building prosthetic body parts, which can be added to those automatons (or used as replacement limbs for other people). You’ll find Steamers first (and their boiler augments), then Fuse Boxes (along with their brainwork augments), and finally Clockworks (accompanied by their analytical engine augments).

#### Automaton Repairs
> Defense +2, Augments +2, Hit Points +7

> Automata Specialty

> 🪙 Cost: 3 AP

You’re able to quickly repair an automaton in battle. To do so, you must be adjacent to it. By making the repairs, you restore a number of wounds to the automaton.

|Marque| Effect|
|--|--|
|1|10 wounds|
|2|15 wounds|
|3|20 wounds|
|4|30 wounds|

#### Interchangeable Parts
> Augments +2, DIY +1, Hit Points +6

> Automata Specialty

> 🪙 Cost: 3 AP

Your automatons are designed so that their parts can be replaced and altered in the middle of battle. This gives the automatons additional slots for augments, but these augments are not always active. At any time, a person may switch out the augments for 3 action points, activating one augment but deactivating another augment. You may replace multiple augments at the same time all for the cost of 3 action points.

        This specialty works within the marque system. The amount of interchangeable slots the automaton has depends on the marque of its creator.  

|Marque| Effect|
|--|--|
|1|1 interchangeable slot for an augment|
|2|2 interchangeable slots for augments|
|3|3 interchangeable slots for augments|
|4|4 interchangeable slots for augments|

#### Steam-Powered Crafter
> Augments +2, DIY +1, Hit Points +4

> Automata Specialty

The first step in crafting your steamer, boilers are the machinery that powers your automaton and gives it motion and strength.  Your boiler is automatically housed within the automaton’s torso, onto which you can attach any body parts you wish.

        Without spending any money, you can build and maintain several steamers based on your current Do-It-Yourself (DIY) score. The boilers in these steamers can then be upgraded with augments. You’ll learn 2 augments from this specialty, which can be selected under “boiler augments” below. These augments have marques. At lower levels, you’ll start with Marque I augments. As your skill in Automata improves, your marques will increase. See the “Crafting” page at the beginning of this chapter for more information.

        Each boiler can be upgraded with 3 augments. Sometimes an augment will take up multiple augment slots. For example, the “Flight” augment is worth 2 slots, so a boiler only has 1 more available slot for an augment after “Flight” has been applied.

#### Number of Steamers you can Maintain  

Without needing to buy pieces or parts, you can build some steamers entirely out of scraps. These automatons must be constantly maintained by you and stop working soon after leaving your care. You may build and maintain a number of steamers for free based on your DIY score. You may build new steamers or augment old ones during any period of downtime you have.

#### The Cost of Steamers 

If you need to build a steamer that you can’t build for free from your DIY score, you will need to buy the materials for it.

        A steamer without augments will cost 50 princes. If you are building a steamer, the steamer will have a base materials cost. It is 1/5th the market price (10 princes).

        Every augment will increase the price. The higher the marque, the greater the price. The market price for an augment can be found in the chart below.

If you are building the augment, you pay 1/5th the price, which is the same as if you were buying an augment one marque lower. (As in, the material cost for a Marque III augment is the market price for a Marque II augment.) The material cost for a Marque 1 augment is 10 princes.

Using your Steamer

Back in the day, steam-powered automatons used coal, so they were often large, hulking affairs that moved slowly and ate up a lot of fuel. Emperor Deylus Luthricien and the royalists changed that during the civil war when they integrated aether technology into their boilers for easier steam generation.  Now, steam-powered automatons are fast, lighter-weight, and can be controlled via remote controls and radio waves.

### Control Method

Steamers require the use of a two-handed remote control, emitting radio waves to the aether resonators inside the automatons. These remote controls allow the user to be up to 100 feet away and still control the automaton. The automaton can generally do anything a normal person could do (such as walking, jumping, fighting, and deflecting), and it takes the controller the same amount of action points to control the automaton as it would for a normal person to make the action (such as 1 action point to make the automaton move, or 2 action points to make the automaton attack). A single remote control can be used to control all of your automatons. Steamers have no action points of their own; therefore, if your steamer is hit with an attack that lowers its number of action points, your steamer is unaffected.  

### Body of the Machine

Steamers will have a Boiler, which will be augmentable with 3 slots. One may later take Beta Boiler to improve on that. Body parts may then be added to the boiler.

        Your steamer (by default) is built with a head to house its radio receptors and a torso and groin to house its boiler. The two are connected via a neck support. To its torso you may attach either two arms or one unit of movement. Its groin may have a unit of movement attached to it. A unit of movement is either a set of legs, wheels, or a propulsion device. (See the Units of Movement sidebar for more information.)

        Your steamer has 0 hit points and 30 wounds. Any “bleeding” damage your steamer takes causes steam to erractically leak out of its boiler, making it take bleeding damage in the same fashion as any regular organism. In addition, the rigid structuring of your steamer’s boiler grants it an extra 3 soak class. As your automaton has no mind of its own, it cannot be affected by anything that requires Spirit or Cunning as a resist.

Units of Movement

Automatons will normally have a unit of movement attached to them. This could be a set of legs, wheels, or a propulsion device.

### Legs

Legs provide movement. Legs can easily traverse almost any terrain. By having a set of legs (typically two), an automaton will have a speed of 20 feet, can walk up stairs, move over all degrees rough terrain, and generally have little problem getting about as do most tephrans. The legs do not allow the automaton to climb or swim. By having legs, the automaton can be affected by called shots to the legs.

### Wheels

Wheels allow for faster movement, though their are some clear disadvantages compared to legs. Wheels will give the automaton a speed of 30 feet, but the automaton will only be able to go up stairs at a speed of 10 feet, and the penalties for moving through rough terrain are doubled. The wheels do not allow the automaton to climb or swim. Wheels can be targeted as called shot locations - they have the same effects as legs.

### Propulsion Device

Sometimes legs and wheels don’t cut it. Underwater automatons will have propellers or automated fins. Some automatons will stand on wheels but use a rocket to propel them forward. A propulsion device will allow the automaton a speed of 20 feet (on either land, surface waters, or under water). The penalties for moving through rough terrain are doubled, and the propulsion device does not allow the automaton to climb or swim (unless the propulsion device is intended for underwater movement, in which case it does not allow the automaton to move across the land). If a propulsion device is targeted as a called shot location, it has the same effect as if it were a leg.

#### Beta Boilers
> Augments +2, DIY +1, Hit Points +4

> Automata Specialty

> ❓ Requires: 4 skill points in Automata & Steam-Powered Crafter specialty

Your boilers are highly advanced yet difficult for most people to use. Such boilers can be upgraded with 2 more augments (bringing the total up to 5 augmentable slots).

        If anybody other than you attempts to use one of your beta steamers, they must succeed in rolling a sciences result one tier higher than the highest marque you have on your steamer. If your steamer has a Marque IV augment, it is impossible for them to use it (unless they can somehow obtain a tier result of 5 with their Sciences attribute).

#### Prototype Boilers
> Augments +1, DIY +1, Hit Points +5

> Automata Specialty

> ❓ Requires: 16 skill points in Automata, Steam-Powered Crafter, & Beta Boiler specialties

You’ve perfected your beta boilers and made them user-friendly. Now anybody can control an automaton with a boiler that you designate as being a prototype.

### Steamer Operator Specialties

#### Steamer Operator
> Accuracy +1, Evade +1, Hit Points +6

> Automata Specialty

As a practiced automaton fighter, you can make a steamer move just the way you want it to. Whenever you are operating a steamer, the steamer can use your accuracy, strike, defense, and evade.

#### Steam Poser
> Evade +1, Augments +1, Hit Points +7

> Automata Specialty

> ❓ Requires: Steam Operator & at least one stance known

You know how to make a steamer accurately strike a pose. Steamers you control will automatically act as if they are in the same stance(s) as you. The instant you stop being a steamer’s current operator, the steamer loses all its stances.

#### Steam Specialist
> Accuracy +1, Augments +1, Hit Points +6

> Automata Specialty

> ❓ Requires: 3 skill points in Automata & Steam Operator specialty

A true joystick master, you rise above any primitive control scheme put in front of you to make your steamer move just as you do. You can now modify the actions of any steamer you are operating with specialties you know.

### Boiler Augments

#### Armored Boiler
>   Boiler Augment

Takes up 2 Augment Slots on a Boiler

Your boiler has armoring built into it so that it won’t impede your steamer in any way. This increases your steamer’s natural soak class.

|Marque| Effect|
|--|--|
|1|+1 soak class|
|2|+2 soak class|
|3|+3 soak class|
|4|+4 soak class|

> 🗒️️ Note: this augment stacks with the soak class bonus granted from the Brickhouse Boiler augment.

#### Automated Boiler Repair
>   Boiler Augment

The boiler contains internal systems which patch up damage it receives. While this won’t remove wound effects, every turn it will replenish some wounds damage.

|Marque| Effect|
|--|--|
|1|1 wound repaired|
|2|2 wounds repaired|
|3|3 wounds repaired|
|4|4 wounds repaired|

#### Brickhouse Boiler
>   Boiler Augment

Takes up 2 Augment Slots on a Boiler

The walls of your boiler are exceptionally thick and well-insulated, protecting your steamer from damage.

|Marque| Effect|
|--|--|
|1|+1 soak class|
|2|+2 soak class|
|3|+3 soak class|
|4|+4 soak class|

> 🗒️️ Note: this augment stacks with the soak class bonus granted from the Armored Boiler augment.

#### Fire Absorbing
>   Boiler Augment

You need heat to make steam. Your boiler can already produce heat on its own, but you’re not opposed to other people helping it out. Any heat damage your steamer would take from being on fire instead heals it.

> 🗒️️         Note: This augment always acts as marque II for the purposes of determining cost, though you can learn this augment despite your skill in automata.

#### Easy Repairs
>   Boiler Augment

Used with the Automaton Repairs specialty

The automaton is designed in a logical, accessible way that makes repairs easy. Whenever somebody is attempting to repair it via the Automaton Repairs specialty, they gain a bonus on their automata roll.

|Marque| Effect|
|--|--|
|1|+3 on the Automaton Repairs rolls|
|2|+6 on the Automaton Repairs rolls|
|3|+9 on the Automaton Repairs rolls|
|4|+12 on the Automaton Repairs rolls|

#### Flight
>   Boiler Augment

Takes up 2 Augment Slots on a Boiler

The automaton has been outfitted with a graviton sphere, allowing it to float and move. It can now fly, with its speed depending on the marque. Any speed penalties you take from armor will penalize this speed.

|Marque| Effect|
|--|--|
|1|5 feet of fly speed per action point|
|2|10 feet of fly speed per action point|
|3|15 feet of fly speed per action point|
|4|25 feet of fly speed per action point|

#### Lightning Resistant
>   Boiler Augment

The automaton is naturally resistant to lightning and electricity. Electrical damage is now soakable as regular damage.

> 🗒️️         Note: This augment always acts as Marque III for the purposes of determining cost, though you can learn this augment despite your skill in Automata.

#### Passenger
>   Boiler Augment

Your automaton has a hollow compartment inside, allowing people to ride inside and see out through eye holes or monitors of your design. While inside an automaton, a character cannot be directly targeted but they in turn cannot interact with anything outside of the automaton, although the automaton can still hear all voice commands. Any remote controls for the automaton can still control it from its passenger compartment. The number of people the automaton can hold is equal to the number of spaces it takes up. The amount of action points required by passengers to get into and out of the automaton is based on the marque of this augment.

|Marque| Effect|
|--|--|
|1|4 AP to enter or exit the automaton (can take multiple turns)|
|2|3 AP to enter or exit the automaton|
|3|2 AP to enter or exit the automaton|
|4|1 AP to enter or exit the automaton|

#### Protected Core
>   Boiler Augment

If your automaton is defeated in battle, its insides are heavily safeguarded to allow for easy repair during your next breather. An automaton with a protected core can be entirely rebuilt in a matter of minutes. The automaton can be repaired up to a certain amount of maximum wounds, based on the marque. It cannot be repaired past its normal maximum number of wounds

|Marque| Effect|
|--|--|
|1|10 wounds after repairs|
|2|20 wounds after repairs|
|3|30 wounds after repairs|
|4|40 wounds after repairs|

#### Realistic
>   Boiler Augment

> ⬇️ Resist: Cunning (negates)

You’ve designed the body of your automaton to look quite real, replicating the appearance of a living or imaginary creature. Any persons who looks upon it will believe it to be real unless they can beat the automaton’s roll with their Cunning. The automaton’s roll gains a bonus based on the marque of this augment.

|Marque| Effect|
|--|--|
|1|+5 to fool target|
|2|+10 to foot target|
|3|+15 to fool target|
|4|+20 to fool target|

#### Reinforced Boiler
>   Boiler Augment

The boiler is all-around better built, granting it extra points of wounds.

|Marque| Effect|
|--|--|
|1|+10 wounds|
|2|+20 wounds|
|3|+30 wounds|
|4|+40 wounds|

#### Resilient Boiler
>   Boiler Augment

Your steamer has a boiler any engineer would be proud of. Durable and reliable, your steamer has extra points of wounds.

|Marque| Effect|
|--|--|
|1|+10 wounds|
|2|+20 wounds|
|3|+30 wounds|
|4|+40 wounds|

#### Remotely Remote Controlled
>   Boiler Augment

The automaton can be controlled from a great distance. If the user has no way of knowing what the automaton can see, they might be wasting commands.

|Marque| Effect|
|--|--|
|1|Up to 200 feet away|
|2|Up to 2,000 feet away|
|3|Up to 2 miles away|
|4|Up to 20 miles away|

#### Shield Mode
>   Boiler Augment

> 🪙 Activation Cost: 2 AP (see below)

Your automaton can hunker down into a solid dense mass for two of its controller’s action points, allowing it to be easily used as medium cover (+6 to evade against ranged attacks) for the same number of spaces it normally takes up. While in shield mode, your automaton cannot move or act. You can command it to exit this form for two action points. It also gains extra soak class while this ability is active.

|Marque| Effect|
|--|--|
|1|+1 soak class|
|2|+2 soak class|
|3|+3 soak class|
|4|+4 soak class|

#### Tactile Controls
>   Boiler Augment

Your highly responsive control scheme allows your automaton to use your Dexterity as a bonus to its own as long as you’re are controlling it with a remote control.

> 🗒️️         Note: This augment always acts as Marque III for the purposes of determining cost, though you can learn this augment despite your skill in Automata.

#### Test of Strength Controls
>   Boiler Augment

Your steamer has been outfitted with pressurized bags of steam which inflate and deflate to replicate the amount of torque placed on the joystick of its remote control. When the person piloting your steamer is using its remote control, the steamer is able to use its operator’s Brute as a bonus onto its own.

> 🗒️️         Note: This augment always acts as Marque II for the purposes of determining cost, though you can learn this augment despite your skill in Automata.

#### Verbal Command Unit
>   Boiler Augment

If your automaton has the ability to hear you, you can now issue it verbal commands without the need for a remote control. It still costs you the same amount of action points to get it to perform any action since you have to guide the automaton through every intricacy of the actions they take. The last person to give your steamer any verbal commands is considered your steamer’s current operator. If your steamer is deafened, getting it to perform an action through verbal commands costs an extra action point.

> 🗒️️         Note: This augment always acts as Marque II for the purposes of determining cost, though you can learn this augment despite your skill in Automata.

### Crafting Fuse Boxes

#### Fuse Box Builder
> Augments +2, DIY +1, Hit Points +4

> Automata Specialty

You can now create electric-powered automatons and upgrade the brainworks that power them and grant them artificial intelligence. Tephrans colloquially call them “fuse boxes.”

        Without spending any money, you can build and maintain one fuse box. The brainworks of this fuse box can then be upgraded with augments. You’ll learn 2 augments from this specialty, which can be selected under “brainworks augments” below. These augments have marques. At lower levels, you’ll start with Marque I augments. As your skill in Automata improves, your marques will increase. See the “Crafting” page at the beginning of this chapter for more information.

        Each brainworks can be upgraded with 3 augments. Sometimes an augment will take up multiple augment slots. For example, the “Lightning Soul” augment is worth 2 slots, so a brainworks unit only has 1 more available slot for an augment after “Lightning Soul” has been applied.

#### Maintaining your Fuse Box  

Without needing to buy pieces or parts, you can build a single fuse box entirely out of scraps. This automaton must be constantly maintained by you and stops working soon after leaving your care. You can replace your fuse box automaton or augment your current one during any period of downtime you have.

#### The Cost of Fuse Boxes 

If you need to build a fuse box that you can’t build for free from your DIY score, you will need to buy the materials for it.

        The fuse box, unaugmented, will have a base cost depending on its marque. (The marque will determine its number of action points per turn.) If you buy a fuse box and augments, you will add the price of the augments onto the price of the automaton.

        Every augment will increase the price. The higher the marque, the greater the price. The market price for an augment can be found in the chart below.

If you are building the augment, you pay 1/5th the price, which is the same as if you were buying an augment one marque lower. (As in, the material cost for a Marque III augment is the market price for a Marque II augment.) The material cost for a Marque 1 augment is 10 princes.

#### Advanced Brainworks
> Augments +2, DIY +1, Hit Points +4

> Automata Specialty

> ❓ Requires: 4 skill points in Automata & Fuse Box Builder specialty

Your brainworks go far beyond what most other automata builders could conceptually imagine. Such brainworks have two more slots for you to place augments into.

Superior Brainworks

> Automata Specialty

> ❓ Requires: 6 skill points in Automata & Fuse Box Builder specialty

Your brainworks are far more advanced than the common artificial intelligence. All brainworks you create with your DIY have 3 action points per turn instead of 2. Buying fuse boxes with superior brainworks costs 10,000 princes (a materials cost of 2,000 princes) on top of any other costs.

#### Heroic Brainworks
> Augments +2, DIY +1, Hit Points +5

> Automata Specialty

> ❓ Requires: 9 skill points in Automata, Fuse Box Builder, & Superior Brainworks specialties

Brainworks crafted with your DIY can fight with the tenacity of a great warrior and think at the speed of a quick-witted rogue. All brainworks you create with your DIY have 4 action points per turn instead of 3. Buying fuse boxes with superior brainworks costs 50,000 princes (a materials cost of 10,000 princes) on top of any other costs.

#### Personality
> Augments +2, DIY +1, Hit Points +5

> Automata Specialty

> ❓ Requires: Fuse Box Builder specialty

It’s understandable for a gentleman adventurer to get lonely on the road. The cold embrace of an unfeeling automaton won’t stave off feelings of solitude for long. Luckily for you, there’s an easy solution: give that unfeeling automaton some feelings! Your brainworks can now be built with personality, making the fuse box you put them into feel the same emotional sensations as any other Tephran. This gives them the ability to gain stories (including ones from your nationality) and learn specialties from the Spirit attribute. Buying fuse boxes with personality costs 5,000 princes (a materials cost of 1,000 princes) on top of any other costs.

> 🗒️️         Note: They still cannot gain DIY or augments through stories or specialties.

Using your Fuse Box

The first electric-powered automatons were invented by the Hazards, and later the technology was adapted and used to create numerous smaller and more efficient automatons that would protect the Hazardlands. Many Evanglessians called electric-powered automatons “Sparkers” or “Fuse Boxes.” Fuse Boxes have the most advanced sensory arrays, and can compute information to such an extent that they seem almost lifelike. This artificial intelligence is known as brainworks.

### Control Method

Fuse boxes use their brainworks to act independently. They begin as creations with 2 action points per turn. The sensory array allows the creator to give it verbal commands, like telling it to “attack that man” or “run to my side,” which costs the creator no action points. Your fuse box must roll priority separately from you as it is an autonomous being with its own turns.

### Body of the Machine

Fuse Boxes will begin with Brainworks, which has 3 slots that can be augmented. This can be upgraded with Advanced Brainworks, which gives the automaton more control over itself and an additional 2 slots for its brainworks. Body parts can then be added onto the brainworks.

        Your fuse box (by default) is built with a head to house its brainworks and a torso and groin to house its drive core. The two are connected via a neck support. To its torso you may attach either two arms or one unit of movement. Its groin may have a unit of movement attached to it. A unit of movement is either a set of legs, wheels, or a propulsion device. (See the Units of Movement sidebar for more information.)

        Your fuse box’s integrity comes from the electricity its brainworks produces, which at default is 8 hit points. Once out of hit points its brainworks is exposed and your automaton will begin to take wounds damage. Fuse boxes have only 12 points of wounds. Any “bleeding” damage your automaton takes causes lightning to shoot out of its electrical conduit, making it take bleeding damage in the same fashion as any regular organism.

        Fuse boxes have 2 action points per turn. As sentient machines, fuse boxes can learn specialties based on their marque. They act as if they have 1 point in the skills they have specialties in for the purposes of prerequisites and for the specialty itself. This point does not increase their attributes at all.

|Marque| Effect|
|--|--|
|1|Has 1 specialty|
|2|Has 2 specialties|
|3|Has 3 specialties|
|4|Has 5 specialties|

### Brainworks Augments

#### Alert
>   Brainworks Augment

The automaton is always alert (unless when out of power or turned off), and will raise an alarm if its sensors pick up anything.

> 🗒️️         Note: This augment always acts as Marque I for the purposes of determining cost, though you can learn this augment despite your skill in Automata.

#### Brute Physics
>   Brainworks Augment

Your brainworks is filled with meticulous physics calculators, allowing it to use the power of mind over matter to perform feats of incredible strength. It has a bonus to Brute.

|Marque| Effect|
|--|--|
|1|+3 Brute|
|2|+12 Brute|
|3|+31 Brute|
|4|+47 Brute|

#### Dexterity Directory
>   Brainworks Augment

Your brainworks contains a vast directory of motion capture data taken from some of the world’s greatest acrobats and rogues. Whenever it needs them, your brainworks can make your automaton mimic these athletes, granting it a bonus to Dexterity.

|Marque| Effect|
|--|--|
|1|+3 Dexterity|
|2|+12 Dexterity|
|3|+31 Dexterity|
|4|+47 Dexterity|

#### Easy Repairs
>   Brainworks Augment

Used with the Automaton Repairs specialty

The automaton is designed in a logical, accessible way that makes repairs easy. Whenever somebody is attempting to repair it via the Automaton Repairs specialty, they gain a bonus on their Automata roll.

|Marque| Effect|
|--|--|
|1|+3 on the Automaton Repairs rolls|
|2|+6 on the Automaton Repairs rolls|
|3|+9 on the Automaton Repairs rolls|
|4|+12 on the Automaton Repairs rolls|

#### Flight
>   Brainworks Augment

Takes up 2 augment slots on the Brainworks

The automaton has been outfitted with a graviton sphere, allowing it to float and move. It can now fly, with its speed depending on the marque. Any speed penalties you take from armor will penalize this speed.

|Marque| Effect|
|--|--|
|1|5 feet of flight speed per action point|
|2|10 feet of flight speed per action point|
|3|15 feet of flight speed per action point|
|4|20 feet of flight speed per action point|

#### Installed Cunning
>   Brainworks Augment

You’ve given your automaton street-smarts and wit, allowing it to analyze the words of others and its surroundings, as well as allowing it to effectively gather new information. This grants it a bonus to Cunning.

|Marque| Effect|
|--|--|
|1|+3 Cunning|
|2|+12 Cunning|
|3|+31 Cunning|
|4|+47 Cunning|

#### Lightning Soul
>   Brainworks Augment

Takes up 2 augment slots on the Brainworks

Electricity is your fuse box’s blood, so when someone makes it bleed, they’re in for quite a shock! Whenever someone deals bleeding damage to your fuse box, including any taken from wounds or fatal effects, bolts of lightning erupt from its body into the person dealing the damage, as long as they are within 20 feet of your fusebox when dealing the damage. The victim of this augment is denied an evade roll.

|Marque| Effect|
|--|--|
|1|8 electric damage|
|2|16 electric damage|
|3|24 electric damage|
|4|32 electric damage|

#### Linguistics
>   Brainworks Augment

Takes up 0 augment slots on the Brainworks

The automaton has learned the basics of language and, with the addition of a soundbox in its mouth, can now process and recreate language.

|Marque| Effect|
|--|--|
|1|It can speak its creator’s language, though poorly|
|2|It can speak its creator’s language fluently|
|3|It can speak three languages fluently|
|4|It can speak ten languages fluently|

> 🗒️️ Note: As an augment that takes up 0 slots, the cost to apply Linguistics to an automaton is half as much as a normal augment (rounded down).

#### Mechanical Sharpness
>   Brainworks Augment

The fusebox can make precisely timed actions, granting it a bonus on its accuracy rolls.

|Marque| Effect|
|--|--|
|1|+2 Accuracy|
|2|+4 Accuracy|
|3|+6 Accuracy|
|4|+8 Accuracy|

#### Passenger
>   Brainworks Augment

Your automaton has a hollow compartment inside, allowing people to ride inside and see out through eye holes or monitors of your design. While inside an automaton, a character cannot be directly targeted but they in turn cannot interact with anything outside of the automaton, although the automaton can still hear all voice commands. The number of people the automaton can hold is equal to the number of spaces it takes up. The amount of action points required by passengers to get into and out of the automaton is based on the marque of this augment.

|Marque| Effect|
|--|--|
|1|4 AP to enter or exit the automaton (can take multiple turns)|
|2|3 AP to enter or exit the automaton|
|3|2 AP to enter or exit the automaton|
|4|1 AP to enter or exit the automaton|

#### Protected Core
>   Brainworks Augment

If your automaton is defeated in battle, its insides are heavily safeguarded to allow for easy repair during your next breather. An automaton with a protected core can be entirely rebuilt in a matter of minutes. The automaton can be repaired up to a certain amount of maximum wounds, based on the marque. It cannot be repaired past its normal maximum number of wounds

|Marque| Effect|
|--|--|
|1|10 wounds after repairs|
|2|20 wounds after repairs|
|3|30 wounds after repairs|
|4|40 wounds after repairs|

#### Realistic
>   Brainworks Augment

> ⬇️ Resist: Cunning (negates)

You’ve designed the body of your automaton to look quite real, replicating the appearance of a living or imaginary creature. Any persons who looks upon it will believe it to be real unless they can beat the automaton’s roll with their Cunning. The automaton’s roll gains a bonus based on the marque of this augment.

|Marque| Effect|
|--|--|
|1|+5 to fool target|
|2|+10 to foot target|
|3|+15 to fool target|
|4|+20 to fool target|

#### Scientific Database
>   Brainworks Augment

Your automaton is a walking library of information and has deep understanding on the inner workings of itself and other machines, granting it a bonus to Sciences.

|Marque| Effect|
|--|--|
|1|+3 Sciences|
|2|+12 Sciences|
|3|+31 Sciences|
|4|+47 Sciences|

#### Shock Absorber
>   Brainworks Augment

Carefully hidden miniature conduits across your fusebox’s body allow it to soak electric damage normally, even if the electric attack states it denies soak or defense. Any electric damage it soaks is harnessed for energy, replenishing your fusebox’s hit points.

> 🗒️️         Note: This augment always acts as marque II for the purposes of determining cost, though you can learn this augment despite your skill in automata.

#### Slippery Circuits
>   Brainworks Augment

Your automaton always seems to slip right out from under its attackers. Its sensors are in full usage, fully optimizing the automaton for jumping away from harm. It gains a bonus of evade rolls.

|Marque| Effect|
|--|--|
|1|+2 Evade|
|2|+3 Evade|
|3|+4 Evade|
|4|+5 Evade|

#### #### Connection
> Accuracy +2, Priority +1, Hit Points +7
>   Brainworks Augment

> ❓ Requires: Brainworks built with Personality

Your brainworks better understands what it means to be ‘alive’ and has been programmed with the mental fortitude of a monk. This allows the automaton to better replicate and control its Tephran emotions, granting it bonuses to Spirit.

|Marque| Effect|
|--|--|
|1|+3 Spirit|
|2|+12 Spirit|
|3|+31 Spirit|
|4|+47 Spirit|

#### Targeting Program
>   Brainworks Augment

Your brainworks can zero in on a target, calculating the exact attack trajectory required to hit them.

|Marque| Effect|
|--|--|
|1|+2 Accuracy|
|2|+4 Accuracy|
|3|+6 Accuracy|
|4|+8 Accuracy|

#### Fuse Box Specialist
>   Brainworks Augment

Your fusebox is better at using its specialties. Instead of having 1 point in their skills for using their specialties and for prerequisites, they use the following numbers:

|Marque| Effect|
|--|--|
|1|2 skill points for specialties|
|2|4 skill points for specialties|
|3|10 skill points for specialties|
|4|18 skill points for specialties|

#### Master Fuse Box
>   Brainworks Augment

> ❓ Requires: fusebox augmented with Fuse Box Specialist augment

Your fusebox is a master of using their specialties. They have an additional skill bonus for their specialties and for prerequisites on top of their Fusebox Specialist augment

|Marque| Effect|
|--|--|
|1|+1 skill points for specialties|
|2|+3 skill points for specialties|
|3|+5 skill points for specialties|
|4|+7 skill points for specialties|

### Crafting Clockworks

#### Clockwork Crafter
> Augments +2, DIY +1, Hit Points +4

> Automata Specialty

You can now create new kinetically-powered automatons and upgrade the analytical engine that controls its pre-determined responses. Tephrans colloquially call them “clockworks.”

        Without spending any money, you can build and maintain several clockworks based on your current Do-It-Yourself (DIY) score. The analytical engines of these clockworks can then be upgraded with augments. You’ll learn 2 augments from this specialty, which can be selected under “analytics augments” below. These augments have marques. At lower levels, you’ll start with Marque I augments. As your skill in Automata improves, your marques will increase. See the “Crafting” page at the beginning of this chapter for more information.  

        Each analytics can be upgraded with 3 augments. Sometimes an augment will take up multiple augment slots. For example, the “flight” augment is worth 2 slots, so an analytics unit only has 1 more available slot for an augment after “flight” has been applied.

#### Number of Clockworks you can Maintain  

Without needing to buy pieces or parts, you can build some clockworks entirely out of scraps. These automatons must be constantly maintained by you and stop working soon after leaving your care. You may build and maintain a number of clockworks for free based on your DIY score. You may build new clockworks or augment old ones during any period of downtime you have.

#### The Cost of Clockworks 

If you need to build a clockwork that you can’t build for free from your DIY score, you will need to buy the materials for it.

        A clockwork without augments will cost 50 princes. If you are building a clockwork, the clockwork will have a base materials cost. It is 1/5th the market price (10 princes).

        Every augment will increase the price. The higher the marque, the greater the price. The market price for an augment can be found in the chart below.

If you are building the augment, you pay 1/5th the price, which is the same as if you were buying an augment one marque lower. (As in, the material cost for a Marque III augment is the market price for a Marque II augment.) The material cost for a Marque 1 augment is 10 princes.

#### Advanced Analytics
> Augments +2, DIY +1, Hit Points +5

> Automata Specialty

> ❓ Requires: 4 skill points in Automata & Clockwork Crafter specialty

Your analytics go far beyond what most other automata builders could conceptually imagine. Such analytics have two more slots for you to place augments into.

Using your Clockworks

Clockwork automatons are the oldest automatons, as Velkya, the founder of Evangless, was fond of using them. Clockwork automatons have come a long way, however, and now use analytical engines to evaluate their surroundings and enact pre-programmed responses. Clockworks can now work for hours and hours without needing any maintenance (or winding up, as it would be).

### Control Method

Clockworks work by using pre-programmed responses called Directives. Without a Directive augmented onto them, a clockwork will not make any actions. Each directive acts separately from the rest. Clockworks don’t have action points, and require no action points from its master to function. Therefore if your clockwork is hit with an attack that lowers its number of action points for a turn, your clockwork is unaffected. Outside of combat you can temporarily turn off and on some (or all) of your clockwork’s directives to avoid it attacking townsfolk.

### Body of the Machine

Clockworks will begin with an analytical engine, which has 3 slots to be augmented with pre-programmed responses. This can be upgraded with Advanced Analytics, which allows for 2 more slots. Some crafters learn how to make punch cards, to re-program their clockworks on the spots. Body parts can then be added onto the analytical engine.

        Your clockwork (by default) is built with a head to house its analytical engine and a torso and groin to house its perpetual motion clockwork. The two are connected via a neck support. To its torso you may attach either two arms or one unit of movement. Its groin may have a unit of movement attached to it. A unit of movement is either a set of legs, wheels, or a propulsion device. (See the Units of Movement sidebar for more information.)

        Clockworks have no hit points and 20 points of wounds. Any “bleeding” damage your automaton takes causes springs and cogs to pop out of place within its clockwork, making it take bleeding damage in the same fashion as any regular organism.

Analytics Augments M

#### Avenge-Me Directive
>   Analytics Augment

Your analytical engine thoughtlessly fights back against those who try and harm the target of this augment. When applying this augment, choose someone (usually yourself) to be its target. When the target of this augment is attacked and the attacker is within range of this clockwork, the clockwork will automatically attack the attacker once, using a weapon if it is wielding one. The number of times the clockwork will do this per combat turn is determined by the marque of this augment.

|Marque| Effect|
|--|--|
|1|Will perform this action once|
|2|Will perform this action twice|
|3|Will perform this action twice|
|4|Will perform this action thrice|

#### Defend-Your-Area Directive
>   Analytics Augment

Your clockwork attacks personnel who enter a space adjacent to them, exempting anyone you’ve pre-programmed them to see as an ally (usually you and any party members you don’t want your automaton to chop into pieces). They will perform this action a number of times per combat turn determined by the marque of this augment. If they are blinded to or cannot recognize someone they are pre-programmed to see as an ally, they will attack them using this augment. They will only attack a specific target once per adjacent space they walk through per turn.

|Marque| Effect|
|--|--|
|1|Will act out this directive once|
|2|Will act out this directive twice|
|3|Will act out this directive twice|
|4|Will act out this directive thrice|

#### Do-Not-Die Component
>   Analytics Augment

Your clockwork is designed to take more hits before its inevitable destruction, not that it cares.

|Marque| Effect|
|--|--|
|1|+10 wounds|
|2|+20 wounds|
|3|+30 wounds|
|4|+40 wounds|

#### Easy Repairs
>   Analytics Augment

Used with the Automaton Repairs specialty

The automaton is designed in a logical, accessible way that makes repairs easy. Whenever somebody is attempting to repair it via the Automaton Repairs specialty, they gain a bonus on their automata roll.

|Marque| Effect|
|--|--|
|1|+3 on the Automaton Repairs rolls|
|2|+6 on the Automaton Repairs rolls|
|3|+9 on the Automaton Repairs rolls|
|4|+12 on the Automaton Repairs rolls|

#### Flight
>   Analytics Augment

Takes up 2 augment slots on the Brainworks

The automaton has been outfitted with a graviton sphere, allowing it to float and move. It can now fly, with its speed depending on the marque. Any speed penalties you take from armor will penalize this speed.

|Marque| Effect|
|--|--|
|1|5 feet of flight speed per action point|
|2|10 feet of flight speed per action point|
|3|15 feet of flight speed per action point|
|4|20 feet of flight speed per action point|

#### Follow-Me Directive
>   Analytics Augment

Your clockwork will aimlessly follow the target of this augment. When applying this augment, choose someone (usually yourself) to be its target. Whenever the target of this augment moves, this clockwork will move as far as it can in the same direction, attempting to get to a space adjacent to them.

|Marque| Effect|
|--|--|
|1|Will move toward their target once per turn|
|2|Will move toward their target twice per turn|
|3|Will move toward their target three times per turn|
|4|Will move toward their target four times per turn|

#### Go-There Directive
>   Analytics Augment

> 🪙 Activation Cost: 1 AP (see below)

Your clockwork is programmed to move where you want it to, although its analytical engine needs guidance when doing so. When applying this augment, choose someone (usually yourself) to be its target. For 1 action point the target of this augment can, through pointing and probably yelling, make your clockwork move once. If your clockwork has a separate directive that makes it move, directing your clockwork somewhere with this augment will not prevent it from carrying out the other directive the first chance it gets.

> 🗒️️         Note: This augment always acts as Marque II for the purposes of determining cost, though you can learn this augment despite your skill in Automata.

#### Hedge-Your-Area Subdirective
>   Analytics Augment

> ❓ Requires: Analytics with the Defend-Your-Area Directive augment

When someone causes your automaton to act out their Defend-Your-Area Directive, your clockwork is pre-programmed to not fail its programmer. It gains an accuracy bonus on any attacks made while acting out the Defend-Your-Area Directive.

|Marque| Effect|
|--|--|
|1|+3 Accuracy|
|2|+6 Accuracy|
|3|+9 Accuracy|
|4|+12 Accuracy|

#### Hit-Them Subdirective
>   Analytics Augment

> ❓ Requires: Analytics with the Avenge-Me Directive augment

When your automaton acts out its Avenge-Me Directive it is accurate about doing so. It gains an accuracy bonus on any attacks made while acting out the Avenge-Me Directive.

|Marque| Effect|
|--|--|
|1|+3 Accuracy|
|2|+6 Accuracy|
|3|+9 Accuracy|
|4|+12 Accuracy|

#### Hurt-Them Subdirective
>   Analytics Augment

> ❓ Requires: Analytics with the Avenge-Me Directive augment

Your analytical engine is designed to mindlessly put all of its strength into dealing more damage whenever it acts out its Avenge-Me Directive. It gains a strike bonus on any attacks made while acting out the Avenge-Me Directive.

|Marque| Effect|
|--|--|
|1|+4 Strike|
|2|+8 Strike|
|3|+12 Strike|
|4|+16 Strike|

#### Lightning Resistant
>   Analytics Augment

The automaton is naturally resistant to lightning and electricity. Electrical damage is now soakable as regular damage.

> 🗒️️         Note: This augment always acts as marque III for the purposes of determining cost, though you can learn this augment despite your skill in automata.

#### March-There Subdirective
>   Analytics Augment

> ❓ Requires: Analytics with the Go-There Directive augment

More people can guide your clockwork’s aimless wandering around the battlefield. Based on the marque of this augment, you can program in more targets for your Go-There Directive.

|Marque| Effect|
|--|--|
|1|One additional guide|
|2|Two additional guides|
|3|Three additional guides|
|4|Four additional guides|

#### Mark-Your-Area Subdirective
>   Analytics Augment

> ❓ Requires: Analytics with the Defend-Your-Area Directive augment

When someone causes your automaton to act out their Defend-Your-Area Directive, your clockwork is designed to make their prey regret it. It gains a strike bonus on any attacks made while acting out the Defend-Your-Area Directive.

|Marque| Effect|
|--|--|
|1|+4 Strike|
|2|+8 Strike|
|3|+12 Strike|
|4|+16 Strike|

#### Passenger
>   Analytics Augment

Your automaton has a hollow compartment inside, allowing people to ride inside and see out through eye holes or monitors of your design. While inside an automaton, a character cannot be directly targeted but they in turn cannot interact with anything outside of the automaton, although the automaton can still hear all voice commands. The number of people the automaton can hold is equal to the number of spaces it takes up. The amount of action points required by passengers to get into and out of the automaton is based on the marque of this augment.

|Marque| Effect|
|--|--|
|1|4 AP to enter or exit the automaton (can take multiple turns)|
|2|3 AP to enter or exit the automaton|
|3|2 AP to enter or exit the automaton|
|4|1 AP to enter or exit the automaton|

#### Protect-Me Directive
>   Analytics Augment

Your clockwork will witlessly jump in the way of attacks made against the target of this augment. When applying this augment, choose someone (usually yourself) to be its target. When adjacent to the target of this augment, this clockwork will take damage in their stead. This clockwork will only do this a certain number of times per turn determined by the marque of this augment.

|Marque| Effect|
|--|--|
|1|Will take damage instead of their target once per turn|
|2|Will take damage instead of their target twice per turn|
|3|Will take damage instead of their target thrice per turn|
|4|Will take damage instead of their target four times per turn|

#### Protect-Us Subdirective
>   Analytics Augment

> ❓ Requires: Analytics with the Protect-Me Directive augment

You can program in additional targets (usually other party members) of your automaton’s Protect-Me Directive, although they still must be adjacent to any persons they attempt to protect.

|Marque| Effect|
|--|--|
|1|1 additional protectee|
|2|2 additional protectees|
|3|3 additional protectees|
|4|4 additional protectees|

#### Protect-Yourself Subdirective
>   Analytics Augment

> ❓ Requires: Analytics with the Protect-Me Directive augment

When your automaton acts out its Protect-Me Directive it is better about protecting itself. It gains a defense bonus while acting out the Protect-Me Directive.

|Marque| Effect|
|--|--|
|1|+4 Defense|
|2|+8 Defense|
|3|+12 Defense|
|4|+16 Defense|

#### Protected Core
>   Analytics Augment

If your automaton is defeated in battle, its insides are heavily safeguarded to allow for easy repair during your next breather. An automaton with a protected core can be entirely rebuilt in a matter of minutes. The automaton can be repaired up to a certain amount of maximum wounds, based on the marque. It cannot be repaired past its normal maximum number of wounds.

|Marque| Effect|
|--|--|
|1|10 wounds after repairs|
|2|20 wounds after repairs|
|3|30 wounds after repairs|
|4|40 wounds after repairs|

#### Realistic
>   Analytics Augment

> ⬇️ Resist: Cunning (negates)

You’ve designed the body of your automaton to look quite real, replicating the appearance of a living or imaginary creature. Any persons who looks upon it will believe it to be real unless they can beat the automaton’s roll with their Cunning. The automaton’s roll gains a bonus based on the marque of this augment.

|Marque| Effect|
|--|--|
|1|+5 to fool target|
|2|+10 to foot target|
|3|+15 to fool target|
|4|+20 to fool target|

#### Run-There Subdirective
>   Analytics Augment

> ❓ Requires: Analytics with the Go-There Directive augment

While your analytical engine still has difficulties following directions, it is faster in getting to its destination once you set it on the right path. It gains a speed bonus while acting out the Go-There Directive.

|Marque| Effect|
|--|--|
|1|+5 feet of movement|
|2|+10 feet of movement|
|3|+15 feet of movement|
|4|+25 feet of movement|

#### Shadow-Me Subdirective
>   Analytics Augment

> ❓ Requires: Analytics with the Go-There Directive augment

Your clockwork is designed to stay beside its target no matter what. Whenever your clockwork acts out its Follow-Me Directive, it can make one additional movement towards its target per combat turn.

> 🗒️️         Note: This augment always acts as Marque I for the purposes of determining cost, though you can learn this augment despite your skill in Automata.

#### Secure-Your-Area Subdirective
>   Analytics Augment

> ❓ Requires: Analytics with the Defend-Your-Area Directive augment

If someone attempts to attack your automaton while already in a space adjacent to them, your automaton will spend one of its Defend-Your-Area Directive actions (if it has any available) attacking them after they finish their attack. They still will not attack anyone programmed into them as a friendly.

> 🗒️️         Note: This augment always acts as Marque II for the purposes of determining cost, though you can learn this augment despite your skill in Automata.

#### Shield-Yourself Subdirective
>   Analytics Augment

> ❓ Requires: Analytics with the Protect-Me Directive augment

When your automaton acts out its Protect-Me Directive it won’t take as much damage. It gains a soak class bonus while acting out the Protect-Me Directive.

|Marque| Effect|
|--|--|
|1|+1 soak class|
|2|+2 soak class|
|3|+3 soak class|
|4|+4 soak class|

#### Spring-Into-Action Component
>   Analytics Augment

Takes up 2 augment slots on the Brainworks

You’ve made sure that anyone who attempts to damage the inner clockwork of your automaton will rue the day they do. Whenever someone deals bleeding damage to your wind-up, including any taken from wounds or fatal effects, the clockwork that springs out has been barbed and serrated and will fly in the direction of the person who dealt the damage. With a range of 10 feet, the victim of this augment is denied an evade roll.

|Marque| Effect|
|--|--|
|1|8 damage|
|2|16 damage|
|3|24 damage|
|4|32 damage|

#### Sprint-to-Me Subdirective
>   Analytics Augment

> ❓ Requires: Analytics with the Follow-Me Directive augment

Your clockwork’s body inanely lurches towards its target as fast as its body will allow. Your automaton can move further per movement whenever it is carrying out its Follow-Me Directive.

|Marque| Effect|
|--|--|
|1|+10 feet of movement|
|2|+20 feet of movement|
|3|+30 feet of movement|
|4|+40 feet of movement|

#### Stay-Alive Component
>   Analytics Augment

You’ve built your wind-up to continue following its pre-programmed directives well past when most other automatons would have fallen on the battlefield.

|Marque| Effect|
|--|--|
|1|+10 wounds|
|2|+20 wounds|
|3|+30 wounds|
|4|+40 wounds|

### Crafting Prosthetics

#### Prosthetician
> Augments +2, DIY +1, Hit Points +6

> Automata Specialty

You now have the ability to craft mechanical prosthetics onto people or augment the limbs of automatons. When you create a prosthetic for a person, it works as a limb replacement. Of course, once you affix your prosthetics with hidden cannons, chainsaws, and ridiculous pistons, the comparison stops there.

        Without spending any money, you can build and maintain several prosthetics based on your current Do-It-Yourself (DIY) score. The prosthetics can then be upgraded with augments. You’ll learn 2 augments from this specialty, which can be selected under “universal prosthetic augments,” “prosthetic arm augments,” “prosthetic hand augments,” or “prosthetic leg augments,” below. These augments have marques. At lower levels, you’ll start with Marque I augments. As your skill in Automata improves, your marques will increase. See the “Crafting” page at the beginning of this chapter for more information.  

        Each prosthetic can be upgraded with 3 augments. Some materials can only be upgraded twice (like wooden prosthetics) or just once (like organic ones). Sometimes an augment will take up multiple augment slots. For example, if an augment is worth 2 slots, a prosthetic only has 1 more available slot for an augment after the 2-slot augment has been applied.

#### Number of Prosthetics you can Maintain  

Without needing to buy pieces or parts, you can build some prosthetics entirely out of scraps. These prosthetics must be constantly maintained by you and stop working soon after leaving your care. You may build and maintain a number of prosthetics for free based on your DIY score. You may build new prosthetics or augment old ones during any period of downtime you have.

        Replacing both an arm and hand does count as 2 prosthetics.

#### The Cost of Prosthetics 

If you need to build prosthetics that you can’t build for free from your DIY score, you will need to buy the materials for it.

        A prosthetic without augments will cost 25 princes at market value. If you are building a prosthetic, the prosthetic will have a base materials cost. It is 1/5th the market price (5 princes).

        Every augment will increase the price. The higher the marque, the greater the price. The market price for an augment can be found in the chart below.

If you are building the augment, you pay 1/5th the price, which is the same as if you were buying an augment one marque lower. (As in, the material cost for a Marque III augment is the market price for a Marque II augment.) The material cost for a Marque 1 augment is 5 princes.

Using Prosthetics

When you first learn to make prosthetics (through the Prosthetician specialty), you can make and augment hands, arms, and legs. If somebody is missing a hand, arm, or leg, you can replace it with a prosthetic. The only lasting penalty to having a prosthetic limb is the permanent loss of wounds that come from losing a limb. Universal prosthetic augments can go on any prosthetics.

### Wounds & Fatals

If you suffer a wound or fatal effect against a prosthetic, the effect is the same, except that you cannot bleed out by having a severed prosthetic.

### Upgrading Automaton Limbs

You can add prosthetics onto an automaton or augment its default limbs. If you upgrade the existing limbs, it counts against your limit of prosthetics you can make and maintain with your DIY score.

#### Beta Prosthetics
> Augments +2, DIY +1, Hit Points +6

> Automata Specialty

> ❓ Requires: 4 skill points in Automata & Prosthetician specialty

Your prosthetics are quite complex, but they can accomplish quite a bit. Such prosthetics can be upgraded with 2 more augments (bringing the total for metal prosthetics up to 5 augmentable slots).

        You can only apply beta prosthetics to yourself and automatons that were created by you as either beta (for boilers) or advanced (for analytics and brainworks) models.

#### Prototype Prosthetics
> Augments +1, DIY +1, Hit Points +7

> Automata Specialty

> ❓ Requires: 16 skill points in Automata, Prosthetician, & Beta Prosthetics specialties

You’ve perfected your beta prosthetics and made them user-friendly. Now anybody can use a prosthetic that you designate as being a prototype.

#### Automata Tinkerer
> Augments +2, DIY +1, Hit Points +4

> Automata Specialty

> ❓ Requires: 3 skill points in Automata & Prosthetician specialty

You can augment all pieces of your automatons - from their eyes to their ears to their torsos. You can learn augments for every part of the automaton’s body.

        If you have Automata Upgrader, you can use that in conjuction with Automata Tinkerer, causing all of the automaton’s body parts to not cost anything or count against your Do-It-Yourself (DIY) score.

#### Automata Upgrader
> Augments +2, DIY +1, Hit Points +4

> Automata Specialty

> ❓ Requires: 3 skill points in Automata & Prosthetician specialty

You’ve tinkered and played with your automatons so much that you no longer use your Do-It-Yourself (DIY) score to determine how many of your automaton’s limbs you can upgrade. Whenever you are augmenting the default limbs of your automatons, those limbs do not count against the maximum number of prosthetics you can create.

#### Nerve Crafting
> Augments +1, DIY +1, Hit Points +7

> Automata Specialty

> ❓ Requires: 7 skill points in Automata & Prosthetician specialty

It is normally impossible to attach a prosthetic arm to one’s ribcage, or a prosthetic hand to one’s spine. You, however, have learned how to create nerve endings in a person where none should exist. You may now apply extra prosthetics to a person beyond what they could normally handle (allowing you to add a third arm, an extra hand, maybe even another leg).

        When you nerve craft somebody, they permanently lose 1 wound in order to gain the prosthetic. They gain the wound back if they surgically remove the prosthetic.

#### Sensory Builder
> Augments +2, DIY +1, Hit Points +7

> Automata Specialty

> ❓ Requires: Prosthetician specialty

Some of the most difficult things to create prosthetics for are one’s ears and eyes, but you’ve learned the craft. You may now create prosthetics for ears and eyes, and they count against your maximum crafting amount of prosthetics based on your Do-It-Yourself (DIY) score.

### Universal Prosthetic Augments

#### Acid Sprayer
>   Universal Prosthetic Augment

You can insert doses of alchemical acids into the prosthetic. When used in an unarmed attack, the body part will spray a single dose of acid onto your target. Luckily the acid is held in such a way that it will not damage your body part. Depending on the marque of this augment, your body part can hold a certain number of acid doses. It costs 3 action points to add in a single additional dose of acid.

|Marque| Effect|
|--|--|
|1|Maximum of Two Doses|
|2|Maximum of Three Doses|
|3|Maximum of Four Doses|
|4|Maximum of Six Doses|

> 🗒️️ Note: For the crafting of acids, see Alchemy.

#### Air Blaster
>   Universal Prosthetic Augment

> ⬇️ Resist: Brute (marques down)

> 🪙 Activation Cost: 1 AP

Powerful ventilators have been built into your body part, allowing you to blow a strong, concentrated stream of pure air straight at an opponent within your 25 feet of range. They cannot evade your air blast, but they can resist with their Brute.

|Marque| Effect|
|--|--|
|1|Your target is pushed back 5 feet|
|2|Your target is pushed back 5 feet|
|3|Your target is pushed back 10 feet|
|4|Your target is pushed back 15 feet|

#### Air Conditioner
>   Universal Prosthetic Augment

> ❓ Requires: Prosthetic with the Air Blaster augment

> 🪙 Activation Cost: 1 AP (see below)

You are able to superheat or supercool the air from your air blaster, causing it to deal either heat or freezing damage whenever you use it, in addition to its normal effects. For one action point, you can switch your air blaster between heat and freezing damage or turn the effects of this augment on and off. The damage is soakable.

|Marque| Effect|
|--|--|
|1|2 heat or freezing damage|
|2|4 heat or freezing damage|
|3|6 heat or freezing damage|
|4|8 heat or freezing damage|

#### Air Tunnel
>   Universal Prosthetic Augment

> ❓ Requires: Prosthetic with the Air Blaster augment

Your air blaster has been overclocked, causing its normal gust of wind to become a miniature tornado erupting from your body part. Whenever you successfully push back your target with your air blaster, they are moved an additional 5 feet backwards. In addition, this augment increases your air blaster’s range.

|Marque| Effect|
|--|--|
|1|5 additional feet of range|
|2|10 additional feet of range|
|3|15 additional feet of range|
|4|20 additional feet of range|

#### Barbed
>   Universal Prosthetic Augment

You’ve laced the body part with spikes of your design. Whenever someone attempts to grab the body part, you get a bonus on your roll to avoid it.

|Marque| Effect|
|--|--|
|1|+2 on the evade roll|
|2|+4 on the evade roll|
|3|+6 on the evade roll|
|4|+8 on the evade roll|

#### Brute Enhancement
>   Universal Prosthetic Augment

The body part is built for rigidity and power. When the body part is involved in a Brute roll, it gains a bonus.

|Marque| Effect|
|--|--|
|1|+2 to Brute|
|2|+4 to Brute|
|3|+6 to Brute|
|4|+8 to Brute|

#### Cold Iron
>   Universal Prosthetic Augment

> ❓ Requires: Body Part must be made of metal & the Freezer Augment somewhere on subject’s body

This prosthetic leaks small amounts of liquid nitrogen diverted from your freezer. Whenever someone comes into contact with this body part, whether it be them grabbing you or you punching them, they suffer first degree frostbite. This means they will suffer increased sensitivity to heat and cold lasting until their next breather. This augment deactivates if your freezer is extinguished. This effect will not stack with multiple exposures to a cold iron body part.

|Marque| Effect|
|--|--|
|1|Target suffers a -3 when resisting effects caused by heat, flames, cold, or ice|
|2|Target suffers a -6 when resisting effects caused by heat, flames, cold, or ice|
|3|Target suffers a -9 when resisting effects caused by heat, flames, cold, or ice|
|4|Target suffers a -12 when resisting effects caused by heat, flames, cold, or ice|

#### Compartment
>   Universal Prosthetic Augment

Your prosthetic has a small hollow container inside of it, allowing the storage of different items. It costs one action point to take an item out of your compartment.

> 🗒️️         Note: This augment always acts as Marque I for the purposes of determining cost, though you can learn this augment despite your skill in Automata.

#### Disguised
>   Universal Prosthetic Augment

The prosthetic is disguised as either being a regular part of the body or being covered in something so that it does not appear to be mechanical, and is built to be silent and react like a normal body part. In order to notice your prosthetic for what it truly is, opponents must roll Cunning.

|Marque| Effect|
|--|--|
|1|Tier 2 Cunning to notice|
|2|Tier 3 Cunning to notice|
|3|Tier 3 Cunning to notice|
|4|Tier 4 Cunning to notice|

> 🗒️️ Note: Organic body parts act one marque higher than normal. If this would make it a Marque V, the body part is indistinguishable from a normal limb unless somebody can get a Tier 5 Cunning result to notice.

#### Flame Exhausts
>   Universal Prosthetic Augment

> ❓ Requires: Furnace Augment somewhere on subject’s body

> ⬇️ Resist: Dexterity (marques down)

> 🪙 Activation Cost: 3 AP

Exhausts protrude from the prosthetic, flames periodically leaking out of them. For three action points you cause the body part to spray flames onto everyone adjacent to you to set them on fire, although they can still attempt to resist the attack. You cannot activate your flame exhausts if your furnace is extinguished.

|Marque| Effect|
|--|--|
|1|Tier 1 Burning: 2 unsoakable damage per turn, 2 AP to put out the fire|
|2|Tier 1 Burning: 2 unsoakable damage per turn, 2 AP to put out the fire|
|3|Tier 2 Burning: 4 unsoakable damage per turn, 4 AP to put out the fire, wooden and cloth items destroyed|
|4|Tier 3 Burning: 8 unsoakable damage per turn, 8 AP to put out the fire, wooden and cloth items destroyed|

#### Freeze Exhausts
>   Universal Prosthetic Augment

> ❓ Requires: Freezer augment somewhere on subject’s body

> ⬇️ Resist: Brute (marques down)

> 🪙 Activation Cost: 3 AP

Exhausts protrude from the prosthetic, icy mist periodically leaking out of it. For three action points you cause the body part to spray this mist onto everyone adjacent to you, although they can still attempt to resist the attack. This mist causes your target’s body temperature to rapidly drop, making them sluggish until the start of your next turn. You cannot activate your freeze exhausts if your freezer is extinguished.

|Marque| Effect|
|--|--|
|1|Target suffers a -2 to evade|
|2|Target suffers a -4 to evade|
|3|Target suffers a -6 to evade|
|4|Target suffers a -8 to evade|

#### Freezer
>   Universal Prosthetic Augment

A cooler filled with liquid nitrogen has been built into your prosthetic. This keeps your body cool even in the hottest of environments. However, if the body part is hit by an attack dealing heat damage, the liquid nitrogen inside will evaporate rapidly, extinguishing your freezer. It costs 1 action point to extinguish the freezer yourself, and it will cost action points to reactivate your freezer when extinguished depending on the augment’s marque.

|Marque| Effect|
|--|--|
|1|2 AP to reactivate|
|2|1 AP to reactivate|
|3|0 AP to reactivate|
|4|0 AP reflexively to reactivate (can do out of turn)|

#### Furnace
>   Universal Prosthetic Augment

Fueled by coal or aether, a live flame burns within you. It keeps your entire body warm, allowing you to take no penalties in subzero environments. If the body part gets splashed with water it will automatically extinguish the flames inside. It costs 1 action point to extinguish the furnace yourself, and it will cost action points to reactivate your furnace when extinguished depending on the augment’s marque.

|Marque| Effect|
|--|--|
|1|2 AP to reactivate|
|2|1 AP to reactivate|
|3|0 AP to reactivate|
|4|0 AP reflexively to reactivate (can do out of turn)|

> 🗒️️ Note: Your furnace is self-contained and will not burn you, though you cannot apply the furnace augment to a wooden prosthetic.

#### Glowing
>   Universal Prosthetic Augment

Your prosthetic shines through the darkness either from a glow-in-the-dark coating, numerous lightbulbs placed in it, or another source of built-in light. Light shines outward from you, dispelling darkness and allowing those within its range to see normally. This can be activated and repressed for 1 action point.

|Marque| Effect|
|--|--|
|1|Light extends to spaces adjacent to you|
|2|Light extends outwards up to 10 feet away|
|3|Light extends outwards up to 15 feet away|
|4|Light extends outwards up to 20 feet away|

#### Hidden Blade
>   Universal Prosthetic Augment

> ❓ Requires: Prosthetic augmented with Weapon Mounting

The prosthetic has a secret weapon hidden inside that can flip out at a moment’s notice. For no action point cost, you can now retract your weapon into your body, as long as the weapon would normally be concealable.

> 🗒️️         Note: This augment always acts as Marque II for the purposes of determining cost, though you can learn this augment despite your skill in Automata.

#### Hidden Claymore
>   Universal Prosthetic Augment

> ❓ Requires: Prosthetic augmented with Hidden Blade

The prosthetic can fit an enormous weapon inside it by breaking down its key components. For no action point cost you can now retract your weapon into your body, even if the weapon would normally not be concealable.

> 🗒️️         Note: This augment always acts as Marque III for the purposes of determining cost, though you can learn this augment despite your skill in Automata.

#### Hot Iron
>   Universal Prosthetic Augment

> ❓ Requires: Prosthetic must be made of metal & the Furnace augment somewhere on subject’s body

As long as fire burns within your body, it constantly heats up this body part, making it burn to the touch. Whenever someone comes into contact with this body part, whether it be them grabbing you or you punching them, they take a small amount of heat damage. This doesn’t apply if your furnace is extinguished.

|Marque| Effect|
|--|--|
|1|2 heat damage|
|2|4 heat damage|
|3|6 heat damage|
|4|8 heat damage|

#### Overglow
>   Universal Prosthetic Augment

> ❓ Requires: Prosthetic with the Glowing Augment

> ⬇️ Resist: Cunning (marques down)

> 🪙 Activation Cost: 1 AP

Light erupts from your prosthetic, arcing toward a single target within 25 feet with the potential to blind them (giving them a -4 on accuracy and evade rolls). They can resist with their Cunning attribute.

|Marque| Effect|
|--|--|
|1|Blinds for one turn|
|2|Blinds for two turns|
|3|Blinds for three turns|
|4|Blinds for four turns|

#### Poison Injector
>   Universal Prosthetic Augment

You can insert doses of alchemical poison into the prosthetic. When used in an unarmed attack, a single dose of the poisons held inside will automatically inject themselves into your target. Depending on the marque of this augment, your body part can hold a certain number of poison doses. It costs 3 action points to add in a single additional dose of poison.

|Marque| Effect|
|--|--|
|1|Maximum of 1 Dose|
|2|Maximum of 2 Doses|
|3|Maximum of 3 Doses|
|4|Maximum of 4 Doses|

> 🗒️️ Note: For the crafting of poisons, see Alchemy.

#### Precision
>   Universal Prosthetic Augment

The body part is built for precision and flexibility. When the body part is involved in a Dexterity roll, it gains a bonus.

|Marque| Effect|
|--|--|
|1|+2 to Dexterity|
|2|+4 to Dexterity|
|3|+6 to Dexterity|
|4|+8 to Dexterity|

#### Reinforced
>   Universal Prosthetic Augment

The prosthetic is strengthened with internal plating, granting it a resist against called shots.

|Marque| Effect|
|--|--|
|1|+4 to resist when struck|
|2|+8 to resist when struck|
|3|+12 to resist when struck|
|4|+16 to resist when struck|

#### Removable
>   Universal Prosthetic Augment

The prosthetic is easily removable and replaceable. If other body parts are connected to it (e.g. if this is placed on the arm, and a hand is attached to the arm) they are removed along with this body part.

|Marque| Effect|
|--|--|
|1|4 AP to remove|
|2|3 AP to remove|
|3|2 AP to remove|
|4|1 AP to remove|

#### Smokestack
>   Universal Prosthetic Augment

> ❓ Requires: Furnace augment or Freezer augment somewhere on subject’s body

> ⬇️ Resist: Cunning (negates)

> 🪙 Activation Cost: 1 AP (see below)

A chimney or metal gratings constantly release clouds of smoke or fog from your furnace. This cloud billows out around you, making you and those within a certain range completely blinded. This will instantly end if your furnace or freezer is extinguished. You can spend one action point to start your smokescreen or to adjust the range of the cloud from affecting characters adjacent to you up to its maximum range detailed by the marque of this augment.

|Marque| Effect|
|--|--|
|1|Only characters adjacent to you are blinded|
|2|Anyone up to 10 feet of you are blinded|
|3|Anyone up to 15 feet of you are blinded|
|4|Anyone up to 20 feet of you are blinded|

#### Weapon Mounting
>   Universal Prosthetic Augment

A weapon of some kind can be mounted into the prosthetic in such a way that in order to sunder or disarm the weapon, the body part itself must be sundered or removed. The weapon can be removed or put into the weapon mounting for 1 action point.

> 🗒️️         Note: This augment always acts as Marque I for the purposes of determining cost, though you can learn this augment despite your skill in Automata.

### Prosthetic Arm Augments

#### Extendable Hand
>   Prosthetic Arm Augment

> ❓ Requires: placed on an arm with an attached hand

The hand attached to your augmented appendage can now be extended outward, whether it be with springs, hydraulics, or some other such method. It costs one action point to extend a limb but no action points to retract it. Extended hands function as normal, able to do anything they normally could do.

|Marque| Effect|
|--|--|
|1|+5 feet of reach|
|2|+10 feet of reach|
|3|+10 feet of reach|
|4|+15 feet of reach|

#### Grasper
>   Prosthetic Arm Augment

A grasper is a small set of pincers that can wield an item just like a hand. A grasper is not dexterous enough to wield items in conjunction with other limbs, preventing it from being able to use two-handed items.

> 🗒️️         Note: This augment always acts as Marque I for the purposes of determining cost, though you can learn this augment despite your skill in Automata.

### Prosthetic Hand Augments

#### Flame Pores
>   Prosthetic Hand Augment

> ❓ Requires: Furnace augment somewhere on subject’s body

> 🪙 Activation Cost: 1 AP (after a successful unarmed attack)

Holes in the limb make it expel flames from your furnace when it is used in an unarmed strike. As long as your furnace isn’t extinguished, you can spend 1 action point after a successful unarmed attack in order to burn your target. Multiple burns caused by your flame pores will not stack the penalties to a single target’s defense. These burns will go away during the foe’s next breather.

|Marque| Effect|
|--|--|
|1|Tier 1 Burns (-1 on all defense rolls)|
|2|Tier 2 Burns (-3 on all defense rolls)|
|3|Tier 3 Burns (-5 on all defense rolls)|
|4|Tier 4 Burns (-7 on all defense rolls)|

#### Freezing Pores
>   Prosthetic Hand Augment

> ❓ Requires: Freezer augment somewhere on subject’s body

> 🪙 Activation Cost: 1 AP (after a successful unarmed called shot)

Holes in the limb make it expel small streams of subzero mist from your freezer which will freeze solid on impact when used in an unarmed attack. As long as your freezer isn’t extinguished, this makes unarmed called shots with this limb ice over the part of your target you hit. This ice will temporarily impede the use of extremities, making called shot effects to extremities (hands, feet, head) last longer.

|Marque| Effect|
|--|--|
|1|Effects of the called shot last an additional turn|
|2|Effects of the called shot last two additional turns|
|3|Effects of the called shot last three additional turns|
|4|Effects of the called shot last four additional turns|

#### Hand-Launcher
>   Prosthetic Hand Augment

> 🪙 Activation Cost: 2 AP

You are able to fire your hand at an opponent, dealing large amounts of damage, though you do not retain the body part if you fire it (at least until you go find it). The range of your hand is 50 feet.

|Marque| Effect|
|--|--|
|1|Damage class 3|
|2|Damage class 4|
|3|Damage class 5|
|4|Damage class 6|

#### Neuromuscular Incapacitator
>   Prosthetic Hand Augment

> ⬇️ Resist: Brute (marques down)

An electrical current covering your body part disrupts voluntary muscular control of anyone you hit it with during an unarmed attack.  

|Marque| Effect|
|--|--|
|1|Your target vibrates for a second, but is otherwise fine|
|2|Your target is stunned for one action point|
|3|Your target is stunned for one action point|
|4|Your target is stunned for two action points|

#### Propellers
>   Prosthetic Hand Augment

Your prosthetic is built with propellers for moving across water, allowing your swim speed to increase based on its marque. Your swim speed is still hindered by your armor.

|Marque| Effect|
|--|--|
|1|+10 swim speed|
|2|+15 swim speed|
|3|+20 swim speed|
|4|+25 swim speed|

#### Retractable
>   Prosthetic Hand Augment

> ❓ Requires: Hand-Launcher augment applied to the hand

You can have your hand tethered to a rope or chain that automatically returns to you after being fired. Though you do not have to do anything, it does take a little bit of time before the hand returns to you.

|Marque| Effect|
|--|--|
|1|Returns within 3 AP|
|2|Returns within 2 AP|
|3|Returns within 1 AP|
|4|Returns immediately|

### Prosthetic Leg Augments

#### Extreme Speed
>   Prosthetic Leg Augment

The appendage is streamlined for speed, allowing the user to have greater overland velocity.

|Marque| Effect|
|--|--|
|1|+5 speed|
|2|+5 speed|
|3|+10 speed|
|4|+15 speed|

#### Springs
>   Prosthetic Leg Augment

By melding high-tension springs into your graft, its user gains extra height whenever jumping or leaping.

|Marque| Effect|
|--|--|
|1|+5 feet jump height|
|2|+10 feet jump height|
|3|+15 feet jump height|
|4|+20 feet jump height|

#### Propellers
>   Prosthetic Leg Augment

Your prosthetic is built with propellers for moving across water, allowing your swim speed to increase based on its marque. Your swim speed is still hindered by your armor.

|Marque| Effect|
|--|--|
|1|+10 swim speed|
|2|+15 swim speed|
|3|+20 swim speed|
|4|+25 swim speed|

Automaton-Specific

While all prosthetic augments can be applied to automatons, the following augments (which would augment an automaton’s head, torso, or groin) cannot be applied to living people. Thus, these are automaton-specific augments that you’ll need the Automaton Tinkerer specialty for.

Automaton Head Augments M

#### Sensory Transmission
>   Automaton Head Augment

The automaton can broadcast its senses to a single device (often held by the user) for 1 action point. The distance it can broadcast is determined by the marque of the augment.

|Marque| Effect|
|--|--|
|1|Up to 200 feet away|
|2|Up to 2,000 feet away|
|3|Up to 2 miles away|
|4|Up to 20 miles away|

### Automaton Torso Augments

#### Back-Pack
>   Automaton Torso Augment

The automaton has the ability to latch onto an ally’s back, acting as the ally’s backpack. This leaves all of the automaton’s limbs free to act as normal while the ally carries the automaton around. The automaton cannot move while being carried. It takes 1 turn for the automaton to become a back-pack or unpack itself and move under its own power again.

> 🗒️️         Note: This augment always acts as Marque I for the purposes of determining cost, though you can learn this augment despite your skill in Automata.

#### Mass Propulsion
>   Automaton Torso Augment

Using a propeller, steamjet, or some other such device attached to the torso’s back, your subject is able to increase their thrust while moving. While this cannot give you the ability to fly, it grants bonuses to both land and flight movement speeds.

|Marque| Effect|
|--|--|
|1|+5 feet per move|
|2|+10 feet per move|
|3|+15 feet per move|
|4|+20 feet per move|

#### Poison Gas Epicenter
>   Automaton Torso Augment

Takes up 2 Augment Slots on the Torso

> 🪙 Activation Cost: 1 AP

You can insert a single vial of alchemical gas into the automaton’s torso. Once you activate this augment, the gas starts taking effect. When you end your turn, the gas releases from the automaton’s torso, affecting those around the automaton. This allows the automaton to move the gas around the battlefield. The automaton serves as the center of the gas’s area of effect, although through clever crafting the automaton doesn’t have to worry about the gas spraying onto itself. The gas lasts as long as it normally would.

        The amount of action points required to refill the gas supply is based off of the marque of this augment. It costs two action points to eject a dose of gas before it has run out on its own. This will cause it to stay in the space the automaton is currently standing in and will no longer move with the automaton.

|Marque| Effect|
|--|--|
|1|3 AP to refill gas chamber|
|2|3 AP to refill gas chamber|
|3|2 AP to refill gas chamber|
|4|1 AP to refill gas chamber|

> 🗒️️ Note: For the crafting of gases, see Alchemy.

#### Poison Gas Receptor
>   Automaton Torso Augment

> ❓ Requires: Torso with the Poison Gas Epicenter augment

You’ve upgraded your gas dispenser to hold extra doses of alchemical gases. You still can only release one gas at a time.

|Marque| Effect|
|--|--|
|1|Maximum of Two Doses|
|2|Maximum of Three Doses|
|3|Maximum of Four Doses|
|4|Maximum of Six Doses|

#### Sensor Array
>   Automaton Torso Augment

You’ve crafted a sensor array into your automaton’s torso. This allows you to place automaton head augments on the torso as well as the ability for the automaton to see and hear through its torso.

> 🗒️️         Note: This augment always acts as Marque II for the purposes of determining cost, though you can learn this augment despite your skill in Automata.

* * *

## Bio-Flux Skill
==============

Bio-flux is the craft of scientifically altering a creature’s basic physiology. Essence warping, genetic restructuring, and basic external flesh augmentations make up the bulk of this science. This is an often grueling and taboo process that has yet to become perfected and often leaves its subjects with unintentional side effects and scars. These can be as subtle as an unexpected change in eye color to as drastic as a reformed central nervous system.

        In its younger years this school of science birthed both the satyr race and the farishtaa-conversion process. It has revolutionized the medical fields, wiping out all birth-defects and genetic diseases from those able to afford it. Even at its most basic levels, bio-flux has the ability to fill a downtrodden gentleman with energy to keep him awake and ready for action regardless of how badly he had been knocked down.

Reputation of Bio-Flux

In addition to the sometimes monstrous consequences of augmenting someone’s genetic code, the process of bio-flux requires large amounts of essence to use as a pallet. Most bio-flux scientists harvest this raw essence from game and livestock animals, though it must be greatly refined multiple times and even then only yields a small amount of low quality essence.

        It was only after many less-than-reputable experimentations that it was discovered that sentient beings produce the purest essence available. These studies led to the widespread superstition that essence was more than primordial genetic material but a collection of personality and memories: a person’s soul. A disdain grew among the population against bio-flux, gaining its more fervent practitioners the nickname of Soul Merchants. Further outcry against the science came when the crown princess of Evangless went mad after being bio-revivified back to life. Unable to cope with scrambled memories and mental inconsistencies caused by the process, the princess’s mind became splintered, causing many government officials from around the world to ban bio-flux research. These days pure essence is a rare commodity that forces most, if not all, wishing to follow this forsaken art to turn to the darker corners of Rilausia to further their studies.

### Invigoration Specialties

#### Bio-Invigoration
> Evade +1, Defense +3, Hit Points +8

> Bio-Flux Specialty

> 🪙 Cost: 3 AP

You’ve crafted a complex light device called a bio-invigorator. This can be worn on any called shot location to avoid having to hold it while it’s drawn. You can restore hit points during combat by treating your allies. If you use invigorate on an ally, you must be adjacent to them.

        You can treat yourself, but you can only heal half as many of your own hit points as you would if you were healing another person. You cannot use Bio-Invigoration to restore hit points to an automaton or other non-living being.

|Tier| Effect|
|--|--|
|1|restores 6 hit points (3 of your own)|
|2|restores 12 hit points (6 of your own)|
|3|restores 18 hit points (9 of your own)|
|4|restores 24 hit points (12 of your own)|

#### Bio-Invigoration Expert
> Evade +1, Wounds +1, Hit Points +11

> Bio-Flux Specialty

> ❓ Requires: Bio-Invigoration specialty

You optimize your bio-invigorator. When you use bio-invigoration, you now restore hit points based on this chart:

|Tier| Effect|
|--|--|
|1|restores 10 hit points (5 of your own)|
|2|restores 20 hit points (10 of your own)|
|3|restores 30 hit points (15 of your own)|
|4|restores 40 hit points (20 of your own)|

#### Quickshot Bio-Invigoration
> Evade +1, Speed +5, Hit Points +8

> Bio-Flux Specialty

> ❓ Requires: Bio-Invigoration specialty

> 🪙 Cost: 2 AP

Combat doesn’t wait for the slow bio-invigorator. You may now use bio-invigoration for only 2 action points.

Medical Marvel

> Bio-Flux Specialty

> ❓ Requires: Bio-Invigoration specialty

You can heal beyond a person’s maximum hit points. If you over-heal somebody, the excess hit points stays with them until the end of their turn (when their action points refresh). If their excess hit points are damaged, then that damage is simply negated.

        For example, you use bio-invigoration on a friend who has a maximum of 50 hit points. He is over-healed, granting him 5 extra hit points. Before the end of his next turn, he is stabbed by an attack that deals 4 hit points, leaving him at 51. His turn ends, and his hit points are reduced to 50, effectively leaving him unharmed.

#### Self-Administer
> Accuracy +1, Evade +1, Hit Points +11

> Bio-Flux Specialty

> ❓ Requires: Bio-Invigoration specialty

Though the bio-invigorator was originally difficult to use on yourself, you’ve outfitted it to work on yourself efficiently. When you use bio-invigoration on yourself, you heal just as much as you would restore of another’s hit points.

#### Manipulate Essence
> Augments +2, DIY +1, Hit Points +6

> Bio-Flux Specialty

You have gained the knowledge and skill to manipulate a creature’s essence. Using a bit of pure base essence, you are able to modify genetics during downtime.

        Without spending any money, you can maintain several people with essence manipulations based on your current Do-It-Yourself (DIY) score. You’ll learn 2 augments from this specialty, which can be selected under “essence augments” below. These augments have marques. At lower levels, you’ll start with Marque I augments. As your skill in Bio-Flux improves, your marques will increase. See the “Crafting” page at the beginning of this chapter for more information.

        Every person can be upgraded with 3 essence manipulations. Sometimes an augment will take up multiple augment slots. For example, the “regeneration” augment is worth 2 slots, so a person only has 1 more available slot for an augment after “regeneration” has been applied.

        You have the ability to remove essence augments off of people during downtime as well.

#### Number of Augmented People you can Maintain  

Without needing to buy workshop space or essence, you can manipulate a few people without spending anything. These people must be constantly maintained by you and their essence reverts back to normal soon after leaving your care, thus losing all of their augments. You may manipulate and maintain a number of people based on your DIY score. You may manipulate or re-augment people during any period of downtime you have.

#### The Cost of Essence Manipulations 

Essence is a volatile and often illegal substance. Bio-flux specialists are ostracized from their community and their manipulations only carried out in the black market. Because of that, the Trust (the world’s economic caretaker) does not regulate the cost of essence manipulations. Nonetheless, an essence manipulator will still need to pay for the materials of an essence manipulation they choose to do. They can, however, get away with charging whatever they’d like for the actual manipulation.

        If you need to manipulate essence that you can’t do for free from your DIY score, you will need to buy the materials for it.

        Every augment increases the market price of the essence manipulation. The higher the marque, the greater the price. The market price for an augment can be found in the chart below.

If you are manipulating the augment, you pay 1/5th the price, which is the same as if you were buying an augment one marque lower. (As in, the material cost for a Marque III augment is the market price fo a Marque II augment.) The material cost for a Marque 1 augment is 10 princes.

#### Beta Essence
> Augments +2, DIY +1, Hit Points +6

> Bio-Flux Specialty

> ❓ Requires: 4 skill points in Bio-Flux & Manipulate Essence specialty

You know your own essence well enough to safely twist it and strengthen it enough to add additional modification onto it. You may now act as though your essence have two additional slots.

#### Prototype Essence
> Augments +2, DIY +1, Hit Points +6

> Bio-Flux Specialty

> ❓ Requires: 16 skill points in Bio-Flux, Manipulate Essence, & Beta Essence specialties

You are now able to manipulate other creatures as well as you manipulate yourself. You may now treat other creatures as though they had 2 additional slots when manipulating their essence.

#### Fast Manipulation
> Evade +1, Augments +2, Hit Points +7

> Bio-Flux Specialty

> ❓ Requires: 6 skill points in Bio-Flux & Manipulate Essence specialty

> ⬇️ Resist: Spirit (negates)

> 🪙 Cost: 3 AP

You are able to use advanced tools in order to quickly manipulate essence on the battlefield. However, because of the haste of these manipulations, they only last until the subject’s next breather.

        If the subject so chooses, they may roll a Spirit resist against your bio-flux to negate the manipulation.

#### Body Renewal
> Defense +2, Augments +1, Hit Points +9

> Bio-Flux Specialty

> ❓ Requires: Manipulate Essence specialty

> 🪙 Cost: 3 AP

By applying just the right amount of essence to a wounded area, you are able to trigger a muscle memory that reverts the subject’s body back to an undamaged state. You may spend 3 action points to heal your subject of one wound point or one effect caused from them (broken bones, blinded, lost senses, et cetera).

#### Gene Therapy
> Augments +2, Wounds +1, Hit Points +9

> Bio-Flux Specialty

> ❓ Requires: 16 skill points in Bio-Flux, Manipulate Essence, & Body Renewal specialties

You force a living subject’s body to painfully regrow a severed body part or critically injured body part. Once per downtime, you are able to remove a lasting Fatal Effect from your subject.  

### Essence Augments

#### Aerodynamize
>   Essence Augment

Your body and features become sleek and slender with curved edges so the wind will always flow with your movements.

|Marque| Effect|
|--|--|
|1|+5 movement speed|
|2|+10 movement speed|
|3|+10 movement speed|
|4|+15 movement speed|

#### Acidic Blood
>   Essence Augment

The blood within your veins is now dark and corrosive. This highlights your veins, making them dark and easily visible. When you receive bleeding damage from a melee attack, the person inflicting the damage suffers an equal amount of soakable damage from being sprayed by your blood as long as they are adjacent to you.

> 🗒️️         Note: This augment always acts as Marque III for the purposes of determining cost, though you can learn this augment despite your skill in bio-flux.

#### Acidic Spit
>   Essence Augment

> 🪙 Cost: 1 AP

Your saliva has been changed to become tar-like and acidic. For 1 action point, you can make a ranged attack using your acidic saliva. This attack has a range of 10 feet and uses strike to determine damage.

|Marque| Effect|
|--|--|
|1|damage class 1|
|2|damage class 2|
|3|damage class 3|
|4|damage class 4|

#### Amphibious
>   Essence Augment

You sprout gills and your lungs gain the ability to extract oxygen from water naturally. You can now breathe while underwater.

> 🗒️️         Note: This augment always acts as Marque II for the purposes of determining cost, though you can learn this augment despite your skill in bio-flux.

#### Body of Flames
>   Essence Augment

The skin on your body is unnaturally warm to the touch, to the point that it slightly burns those who touch you. This causes your skin to have a slight red tinge and your hair to lighten.

|Marque| Effect|
|--|--|
|1|When being grabbed or grabbing, victims take 1 heat damage per turn|
|2|When being grabbed or grabbing, victims take 2 heat damage per turn|
|3|When being grabbed or grabbing, victims take 3 heat damage per turn|
|4|When being grabbed or grabbing, victims take 4 heat damage per turn|

#### Chameleon
>   Essence Augment

> 🪙 Cost: 1 AP (see below)

Much like the scales of a lizard, you are able to change both the color and texture of your skin and hair. For one action point you may undergo this change.

|Marque| Effect|
|--|--|
|1|+2 to Cunning rolls when hiding without clothing or disguising yourself|
|2|+4 to Cunning rolls when hiding without clothing or disguising yourself|
|3|+6 to Cunning rolls when hiding without clothing or disguising yourself|
|4|+8 to Cunning rolls when hiding without clothing or disguising yourself|

#### Chloroplast
>   Essence Augment

You are able to survive with only a bit of sunlight and water due to large amounts of chloroplasts in your cells. Unfortunately it’s not easy being green, as your appearance becomes splotched with patches of green skin.

> 🗒️️         Note: This augment always acts as marque III for the purposes of determining cost, though you can learn this augment despite your skill in bio-flux.

#### Electric Flow
>   Essence Augment

Thin strains of static electricity flow across your body. When attacked by electricity, these strains help dissipate the electricity, allowing you to defend against electrical attacks as per normal.

> 🗒️️         Note: This augment always acts as Marque II for the purposes of determining cost.

#### Energize
>   Essence Augment

Your body chemistry has been modified to grant you a seemingly endless supply of energy. Because of this you rarely feel tired and always shake when sitting still.

|Marque| Effect|
|--|--|
|1|+3 hit points|
|2|+6 hit points|
|3|+9 hit points|
|4|+12 hit points|

#### Excess Adrenaline
>   Essence Augment

Takes up 3 Essence Slots on a Person

Your body has been modified to produce and use twice the amount of adrenaline of a normal being. While this does speed you up temporarily, it exhausts you immediately afterwards. You are able to gain additional action points during a turn; however, you are stunned for an equal number of action points when your action points refresh. In other words, you’re stealing action points away from your next turn.

|Marque| Effect|
|--|--|
|1|You can gain 1 action point|
|2|You can gain up to 2 action points|
|3|You can gain up to 2 action points|
|4|You can gain up to 3 action points|

#### Exoskeleton
>   Essence Augment

Your bones thicken and elongate, some slightly jutting out of your body. This makes them harder to damage and even more difficult to break.

|Marque| Effect|
|--|--|
|1|+2 to Brute when resisting a called shot|
|2|+4 to Brute when resisting a called shot|
|3|+6 to Brute when resisting a called shot|
|4|+8 to Brute when resisting a called shot|

#### Flame Retardant
>   Essence Augment

Your skin becomes a darkened charcoal color that is less likely to ignite or burn. You are never denied your defense roll when on fire. You also gain extra soak against damage from being on fire and any other damage from fire.

|Marque| Effect|
|--|--|
|1|You gain a +2 soak against fire|
|2|You gain a +3 soak against fire|
|3|You gain a +4 soak against fire|
|4|You gain a +5 soak against fire|

#### Flaming Breath
>   Essence Augment

> 🪙 Cost: 2 AP

You’ve grown an extra gland in your throat that allows you to expel flames from your mouth. For 2 action points, you may make a ranged attack using your fiery saliva. This attack has a range of 10 feet, and uses strike to determine damage.

|Marque| Effect|
|--|--|
|1|damage class 3|
|2|damage class 4|
|3|damage class 5|
|4|damage class 6|

#### Genetic Stability
>   Essence Augment

Your essence has been solidified to a point that bio-fluxing your essence is a gruelling, painful ordeal. This  augment negates any cosmetic changes caused by other augments your essence has.

|Marque| Effect|
|--|--|
|1|+4 when resisting a Bio-Zapper|
|2|+8 when resisting a Bio-Zapper|
|3|+12 when resisting a Bio-Zapper|
|4|+16 when resisting a Bio-Zapper|

#### Iron Lung
>   Essence Augment

Your lungs have adapted to filter anything you breathe in, allowing you to breathe normally even when surrounded by noxious gases.

|Marque| Effect|
|--|--|
|1|+4 to Brute when resisting anything you inhale|
|2|+8 to Brute when resisting anything you inhale|
|3|+12 to Brute when resisting anything you inhale|
|4|+16 to Brute when resisting anything you inhale|

#### Jellyskin
>   Essence Augment

Your skin is exceptionally flexible and holds shape when you move it, allowing you to change the structure of your face more easily. Any time you are attempting to disguise yourself, you may gain a bonus to your roll. However, when you are nudged, punched, or attacked, your face retains dents until you can fix your appearance.

|Marque| Effect|
|--|--|
|1|+3|
|2|+5|
|3|+7|
|4|+9|

#### Lead Blood
>   Essence Augment

Your blood is made of molten metal, solidifying into small pellets after leaving your body. Drops of your blood can thus be used as crude ammunition for firearms. It causes you no damage and costs no action points to bleed out bullets, although your firearm must still be readied with them.

|Marque| Effect|
|--|--|
|1|-3 Accuracy when used as ammo|
|2|-2 Accuracy when used as ammo|
|3|-1 Accuracy when used as ammo|
|4|No accuracy penalty when used as ammo|

#### Limitless Memory
>   Essence Augment

Small flashing lights coming from the inside of your head skitter across your crown, a visible sign of the massive amount of information processing going on inside the left side of your brain.

|Marque| Effect|
|--|--|
|1|+3 to the Sciences attribute|
|2|+6 to the Sciences attribute|
|3|+9 to the Sciences attribute|
|4|+12 to the Sciences attribute|

#### Luminescent
>   Essence Augment

You cause your essence to radiate light, making your entire body glow. This light extends only so far.

|Marque| Effect|
|--|--|
|1|It lights the area adjacent to you|
|2|It lights up the area 10 feet around you|
|3|It lights up the area 15 feet around you|
|4|It lights up the area 20 feet around you|

#### Marathon Body
>   Essence Augment

Your body has been enriched to take a beating. This change has made you thick and stocky.

|Marque| Effect|
|--|--|
|1|Your maximum number of wounds is increased by 1|
|2|Your maximum number of wounds is increased by 2|
|3|Your maximum number of wounds is increased by 3|
|4|Your maximum number of wounds is increased by 4|

#### Metal Exoskeleton
>   Essence Augment

> ❓ Requires: Exoskeleton augment

Your exoskeleton is made of natural metal armoring, weighing you down but acting as a natural kind of armor. You may act as if you were wearing any kind of armor on your body permanently, so if you armor is broken or destroyed, you still act as if you are wearing armor.

> 🗒️️         Note: This augment always acts as Marque III for the purposes of determining cost, though you can learn this augment despite your skill in bio-flux.

#### Needles
>   Essence Augment

> 🪙 Cost: same as a Light Throwing Weapon attack (generally 2 AP)

Small porcupine needles quickly grow out of your body. You can throw some of your needles at a single target with a damage class of 3 without the need to be drawn.

|Marque| Effect|
|--|--|
|1|10 feet of range|
|2|15 feet of range|
|3|20 feet of range|
|4|25 feet of range|

#### Nobotic
>   Essence Augment

> ⬇️ Resist: Cunning (see below)

Your body appears to be made out of metal, making you appear to be some kind of robot when you aren’t. You gain a bonus to Cunning against anyone using Notice on you when you’re trying to hide the fact that you’re an organic being.

|Marque| Effect|
|--|--|
|1|+5, your body appears to be metallic|
|2|+10, your metal body is angular and disproportionate|
|3|+15, your metal body appears to be made of separate parts bolted together|
|4|+20, your metal body looks machine-made with soulless eyes, emitting clear water vapor instead of sweat|

#### Osmote
>   Essence Augment

> ❓ Requires: Jellyskin augment

Your body is capable of actually absorbing the body of an opponent. When you successfully grapple or grab an opponent, your skin sinks through their armor and they automatically begin to take unsoakable damage. Your body is partially transparent and your solid organs bend like rubber. When you absorb organic material, it counts as eating.

|Marque| Effect|
|--|--|
|1|2 unsoakable damage per turn|
|2|3 unsoakable damage per turn|
|3|4 unsoakable damage per turn|
|4|5 unsoakable damage per turn|

#### Overflow
>   Essence Augment

Your skin tone, regardless of its color, takes on a more vibrant, healthy shade than it previously had. You find it increasingly easy to focus and feel more in tune with yourself.

|Marque| Effect|
|--|--|
|1|+2 on all Spirit resists|
|2|+4 on all Spirit resists|
|3|+6 on all Spirit resists|
|4|+8 on all Spirit resists|

#### Performance Enhancer
>   Essence Augment

You instantly swell with muscles all over your body, feeling stronger than usual.

|Marque| Effect|
|--|--|
|1|+1 Brute|
|2|+2 Brute|
|3|+3 Brute|
|4|+4 Brute|

#### Regeneration
>   Essence Augment

Takes up 2 Essence Slots on a Person

Your body has been modified to heal at a much faster rate than normal. The areas where you’ve taken wounds damage now have yellow skin blemishes. Along with this accelerated wound recovery, lingering wound effects are removed twice as fast.

|Marque| Effect|
|--|--|
|1|You recover 1 additional wound per day|
|2|You recover 2 additional wounds per day|
|3|You recover 3 additional wounds per day|
|4|You recover 4 additional wounds per day|

#### Scaleskin
>   Essence Augment

Takes up 2 Essence Slots on a Person

Your skin mutates into thick scales, the outer layer of your grafts taking on a bumpy texture. This gives you a small amount of natural armoring.

|Marque| Effect|
|--|--|
|1|+1 soak class|
|2|+2 soak class|
|3|+3 soak class|
|4|+4 soak class|

#### Scoped Vision
>   Essence Augment

This modification manipulates your browline, making it more narrow and focused. Your vision is radically enhanced granting, you the ability to see things others often overlook.

|Marque| Effect|
|--|--|
|1|+4 to Notice|
|2|+8 to Notice|
|3|+12 to Notice|
|4|+16 to Notice|

#### Semi-Transparent Skin
>   Essence Augment

While making your skin partially see-through to reveal the inner workings of your body is horrifying to most, this serves as a useful tool for doctors trying to figure out what ails their patient. When being diagnosed, the person performing the diagnosis gains a bonus on their roll.

|Marque| Effect|
|--|--|
|1|+4 to being diagnosed|
|2|+8 to being diagnosed|
|3|+12 to being diagnosed|
|4|+16 to being diagnosed|

#### Sixth Sense
>   Essence Augment

The frontal lobe of your brain has been enlarged, giving you the ability to sense impending danger. This change in your brain chemistry causes you to have frequent headaches.

|Marque| Effect|
|--|--|
|1|+2 priority|
|2|+4 priority|
|3|+6 priority|
|4|+8 priority|

#### Sleepless
>   Essence Augment

You no longer have a need to sleep, although you still must rest to recover from fatigue and regain hit points during breathers. Thick ringlets form around your eyes.

> 🗒️️         Note: This augment always acts as Marque III for the purposes of determining cost, though you can learn this augment despite your skill in bio-flux.

#### Slimy
>   Essence Augment

Your sweat glands naturally secrete a thin layer of grease, making it easier to slip out of grabs.

|Marque| Effect|
|--|--|
|1|+4 to Dexterity when trying to break a grab|
|2|+8 to Dexterity when trying to break a grab|
|3|+12 to Dexterity when trying to break a grab|
|4|+16 to Dexterity when trying to break a grab|

#### Stonebones
>   Essence Augment

Your bones have been augmented to be as hard as steel. Your bones may not be severed or broken. If you receive a fatal effect which would normally remove a limb, your limb stays attached. However you still suffer all other negative effects of the fatal effect. This makes your bones thicker and heavier, reducing your movement speed.

|Marque| Effect|
|--|--|
|1|-10 feet of movement speed|
|2|-10 feet of movement speed|
|3|-5 feet of movement speed|
|4|movement speed is unaffected|

#### Vile Fumes
>   Essence Augment

> 🪙 Cost: 3 AP

Your body expels an odor that causes those around you to become nauseated. For 3 action points, you may intensify these gases, forcing all those adjacent to you to make a Brute resist or become disoriented for a turn.

|Marque| Effect|
|--|--|
|1|tier 2 Brute resist|
|2|tier 3 Brute resist|
|3|tier 3 Brute resist|
|4|tier 4 Brute resist|

#### Wall-Crawler
>   Essence Augment

Much like a gecko, your hands and feet are covered in small indentures that aid in climbing. You may climb on any ceiling or wall in any direction as per your normal movement speed.

> 🗒️️         Note: This augment always acts as Marque III for the purposes of determining cost, though you can learn this augment despite your skill in bio-flux.

### Crafting Bio-Zappers

For years bio-flux scientists experimented, attempting to find a way to affect somebody’s essence in a matter of seconds rather than spending hours in surgery. Eventually, bio-zappers came about: a device that vaguely resembles a gun, but blasts out a ray that instantly morphs the target’s essence. The change doesn’t last very long, but, in a battle, it definitely gets the job done.

        A normal bio-zapper comes as a heavy weapon that looks like an advanced firearm. You point it at somebody within 25 feet and pull the trigger (just like a ranged attack costing 2 action points). You roll your accuracy and they roll their evade. If you meet or exceed their evade, you hit them with the bio-zapper.

        You choose the setting on your bio-zapper. (Without a setting, the bio-zapper does nothing). The effect will last until the target’s next breather. You can change the setting on your bio-zapper in order to gain different effects. Switching between settings costs 1 action point. If you hit somebody with a new bio-zapper setting, the new effect replaces the old one.

#### Bio-Zapper Developer
> Augments +2, DIY +1, Hit Points +4
Bio-Flux Specialty  
You can now create the battle-ready, essence-manipulating bio-zapper and use it in battle.  
        Without spending any money, you can build and maintain several bio-zappers based on your current Do-It-Yourself (DIY) score. These bio-zappers can then be upgraded with augments. You’ll learn 2 augments from this specialty, which can be selected under “bio-zapper augments” below. These augments have marques. At lower levels, you’ll start with Marque I augments. As your skill in Bio-Flux improves, your marques will increase. See the “Crafting” page at the beginning of this chapter for more information.  
        Each bio-zapper can be upgraded with 3 augments. Some materials can only be upgraded twice (like wooden bio-zappers) or just once (like organic bio-zappers). Sometimes an augment will take up multiple augment slots. If an augment is worth 2 slots, a metal bio-zapper only has 1 more available slot for an augment after the 2-slot augment has been applied.  
        Bio-zapper augments come in two varieties: those that augment the bio-zapper (like normal) and bio-zapper settings. Only one setting can be active at a time, and switching between settings costs 1 action point.

  
Number of Bio-Zappers you can Maintain  z  
Without needing to buy pieces or parts, you can develop some bio-zappers entirely out of scraps. These bio-zappers must be constantly maintained by you and stop working soon after leaving your care. You may build and maintain a number of bio-zappers based on your DIY score. You may build new bio-zappers or augment old ones during any period of downtime you have.  
  
The Cost of Bio-Zappers z  
If you need to build a bio-zapper that you can’t build for free from your DIY score, you will need to buy the materials for it.  
        Every augment increases the market price of the bio-zapper. The higher the marque, the greater the price. The market price for an augment can be found in the chart below.  
  
If you are building the augment, you pay 1/5th the price, which is the same as if you were buying an augment one marque lower. (As in, the material cost for a Marque III augment is the market price fo a Marque II augment.) The material cost for a Marque 1 augment is 8 princes.

#### Beta Bio-Zappers
> Augments +2, DIY +1, Hit Points +4
Bio-Flux Specialty  
> ❓ Requires: 4 skill points in Bio-Flux & Bio-Zapper Developer specialty  
Your bio-zappers are complex pieces of machinery, but they can do some crazy things compared to most bio-zappers. Such bio-zappers can be upgraded with 2 more augments (bringing the total for metal bio-zappers up to 5 augmentable slots).  
        If anybody other than you attempts to use one of your beta bio-zappers, they must succeed in rolling a sciences result one tier higher than the highest marque you have on your bio-zapper. If your bio-zapper has a Marque IV augment, it is impossible for them to use it (unless they can somehow obtain a tier result of 5 with their sciences attribute).  
  
#### Prototype Bio-Zappers
> Augments +1, DIY +1, Hit Points +4
Bio-Flux Specialty  
> ❓ Requires: 16 skill points in Bio-Flux, Bio-Zapper Developer, & Beta Bio-Zappers specialties  
You’ve perfected your beta bio-zappers and made them user-friendly. Now anybody can use a bio-zapper that you designate as being a prototype.  

  
Concentrated Stream  
Bio-Flux Specialty  
> 🪙 Cost: Bio-Zapper Attack to initiate and 1 AP to continue in subsequent turns  
You lock your weapon onto your target, pull the trigger, and don’t let go. After successfully hitting a target with a bio-zapper, all subsequent attacks with that bio-zapper automatically hit. You must spend one action point each turn to continue your ray. If at any point you do not have a clear line of site to your target or either of you moves out of the range of the bio-zapper, this effect is broken.

  
#### Extra Settings
> Augments +2, DIY +1, Hit Points +4
Bio-Flux Specialty  
Your bio-zappers are designed to hold extra settings, effectively giving you extra augment slots to place bio-zapper settings into.  

        This specialty works within the marque system. The amount of interchangeable slots the bio-zapper has depends on the marque of its developer. Each extra augment slot increases the price, just like normal, but only a person with this specialty can access the extra settings.

|Tier| Effect|
|--|--|
|1|1 extra augment slot of a setting  |
|2|2 extra augment slots of a setting   |
|3|3 extra augment slots of a setting   |
|4|4 extra augment slots of a setting  |
  
#### Multi-Ray
> Accuracy +1, Augments +1, Hit Points +6
Bio-Flux Specialty  
> 🪙 Cost: Bio-Zapper Attack +1 AP  
By double tapping your trigger finger, you are able to quickly launch off two rays. These rays may be launched at different targets.

  
#### Splicer
> Accuracy +1, Augments +1, Hit Points +6
Bio-Flux Specialty  
Your bio-zapper settings don’t replace each other; now, they overlap! When you hit an opponent with two different settings, they both affect the target until the target’s next breather. (If you hit the same target with the same setting twice, there’s no effect.)

  
#### Tracing
> Accuracy +1, Augments +1, Hit Points +7
Bio-Flux Specialty  
> 🪙 Cost: Bio-Zapper Attack +1 AP  

You hold down the button on your bio-zapper, sending a ray arching across the battlefield. Then you move your bio-zapper’s ray until you hit the target, radically increasing its accuracy and your chances of your hitting your target.

|Tier| Effect|
|--|--|
|1|+4 accuracy  |
|2|+8 accuracy  |
|3|+12 accuracy   |
|4|+16 accuracy   |
        
  
#### ### Bio-Zapper Augments
#### Accurate  
>   Bio-Zapper Augment  

Fine attention has been placed on the quality of your bio-zapper. You gain a bonus to accuracy with the bio-zapper.   
        
|Marque| Effect|
|--|--|
|1|+1   |
|2|+2   |
|3|+3   |
|4|+4   |
        
  
Armor-Encumbering  
Bio-Zapper Augment (setting)  
> ⬇️ Resist: Dexterity (marques down)  

Your bio-zapper makes your target’s body react as if the clothing on it is much heavier than it actually is. The penalty is greater depending on how much armor the target is wearing.  

|Marque| Effect|
|--|--|
|1|-5 speed penalty (heavy or larger armor)   |
|2|-10 speed penalty (heavy or larger armor) or -5 speed penalty (light or medium armor)   |
|3|-15 speed penalty (heavy or larger armor) or -10 speed penalty (light or medium armor)   |
|4|-20 speed penalty (heavy or larger armor) or -15 speed penalty (light or medium armor)   |
        
  
Blood Thinning  
Bio-Zapper Augment (setting)  
> ⬇️ Resist: Brute (marques down)  

The victim’s blood refuses to clot when bleeding.  

|Marque| Effect|
|--|--|
|1|Patching Bleeding now only stops 3 bleeding damage   |
|2|Patching Bleeding now only stops 2 bleeding damage   |
|3|Patching Bleeding now only stops 1 bleeding damage   |
|4|Bleeding can only be stopped when taking a breather    |
  
Bone Spurring  
Bio-Zapper Augment (setting)  
> ⬇️ Resist: Spirit (marques down)  

The victim’s bones become spiked inside of their bodies. This causes extreme pain when the victim does anything.

|Marque| Effect|
|--|--|
|1|The victim takes 1 unsoakable damage for each action point they spend  |
|2|The victim takes 1 unsoakable damage for each action point they spend   |
|3|The victim takes 2 unsoakable damage for each action point they spend   |
|4|The victim takes 2 unsoakable damage for each action point they spend   |
        
#### Collapsible  
>   Bio-Zapper Augment  

Sometimes discretion is the better part of not having your bio-zapper confiscated, so you create a clever collapsing mechanism for your bio-zapper which makes it easier to conceal. Any bio-zapper this is applied to can be broken down for 3 action points and reassembled for 3 action points. It is treated, for purposes of concealment, as being smaller than it is, but only when broken down.  

|Marque| Effect|
|--|--|
|1|1 category smaller   |
|2|2 categories smaller   |
|3|3 categories smaller   |
|4|4 categories smaller   |
        
  
Crippling  
Bio-Zapper Augment (setting)  
> ⬇️ Resist: Brute (marques down)  

When hit by a crippling ray, your body becomes weak and sickly.   
        
|Marque| Effect|
|--|--|
|1|-3 on Brute rolls   |
|2|-6 on Brute rolls   |
|3|-9 on Brute rolls   |
|4|-12 on Brute rolls   |
        
#### Custom  
>   Bio-Zapper Augment  

This bio-zapper was designed to be used by one person and one person only. That person must be designated at the time of the bio-zapper’s crafting. If anybody else attempts to use the custom bio-zapper, they suffer a penalty on all accuracy rolls with it.

|Marque| Effect|
|--|--|
|1|-3   |
|2|-6   |
|3|-9   |
|4|-12    |
        
#### Deflecting  
>   Bio-Zapper Augment  

You may use this bio-zapper like a shield, allowing you to deflect incoming attacks (gaining a +4 to evade in exchange for 1 reflexive action point).  
> 🗒️️         Note: This augment always acts as Marque I for the purposes of determining cost, though you can learn this augment despite your skill in bio-flux.

  
Debilitating  
Bio-Zapper Augment (setting)  
> ⬇️ Resist: Brute (marques down)  

Your target has sporadic spells of exhaustion, conveniently taking place right as they try to attack.  

|Marque| Effect|
|--|--|
|1|-2 on strike rolls|
|2|-4 on strike rolls|
|3|-6 on strike rolls|
|4|-8 on strike rolls|

Demoralizing  
Bio-Zapper Augment (setting)  
> ⬇️ Resist: Cunning (marques down)  

Bio-Zappers on this setting rewrite its victim’s face to resemble a Cubist painting, making them feel less confident and ugly.  

|Marque| Effect|
|--|--|
|1|-2 on Spirit rolls  |
|2|-4 on Spirit rolls   |
|3|-6 on Spirit rolls   |
|4|-8 on Spirit rolls   |
        
  
Discombobulating  
Bio-Zapper Augment (setting)  
> ⬇️ Resist: Spirit (negates, see below)  

Victims hit with rays on this setting have their organs rearranged in such a weird way that it becomes impossible for them to recover from wounds damage (but not wounds effects) while under its effects.   
        
|Marque| Effect|
|--|--|
|1|tier 2 Spirit resist to negate   |
|2|tier 3 Spirit resist to negate   |
|3|tier 3 Spirit resist to negate   |
|4|tier 4 Spirit resist to negate   |
        
  
Essence Draining  
Bio-Zapper Augment (setting)  
> ⬇️ Resist: Spirit (marques down)  

Victims hit by this ray see their essence slots temporarily shut down. This not only makes augments to those slots useless but also causes the person to become grotesque like an elf. The person wielding the bio-zapper chooses which, if any, augments on the target are deactivated.  
|Marque| Effect|
|--|--|
|1|1 essence slot is turned off   |
|2|2 essence slots are turned off   |
|3|3 essence slots are turned off   |
|4|4 essence slots are turned off   |
        
  
Exhausting  
Bio-Zapper Augment (setting)  
> ⬇️ Resist: Spirit (marques down)  

Hitting a victim with a ray on the exhausting setting tires the opponent. A quick blast from one of these ensures the victim will not be going the distance.   
        
|Marque| Effect|
|--|--|
|1|maximum hit points are lowered by 5   |
|2|maximum hit points are lowered by 10   |
|3|maximum hit points are lowered by 15   |
|4|maximum hit points are lowered by 20   |
        
#### Extended Range  
>   Bio-Zapper Augment  

Your bio-zapper can hit targets well past 25 feet.   
        
|Marque| Effect|
|--|--|
|1|targets up to 50 feet away   |
|2|targets up to 75 feet away   |
|3|targets up to 100 feet away   |
|4|targets up to 150 feet away   |
        
  
Eyelid-Fusing  
Bio-Zapper Augment (setting)  
> ⬇️ Resist: Brute (negates, see below)  

Victims unlucky enough to be in the path of a blinding ray find their eyelids fused together, making it impossible for them the see anything without drastic actions. For a single action point they can deal a point of wounds damage to themselves to rip their eyes open. They don’t take a wounds effect for doing so.   
        
|Marque| Effect|
|--|--|
|1|tier 2 Brute resist to negate   |
|2|tier 3 Brute resist to negate   |
|3|tier 3 Brute resist to negate   |
|4|tier 4 Brute resist to negate  |
  
Flesh Melting  
Bio-Zapper Augment (setting)  
> ⬇️ Resist: Brute (marques down)  

Flesh melting rays cause the victim’s skin to melt and tear, making all attacks against them more deadly. This cannot lower their soak class below zero.   
        
|Marque| Effect|
|--|--|
|1|soak class lowered by 1   |
|2|soak class lowered by 2   |
|3|soak class lowered by 3   |
|4|soak class lowered by 4  |
  
Growing  
Bio-Zapper Augment (setting)  
> ⬇️ Resist: Brute (marques down)  

This ray causes the victim to grow just large enough for their armor and clothing to become unbearably tight and uncomfortable. This ray only affects victims in armor.   
        
|Marque| Effect|
|--|--|
|1|-2 on defense rolls   |
|2|-4 on defense rolls   |
|3|-6 on defense rolls   |
|4|-8 on defense rolls   |
        
  
Hesitating  
Bio-Zapper Augment (setting)  
> ⬇️ Resist: Spirit (marques down)  

When hit with this ray, its victim is dropped on the priority list as they hesitate to act, allowing others to take their turn first. If it drops them to the bottom of the priority list, they act there. They must wait until everyone has taken their turn before going.   
        
|Marque| Effect|
|--|--|
|1|Dropped 1 priority slot   |
|2|Dropped 2 priority slots   |
|3|Dropped 3 priority slots   |
|4|Dropped 4 priority slots   |
  
Hexing  
Bio-Zapper Augment (setting)  
> ⬇️ Resist: Spirit (negates, see below)  

This ray alters your victim’s body chemistry, causing them to feel drained and demoralized. It forces them to see their own limits. When affected by this ray, victims may no longer re-roll pure 12s.  

|Marque| Effect|
|--|--|
|1|tier 2 Spirit resist to negate  |
|2|tier 3 Spirit resist to negate  |
|3|tier 3 Spirit resist to negate  |
|4|tier 4 Spirit resist to negate  |

  
  
Hindering  
Bio-Zapper Augment (setting)  
> ⬇️ Resist: Brute (marques down)  

The victim’s equilibrium is thrown off, making acrobatics and fast movements almost impossible.  

|Marque| Effect|
|--|--|
|1|-2 on Dexterity rolls  |
|2|-4 on Dexterity rolls  |
|3|-6 on Dexterity rolls  |
|4|-8 on Dexterity rolls  |

#### Intensity  
>   Bio-Zapper Augment  

You have increased your victim’s dosage and in doing so made the blast more difficult to resist.  

|Marque| Effect|
|--|--|
|1|The target gets a -2 to resist the ray’s effects  |
|2|The target gets a -4 to resist the ray’s effects  |
|3|The target gets a -6 to resist the ray’s effects  |
|4|The target gets a -8 to resist the ray’s effects  |

#### Micro-Zapper  
>   Bio-Zapper Augment  

You have made your bio-zapper significantly smaller, letting you wield it as a smaller weapon.  

|Marque| Effect|
|--|--|
|1|wields as a medium weapon  |
|2|wields as a medium weapon  |
|3|wields as a light weapon  |
|4|wields as a light weapon  |

  
Muffling  
Bio-Zapper Augment (setting)  
> ⬇️ Resist: Brute (negates, see below)  

The victim’s ears melt and fuse with the side of their head causing them to become deafened. For a single action point they can deal a point of wounds damage to themselves to rip their ears open. They don’t take a wounds effect for doing so.  

|Marque| Effect|
|--|--|
|1|tier 2 Brute resist to negate  |
|2|tier 3 Brute resist to negate  |
|3|tier 3 Brute resist to negate  |
|4|tier 4 Brute resist to negate  |
  
  
  
#### Multi-Setting  
>   Bio-Zapper Augment  

This augment allows the bio-zapper to be in two settings at once. This allows you to choose between multiple settings to fire without spending 1 action point to switch between them. You still can only fire from one setting at a time.  

|Marque| Effect|
|--|--|
|1|The zapper may be in 2 settings  |
|2|The zapper may be in 3 settings  |
|3|The zapper may be in 4 settings  |
|4|The zapper may be in 5 settings  |

  
Nail Growing  
Bio-Zapper Augment (setting)  
> ⬇️ Resist: Dexterity (marques down)  

The victim’s fingers swell, making it difficult for them to smoothly pull the trigger on a firearm.  

|Marque| Effect|
|--|--|
|1|-2 accuracy with a firearm or crossbow  |
|2|-4 accuracy with a firearm or crossbow  |
|3|-6 accuracy with a firearm or crossbow  |
|4|-8 accuracy with a firearm or crossbow  |

  
Nausea-Inducing  
Bio-Zapper Augment (setting)  
> ⬇️ Resist: Brute (negates, see below)  

Your bio-zapper shakes and rattles your target’s stomach, making them unsettled and nauseated (-2 to all rolls until 3 action points are spent emptying their stomach).  

|Marque| Effect|
|--|--|
|1|tier 2 Brute resist to negate  |
|2|tier 3 Brute resist to negate  |
|3|tier 3 Brute resist to negate  |
|4|tier 4 Brute resist to negate  |

  
Perspiring  
Bio-Zapper Augment (setting)  
> ⬇️ Resist: Brute (marques down)  

Victims hit by a bio-zapper set to perspiring will begin to rapidly sweat. Their sweaty hands make it very difficult to hold on to any items and even more difficult to hold on to a person.  

|Marque| Effect|
|--|--|
|1|-2 to all disarm resists and maintaining grabs  |
|2|-4 to all disarm resists and maintaining grabs  |
|3|-6 to all disarm resists and maintaining grabs  |
|4|-8 to all disarm resists and maintaining grabs  |

  
Rattling  
Bio-Zapper Augment (setting)  
> ⬇️ Resist: Brute (marques down)  

After being hit by a ray on this setting, the victim’s legs become weak like jelly. Because of this, all called shots to to the legs become much more effective. 

|Marque| Effect|
|--|--|
|1|-2 on resist rolls against the legs  |
|2|-4 on resist rolls against the legs  |
|3|-6 on resist rolls against the legs  |
|4|-8 on resist rolls against the legs  |

#### Reinforced  
>   Bio-Zapper Augment  

You build your bio-zapper solidly, giving it little room to break on the battlefield. Whenever somebody attempts to sunder the reinforced bio-zapper, it acts as if it is several size categories larger than it is. Once these “reinforced” size categories are gone, then it will actually break.  

|Marque| Effect|
|--|--|
|1|1 reinforced size category  |
|2|2 reinforced size categories  |
|3|3 reinforced size categories  |
|4|4 reinforced size categories  |

  
Skin-Papering  
Bio-Zapper Augment (setting)  
> ⬇️ Resist: Brute (marques down)  

Your bio-zapper makes your target’s skin become thinner, less moist, and burn easier.  

|Marque| Effect|
|--|--|
|1|-4 on resists against burns  |
|2|-8 on resists against burns  |
|3|-12 on resists against burns  |
|4|-16 on resists against burns  |
  
Skin-Inflaming  
Bio-Zapper Augment (setting)  
> ⬇️ Resist: Brute (marques down)  

Your bio-zapper makes your target’s skin secrete a flammable liquid. They have a harder time resisting catching on fire.  

|Marque| Effect|
|--|--|
|1|-3 on resists against catching on fire  |
|2|-6 on resists against catching on fire  |
|3|-9 on resists against catching on fire  |
|4|-12 on resists against catching on fire  |
  
Silencing  
Bio-Zapper Augment (setting)  
> ⬇️ Resist: Brute (negates, see below)  

Victims hit by this ray find their mouths sewn shut, their ability to speak or make any sound stripped from them. In addition, victims lose the ability to use any specialties with required vocal components, such as yelling or singing. For a single action point they can deal a point of wounds damage to themselves to rip their lips open. They don’t take a wounds effect for doing so.  

|Marque| Effect|
|--|--|
|1|tier 2 Brute resist to negate  |
|2|tier 3 Brute resist to negate  |
|3|tier 3 Brute resist to negate  |
|4|tier 4 Brute resist to negate  |
#### Silent  
>   Bio-Zapper Augment  

This bio-zapper is whispering death. It makes almost no sound when fired, making it almost impossible for people to figure out where it is by sound alone. Any time anybody is attempting to figure out where the bio-zapper was shot from based on sound they must make a tier result with their Cunning.  

|Marque| Effect|
|--|--|
|1|tier 2 Cunning to hear  |
|2|tier 3 Cunning to hear  |
|3|tier 4 Cunning to hear  |
|4|impossible (unless they can get a tier 5 Cunning)  |
  
Sluggish  
Bio-Zapper Augment (setting)  
> ⬇️ Resist: Brute (marques down) 

Your bio-zapper makes your target’s legs mutate to resemble elephant feet, slowing them down.  

|Marque| Effect|
|--|--|
|1|speed reduced by 5 feet  |
|2|speed reduced by 10 feet  |
|3|speed reduced by 15 feet  |
|4|speed reduced by 20 feet  |
  
Stupifying  
Bio-Zapper Augment (setting)  
> ⬇️ Resist: Cunning (marques down)  

The stupifying setting causes victims to become dazed and unobservant. They take a penalty on Cunning rolls.  

|Marque| Effect|
|--|--|
|1|-3 on Cunning rolls  |
|2|-6 on Cunning rolls  |
|3|-9 on Cunning rolls  |
|4|-12 on Cunning rolls  |

  
Swelling  
Bio-Zapper Augment (setting)  
> ⬇️ Resist: Brute (marques down)  

This setting creates rays that cause the victim’s called shot locations to swell to ridiculous sizes. These swollen called shot are then easier to hit due to their increased size. You must select a single called shot location to affect when firing your bio-zapper set to this setting.  

|Marque| Effect|
|--|--|
|1|+2 accuracy to that called shot  |
|2|+4 accuracy to that called shot  |
|3|+6 accuracy to that called shot  |
|4|+8 accuracy to that called shot  |
  
Weakening  
Bio-Zapper Augment (setting)  
> ⬇️ Resist: Brute (marques down)  

The rays fired from this zapper make it difficult for your victim to lift their weapon, much less harm you with it. The damage class on any melee attacks the target makes is lowered.  

|Marque| Effect|
|--|--|
|1|melee damage class lowered by 1  |
|2|melee damage class lowered by 2  |
|3|melee damage class lowered by 3  |
|4|melee damage class lowered by 4  |

* * *

## Engineer Skill
==============

Engineering is the creation of vehicles. Engineering is one of the most inclusive skills, for it involves the creation of everything from flyers to blimps to trains to sailing vessels. With Engineering, you could create walking land machines, a comfortable carriage with a cannon mounted on top, or a graviton sphere-powered flyer that spews poisonous gas and can comfortably carry your entire party of stalwart adventures. A couple points in Engineering and soon the world is just a speck below you.

#### Maintenance
> Augments +2, DIY +1, Hit Points +6
Engineer Specialty  
> 🧘 Stance (costs 1 AP to enter)  
> ❓ Requires: 2 skill points in Engineer  

Patch up a few bullet holes here, change the pressure valves over there - the job of a mechanic is never done. While in this stance, you make a series of constant small repairs to the section of the ship you’re in. This repairs 1 vehicle wound for every 2 points you have in Engineering at the end of your turn (when your action points refresh).

  
#### Power Surge
> Augments +1, Priority +3, Hit Points +6
Engineer Specialty  
> 🪙 Cost: 1 AP  

When you’re not the pilot, you may tweak the coolant to grant the vehicle greater movement without overheating. This grants the pilot an additional action point in order to move or steer the vehicle once more for the turn.

  
#### Quick Upgrades
> Augments +2, DIY +1, Hit Points +5
Engineer Specialty  

You may switch the parts on a vehicle quickly without waiting for downtime. You must know the augment you are about to put onto the part. You choose which slot(s) to empty and which slot(s) to replace. For every slot that you change, you roll your Engineer skill to determine how quickly you do so. You must be adjacent to the vehicle in order to change its parts.  

        If changing out the parts requires more action points than you have for the turn, you may do so over multiple, non-consecutive turns. For example, if it requires 7 action points to change out an augment, you could spend 3 action points this turn, and then wait a couple turns before spending the final 4 action points to switch out the augment. However, once you begin changing out parts, the original augment ceases to function and the new augment does not function until it is completely installed. 
         
|Marque| Effect|
|--|--|
|1|10 AP|
|2|7 AP |
|3|4 AP |
|4|2 AP |
         
  
#### Vehicle Repairs
> Defense +2, Augments +1, Hit Points +7
Engineer Specialty  
> ❓ Requires: either Auto-Wright or Manual-Wright specialty  
> 🪙 Cost: 3 AP  

You are capable of repairing any vehicle that you are capable of building. You must be either inside or adjacent to the vehicle in order to make these repairs.  

|Marque| Effect|
|--|--|
|1|10 vehicle wounds  |
|2|20 vehicle wounds  |
|3|30 vehicle wounds  |
|4|40 vehicle wounds  |
  
  
### Grease Monkey Specialties
#### Gearhead
> Augments +1, DIY +1, Hit Points +5
Engineer Specialty  
> 🧘 Stance (costs 1 AP to enter)  

> ❓ Requires: 2 skill points in Engineering  

By constantly regulating steam intake and graviton rotations, you’re able to push a vehicle’s engine to its limits. When in this stance and either inside or adjacent to a vehicle, the vehicle’s speed increases by 5 feet per 2 skill points you have in Engineering. Moving away from the vehicle breaks this stance.  

#### Gearjunkie
> Augments +1, Evade +1, Hit Points +6
Engineer Specialty  
> ❓ Requires: 5 skill points in Engineer & Gearhead specialty  

When you’re working on a vehicle, efficiency is maximized. While in Gearhead stance, you may also grant a vehicle a +1 on evade rolls for every 5 points you have in Engineer.  
  
#### Auto-Wright
> Augments +2, DIY +1, Hit Points +4
Engineer Specialty  

Automatically propelled vehicles are ideal at getting adventurers from point A to point B. You’ll be able to craft everything from jetpacks to gyrocycles, ironbirds, motorcars, powerboats, and more. You can now craft one-person automatically propelled vehicles (called autos for short).  

        Without spending any money, you can build and maintain several autos based on your current Do-It-Yourself (DIY) score. They can then be upgraded with augments. You’ll learn 2 augments from this specialty, which can be selected under “auto augments” below. These augments have marques. At lower levels, you’ll start with Marque I augments. See the “Crafting” page at the beginning of this chapter for more information.  
        Each auto can be upgraded with 3 augments. Some materials can only be upgraded twice (like wooden autos) or just once (like organic ones). Sometimes an augment will take up multiple augment slots. If an augment is worth 2 slots, an auto only has 1 more available slot for an augment after the 2-slot augment has been applied.

  
Number of Autos you can Maintain  z  
Without needing to buy pieces or parts, you can build some autos entirely out of scraps. These vehicles must be constantly maintained by you and stop working soon after leaving your care. You may build and maintain a number of autos for free based on your DIY score. You may build new autos or augment old ones during any period of downtime you have.  
  
The Cost of Autos z  
If you need to build an auto that you can’t build for free from your DIY score, you will need to buy the materials for it. An auto will have a base materials cost. It is 1/5th the market price.  

        The automatic vehicle, unaugmented, will have a base cost depending on its marque. (The marque will determine its maximum speed per turn.) If you buy a vehicle and augments, you will add the price of the augments onto the price of the vehicle.  
  
  
  
        Every augment will increase the price. The higher the marque, the greater the price. The market price for an augment can be found in the chart below.  
  
If you are building the augment outside of your DIY Score, you pay 1/5th the price, which is the same as if you were buying an augment one marque lower. (As in, the material cost for a Marque III augment is the market price for a Marque II augment.) The material cost for a Marque 1 augment is 14 princes.

  
#### Beta Autos
> Augments +2, DIY +1, Hit Points +5
Engineer Specialty  
> ❓ Requires: 4 skill points in Engineer & Auto-Wright specialty  

Your automatic vehicles are exceptionally advanced but quite difficult to use. Such autos can be upgraded with 2 more augments (bringing the total for metal autos up to 5 augmentable slots).  

        If anybody other than you attempts to operate one of your autos, they must succeed in rolling a science result one tier higher than the highest level marque you have on your auto. If your auto has a Marque IV augment, it is impossible for them to use it (unless they can somehow obtain a tier result of 5 with their science attribute).  
        If you are a passenger in a beta auto that you created, you can allow another person to pilot the vehicle.

  
#### Prototype Autos
> Augments +1, DIY +1, Hit Points +6
Engineer Specialty  
> ❓ Requires: 16 skill points in Engineer, Auto-Wright, & Beta Autos specialties  

You’ve perfected your beta autos and made them user-friendly. Now anybody can pilot your automatic vehicle as long as you designate it as being a prototype.    
  
#### Manual-Wright
> Augments +2, DIY +1, Hit Points +4
Engineer Specialty  

Manual vehicles are complex, powerful vehicles that move under the pilot’s power. You’ll be able to build walkers, steamtanks, ornithopters, motorships, and everything in-between. You can now craft one-person manual vehicles, called clankers for short.  

        Without spending any money, you can build and maintain several clankers based on your current Do-It-Yourself (DIY) score. They can then be upgraded with augments. You’ll learn 2 augments from this specialty, which can be selected under “clanker augments” below. These augments have marques. At lower levels, you’ll start with Marque I augments. See the “Crafting” page at the beginning of this chapter for more information.  
        Each clanker can be upgraded with 3 augments. Some materials can only be upgraded twice (like wooden clankers) or just once (like organic ones). Sometimes an augment will take up multiple augment slots. For example, the “flying” augment is worth 2 slots, so a clanker only has 1 more available slot for an augment after “flying” has been applied.

  
Number of Clankers you can Maintain  z  
Without needing to buy pieces or parts, you can build some clankers entirely out of scraps. These vehicles must be constantly maintained by you and stop working soon after leaving your care. You may build and maintain a number of clankers for free based on your DIY score. You may build new clankers or augment old ones during any period of downtime you have.  
  
The Cost of Clankers z  
If you need to build a clanker that you can’t build for free from your DIY score, you will need to buy the materials for it. A clanker will have a base materials cost. It is 1/5th the market price.  

        The manual vehicle, unaugmented, will have a base cost depending on its marque. (The marque will determine its maximum speed per action point spent.) If you buy a vehicle and augments, you will add the price of the augments onto the price of the vehicle.  
  
        Every augment will increase the price. The higher the marque, the greater the price. The market price for an augment can be found in the chart below.  
  
If you are building the augment outside of your DIY Score, you pay 1/5th the price, which is the same as if you were buying an augment one marque lower. (As in, the material cost for a Marque III augment is the market price for a Marque II augment.) The material cost for a Marque 1 augment is 12 princes.

#### Beta Clankers
> Augments +2, DIY +1, Hit Points +5
Engineer Specialty  
> ❓ Requires: 4 skill points in Engineer & Manual-Wright specialty  
Your manual vehicles are exceptionally advanced but quite difficult to use. Such clankers can be upgraded with 2 more augments (bringing the total for metal clankers up to 5 augmentable slots).  

        If anybody other than you attempts to operate one of your clankers, they must succeed in rolling a science result one tier higher than the highest level marque you have on your vehicle. If your clanker has a Marque IV augment, it is impossible for them to use it (unless they can somehow obtain a tier result of 5 with their Sciences attribute).  
        If you are a passenger in a beta clanker that you created, you can allow another person to pilot the vehicle.  
  
#### Prototype Clankers
> Augments +1, DIY +1, Hit Points +5
Engineer Specialty  
> ❓ Requires: 16 skill points in Engineer, Manual-Wright, & Beta Clankers specialties  
You’ve perfected your beta clankers and made them user-friendly. Now anybody can pilot your manual vehicle as long as you designate it as being a prototype.  

  
#### ### Auto & Clanker Augments
#### Aerial Propulsion  
>   Vehicle Augment
Can only be applied to Autos  

Your vehicle has jets, wings, propellers, sails, or some other way of moving through the air. You may apply your vehicle speed to moving in the air. You do not gain the ability to move vertically in the air - you can only move horizontally. Aerial propulsion also allows you to move through water, but you move at half your speed (unless you have Underwater Propulsion).  

> 🗒️️         Note: This augment always acts as Marque II for the purposes of determining cost, though you can learn this augment despite your skill in Engineer.

#### Alchemy Refill Station  
>   Vehicle Augment  

You’ve equipped your vehicle with everything you need in order to refill consumed alchemical substances during a breather (a 15-30 minute break). Over the course of a single breather the refill station in your vehicle will allow an alchemist to refill used alchemical potions they’ve brewed. The alchemy refill station will allow a maximum number of refills per breather based on the marque of this augment. You must be adjacent to or inside the vehicle in order to use the refill station.    

|Marque| Effect|
|--|--|
|1|1 alchemical potion  |
|2|2 alchemical potions   |
|3|3 alchemical potions   |
|4|4 alchemical potions   |
        
#### All Terrain  
>   Vehicle Augment  

Your vehicle is adept at moving through terrain (either by having rotating machetes on the front or by having a vehicle that’s just great at sliding in-between brush). Your vehicle cannot be slowed by rough terrain.   
        
> 🗒️️         Note: This augment always acts as marque I for the purposes of determining cost, though you can learn this augment despite your skill in engineer.  
#### Armsmith Utilities  
>   Vehicle Augment  

Your vehicle is outfitted with a small forge, extra equipment, and various tools used to fix your armor, weapons, and firearms. A vehicle outfitted with armsmith utilities can be used to repair any broken armor or weaponry during a breather (a 15-30 minute break). A person who knows Armsmith augments can also switch out augments on an item. The armsmither may change a number of marques based on the marque of this augment. You must be adjacent to or inside this vehicle in order to use armsmith utilities.  

|Marque| Effect|
|--|--|
|1|1 armsmith augment may be changed  |
|2|2 armsmith augments may be changed  |
|3|3 armsmith augments may be changed  |
|4|4 armsmith augments may be changed  |
        
#### Blazing Speed  
>   Vehicle Augment  
Can only be applied to Autos  
> ❓ Requires: vehicle to be augmented to have the Improved Speed augment  

Your vehicle is stupidly fast. Your speed is improved based on the marque of this augment. (This bonus stacks with that granted by Improved Speed.)  

|Marque| Effect|
|--|--|
|1|+50 speed   |
|2|+100 speed   |
|3|+150 speed   |
|4|+200 speed   |
        
#### Blurring Speeds  
>   Vehicle Augment  
Can only be applied to Autos  

Your vehicle is built to go so fast that nothing else can hit it. When traveling at your top speeds, your vehicle is little more than a blur. You vehicle gains a bonus to its evade rolls whenever you’re moving at its top speed (and until the pilot’s next turn, when the speed is adjusted). The evade bonus is based on the marque of this augment.  

|Marque| Effect|
|--|--|
|1|+3 on evade rolls   |
|2|+4 on evade rolls   |
|3|+5 on evade rolls   |
|4|+6 on evade rolls   |
        
#### Buoyancy Release  
>   Vehicle Augment  
Cost to Activate: 1 AP reflexively  

When your vehicle starts to sink or hits the water and isn’t equipped for staying afloat, you can hit your buoyancy release in order to stay afloat. The pilot may activate the buoyancy release for 1 action point, and it will cause the vehicle to stay afloat despite any injuries the vehicle has taken.  

        A foe may attack the buoyancy release mechanism. The buoyancy has a number of wounds based on the marque of this augment.    

|Marque| Effect|
|--|--|
|1|6 wounds   |
|2|12 wounds   |
|3|18 wounds   |
|4|24 wounds   |
        
#### Climber  
>   Vehicle Augment  

Whether it be iron hooks built into your treads or a more straightforward set of arms and legs extending from your vehicle, it now has the ability to climb. You may apply your vehicle speed to moving up and down vertical surfaces.  

> 🗒️️         Note: This augment always acts as Marque I for the purposes of determining cost, though you can learn this augment despite your skill in Engineer.

#### Difficult Controls  
>   Vehicle Augment  

Your vehicle is complex, poorly organized, and the controls aren’t well labeled. In all ways, it is designed for you. Anybody else attempting to pilot your vehicle must spend extra action points in order to change the speed or turn the vehicle.  

|Marque| Effect|
|--|--|
|1|+1 action point to pilot   |
|2|+2 action points to pilot   |
|3|+3 action points to pilot   |
|4|+4 action points to pilot   |
        
  
> 🗒️️ Note: You may designate one other person as being able to pilot your vehicle as well, so they do not take the penalties for the difficult controls.  
#### Drill  
>   Vehicle Augment  

Your vehicle has a drill, and this drill lets you move through the earth. You may apply your vehicle speed to moving through the earth. You may change directions as you drill forward, going up, down, left, or right.  

        The drill does not allow you to drill through all substances. The thickness of the substance you can drill through depends on the marque of this augment. If it is too thick for you to drill through (which will be decided by the narrator), you will not be able to pass through it.

|Marque| Effect|
|--|--|
|1|Thin soil, dirt, and sands   |
|2|Ice, thick soils, and wet sands   |
|3|Weak stone, compressed soils, and clays   |
|4|Worked stones, mountains, soft metals   |
        
#### Ease of Repair  
>   Vehicle Augment  

Your vehicle is made so that the gears and engine are easy to access and repair. Whenever somebody is attempting to use the Vehicle Repairs specialty or Maintenance specialty on the vehicle, they gain a bonus on the roll.  

|Marque| Effect|
|--|--|
|1|+2   |
|2|+4   |
|3|+6   |
|4|+8   |
        
#### Efficient Movement  
>   Vehicle Augment  

Can only be applied to Clankers  
You clockwork engines respond seamlessly, letting you squeeze out every ounce of movement from your clanker. Your speed increases.  

|Marque| Effect|
|--|--|
|1|+20 speed  |
|2|+40 speed  |
|3|+60 speed  |
|4|+80 speed  |
        
#### Emergency Parachute  
>   Vehicle Augment  
Cost to Activate: 1 AP reflexively  

When your vehicle starts to lose altitude and is going to crash, you can release your emergency parachute in order to slow your descent. The pilot may activate the emergency parachute for 1 action point, and it will cause the vehicle to float slowly to the ground despite any injuries the vehicle has taken. The vehicle will fall 10 feet per turn.  

        A foe may attack the emergency parachute. The parachute has a number of wounds based on the marque of this augment.    

|Marque| Effect|
|--|--|
|1|3 wounds   |
|2|6 wounds   |
|3|9 wounds   |
|4|12 wounds   |
        
#### Extra Passengers  
>   Vehicle Augment  
> ❓ Requires: vehicle to be augmented to have the Passenger augment  

You’ve outfitted your vehicle with small compartments, uncomfortable back seats, and barrels that hang from the main seat that are all capable of carrying extra passengers. You may add a number of extra passengers (in addition to the passenger allowed from your Passenger augment). This does not make the vehicle much larger, despite logic dictating otherwise (hey, it’s steampunk!).   
        

|Marque| Effect|
|--|--|
|1|+1 passengers (to a total of 1 pilot & 2 passengers)   |
|2|+2 passengers (to a total of 1 pilot & 3 passengers)   |
|3|+3 passengers (to a total of 1 pilot & 4 passengers)   |
|4|+4 passengers (to a total of 1 pilot & 5 passengers)   |
        
#### Flying  
>   Vehicle Augment  
Can only be applied to Clankers & Takes up 2 Augment Slots  

You’ve attached cranked propellers or wings to your clanker, letting you soar through the air. You may move your vehicle through the air, traveling in any direction.  

> 🗒️️         Note: This augment always acts as Marque II for the purposes of determining cost, though you can learn this augment despite your skill in Engineer.  
#### Gliding  
>   Vehicle Augment  

Your vehicle is equipped to glide for a while after the vehicle has stopped moving. If you turn off your vehicle or it can’t move (for any reason) while it’s in the air, the vehicle will glide forward for a number of turns before it loses its ability to glide. It will continue to move forward at its speed but will lose 10 feet of altitude per turn. It will glide for a number of turns based on the marque of this augment.  

|Marque| Effect|
|--|--|
|1|2 turns   |
|2|4 turns   |
|3|6 turns   |
|4|8 turns   |
        
#### Improved Construction  
>   Vehicle Augment  

Your vehicle is built exceptionally well, made sturdy, strong, and tough to take down. Your vehicle has extra wounds based on the marque of this augment.  

|Marque| Effect|
|--|--|
|1|+12 wounds   |
|2|+24 wounds   |
|3|+36 wounds   |
|4|+48 wounds   |
        
#### Improved Speed  
>   Vehicle Augment  
Can only be applied to Autos  

You’ve streamlined your vehicle to make it faster. Your speed is improved based on the marque of this augment.  

|Marque| Effect|
|--|--|
|1|+50 speed   |
|2|+100 speed   |
|3|+150 speed   |
|4|+200 speed   |
        
#### Lift  
>   Vehicle Augment  
Can only be applied to Autos  

Your vehicle can support itself through vertical lift, either through a lighter-than-air envelope or a graviton sphere. If you are combining Lift with Aerial Propulsion, you’ll be able to move vertically a certain distance based on the marque of this augment as well as move horizontally based on your speed. Lift only lets you move vertically, and it acts independently of all other speeds. The pilot of the automatic vehicle will need to set the lift speed as well (which costs 1 action point to change).  

|Marque| Effect|
|--|--|
|1|20 feet of vertical movement   |
|2|40 feet of vertical movement   |
|3|60 feet of vertical movement   |
|4|80 feet of vertical movement   |
        
#### Passenger  
>   Vehicle Augment  

You’ve installed a second seat in your vehicle - good job, now your friend can come with you! You may carry a single passenger in your vehicle. (This does not increase the size of your vehicle.)  

> 🗒️️         Note: This augment always acts as Marque I for the purposes of determining cost, though you can learn this augment despite your skill in Engineer.

#### Power Thrusters  
>   Vehicle Augment  
Can only be applied to Autos  
> 🪙 Activation Cost: 0 AP (but done when changing speed)  

You’ve applied extra thrusters to your vehicle that make you move exceptionally fast. Unfortunately, it makes you so fast that your vehicle starts to break down. Any time you adjust your speed, you can choose to freely activate your power thrusters. This increases the vehicle’s maximum speed based on the marque of this augment. However, every turn that you have your power thrusters activated, your vehicle loses 1 wound.  

|Marque| Effect|
|--|--|
|1|+100 speed beyond your vehicle’s maximum speed   |
|2|+200 speed beyond your vehicle’s maximum speed   |
|3|+300 speed beyond your vehicle’s maximum speed   |
|4|+400 speed beyond your vehicle’s maximum speed   |
        
#### Side Thrusters  
>   Vehicle Augment  
> 🪙 Cost: 1 AP reflexively  

Side thrusters allow you to quickly toss to the side, bolting out of the way. Side thrusters can be activated at any time for 1 action point, and they send your vehicle a certain distance based on the marque of this augment. You may have your side thrusters send you in any direction (regardless of the direction you’re traveling), and you may go any distance, up to the maximum allowed by the side thrusters. You cannot travel through solid terrain.  

|Marque| Effect|
|--|--|
|1|up to 20 feet   |
|2|up to 40 feet   |
|3|up to 60 feet   |
|4|up to 80 feet   |
        
#### Silent Movement  
>   Vehicle Augment  

Your vehicle is well-greased, quiet, and difficult to hear. When somebody is listening for your vehicle, they must make a Cunning result to hear, based on the marque of this augment.  

|Marque| Effect|
|--|--|
|1|Tier 2 Cunning to detect   |
|2|Tier 3 Cunning to detect   |
|3|Tier 4 Cunning to detect   |
|4|Undetectable (unless you can achieve a tier 5 Cunning)   |
        
#### Speed Durability  
>   Vehicle Augment  

It’s difficult for attacks to damage your engine. An attack that damages your vehicle must do a certain amount of wounds damage before it decreases your speed. (Normally 1 wound damage would decrease your speed by 10 for autos or 5 feet for clankers.)  

|Marque| Effect|
|--|--|
|1|3 wounds from a single attack before speed is reduced   |
|2|6 wounds from a single attack before speed is reduced   |
|3|9 wounds from a single attack before speed is reduced   |
|4|12 wounds from a single attack before speed is reduced   |
        
#### Sturdy  
>   Vehicle Augment  

The vehicle is strongly built and gains an increased soak class. This soak class is not from armoring, but rather comes from the basic soak of the vehicle. (As such, it cannot be sundered or broken like normal armoring.) The amount of soak class is based on the marque of this augment.  

|Marque| Effect|
|--|--|
|1|+1 soak class   |
|2|+2 soak class   |
|3|+3 soak class   |
|4|+4 soak class   |
        
#### Thick Exhausts  
>   Vehicle Augment  

Thick steam, smoke, and smog rolls out from your vehicle, clouding the area around you. The exhaust coats your vehicle, making it difficult to target your vehicle. Generally, this is made not to negatively effect the pilot of any gunners on board. The thick exhausts gives you a bonus on your evade based on the marque of the augment. (If the attacker has any ways of negating penalties caused from cover or poor sight, they can negate this as well.)  

|Marque| Effect|
|--|--|
|1|+1 on evade rolls   |
|2|+2 on evade rolls   |
|3|+3 on evade rolls   |
|4|+4 on evade rolls   |
        
#### Underwater Propulsion  
>   Vehicle Augment  

Your vehicle has fins, jets, propellers, or some other way of moving through the water. You may apply your vehicle speed to moving underwater. You can change directions, moving up, down, or forward. Underwater propulsion also allows you to move through the air, but you move at half your speed (unless you have Aerial Propulsion).  

> 🗒️️         Note: This augment always acts as Marque II for the purposes of determining cost, though you can learn this augment despite your skill in Engineer.

#### Weapon Mount  
>   Vehicle Augment  

You’ve outfitted your vehicle with a weapon turret that you can fire from the cockpit. The weapon is built into the vehicle, and can be augmented as a weapon. If you have any passengers in the vehicle, they can operate the weapon instead. If the weapon requires the wielder to be in firing position, the weapon always counts as being in firing position.  

        You can detach or reattach the weapon for a number of action points depending on the marque of this augment.   
        
|Marque| Effect|
|--|--|
|1|3 action points   |
|2|2 action points   |
|3|1 action point   |
|4|1 action point reflexively  |
  
Automatic versus Manual Vehicles

There are two ways of controlling vehicles, automatically propelled vehicles and those that require manual control.

        Automatically propelled vehicles (autos) are those that go at a constant speed unless the pilot changes the direction or speed of the vehicle. Autos typically have propellers, wheels, jets, or sails. Autos have a maximum speed, and they travel a set distance every turn (typically at the beginning of the pilot’s turn).

        Manual vehicles (clankers) require the pilot to spend action points for every movement. Typically these are walking vehicles, where the pilot must move gears and levers every time the clanker wants to move. In effect, every time the pilot spends 1 action point to move the clanker, the clanker moves its speed.

        You’ll find information on autos starting on this page, with their augments directly after. After autos, you’ll get information on the clankers. Once all of that is done, you can build a vehicle’s hull, providing it with armoring and protection for those inside.

Piloting your Auto

### Control Method

While autos can be propelled by anything in your imagination, at default your craftable mount is grounded, with a maximum land or water surface speed based off of the marque of this specialty. It costs an action point to mount or dismount your auto. Your auto can be turned on and off for one action point. While piloting your auto, your defense and evade (and accuracy and strike, if applicable) are added to that of the vehicle.

        When you start your auto, select a speed setting between 5 feet per turn and its maximum speed per turn. While turned on, it will move at that speed every turn at the beginning of the pilot’s turn. It costs one action point to change the speed. It also costs one action point to control its movement during a turn, moving up to its current speed setting. If you do not spend the action point to control it, it will automatically move its last set speed in a straight line in the same direction it last moved.

### Body of the Machine

At default your vehicle can hold only one person at a time: its pilot. Your vehicle cannot protect its pilot from harm; the pilot can still be targeted as per normal.

        Your automatic vehicle has a default maximum wounds of twelve and can be targeted by hostiles without the need of a called shot. Whenever your vehicle takes wounds damage, its maximum speed decreases by 10 feet. Should your automatic vehicle lose all of its wounds, your auto ceases to move. If it is in the air when this happens, anyone riding your vehicle will suffer falling damage. If on water, your vehicle will begin to sink at a rate of twenty feet per turn.

        This specialty works within the marque system. As your skill in Engineering grows, your autos will become faster and more efficient, increasing its maximum speed per turn.
        

|Tier| Effect|
|--|--|
|1|Maximum of 150 feet per turn|
|2|Maximum of 200 feet per turn|
|3|Maximum of 300 feet per turn|
|4|Maximum of 500 feet per turn|

Conceptualizing your Vehicle

When you first start building vehicles, they’re not going to be full-fledged airships. Taking Auto-Wright will basically give you an engine that you can sit on or strap to your back. At the most basic levels, you probably have little more than a motorized bicycle or a propellered surf-board.

        By taking augments, you can improve on the vehicle. When you take Aerial Propulsion or Lift, you’ll be able to soar the skies on a rocketpack. Want to take your friends with you? The Passenger and Extra Passengers augments will be your choice. Is your vehicle’s a little too rickety and easily destroyed for your liking? Taking Improved Construction and Sturdy will solve that problem.

        By adding armoring, your vehicle can become a mobile tank, a weapon platform, a flying gunship, or whatever you can imagine.

        And though you’ll be able to accomplish a lot, your vehicle’s going to start off simple. Barely a vehicle at all: just an engine, and some basic controls.

Piloting your Clanker

### Control Method

While manual vehicles can be propelled by anything you imagine, by default your craftable mount is grounded, with a land or water surface speed based off of the marque of this specialty. It costs an action point to mount or dismount your clanker. Your clanker can be turned on and off for one action point. While piloting your clanker, your defense and evade (and accuracy and strike, if applicable) are added to that of the vehicle.

        It costs the pilot one action point to move the vehicle up to its full movement speed. The vehicle can move as many times per turn as the pilot can spend action points to do so. It can move in any direction without having to spend action points to turn, slow down, or any of that hogwash. It cannot climb or submerge itself. It costs an action point to mount or dismount your vehicle.

### Body of the Machine

At default your vehicle can hold only one person at a time: its pilot. Your vehicle cannot protect its pilot from harm; the pilot can still be targeted as per normal.

        Your manual vehicle has a default maximum wounds of twelve and can be targeted by hostiles without the need of a called shot. Whenever your vehicle takes wounds damage, its maximum speed decreases by 5 feet. Should your vehicle run out of wounds, your manual vehicle ceases to move. If it is in the air when this happens, anyone riding your manual vehicle will suffer falling damage. If on water, your manual vehicle will begin to sink at a rate of twenty feet per turn.

        This specialty works within the marque system. As your skill in Engineer grows, your clankers will become faster and more efficient, increasing its speed per action point spent.

|Tier| Effect|
|--|--|
|1|Up to 40 feet per action point spent|
|2|Up to 80 feet per action point spent|
|3|Up to 120 feet per action point spent|
|4|Up to 160 feet per action point spent|

### Armoring Vehicles
-----------------

#### Vehicle Armorer
> Augments +2, DIY +1, Hit Points +6

> Engineer Specialty

A vehicle without armoring is little more than an engine and some sort of control system. You’ll give it a shell, a hull to keep the pilot, the engine, and everything you love safe. You can now armor vehicles.

        Without spending any money, you can build and maintain the armoring on several vehicles based on your current Do-It-Yourself (DIY) score. They can then be upgraded with augments. You’ll learn 2 augments from this specialty, which can be selected under “armoring augments” below. These augments have marques. At lower levels, you’ll start with Marque I augments. See the “Crafting” page at the beginning of this chapter for more information.

        Vehicle armoring can be upgraded with 3 augments. Some materials can only be upgraded twice (like wooden armoring) or just once (if you choose organic armoring). Sometimes an augment will take up multiple augment slots. If an augment is worth 2 slots, armoring only has 1 more available slot for an augment after the 2-slot augment has been applied.

#### Amount of Armoring you can Maintain  

Without needing to buy pieces or parts, you can build the armoring on some vehicles entirely out of scraps. This armoring must be constantly maintained by you and stops functioning soon after leaving your care. You may build and maintain a number of vehicle armoring for free based on your DIY score. You may create more armoring or augment old armoring during any period of downtime you have.

#### The Cost of Armoring 

If you need to create armoring that you can’t build for free from your DIY score, you will need to buy the materials for it. Armoring will have a base materials cost. It is 1/5th the market price.

        Armoring, unaugmented, will have a base cost depending on its size. If you buy armoring with augments, you will add the price of the augments onto the price of the vehicle.

        Every augment will increase the price. The higher the marque, the greater the price. The market price for an augment can be found in the chart below.

If you are building the augment outside of your DIY Score, you pay 1/5th the price, which is the same as if you were buying an augment one marque lower. (As in, the material cost for a Marque III augment is the market price for a Marque II augment.) The material cost for a Marque 1 augment is 30 princes.

#### Beta Armoring
> Augments +2, DIY +1, Hit Points +6

> Engineer Specialty

> ❓ Requires: 4 skill points in Engineer & Vehicle Armorer specialty

Your armoring is beyond that seen before, but it is nearly impossible to pilot a vehicle covered in the stuff. Such armoring can be upgraded with 2 more augments (bringing the total for metal armoring up to 5 augmentable slots).

        If anybody other than you attempts to operate a vehicle covered in your armoring, they must succeed in rolling a science result one tier higher than the highest level marque you have on your armoring. If your armoring has a Marque IV augment, it is impossible for them to use it (unless they can somehow obtain a tier result of 5 with their Sciences attribute).

        If you are a passenger in a vehicle that has beta armoring that you created, you can allow another person to pilot the vehicle.

#### Prototype Armoring
> Augments +1, DIY +1, Hit Points +7

> Engineer Specialty

> ❓ Requires: 16 skill points in Engineer, Vehicle Armorer, & Beta Armoring specialties

You’ve perfected your beta armoring and made it user-friendly. Now anybody can pilot a vehicle covered in your armoring as long as you designate it as being a prototype.

### Armoring Augments

#### Airtight
>   Armoring Augment

The vehicle can go higher in the atmosphere or underwater. While the airtightness is a factor, so is the vehicle’s ability to withstand pressure. If there is no way of breathing, oxygen will eventually run out of an airtight vehicle.

|Marque| Effect|
|--|--|
|1|The vehicle can go to cloud level or just below the surface of water|
|2|The vehicle can go to cloud level or deep underwater|
|3|The vehicle can go above clouds or to the ocean’s bottom|
|4|The vehicle can skim the top of the atmosphere or reach the abysses of the ocean|

#### Effective Armoring
>   Armoring Augment

Your vehicle’s armoring is streamlined, slick, and it’s surprisingly easy to ensure that bullets aren’t going to make it through. The pilot gains a bonus on the vehicle’s defense roll.

|Marque| Effect|
|--|--|
|1|+2 on defense rolls|
|2|+4 on defense rolls|
|3|+6 on defense rolls|
|4|+8 on defense rolls|

#### Electro-Absorption
>   Armoring Augment

The armoring re-routes electricity through it and into specially created devices that absorb the shock. Anything that deals electricity damage is now entirely soakable, and attacks that deal electricity damage (even if they are only partially electrical, such as attacks with a pulsing weapon) increase the soak class of the armor.

|Marque| Effect|
|--|--|
|1|+2 soak class against electrical attacks|
|2|+4 soak class against electrical attacks|
|3|+6 soak class against electrical attacks|
|4|+8 soak class against electrical attacks|

#### Emergency Landing
>   Armoring Augment

Your armor is built to withstand crashes. When crashing (though we hope it’s not often), the hull can soak a certain amount of damage and keep it from being inflicted upon the crew.

|Marque| Effect|
|--|--|
|1|10 damage|
|2|50 damage|
|3|100 damage|
|4|250 damage|

#### Exceptional Structure
>   Armoring Augment

> ❓ Requires: armoring to be augmented to have the Structural Integrity augment

Your vehicle’s armoring is exceptional, providing greater support and a thicker buffer between your hard, outer shell and the gooey insides. This grants you additional hit points, which stack with those granted by Structural Integrity.

|Marque| Effect|
|--|--|
|1|+20 hit points|
|2|+40 hit points|
|3|+60 hit points|
|4|+80 hit points|

#### Fireproofing
>   Armoring Augment

While coated in armoring that has fireproofing, all fire attacks are soakable. Furthermore, the soak class for the armoring is improved against fire.

|Marque| Effect|
|--|--|
|1|+1 soak class against fire|
|2|+2 soak class against fire|
|3|+3 soak class against fire|
|4|+4 soak class against fire|

#### Flame Retardant
>   Armoring Augment

The armoring generally cannot be caught on fire, depending on how intense the fire is.

|Marque| Effect|
|--|--|
|1|Cannot be caught on tier 1 fire|
|2|Cannot be caught on tier 2 fire|
|3|Cannot be caught on tier 3 fire|
|4|Cannot be caught on tier 4 fire|

#### Greater Crew Cover
>   Armoring Augment

Your vehicle’s armoring is designed to keep you and any passengers alive for significantly longer. The greater crew cover improves the cover by a degree (or more, depending on the marque). For example, if you’re in vehicle with Marque I Greater Crew Cover light armoring, everyone on board can take advantage of medium cover. The maximum cover is total cover, in which the people inside the vehicle cannot be targeted.

|Marque| Effect|
|--|--|
|1|improves cover by 1 degree|
|2|improves cover by 2 degrees|
|3|improves cover by 3 degrees|
|4|improves cover by 4 degrees|

#### High Mobility
>   Armoring Augment

The armor is optimized to be lightweight, easy to move in, and not clutter up your vehicle’s amazing speed. This augment decreases the speed penalty the vehicle takes from wearing the armor. It decreases it by a degree - for example, if you’re using Marque I High Mobility light armoring, it would have the speed penalty of minimal armoring.

|Marque| Effect|
|--|--|
|1|the speed penalty is 1 degree lighter|
|2|the speed penalty is 2 degrees lighter|
|3|the speed penalty is 3 degrees lighter|
|4|the speed penalty is 4 degrees lighter|

#### Internal Oxygen Supply
>   Armoring Augment

Your armoring provides oxygen for your vehicle. It will last for a definite period, depending on the marque.

|Marque| Effect|
|--|--|
|1|10 minutes|
|2|60 minutes|
|3|1 day|
|4|1 week|

#### Structural Integrity
>   Armoring Augment

The armoring was built to be exceptionally sturdy, granting hit points to the vehicle. Like normal, hit points are damaged before wounds.

|Marque| Effect|
|--|--|
|1|+10 hit points|
|2|+20 hit points|
|3|+30 hit points|
|4|+40 hit points|

#### Thick Armoring
>   Armoring Augment

You’ve thickened your armoring without impeding your creation at all. You grant the armor extra soak.

|Marque| Effect|
|--|--|
|1|+1 soak class|
|2|+2 soak class|
|3|+3 soak class|
|4|+4 soak class|

### Vehicle Armoring Table

|Type|Soak Class|Evade Penalty|Auto Penalty|Clanker Penalty|Passenger Cover|Material Options|
|----|----------|-------------|------------|---------------|---------------|----------------|
|Unarmored|0|-|-0 ft.| -0 ft.|None|None, Organic, or Textile|
|Minimal|1|-1|-10 ft.|-5 ft.|Poor (+2 evade)|Metal, Organic, Textile, or Wood|
|Light |2|-2|-20 ft.|-10 ft.|Light (+4 evade)|Metal, Organic, Textile, or Wood|
|Medium|3|-4|-40 ft.|-20 ft.|Medium (+6 evade)|Metal, Organic, or Wood|
|Heavy|4|-6|-60 ft.|-30 ft.|Heavy (+8 evade)|Metal or Wood|
|Super-Heavy|5|-8|-80 ft.|-40 ft.|Total (cannot be targeted)|Metal or Wood|

* * *

## Gadgetry Skill
==============

Gadgets, doodads, trinkets, whatchamacallits - these are inventions that mad scientists would be proud of, items that permeate society and make life easier (or, at the very least, more exciting). A gadgeteer is a specialist in the wild mechanisms of technology, building explosives, fantastic eyewear, and any number of trinkets. The gadgeteer can be rather creative, using his skillset to build and augment a wide variety of items.

#### Beta Hacker
> Evade +1, Augments +1, Hit Points +8

> Gadgetry Specialty

Your tinkering and logic has made you adept at hacking into beta equipment. Whenever you are trying to work a beta item, you may add your skill in gadgetry to the roll. In addition, if you receive a result of 40 or higher, you have made a tier 5 result, and can now use Marque IV beta items.

#### Dud
> Accuracy +1, Evade +1, Hit Points +7

> Gadgetry Specialty

> ⬇️ Resist: Cunning (negates)

> 🪙 Cost: 1 AP reflexively

Whenever someone adjacent to you is activating the fuse of an explosive, you can make the explosive a dud. The explosive will not explode, and your enemies must make a Cunning resist to realize the explosive isn’t functioning.

#### Item Breaker
> Accuracy +1, Strike +3, Hit Points +9

> Gadgetry Specialty

> ❓ Requires: Saboteur specialty

> 🪙 Cost: same as a Sunder

Whenever you successfully sunder an item, you are able to turn off some of its augments until the next breather.

|Tier| Effect|
|--|--|
|1|Marque 1 augments are turned off|
|2|Marque 2 augments are turned off|
|3|Marque 3 augments are turned off|
|4|Marque 4 augments are turned off|

#### Reverse Engineer
> Augments +2, DIY +1, Hit Points +5

> Gadgetry Specialty

You are able to move augments from one item to another, including those you yourself are not capable of crafting. When reverse engineering an item, roll your Gadgetry. If you are unsuccessful, the augment is destroyed. Augments may only be moved to an item the augment could normally be attached to. For instance, you cannot move a melee weapon augment to a vehicle. This can be performed during any breather, though the exact amount of time this takes is dependant on the size of the device, at the narrator’s discretion.

|Tier| Effect|
|--|--|
|1|Can move up to a Marque 1 augment|
|2|Can move up to a Marque 2 augment|
|3|Can move up to a Marque 3 augment|
|4|Can move up to a Marque 4 augment|

#### Saboteur
> Accuracy +1, Priority +3, Hit Points +8

> Gadgetry Specialty

> ⬇️ Resist: Dexterity (negates)

> 🪙 Cost: 1 AP

You know how to throw a wrench into another person’s plans...and their items. You can turn off a single augment of an item either adjacent to you or being wielded by someone adjacent to you.

#### Improvised Improvements
> Evade +1, Augments +1, Hit Points +7

> Gadgetry Specialty

> ❓ Requires: 4 skill points in Gadgeteer

> 🧘 Stance (costs 1 AP to enter)

Crafted items seem to perform better when you’re around. When entering this stance, choose augments on any items within 5 feet of your current position, whether you are wielding the item or not. These augments act as if they were 1 marque higher. You can do this to 1 augment for every 4 skill points you have in Gadgetry. If any item whose augments you’re improving moves away from being adjacent to you, the improvements wear off. To reassign which augments you’re improving, you must re-enter your Improvised Improvements stance. You cannot make an augment exceed Marque IV.

#### Pyrotechnician
> Augments +2, DIY +1, Hit Points +4

> Gadgetry Specialty

You’re a master of explosives, so full of gunpowder and fuses that you always run the risk of blowing off your left ear. You turn the battlefield into a place of nightmares, your explosions ringing in your enemies’ ears and burns searing off their flesh. You can now create explosives.

        Without spending any money, you can build and maintain several explosives based on your current Do-It-Yourself (DIY) score. These explosives can then be upgraded with augments. You’ll learn 2 augments from this specialty, which can be selected under “explosive augments” below. These augments have marques. At lower levels, you’ll start with Marque I augments. As your skill in Gadgetry improves, your marques will increase. See the “Crafting” page at the beginning of this chapter for more information.

        Each explosive can be upgraded with 3 augments. Some materials can only be upgraded twice (like wooden explosives) or just once (like organic ones). Sometimes an augment will take up multiple augment slots. For example, the “gear-rattler” augment is worth 2 slots, so an explosive only has 1 more available slot for an augment after “gear-rattler” has been applied.

#### Number of Explosives you can Maintain  

Without needing to buy pieces or parts, you can build some explosives entirely out of scraps. These explosives must be constantly maintained by you and stop working soon after leaving your care. You may build and maintain a number of explosives based on your DIY score. You may build new explosives or augment old ones during any period of downtime you have.

#### The Cost of Explosives 

If you need to build an explosive that you can’t build for free from your DIY score, you will need to buy the materials for it.

        An explosive will have a base materials cost. It is 1/5th the market price.

        Every augment will increase the price. The higher the marque, the greater the price. The market price for an augment can be found in the chart below.

If you are building the augment, you pay 1/5th the price, which is the same as if you were buying an augment one marque lower. (As in, the material cost for a Marque III augment is the market price for a Marque II augment.) The material cost for a Marque I augment is 1 prince and 6 dukes.

#### Beta Explosives
> Augments +2, DIY +1, Hit Points +5

> Gadgetry Specialty

> ❓ Requires: 4 skill points in Gadgetry & Pyrotechnician specialty

Your explosives are exceptionally complex little gizmos, but the pack they punch is not to be underestimated. Such explosives can be upgraded with 2 more augments (bringing the total for metal explosives up to 5 augmentable slots).

        If anybody other than you attempts to use one of your beta explosives they must succeed in rolling a sciences result one tier higher than the highest marque you have on your explosive. If your explosive has a Marque IV augment, it is impossible for them to use it (unless they can somehow obtain a tier result of 5 with their Sciences attribute).

#### Prototype Explosives
> Augments +2, DIY +1, Hit Points +6

> Gadgetry Specialty

> ❓ Requires: 16 skill points in Gadgetry, Pyrotechnician, & Beta Explosives specialties

You’ve perfected your beta explosives and made them user-friendly. Now anybody can use an explosive that you designate as being a prototype.

#### Major Explosion
> Augments +2, DIY +1, Hit Points +4

> Gadgetry Specialty

> 🪙 Cost: Activating the Fuse +1 AP

You’re skill with explosives ensures that their blast radius is larger than that of anybody else. When you use major explosion with an explosive, roll below in order to increase the area of effect for the blast.

|Tier| Effect|
|--|--|
|1|+5 feet spread|
|2|+10 feet spread|
|3|+15 feet spread|
|4|+20 feet spread|

> 🗒️️ Note: You can, at your discretion, take a lower spread result.

### Explosive Augments

#### Banshee
>   Explosive Augment

The blast releases an ear-splitting wail, causing its victims to become temporarily deafened (and greatly annoying everybody else).

|Marque| Effect|
|--|--|
|1|Deafened for 1 turn|
|2|Deafened for 2 turns|
|3|Deafened for 3 turns|
|4|Deafened for 4 turns|

#### Concealable
>   Explosive Augment

The bomb is camouflaged and can be concealed in its environment or on a character’s body. The character gains bonuses on any cunning rolls used to hide the explosive.

|Marque| Effect|
|--|--|
|1|+3|
|2|+6|
|3|+9|
|4|+12|

#### Concussive
>   Explosive Augment

Characters caught within the blast are subject to a powerful pulse, causing their brains to rattle in their skulls as their heads are jerked by the shockwave.

|Marque| Effect|
|--|--|
|1|Disoriented for 1 turn|
|2|Disoriented for 2 turns|
|3|Disoriented for 3 turns|
|4|Disoriented for 4 turns|

#### Damaging
>   Explosive Augment

The blast is souped-up to be more powerful, dealing greater amounts of total damage to those caught within its range.

|Marque| Effect|
|--|--|
|1|11 damage per the Explosive’s Marque|
|2|12 damage per the Explosive’s Marque|
|3|13 damage per the Explosive’s Marque|
|4|14 damage per the Explosive’s Marque|

#### Delay
>   Explosive Augment

The bomb can be set to automatically go off in a number of turns specified by the player.

> 🗒️️         Note: This augment always acts as Marque I for the purposes of determining cost, though you can learn this augment despite your skill in Gadgetry.

#### Extended Blast
>   Explosive Augment

> ❓ Requires: Marque II Explosives

The bomb has a greater radius of effect. The bomb acts normally, but also blasts out beyond its normal range. The bomb does damage of one marque lower than its own marque beyond its normal range.

|Marque| Effect|
|--|--|
|1|5 feet further|
|2|10 feet further|
|3|15 feet further|
|4|20 feet further|

#### Flare
>   Explosive Augment

The explosion leaves behind a piece of material which burns brightly like a flare, lighting the area within 25 feet for several turns. Dim light extends for an additional 25 feet from the source.

|Marque| Effect|
|--|--|
|1|2 turns|
|2|4 turns|
|3|6 turns|
|4|8 turns|

#### Flash
>   Explosive Augment

The blast releases a powerful flash of light, causing temporary blindness to all within the area of the explosion.

|Marque| Effect|
|--|--|
|1|Blinded for 1 turn|
|2|Blinded for 2 turns|
|3|Blinded for 3 turns|
|4|Blinded for 4 turns|

#### Flypaper
>   Explosive Augment

If the target is wearing organic or wooden armor, the bomb sticks to them like a magnet. The target must spend 2 action point and fully resist the augment or be denied their ability to evade the blast.

        If the target is not wearing organic or wooden armor, it falls to the ground in front of them.

> 🗒️️         Note: This augment always acts as Marque II for the purposes of determining cost, though you can learn this augment despite your skill in Engineer.

#### Gear-Rattler
>   Explosive Augment

Takes up 2 Augment Slots on an Explosive

The bomb is built to explode in such a way that it disables machinery, rattling gears and causing it to become stunned. When an automaton is affected by this augment, it is stunned for a number of action points.

|Marque| Effect|
|--|--|
|1|Stunned for 1 AP|
|2|Stunned for 2 AP|
|3|Stunned for 3 AP|
|4|Stunned for 4 AP|

When a vehicle is affected by an explosive with this augment, it is uncontrollable and moves directly forward for a number of action points.

|Marque| Effect|
|--|--|
|1|Cannot be controlled for 1 AP|
|2|Cannot be controlled for 2 AP|
|3|Cannot be controlled for 3 AP|
|4|Cannot be controlled for 4 AP|

#### Implosion
>   Explosive Augment

Your explosive pulls anyone within its blast radius towards its epicenter when it explodes.

|Marque| Effect|
|--|--|
|1|Pulled 5 feet towards the explosion’s center|
|2|Pulled 10 feet towards the explosion’s center|
|3|Pulled 10 feet towards the explosion’s center|
|4|Pulled 15 feet towards the explosion’s center|

#### Knock Back
>   Explosive Augment

The blast sends out a shockwave which knocks opponents back. Opponents who are unable to dodge the blast are pushed back from the center of the blast and may be knocked prone.

|Marque| Effect|
|--|--|
|1|Pushed back 5 feet|
|2|Pushed back 10 feet|
|3|Pushed back 10 feet and prone|
|4|Pushed back 15 feet and prone|

#### Latch
>   Explosive Augment

The bomb contains a clamp which latches it to opponents. When you throw or otherwise attach the bomb, the opponent must spend 1 action point to remove the bomb or be denied their ability to evade the blast. Also, if you make a called shot when attaching the weapon, the blast counts as a called shot to the called shot location.

> 🗒️️         Note: This augment always acts as Marque II for the purposes of determining cost, though you can learn this augment despite your skill in Gadgetry.

#### Launching
>   Explosive Augment

The bomb is built to be launchable, increasing the distance that it can be fired.

|Marque| Effect|
|--|--|
|1|+10 feet|
|2|+20 feet|
|3|+30 feet|
|4|+40 feet|

#### Magnetic
>   Explosive Augment

If the target is wearing metal armor, the bomb sticks to them like flypaper. The target must spend 2 action point and fully resist the augment or be denied their ability to evade the blast.

        If the target is not wearing metal armor, it will stick on to any person within 5 feet wearing the heaviest metal armor. (If multiple people are wearing equally heavy armor within 5 feet, have them roll randomly to see who wins the right to wear the magnetic explosive. The highest roller gets the bomb.)

> 🗒️️         Note: This augment always acts as Marque II for the purposes of determining cost, though you can learn this augment despite your skill in Gadgetry.

#### Melter
>   Explosive Augment

Takes up 2 Augment Slots on an Explosive

The blast splashes molten metal onto armor and clothing, decreasing its potency and melting through it, causing it to be less useful until it can be repaired (which can normally be done during a breather).

|Marque| Effect|
|--|--|
|1|-1 soak class|
|2|-1 soak class|
|3|-2 soak class|
|4|-2 soak class|

#### Proximity Fuse
>   Explosive Augment

The bomb is set up with a fuse that causes it to detonate when somebody walks within the bomb’s blast area. Characters attempting to pass through the area without detonating the fuse must move no faster than 10 feet per action point and roll to resist setting off the trigger.

        In order to activate a Proximity Fuse, you must first spend the usual 1 action point to activate the fuse, then either drop or throw it. The fuse effect becomes active at the end of your turn.

> 🗒️️         Note: This augment always acts as Marque II for the purposes of determining cost, though you can learn this augment despite your skill in Gadgetry.

#### Powerful Blast
>   Explosive Augment

The blast occurs with such speed that it is more difficult to jump out of its way. Characters who wish to use their dexterity to resist the blast must act as if their Dexterity roll to resist the blast was one tier lower.

> 🗒️️         Note: This augment always acts as Marque II for the purposes of determining cost, though you can learn this augment despite your skill in Gadgetry.

#### Quick-Set
>   Explosive Augment

Activating the fuse on an explosive costs 1 less action point (normally bringing the cost down to 0 action points).

> 🗒️️         Note: This augment always acts as Marque II for the purposes of determining cost, though you can learn this augment despite your skill in Gadgetry.

#### Remote Activation
>   Explosive Augment

You have a trigger device that lets you activate the explosive from a distance whenever you so desire. It must have been set previously (for at least 1 turn) and you cannot be outside of a certain distance (based on the marque). An explosive with remote activation will not go off until you so designate and using the remote activation costs 1 action point.

|Marque| Effect|
|--|--|
|1|25 foot range|
|2|50 foot range|
|3|75 foot range|
|4|100 foot range|

#### Searing
>   Explosive Augment

Victims caught within the white phosphorous blast find their skin covered in hot material, searing the skin. These burns will recover during the victim’s next breather.

|Marque| Effect|
|--|--|
|1|Tier 1 Burns (-1 on defense rolls)|
|2|Tier 2 Burns (-3 on defense rolls)|
|3|Tier 3 Burns (-5 on defense rolls)|
|4|Tier 4 Burns (-7 on defense rolls)|

#### Slippery
>   Explosive Augment

When your explosive is thrown, it begins to secrete a slimy liquid, making it difficult to pick up. It costs 2 action points to attempt to pick your explosive up, and the person attempting to do so must roll their Dexterity.

|Marque| Effect|
|--|--|
|1|Tier 2 Dexterity to pick it up|
|2|Tier 3 Dexterity to pick it up|
|3|Tier 4 Dexterity to pick it up|
|4|Impossible (Unless you can somehow attain a Tier 5 Dexterity result)|

#### Smoking
>   Explosive Augment

The blast fills the area with a thick screen of smoke, causing those within the area to be blinded.

|Marque| Effect|
|--|--|
|1|Dissipates in 1 turn|
|2|Dissipates in 2 turns|
|3|Dissipates in 3 turns|
|4|Dissipates in 4 turns|

Resisting Explosives      

If a person is within an explosion when it goes off, they’ll take the damage from the explosion. The damage is soakable (meaning that they can roll their defense and soak some or all of the damage).

        A person can attempt to dive out of the explosion. In order to get out of the explosion as it’s going off, the target must spend 1 action point reflexively in order to try to move. The affected targets must make a Dexterity resist in order to escape the blast. For every tier above tier 1 that the person receives with their Dexterity, they may lower the marque of the explosion by 1 (normally taking the damage class down by 10). If they lower it at all, they may move to the edge of the blast. If they entirely negate the blast, they move out of it.

### Crafting Eyewear

#### Optician
> Augments +2, DIY +1, Hit Points +4

> Gadgetry Specialty

While you’re friends are out there becoming master swordsmen and crafting great war-engines, you’ve been learning how to craft goggles. You can now create eyewear.

        Unless you’ve gained extra eyes or somehow have grafted eyewear, you are limited to looking through one piece of eyewear. Even if you have multiple sets of eyes, you can only look through a single piece of eyewear at a time.

        Without spending any money, you can build and maintain several sets of eyewear based on your current Do-It-Yourself (DIY) score. These eyewears can then be upgraded with augments. You’ll learn 2 augments from this specialty, which can be selected under “eyewear augments” below. These augments have marques. At lower levels, you’ll start with Marque I augments. As your skill in Gadgetry improves, your marques will increase. See the “Crafting” page at the beginning of this chapter for more information.

        Each eyewear can be upgraded with 3 augments. Some materials can only be upgraded twice (like wooden eyewear) or just once (like organic ones). Sometimes an augment will take up multiple augment slots. For example, the “alert” augment is worth 3 slots, so an eyewear has 0 more available slot for an augment after “alert” has been applied.

#### Number of Eyewears you can Maintain  

Without needing to buy pieces or parts, you can build some sets of eyewear entirely out of scraps. These eyewears must be constantly maintained by you and stop working soon after leaving your care. You may build and maintain a number of eyewears based on your DIY score. You may build new eyewears or augment old ones during any period of downtime you have.

#### The Cost of Eyewear 

If you need to build eyewear that you can’t build for free from your DIY score, you will need to buy the materials for it.

        Every augment will increase the price. The higher the marque, the greater the price. The market price for an augment can be found in the chart below.

If you are building the augment, you pay 1/5th the price, which is the same as if you were buying an augment one marque lower. (As in, the material cost for a Marque III augment is the market price for a Marque II augment.) The material cost for a Marque 1 augment is 1 prince and 2 dukes.

#### Beta Eyewear
> Augments +2, DIY +1, Hit Points +4

> Gadgetry Specialty

> ❓ Requires: 4 skill points in Gadgetry & Optician specialty

You thought you were being clever when you decided to add a bunch of extra lenses and loupes onto your monocle. Instead, now nobody else can figure out how to use them. Such eyewears have two more slots for you to place augments into.

        If anybody other than you attempts to use one of your beta eyewears, they must succeed in rolling a science result one tier higher than the highest level marque you have on your eyewear. If your eyewear has a Marque IV augment, it is impossible for them to use it (unless they can somehow obtain a tier result of 5 with their Sciences attribute).

#### Prototype Eyewear
> Augments +2, DIY +1, Hit Points +5

> Gadgetry Specialty

> ❓ Requires: 16 skill points in Gadgetry, Optician, & Beta Eyewear specialties

You’ve perfected your beta eyewears and made them user-friendly. Now anybody can use an eyewear that you designate as being a prototype.

### Eyewear Augments

#### Alert
>   Eyewear Augment

Takes up 3 Augment Slots on Eyewear

Alert goggles have small sirens built into them which go off whenever they sense something headed in your direction. Small lights in the lenses point you in the direction of the objects in question.

|Marque| Effect|
|--|--|
|1|+1 on evade rolls|
|2|+2 on evade rolls|
|3|+3 on evade rolls|
|4|+4 on evade rolls|

#### Dark Adaptor
>   Eyewear Augment

The eyewear is built to allow you to see in the dark as if it were normal daytime vision.

|Marque| Effect|
|--|--|
|1|Poor Lighting|
|2|Poor Lighting|
|3|Total Darkness|
|4|Total Darkness|

#### Far-Sight
>   Eyewear Augment

Far-sight goggles allow the wearer to see things at distances that seem superhuman.

|Marque| Effect|
|--|--|
|1|Can see 1,000 feet farther away with ease|
|2|Can see 2,000 feet farther away with ease|
|3|Can see 3,000 feet farther away with ease|
|4|Can see 4,000 feet farther away with ease|

#### Frightening Faceplate
>   Eyewear Augment

You’ve added decoration to your eyewear designed to make your face more intimidating while wearing them. This gives you a bonus when intimidating someone

|Marque| Effect|
|--|--|
|1|+2 to Cunning for intimidation|
|2|+4 to Cunning for intimidation|
|3|+8 to Cunning for intimidation|
|4|+16 to Cunning for intimidation|

#### Happy Place Vision
>   Eyewear Augment

Takes up 3 Augment Slots on Eyewear

Regardless of what kind of situation you are in, Happy Place Vision makes whatever you fear the most look like your favorite dessert! While using eyewear with Happy Place Vision, you will never become scared or flustered.

> 🗒️️         Note: This augment always acts as Marque II for the purposes of determining cost, though you can learn this augment despite your skill in Gadgetry.

#### Heat Detection
>   Eyewear Augment

Your eyewear can pick up heat signatures on other people, allowing you to faintly see people through cover. You can see people through degrees of cover based on your marque.

|Marque| Effect|
|--|--|
|1|Poor Cover|
|2|Light Cover|
|3|Medium Cover|
|4|Heavy Cover|

#### Inventory Investigator
>   Eyewear Augment

> 🪙 Cost: 1 AP to activate

Perfect for lawmen searching hoodlums for hidden weaponry (and for pickpockets sizing up a potential target), you’ve augmented your goggles with the ability to see some of the items a target within 25 feet has concealed.

|Marque| Effect|
|--|--|
|1|Can tell if they have concealed items, but can’t identify them|
|2|Knows how many concealed items they have, but can’t identify them|
|3|Identifies all concealed items on target|
|4|Identifies all items on target and determines if they are augmented|

#### Pin-Pointing
>   Eyewear Augment

> 🪙 Cost: 1 AP to activate

The user of Pin-Pointing eyewear is capable of improving his or her depth perception and accuracy using a cross-hair and a series of finely tuned zoom lenses. The lenses must be adjusted, so the user must spend 1 action point before firing to reap its effects.

|Marque| Effect|
|--|--|
|1|+1 to accuracy|
|2|+2 to accuracy|
|3|+3 to accuracy|
|4|+4 to accuracy|

#### Poison Detection
>   Eyewear Augment

Poison-detecting eyewear reacts to invisible poisons, helping to alert the wearer of possible threatening chemicals.

|Marque| Effect|
|--|--|
|1|+3 to notice poisons|
|2|+6 to notice poisons|
|3|+9 to notice poisons|
|4|+12 to notice poisons|

#### Protective
>   Eyewear Augment

Protective eyewear add extra tiers of soak and resist bonuses against attacks that are directed against the eyes.

|Marque| Effect|
|--|--|
|1|+1 soak class against called shots to the eyes & +3 on Brute resists against things that affect eyes|
|2|+2 soak class against called shots to the eyes & +6 on Brute resists against things that affect eyes|
|3|+3 soak class against called shots to the eyes & +9 on Brute resists against things that affect eyes|
|4|+4 soak class against called shots to the eyes & +12 on Brute resists against things that affect eyes|

#### Tinted
>   Eyewear Augment

Tinted eyewear not only looks super-cool, but also prevents the user from experiencing the negative effects of some flashes of light.

|Marque| Effect|
|--|--|
|1|+3 to resist being blinded from flashes|
|2|+6 to resist being blinded from flashes|
|3|+9 to resist being blinded from flashes|
|4|+12 to resist being blinded from flashes|

#### Weatherproof
>   Eyewear Augment

The eyewear allows you to see well through sleet, snow, and rain. You take no penalty for looking through inclement weather.

> 🗒️️         Note: This augment always acts as Marque II for the purposes of determining cost, though you can learn this augment despite your skill in Gadgetry.

#### Zoom Lens
>   Eyewear Augment

> 🪙 Cost: 1 AP to activate

Zoom lenses are used commonly by long-ranged riflemen in order to increase their range on the battlefield. For an extra action point to adjust, ranged weapon users (except those using throwing weapons) may gain a bonus to their range.

|Marque| Effect|
|--|--|
|1|+50 feet|
|2|+100 feet|
|3|+150 feet|
|4|+200 feet|

#### Trinket Crafter
> Augments +2, DIY +1, Hit Points +4

> Gadgetry Specialty

You can create trinkets, a catch-all term for small contraptions that do some really unique things.

#### Number of Trinkets you can Maintain  

Without needing to buy pieces or parts, you can build some trinkets entirely out of scraps. These trinkets must be constantly maintained by you and stop working soon after leaving your care. You may build and maintain a number of trinkets based on your DIY score. You may build new trinkets or replace old ones during any period of downtime you have.

#### The Cost of Trinkets 

If you need to build a trinket that you can’t build for free from your DIY score, you will need to buy the materials for it.

        Each trinket has a slight variation in cost, and that cost will be noted below the trinket (in a chart showing the market prices).

        If you are building the trinket, you pay 1/5th the price, which is the same as if you were buying a trinket one marque lower. (As in, the material cost for a Marque III trinket is the market price for a Marque II trinket.) The material cost for a Marque 1 trinket is 1/5th the price of the Marque I trinket.

### Trinkets

Aether Pointer

Trinket

Size: Light

> ⬇️ Resist: Cunning (marques down)

> 🪙 Activation Cost: 2 AP

A miniscule, highly concentrated beam of light is emitted by this contraption through the use of a weak aether resonator. Not large enough to light much of anything, the best use of this object is shining it into the eyes of others for your own enjoyment. (If you attempt to point the aether pointer at somebody’s eyes, you must make an accuracy versus their evade, and then they must resist the effect.)

|Marque| Effect|
|--|--|
|1|Blurry vision (-2 to accuracy and evade) until the end of their next turn|
|2|Blurry vision (-2 accuracy & evade) for the next 2 turns|
|3|Blurry vision (-2 accuracy & evade) for the next 3 turns|
|4|Blurry vision (-2 accuracy & evade) for the next 4 turns|

Alarm Box

Trinket

Size: Light

> ⬇️ Resist: Cunning (marques down)

> 🪙 Activation Cost: 1 AP (see below)

This small, unremarkable box can be activated and deactivated for 1 action point. Whenever someone enters a space adjacent to the person carrying an activated alarm box, its pivoting speaker turns toward them and blasts an ear-piercing siren. Afterward, the alarm box shuts down, requiring it to be reactivated. Someone must move towards the alarm box for it to sound; carrying or throwing an alarm box past someone or activating an alarm box for use against someone already adjacent to you will not activate its alarm.

|Marque| Effect|
|--|--|
|1|Deafened (-2 evade) for 1 turn|
|2|Deafened (-2 evade) for 2 turns|
|3|Deafened (-2 evade) for 3 turns|
|4|Deafened (-2 evade) for 4 turns|

Alchemical Tooth

Trinket

Size: Light

> 🪙 Cost: 0 AP

This fake tooth is in reality a small capsule you can fill with a dose of an alchemical potion! For no action point cost during your turn you can bite down on it to release the potion into your system.

|Marque| Effect|
|--|--|
|1|Tooth is crushed after every use, ruining your smile|
|2|Tooth can be refilled during a breather (15-30 minute break)|
|3|Tooth can be refilled for 3 AP|
|4|Tooth can be refilled for 1 AP|

Collapsible Ladder

Trinket

Size: Medium or Heavy (when unwound)

The collapsible ladder is a bundle of rods that, when unwound, form together to make a ladder. The ladder requires action points to set up and take down, and it only goes so high, depending on the marque of the ladder.

|Marque| Effect|
|--|--|
|1|10 feet high and requires 10 AP to set up|
|2|20 feet high and requires 7 AP to set up|
|3|30 feet high and requires 4 AP to set up|
|4|40 feet high and requires 2 AP to set up|

Engineer’s Patch

Trinket

Size: Medium

Application Cost: 2 AP

Engineer’s patches are magnetic patches able to quickly attach and shape to an automaton or vehicle, temporarily repairing it. Patches are not a permanent fix. Patches can be hit as a called shot location on the item they’re attached to. If the patch is hit by a called shot, its wearer will lose the regained wounds from the patch as well as take the damage from the attack.

|Marque| Effect|
|--|--|
|1|Restores 4 wounds|
|2|Restores 8 wounds|
|3|Restores 12 wounds|
|4|Restores 16 wounds|

Extending Periscope

Trinket

Size: Medium

> 🪙 Cost: 1 AP per 5 feet extended

You have a one-handed and portable periscope that can be maneuvered around any number of corners, out of water, or out of a cloud of smoke. The periscope is not very discreet in-and-of itself, and trying to be sneaky about it will require a Cunning roll as if you were hiding yourself. It costs 1 action point to extend your periscope 5 feet, and the periscope can be extended a maximum distance based on its marque.

|Marque| Effect|
|--|--|
|1|20 feet|
|2|40 feet|
|3|60 feet|
|4|80 feet|

Gasmask

Trinket

Size: Light

Cost to Put On: 3 AP

Not only does this poignant fashion statement require no hands when it’s strapped to your face, it also helps filter your breathing, protecting you from harmful airborne chemicals. Only one can be worn at a time.

|Marque| Effect|
|--|--|
|1|+4 to resists against alchemical gases|
|2|+8 to resists against alchemical gases|
|3|+12 to resists against alchemical gases|
|4|+16 to resists against alchemical gases|

Grabnet

Trinket

Size: Medium

> 🪙 Cost: as a thrown weapon attack (generally 2 AP)

A medium-sized restraining device, people you successfully hit with your net act as if they have been grabbed without you having to be up-close-and-personal with them. While under your net they cannot take any actions other than removing the net capturing them. It can be used on enemies adjacent to you or shot through a firearm with the delivery augment.

|Marque| Effect|
|--|--|
|1|Costs 1 AP to remove the grabnet|
|2|Costs 2 AP to remove the grabnet|
|3|Costs 3 AP to remove the grabnet|
|4|Costs 4 AP to remove the grabnet|

Grapple-Gun

Trinket

Size: Medium

> 🪙 Cost: 1 AP to launch, 1 AP to unlatch, 1 AP to retract

This is a metal grappling hook launching device. The hook is designed to mechanically bend outward, attaching itself onto most surfaces perfectly. You can unlatch the hook from any surface with a simple tug on its line. It can stick to most ledges and flat surfaces regardless of whether it is rocky, magnetic, rough, or slick. The maximum length of chain, cable, or rope that it can successfully propel upwards is 50 feet. How fast it can pull you upwards is based on its marque. If you can get footing on the surface you are climbing, your climb speed is added to the speed of your grappling hook. While hanging from one grappling hook, you can easily fire another one further upwards with your other hand if you have a spare.

|Marque| Effect|
|--|--|
|1|20 feet per action point spent retracting|
|2|30 feet per action point spent retracting|
|3|40 feet per action point spent retracting|
|4|50 feet per action point spent retracting|

Grease Guzzler

Trinket

Size: Light

> ⬇️ Resist: Dexterity (negates, see below)

> 🪙 Activation Cost: 3 AP

This little contraption pours slippery grease all over a small area. Anybody who walks into the space of a grease guzzler must make a Dexterity result of tier 2 or fall prone. Upon standing up and trying to move, they must make the resist again. A grease guzzler may be used up to three times before having to refill it with the slippery grease.

|Marque| Effect|
|--|--|
|1|Covers a space 5 feet x 5 feet|
|2|Covers a space 10 feet x 5 feet|
|3|Covers a space 10 feet x 10 feet|
|4|Covers a space 15 feet x 10 feet|

Handcuffs

Trinket

Size: Light

> ⬇️ Resist: Brute (negates)

> 🪙 Cost: 1 AP

Handcuffs allow you to put a hold on someone you’re already grabbing. Perfect for those gentlemen adventurers of weaker physique, you can attach one cuff of your set to any body part you currently are grabbing. Its extendable chain design allows it to tighten around the biggest (and smallest) of limbs. One set of handcuffs consists of two cuffs which can attach to one limb apiece. They remain attached until the person bound by them is able to break free or until you voluntarily remove them. Until then, the wearer of your handcuffs acts constantly grabbed. They can move, but once both cuffs are grabbing something they can’t use any limbs your handcuffs are attached to.

|Marque| Effect|
|--|--|
|1|Tier 2 Brute to break free|
|2|Tier 3 Brute to break free|
|3|Tier 4 Brute to break free|
|4|Impossible (Unless you can somehow attain a Tier 5 Brute)|

HoverPack

Trinket

Size: Heavy

The hover pack is a large boxy backpack that contains a graviton sphere allowing its wearer to hover vertically.

|Marque| Effect|
|--|--|
|1|May move vertically 5 feet per move|
|2|May move vertically 10 feet per move|
|3|May move vertically 15 feet per move|
|4|May move vertically 20 feet per move|

Illumitorch

Trinket

Size: Light

> 🪙 Activation Cost: 1 AP

An illumitorch looks, in many ways, like a normal torch. The difference is that it has a small bulb at the end. By rotating a knob, the illumitorch creates light up to a distance away determined by its marque. It can be dimmed to illuminate anywhere from 5 to the maximum feet away. If you’re using an illumitorch in a small or enclosed area, it’s likely to light the entire area.

|Marque| Effect|
|--|--|
|1|25 feet|
|2|50 feet|
|3|75 feet|
|4|100 feet|

Insta-Bridge

Trinket

Size: Medium

> 🪙 Cost: 1 AP (see below)

The Insta-Bridge is perfect for the more venturesome traveler. Perfect for passing over ravines or creating handy ramps up hills, the Insta-Bridge is 5 feet wide and hand-cranked. For every action point you spend cranking it, it will extend or retract 10 feet, extending as far as its maximum length. It can be cranked in and out from either of its two ends. While extended out 10 feet or more, the Insta-Bridge becomes too cumbersome to lift.

|Marque| Effect|
|--|--|
|1|25 foot maximum length|
|2|50 foot maximum length|
|3|75 foot maximum length|
|4|100 foot maximum length|

Jaws of Life

Trinket

Size: Medium

> 🪙 Cost: 2 AP to activate

The jaws of life is a tool with thin, metal blades sticking out from its base. It’s used for prying things open by sliding its blades into an opening and activating it. Roll Brute on behalf of the tool, using the indicated bonus.

|Marque| Effect|
|--|--|
|1|+4 on the Brute roll|
|2|+8 on the Brute roll|
|3|+12 on the Brute roll|
|4|+16 on the Brute roll|

Metal Cutter

Trinket

Size: Medium

> 🪙 Cost: 3 AP

The metal cutter looks like a large pair of steam-powered wire snips and functions in much the same way. Using the metal cutter requires 3 action points, but it can automatically cut through a certain thickness of metal.

|Marque| Effect|
|--|--|
|1|3 inches of metal|
|2|6 inches of metal|
|3|1 foot of metal|
|4|3 feet of metal|

Messenger Sphere

Trinket

Size: Light

> 🪙 Cost: 2 AP to insert item, 3 AP to activate

An efficient mode of communication, the messenger sphere appears as nothing more than a small hollow sphere you can place a single concealable item, most commonly a written note, into. A destination can be programmed into it using dials representing longitude and latitude. When activated sides of the sphere extend outward to form propellers, spinning quickly to create lift. It will then fly toward its destination at a speed based on its marque.

|Marque| Effect|
|--|--|
|1|185 feet per turn (roughly 500 miles a day)|
|2|370 feet per turn (roughly 1000 miles a day)|
|3|555 feet per turn (roughly 1500 miles a day)|
|4|925 feet per turn (roughly 2500 miles a day)|

Mold-Maker

Trinket

Size: Light

A trinket made for the more thrifty adventurer, the mold-maker is a blanket which hardens when it is wrapped around items of a size determined by its marque. This allows you to recreate any item you come across at its base material cost without the need of a crafter specializing in that field. The mold-maker only copies the shape of an item and thus cannot replicate augments of any kind. After so many uses the mold-maker will become stuck in the form of the last item it copied. The mold-maker takes about two minutes to use.  

|Marque| Effect|
|--|--|
|1|Can wrap around light or smaller items|
|2|Can wrap around medium or smaller items|
|3|Can wrap around heavy or smaller items|
|4|Can wrap around super-heavy or smaller items|

> 🗒️️ Note: Most merchants won’t appreciate you making molds of their wares. Please use responsibly.

Mostly-Universal Lock

Trinket

Size: Light

> 🪙 Cost: 1 AP to lock a held item, Ranged Called Shot to throw

As the name implies, this lock can secure almost anything. Doors, luggage, handcuffs, vehicles and more can be sealed shut with this device, and it takes anyone trying to lockpick it two free hands and an amount of action points based on the marque of this augment to do so. If thrown accurately, it will automatically lock any compatible item it lands on.

        If you crafted the lock, you can also craft keys for it at no cost. Keys allow you to unlock the lock for 1 action point.

|Marque| Effect|
|--|--|
|1|3 AP to pick the lock|
|2|5 AP to pick the lock|
|3|7 AP to pick the lock|
|4|10 AP to pick the lock|

> 🗒️️ Note: Making keys for a Mostly-Universal Lock has a material cost of 1 dukes per key and a market cost of 5 dukes.

Omni-Trinket

Trinket

Size: Light (see below)

Tired of having to switch between all of your trinkets? The omni-trinket combines trinkets together into a single item, allowing you to hold multiple trinkets at once! The omni-trinket at default is a light item, but increases in size to match the largest sized item attached to it. Items attached can be switched out during a breather (a 15-30 minute break). You must spend action points to use each item individually; you cannot combine actions to reduce action point cost.

|Marque| Effect|
|--|--|
|1|Combines up to 2 Trinkets|
|2|Combines up to 4 Trinkets|
|3|Combines up to 6 Trinkets|
|4|Combines up to 10 Trinkets|

Palm Injector

Trinket

Size: Light

> 🪙 Activation Cost: 0 AP

This trinket puts a small button in your palm, hooked up to an injector line. When you push the button (an action that requires no action points), you are injected with a single alchemical substance of your choosing. Replacing the alchemical substance after use requires a number of action points based on the marque of the palm injector.

|Marque| Effect|
|--|--|
|1|4 action points to reload|
|2|3 action points to reload|
|3|2 action points to reload|
|4|1 action point to reload|

Alchemical substance not included.

Parachute Glider

Trinket

Size: Medium

> 🪙 Cost: 1 AP (can be done reflexively)

Designed with daredevils in mind, the parachute glider is a trinket you can wear on your back. Not only will it prevent you from taking any damage from falling, it will also keep you aloft by gliding in place without falling for a number of turns based on its marque, allowing you to keep on fighting any airborne foes who sank your battle-airship. Your fall begins when your action points refresh after your final turn of gliding. The parachute glider includes a ripchord allowing you to end your time gliding prematurely for no action point cost.

|Marque| Effect|
|--|--|
|1|1 turn before falling|
|2|2 turns before falling|
|3|5 turns before falling|
|4|10 turns before falling|

Pitcase

Trinket

Size: Medium

> 🪙 Cost: 1 AP to place, 3 AP to throw

This trendy leather briefcase can quickly reveal the metal spiral on its side to be a collapsible drill. When placed in an adjacent space or thrown up to 25 feet away, the pitcase will drill a circle 5 feet in diameter downwards up to 15 feet (you can set it to a lower distance manually for no action point cost at any time before use). It can also be held to tunnel you in any direction at a rate of 15 feet per action point. The pitcase cannot burrow through worked stones, mountains, metals or similar materials.

> 🗒️️         Note: This market price for this trinket is 25 princes (and so costs 5 princes to make).

Portable Doorframe

Trinket

Size: Medium

> 🪙 Cost: Based on Marque (see below)

Designed for true eccentrics, this gadget produces a free-standing doorframe, complete with door and knobs on both sides, at a moment’s notice. This doorframe accomplishes little past putting a door between you and any pursuers, although they can simply open it for 1 action point or alternatively move around it. It easily attaches to any wall or to other doorframes, although opening it while it’s attached to a wall will reveal the solid wall behind it. It provides heavy cover to anyone standing behind it while it is free-standing and closed.

|Marque| Effect|
|--|--|
|1|2 AP to deploy/retract|
|2|1 AP to deploy/retract|
|3|0 AP to deploy/retract|
|4|0 AP to deploy/retract (and can be done reflexively)|

Porta-Bull

Trinket

Size: Light

> 🪙 Cost: 3 AP

Enemies behind cover will fear your crafty visage when you walk up to their barricades and smash them with your tiny trinket. The porta-bull is a handheld device designed to attach to a single piece of cover and repeatedly beat it with a hydraulic ram until it breaks. It can be thrown a maximum of 50 feet. After a certain amount of damage to the cover it attaches itself to, the porta-bull will fall off and will need to be picked up before it can be used again. Any damage it does to a piece of cover lasts until someone can repair the cover outside of combat. This device has too much trouble staying attached to flat, unmoving surfaces to be able to ever attach to a living creature.

|Marque| Effect|
|--|--|
|1|Lowers cover by one degree before falling off|
|2|Lowers cover by two degrees before falling off|
|3|Lowers cover by three degrees before falling off|
|4|Lowers cover by four degrees before falling off|

> 🗒️️ Note: The porta-bull is not strong enough to damage anything considered full cover.

Propeller Boots

Trinket

Size: Medium

These boots are simple looking enough, except for the large flip-out device on the back. Once activated (for just 2 action points), the boots activate a propeller on the back that comes around over the heel of the boot. This grants a speed bonus to the wearer when swimming.

|Marque| Effect|
|--|--|
|1|+5 to swim speed|
|2|+10 to swim speed|
|3|+15 to swim speed|
|4|+20 to swim speed|

Pulse Detector

Trinket

Size: Light

> ⬇️ Resist: Cunning or Spirit (negates, see below)

> 🪙 Activation Cost: 2 AP

This is a small, flat, metal device with a glass cover. It is able to pick up on the heartbeats of organisms around it. After picking up on a heart beat, the trinket will display it by raising small pins under the glass in the direction of the heart beat’s source.

        Somebody who is aware of the pulse detector can attempt to hide their heartbeat, which requires a Cunning or Spirit resist (target’s choice) with a result equal to the item’s marque.

|Marque| Effect|
|--|--|
|1|Detects heart beats within 25 feet|
|2|Detects heart beats within 50 feet|
|3|Detects heart beats within 75 feet|
|4|Detects heart beats within 100 feet|

Reasonable Doubt

Trinket

Size: Light

> 🪙 Cost: 1 AP to place, 2 AP to throw

Worried about getting fingerprints on a lock? Perhaps you simply prefer to have someone, or something, else do the dirty work? The Reasonable Doubt lockpicking device will attach to any lock and quickly get to work picking it for you. When your action points refresh it will spend a number of action points picking the lock based off of its marque.

|Marque| Effect|
|--|--|
|1|1 AP per turn spent lockpicking|
|2|2 AP per turn spent lockpicking|
|3|3 AP per turn spent lockpicking|
|4|4 AP per turn spent lockpicking|

Spring-Loaded Sleeve

Trinket

Size: Light

> 🪙 Cost: 0 AP to activate

The spring loaded sleeve holds a light weapon or small item (such as a potion vial or explosive). With a flick, you can release the spring, sending the item straight into your hand. (If your hand is not free, it’ll fall at your feet...and break, if it’s a potion vial.) Retracting the spring takes a few seconds, depending on the marque of the sleeve.

|Marque| Effect|
|--|--|
|1|2 AP to retract|
|2|1 AP to retract|
|3|0 AP to retract|
|4|0 AP reflexively to retract (can do it during anyone’s turn)|

Toolbelt

Trinket

Size: Medium

This utilitarian fashion accessory can store a number of light-size items. These items are considered drawn, allowing you to switch between them at no action point cost during your turn. Unfortunately any items attached to your toolbelt are unconcealable and suffer a -8 when resisting sundering. Wearing the toolbelt does not require hands.

|Marque| Effect|
|--|--|
|1|Can hold two light items|
|2|Can hold four light items|
|3|Can hold six light items|
|4|Can hold eight light items|

Vacuum of Fire  

Trinket

Size: Medium

> 🪙 Cost: 2 AP to release fire

What appears to be nothing more than a common household cleaning appliance is actually the latest in fire-fighting technology. When aimed at a fire of any kind within 10 feet, this suction device will decrease the amount of action points required to put the fire out to a minimum of 1 action point. Once the required action points have been spent, you can choose to extinguish the fire normally or suck the fire into your vacuum. If you choose the latter, you can spray the fire back out onto a different area or person. It retains whatever tier of fire it was previously and can be blasted anywhere within 10 feet. The vacuum can hold one charge of fire at a time. If the fire it ingests would normally destroy it from the outside, it will destroy it from the inside as well, the flame extinguishing afterwards. You can permanently extinguish a flame inside of your vacuum for no action point cost at any time.

|Marque| Effect|
|--|--|
|1|-1 AP to extinguish flame (to a minimum of 1)|
|2|-2 AP to extinguish flame (to a minimum of 1)|
|3|-3 AP to extinguish flame (to a minimum of 1)|
|4|-4 AP to extinguish flame (to a minimum of 1)|

Walker

Trinket

Size: Medium

> 🪙 Cost: 1 AP to activate

This is a device that walks forward in a straight line at a speed of 20 feet per turn. It weighs roughly equivalent to a gnome and sets off most traps it walks across.

|Marque| Effect|
|--|--|
|1|Will take 10 damage before breaking|
|2|Will take 20 damage before breaking|
|3|Will take 30 damage before breaking|
|4|Will take 40 damage before breaking|

Wall-Scaler

Trinket

Size: Light

This handheld item has a rotating, barbed head at the end. It will snatch on to any wall it is put up against and climb up it, leaving small indentions along the wall. It requires one hand to hold on to, and cannot climb up very solid walls (like a wall made out of steel) or inclines.

|Marque| Effect|
|--|--|
|1|Wall-scaler can move 10 feet per turn|
|2|Wall-scaler can move 20 feet per turn|
|3|Wall-scaler can move 30 feet per turn|
|4|Wall-scaler can move 40 feet per turn|

Water-Filter

Trinket

Size: Light

> 🪙 Cost: 2 AP per vial

Ideal for travelers entering less-hospitable areas of the world (or afraid of poisons), this bottle cap fits onto most vials of liquid. Its perforated top can be used to pour out any unwanted particles and chemicals, leaving you with a vial of pure water. This filter will break with repeated uses.

|Marque| Effect|
|--|--|
|1|3 uses before breaking|
|2|15 uses before breaking|
|3|75 uses before breaking|
|4|500 uses before breaking|

> 🗒️️ Note: A water filter made with your DIY will never break unless it leaves your care.

