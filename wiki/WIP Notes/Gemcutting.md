# Gemcutting

## Using Gems

Gems are exceptionally good vessels. With the correct type and preperation, they can capture nearly any kind of energy, and that energy can be used in a variety of ways.

### Gathering Charge

Charging gems is simple, but not easy. You must first obtain a gem of the proper aspecting, some gems find one type of energy abhorent, but naturally soak up another.  The simpliest way to charge a gem is to simply place it next to a source where it's aspect aligns, and it will naturally pull from that source. For other methods, well, it's easier to explain with an example.

Lets take a look at a Ruby, Rubies are aligned with Heat and Light, deviant to Force, antithetical to Cold and Dark, and neutral to everything else.

Since they are heat and light aspected. If you place one in sunlight, a flame, or an oven, it will charge. 

If you wanted to charge that Ruby with a neutral aspect, Like suction, the easiest way is to place it in between an imbalance of it's opposite. Suctions opposite is pressure, so if you put it on end of a tube and pulled a vaccum, it would (if a little slowly), charge with pressure. You can apply this same concept to it's aligned affinities to charge it more quickly or possibly overcharge it.

If you reallly wanted to charge a Ruby with Force, Ruby's deviant aspect, in a ruby for some reason, you could do it by amplifiying the contrast, or applying an overwhelming amount of charge. If you placed a Ruby inside a highly pressurized container for a few days, it might pick up a tiny bit of its charge for a little bit.

If you were dumb enough to try to charge a Ruby with Dark or Cold, you couldn't. If you were trying really hard with some of the other methods here such as putting it in a perfectly dark, freezing container for days, it would shatter. If it were charged and you attempted something like this, it would result in a big messy break.

### Expending Charge

The basic ways to expend charge, are through loss, the natural tendency of Gems to leak energy, by exchanging them with other gems, or through a medium.

All Gems have a natural rate of loss, measured in time. It's a measurement of how long a full charge will stay in a gem before it becomes to little to use. Gems are relatively stable when fully charged, and only leak tiny bits until a threshold of instability is reached, at which point they rapidly drain.

The result is that gems can be used for their full capacity even when they're about to hit their loss time, but you can't store part of a charge in a gem unless you immediately use that or partially use a gem unless youre using the remainder in the next couple seconds.

Exchanging energy between gems is trivial. You simply touch two gems together and the charge is naturally drawn to the gem with the higher alignment. If they are equally aligned, you can give a gentle nudge on one to force all the energy in the other, depending on the energy you're trying to transfer. There are also some simple tools availible to any gemcutter for transfering energy however you want.

Lastly, charge can be used through mediums. This is any device specifically designed to use energy from gems for some specific effect. This typically requires the gem to have a cut specifically to the medium, particularly if they use the charge gradually, over a longer period of time, and are designed to stabalize its loss. 

Any kind of weapon, armor, jewelry can take a general augment to store and use a gem, though the gem must also be cut specifically for that piece of equipment.

### Raw gems

Before you can make a gem, you first have to understand the qualities of raw gems, grade, capacity, aspect and loss.

Grade is a measure of how much work you can do on any given gem. There are average grades for gems you will find, but you can occasionally find outliers if you're willing to pay enough. Each gem can take polishes and cuts equal to its grade. So a grade 3 gem could have 3 polishes and 3 cuts applied to it.

Capacity is a rough estimate of how much energy a Gem can store, but a rule of thumb is a gem with 1 capacity can perform one simple action of it's aligned aspect.

|Aspect|Effect of 1 Capacity|
|-------|-------|
|Life| Heal one wound|
|Death| Inflict one wound|
|Cohesion| Repair a tear in a simple cloth|
|Dissolution| Obliterate an hand sized object|
|Force| Push a person back 15 ft|
|Suction| Pull a tub full of water 5 ft up|
|Heat| Boil a full pot of water from room temperature in ~10 seconds|
|Cold| Freeze a pot of water from room temperature in ~10 seconds|
|Light| Fully light a dark room up to about 60ft for an hour|
|Dark| Obscure the light in a well lit room/single large campfire for an hour|

Note: this is not an exhaustive list of all the different aspects.

These are just examples, and more creative uses of even small amounts of capacity can have a remarkable effect. Capacity is generally determined by the Gems size, so different types of gems might have different average capacities, but could find outliers at a premium if you can find a source for them. Gems with capacity below 1 are functionally impossible to use.

Loss is how long a gem can hold its full capacity before it hits the loss threshold and loses all its charge, this is measured anywhere from seconds to years. This is remarkably stable across gem type, though cuts and polish can have a massive impact on this.

Aspects are the types of energies that a gem can hold. Most have a 1-2 aligned aspects, matching antihesis, and maybe 1-2 deviants. 

|Aspect| Effects|
|-|-|
|Aligned|100% of loss time|
|Neutral | 50% of loss time|
|Deviant | 10% of loss time|
|Antithetical | Cannot charge, might break if a charge is attempted|

### Gemcutting Specialties And Augments

#### Gemcutter
>Augments +2, DIY +1, Hit Points +6

>Gemcutting Specialty

Gems can be fashioned to change many of their properties, and you have researched them. You may learn any polish and augment slots, and also know the Gem Slot augment, which you can use to embed any gems you craft and cut into anything you augment with it.

#### Artifical Gem Summoner
> Augments +2, DIY +1 Hit Points +7

You can generate a small supply of small artifical gems each day, though the are 0 grade, always have 1 capacity, 1 hour loss, and are randomly aspected.

>I will write a generator for this later promise.

//TODO Test Gems (universally aspected gems for testing, will probably require mq II skills)

#### Gem Slot
> General Augment
> 3 Augment Slots

This allows any piece of equipment to use a gem, though it is specific to the type and capacity gem you're using. This can be easily changed during downtime, but you can only slot for one type of gem at a time. This allows partial use of charges, and also acts to significantly reduce loss rates.

|Marque|Effect|
|------|-------|
|1|2x loss time|
|2|5x loss time|
|3|10x loss time|
|4|20x loss time|

#### Interchangeable Gem Slot
> General Augment
> 3 Augment Slots

This acts almost exactly like the Gem slot, but you no longer have to prepare it per gem type. There is a bit of a tradeoff in terms of loss stabilization with this though.

|Marque|Effect|
|------|-------|
|1|2x loss time|
|2|5x loss time|
|3|10x loss time|
|4|20x loss time|

### Gem Types

|Type| Avg Grade | Avg Capacity| Loss| Aligned|Deviant|Antithetical| Availibility |
|----|-----------|-------------|-----|--------|-------|------------|--------|
|Ruby| 2| 3| 6 Hours| Heat, Light| Pressure | Cold, Dark| Uncommon |
|Saphire| 2| 3| 6 Hours| Cold |  | Heat| Uncommon |
|Emerald| 2| 3| 1 Week | Life  |  | Death| Uncommon |
|Quartz| 2| 2| 1 Hour | Force |  | Pressure| Common |
|Onyx | 2| 2| 1 Day | Dark, Death | Heat| Life, Light| Uncommon | 
|Diamond| 4| 2| 1 Week| Force, Pressure, Light | Life, Death, Cohesion, Dissolution | Dark| Rare |
|Opal| 2| 1| 1 Week| Cohesion, Dissolution| | Life, Death, Dark, Light| Uncommon |

These are very subject to change.


### Polishes

#### Retaining
>Gem Polish
>2 Polish Slots

A fine polished gem is much better at retaining its energy, letting the gem hold energy for much longer. 

|Marque|Effect|
|------|-------|
|1|2x loss time|
|2|3x loss time|
|3|5x loss time|
|4|10x loss time|

### Cuts

#### Cut for Use
>Gem Cut
>1 Cut Slot

You must cut a gem like this in order to place it in any piece of equipment that allows the use of Gem Slots

#### Sharp
> Gem Cut
> 1 Cut Slots

A well cut gem has a much higher capacity, but the hard edges tend to accelerate loss.

|Marque|Effect|
|------|-------|
|1|2x capacity,  10% loss time|
|2|3x capacity 25% loss time|
|3|5x capacity 50% loss time|
|4|10x capacity|

