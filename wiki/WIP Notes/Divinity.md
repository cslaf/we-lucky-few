# Divinity Skill

You embody a piece of the divine. You have some medium through which you draw your divinity, be it a holy symbol, or even a divine companion (Iris), and you can use that divinity and light to blind your foes, guide your allies, and help those in pain.

#### Divine Flashbang
> Accuracy +1, Evade +1, Hit Points +9

> Cost : 2 AP

A fan of shock and awe tactics, you can channel a piece of divine light through your medium, blinding those without eye protection in who can see you.

|Tier|Effect|
|----|------|
|1|Blinded for 1 turn|
|2|Blinded for 2 turns|
|3|Blinded for 3 turns|
|4|Blinded for 4 turns|


Note: the following specialties are derivatives from specialties in the core system.

#### Praise
> Accuracy +1, Evade +1, Hit Points +7

> Divinity Specialty

> 🪙 Cost: 1 AP reflexively

You bless the struggles of your fellow adventurers. You may spend 1 action point reflexively to allow a party member to re-roll any resist, as long as they can hear you. You can only do this once per resist.


## Faith Skill
-----------

Faith is a powerful tool, and it’s one you can utilize to make your foes wish they had half as much faith as you. Regardless of where your faith comes from or toward what goals you put it, your devotion to a cause manifests itself in your actions. A person of faith can reinvigorate allies, move through obstacles as though they don’t exist, and survive far longer than what the faithless think possible.

#### Blind Faith
> Evade +1, Priority +2, Hit Points +8

> Faith Specialty

> ❓ Requires: 6 skill points in Faith

> 🪙 Cost: same as an Attack

You lash out without hesitation, believing that your next attack is going to land true. When making an unaltered attack (as in, no other specialties are applied to it, including those that do not use action points, such as Critical Hits in Espionage), you may use your skill in Faith in place of your accuracy bonus for the accuracy roll.

#### Conviction
> Accuracy +1, Strike +3, Hit Points +8

> Faith Specialty

> ⬇️ Resist: Spirit (negates)

> 🪙 Cost: Attack +1 AP

Any time you are attacking a corrupted creatures, such as those who have risen from the dead, automatons, or horrible abominations, you can attack with Conviction. If the target fails to resist, all of their damage soak is negated.

#### Divine Guidance
> Strike +2, Defense +2, Hit Points +9

> Faith Specialty

> 🪙 Cost: 2 AP reflexively

You close your eyes and guide your injured ally through faith alone. You can eliminate all status penalties on a person for one turn by giving them Divine Guidance at the beginning of their turn. Divine Guidance negates penalties from wounds, status effects, stuns, et cetera. (It does not negate fatal effects.) Any penalties incurred during their turn begin when their action points refresh, and all of their normal penalties return to them.

#### Flowing Vigor
> Evade +1, Defense +2, Hit Points +9

> Faith Specialty

> 🪙 Cost: 2 AP to begin and 1 AP to channel

You channel life back into an ally within 25 feet, replenishing their hit points. When you begin Flowing Vigor, you spend 2 action points and roll on the tier chart. Thereafter, you may heal that amount of hit points again for only 1 action point. You may continue the channel through your turn and into consecutive turns, but if you take any action other than channeling and moving, Flowing Vigor is broken and must be begun anew.

|Tier| Effect|
|--|--|
|1|2 hit points|
|2|4 hit points|
|3|6 hit points|
|4|8 hit points|

#### Grief & Hope
> Evade +1, Priority +2, Hit Points +8

> Faith Specialty

> 🪙 Cost: 3 AP (1 AP sacrifices)

The healing power of Grief and Hope can be an awesome sight to behold. You can channel that energy to reinvigorate ailing allies. For three action points, you can call upon your allies within 25 feet to give you their Grief and Hope in order to save another. When you do this, you can roll your Faith to heal 3 hit points per tier you reach. The target of the healing can be any ally within 25 feet.

|Tier| Effect|
|--|--|
|1|Restores 3 hit points|
|2|Restores 6 hit points|
|3|Restores 9 hit points|
|4|Restores 12 hit points|

For each action point an ally gives you, the amount of hit points the target heals increases by 2 per tier. Thus, if two people sacrifice an action point each for your channeled healing, the target will heal 7 hit points per tier your Faith roll receives.

        Each ally may only sacrifice 1 action point for any given usage of Grief & Hope.

#### Healing Halo
> Evade +1, Defense +2, Hit Points +10

> Faith Specialty

> 🧘 Stance (costs 1 AP to enter)

Everyone around you draws upon your divine energies. At the end of every turn that you are in your Healing Halo stance, you and your living allies within 25 feet regain a small amount of hit points. You and your allies gain 1 hit point, plus 1 for every 5 skill points you have in faith, at the end of every turn that you spent in Healing Halo (when your action points refresh).

#### Infallible Faith
> Evade +1, Defense +2, Hit Points +8

> Faith Specialty

> 🪙 Cost: 1 AP reflexively (1 AP sacrifices)

Your allies’ belief in you is infallible, their faith ensuring that you never fail. You may use Infallible Faith whenever you must roll a resist. You immediately gain a +2 bonus on the resist. Every ally within 25 feet may also sacrifice a single action point to you, increasing the bonus by another +3 per sacrificing ally.

#### Moral Support
> Accuracy +1, Evade +1, Hit Points +7

> Faith Specialty

> ❓ Requires: a Spirit of 8

> 🪙 Cost: 2 AP reflexively

When a friend is in need, you can push them forward to greater feats of heroism. Any time you know of an ally within 25 feet being forced to use an attribute as a resist or in some momentary peril (such as having to jump a chasm or wrestle free a powerful device from a crazed maniac), you may roll your heroics (found under the Spirit attribute) and give them the bonus. If you roll poorly and receive a penalty as per your heroics result, that penalty is applied on your ally’s attribute roll, regardless of whether they want it there or not.  

#### Prayer
> Accuracy +1, Defense +2, Hit Points +9

> Faith Specialty

> 🪙 Cost: 1 AP (1 AP sacrifices)

With your allies on each side of you, you know you’re able to push onward with only their prayers. When you begin the Prayer, you roll on the chart, granting yourself temporary hit points. For each action point an ally gives you, roll again on the chart. Each ally may only sacrifice 1 action point for your Prayer and must be within 25 feet to do so. Prayer may not be used again until these temporary hit points are gone. If you were damaged at the time the Prayer begins, these hit points instead act as healing.

|Tier| Effect|
|--|--|
|1|Gain 2 temporary hit point|
|2|Gain 3 temporary hit points|
|3|Gain 4 temporary hit points|
|4|Gain 5 temporary hit points|

#### Purify
> Evade +1, Priority +2, Hit Points +8

> Faith Specialty

> 🪙 Cost: 2 AP

By laying your hands on yourself or an ally, your devotion allows your target to ignore the penalties from an ongoing poison, disease, parasite, called shot, wound, or other effect for a number of turns equal to your skill in faith. The target can not be Purified again until the effects of the first Purification wear off. If the effect of the penalty would have naturally worn off during the time the Purification was occurring, it does not resume once the Purification ends.

|Tier| Effect|
|--|--|
|1|Ignore 1 effect|
|2|Ignore up to 2 effects|
|3|Ignore up to 3 effects|
|4|Ignore all effects|

#### Shock of Life
> Evade +1, Defense +3, Hit Points +11

> Faith Specialty

> ❓ Requires: 10 skill points in Faith

> 🪙 Cost: 2 AP

When times seem dire, you know how to shock somebody back into existence. You instantly heal, through touch, a number of hit points equal to twice your skill in Faith. This burst of energy, however, has some negative effects on the target. You roll your faith to determine how minor the negative effects are. A target may, at their discretion, ignore the Shock of Life altogether, though this must be decided prior to the Faith roll.

|Tier| Effect|
|--|--|
|1|Target is stunned for 3 AP|
|2|Target is stunned for 2 AP|
|3|Target is stunned for 1 AP|
|4|Target suffers no ill effect|

### AP Sacrifice Upgrade Specialties

#### Devoted Peers
> Evade +1, Defense +2, Hit Points +10

> Faith Specialty

> ❓ Requires: any specialty that calls upon sacrificial action points

Your friends are loyal and devoted like few others. When you call upon them to sacrifice their action points for your specialties, they may sacrifice up to two action points each.  

#### Self-Sacrifice
> Evade +1, Strike +2, Hit Points +8

> Faith Specialty

> ❓ Requires: 4 action points per turn & any specialty that calls upon sacrificial action points

Your faith is so strong that it alone can guide your attacks. When you call upon others to sacrifice action points to your specialties, you may sacrifice your own action points as well. You, however, are not limited, and may sacrifice as many action points as you have available to you.

#### Silent Devotion
> Evade +2, Priority +1, Hit Points +7

> Faith Specialty

> ❓ Requires: any specialty that calls upon sacrificial action points

Your beliefs do not need to be yelled, exclaimed, or shouted. When you call upon others to sacrifice action points to you, you do so by your willpower alone. You may now ask for your allies’ sacrifices with complete silence and no outward visual or audible  cues to your foes.  

### Champion Specialties

#### Appointed Champion
> Evade +1, Defense +2, Hit Points +9

> Faith Specialty

> 🧘 Stance (costs 1 AP to enter)

You mark a nearby ally as your Champion and channel your Faith into him. While in this stance, any time said Champion would need to make a Spirit roll, he may choose to let you roll for him and apply your Spirit attribute’s bonus.

#### Conduit of Faith
> Accuracy +1, Evade +1, Hit Points +8

> Faith Specialty

> ❓ Requires: Appointed Champion specialty

You draw the breath from nearby allies and empower your Champion. Anyone within 25 feet may sacrifice their last action point they have for the turn (at the end of their turn, when their action points refresh) to your Appointed Champion. It must be their last action point, and the action point is immediately added to the Appointed Champion’s pool of action points.

### Inquisition Specialties

#### Proclaim the Heretic
> Evade +1, Strike +2, Hit Points +8

> Faith Specialty

> 🧘 Stance (costs 1 AP to enter)

> 🪙 Cost: 1 AP reflexively

You proclaim an enemy to be a heretic, a faithless vagabond, and now no one will lose their courage against him. Choose a single opponent when you enter this stance. When that heretic attacks you or one of your allies, you or the ally can spend 1 action point in order to roll the Spirit attribute instead of defense in order to determine damage soaked.

        To Proclaim a different Hheretic, you will need to change stance (costing 1 action point).

#### Light in the Dark
> Accuracy +1, Evade +1, Hit Points +8

> Faith Specialty

> ❓ Requires: Proclaim the Heretic specialty

Your allies are protected by your faith when confronting the heretic. You and all of your allies may choose to use your Spirit in place of their defense bonus when soaking damage dealt by the Proclaimed Heretic. Making this choice does not cost anybody any action points.

### Smiting Specialties

#### Smite
> Accuracy +1, Strike +1, Hit Points +8

> Faith Specialty

> 🪙 Cost: Melee Attack +1 AP (1 AP sacrifices)

Your allies’ belief in your attack guides your hand against the unfaithful. You call for your allies’ faith, and their prayers give your attack strength. When you begin to make a Smite, you call for your allies within 25 feet to sacrifice 1 action point in order to empower your attack. Any ally who can hear you (or knows that you called for their belief) and is within range can, reflexively, sacrifice the action point. If your attack lands, your attack deals damage as if it were one damage class higher for every action point an ally sacrificed to you. If nobody sacrificed an action point for the smite, it is treated like a normal attack.

        An ally can only sacrifice one action point per smite, and if the smite misses, the sacrificed action points are simply lost.

#### Assured Success
> Accuracy +2, Priority +1, Hit Points +6

> Faith Specialty

> ❓ Requires: Smite specialty

When you Smite your foes, your blade is guided by faith into the enemy. For every action point sacrificed for your Smite, the attack gains a +1 on the accuracy roll.  

#### Impassioned Victory
> Accuracy +1, Strike +3, Hit Points +7

> Faith Specialty

> ❓ Requires: 17 skill points in Faith & Smite specialty

When your Smite fells an evil foe, the attack is inspiring and leads your allies on to greater deeds. When your Smite kills or incapacitates the person being attacked, all of those who sacrificed action points to empower your Smite instantly regain those sacrificed action points.

#### Smiting Shot
> Accuracy +1, Priority +2, Hit Points +8

> Faith Specialty

> ❓ Requires: Smite specialty

Your allies’ faith steadies your shot and flies with the bullet. You may now make ranged Smites, be it with a bow, firearm, or other ranged weapon.

#### Zealous Smite
> Accuracy +1, Strike +2, Hit Points +7

> Faith Specialty

> ❓ Requires: Smite specialty & 7 skill points in Faith

Your smite does not merely deal great destruction but is also a weapon of finesse and a tool of your faith. Any action point sacrificed to your Smite can, instead of increasing your attack’s damage class, be used like a normal action point for modifying the attack with another specialty that you know.

        For example, if you have the Conviction specialty (normally made as an attack +1 AP) and you make a Smite, you can convert one of the action points sacrificed to you in order to make your Smite into a Smite with Conviction.  


