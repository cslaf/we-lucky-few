For most purposes materials are roughly classified into Metals, Organics, Textiles and Woods. However, crafters find a lot of use in different types of alloys, textiles, woods and sometimes even stone! These may have more restrictions than their parent materials, and might be hard to procure, but they can also have some impressive advantages.

You may have to discover some of the properties of these materials through experimentation.

## Plastics
> +2 Augment slots

> Cannot be repaired if sundered
 
> Can only be used in light or lighter armor

> Can only be used in medium or lighter weapons

Plasics are quite easy to use, and can be used for just about anything, but their production is beyond reach for most modern societies, and are nearly impossible to repair if broken. Also, they are still unsuitable for the costruction of heavier weapons, armor, or any other applications.

There's still plenty to discover about plastics.


## Metal Alloys

More research needs to be done on metalurical techniques for alloying.

#### Hollow Steel

Super-light, strong and easy to work, but the techinque for its creation means it can only be created on a very small scale. Ideal for small machinery. A product of the GKE.


## Textiles

#### Sythetic fabrics
>No augment penalties, resistant to fire

#### Silk
> Much stronger, allow the use of textiles in heavier applications

## Organics

#### Otataral

A mysterious red stone, is malliable when heated and can be crushed into dust to work it in to basically anything. Rumored to have anti-magic properties.

