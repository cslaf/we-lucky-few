## Chronology Skill

(Maybe a science skill?)

### Using a Timepiece

A timepiece is attuned to its users time, they can invest a pool of AP from which they can pull or channel at will. Each timepiece has 4 properties based on its size, Storage, the amount of AP it can hold, Pull, the limited amount of AP you can draw or use from it per turn, it's Charge Ratio, how much time you need to spend to charge it for 1 AP, and Durability, which is the amount of damage it can take before it's rendered completely inoperable.

|Size|Storage|Pull|Charge Ratio|Durability|
|----|----------|----------------|-----------|----------|
|Light| 8 AP | 4 AP | 1 AP -> 1 AP/Second | 2 Wnds |
|Medium| 16 AP | 3 AP | 1 AP -> (1 min per AP) | 5 Wnds |
|Heavy| 24 AP | 2 AP | 1 AP -> (5 mins per AP)  | 8 Wnds |
|Super-Heavy| 48 AP | 1 AP | 1 AP -> (30 mins per AP)  | 10 Wnds |

This makes a full charge for a standard piece of each ~8 seconds, ~16 mins, 2 hrs, and 1 day.

You may transferring time between time pieces if you have multiple, and uses half the charge ratio of the recieving piece. (I.e. transfering 6 AP from a heavy->light would take 3 AP, but transfering 6 AP from light->heavy would take 15 mins.) Most timekeepers keep a light piece on as soon as they learn to manage two, as carrying one effectively halves the charge time for any other pieces they have.

### Larger Pieces

You've heard rumors of even larger pieces used by timekeepers, massive clock towers or even larger, but these require massive efforts of coordination between different timekeepers, and are closely held secrets. 

## Damaged Timepieces

When hit into wounds, roll a d12, if the die total is higher than the pieces total wounds, it is rendered temporarily inoperable until it is fixed. On a 12, the piece is fully sundered, destroying it completely and exploding with unpredictable results.


### Specialties

#### Xeno's Arrow
>2 Eva, +1 Aug, +6 Hp

>Chronology Specialty

>Requires: 12 points in chronology

You've stepped outside of space-time, and you realize that getting hit is merely matter of perception. You can spend any amount of stored AP to add to your evade against any ranged attack made against you, after you've seen both rolls.
(add a melee version, but higher level)


#### Reversion to the Mean
>1 Eva, +1 Aug, +7 Hp

>Chronology Specialty

You can reverse the flow of time to aid someone in dire straits. When somebody takes damage to wounds, or recieves a fatal wound, you can spend 4 stored AP the next turn to reverse it's effects. This AP cost doubles each turn in combat, or each minute out of combat.

#### All The Time in the World
> +1 Acc, +1 Aug, +8 Hp

>Chronology Specialty

You can transfer your pieces AP to a willing person by touching them. You are still restricted by it's pull, and they can either act immediately or use the AP on their turn.

#### It's Clock Time
> +2 Acc, +1 Aug, +8 Hp

>Chronology Specialty

>Requires: 6 points in chronology

Specialty that lets you maintain an additional timepiece per 6 points in chronology.

#### Quick Swapping
> +2 Eva, +1 Aug, +7 Hp

>Chronology Specialty

Allows quick swapping of timepiece augments without loss of charge or downtime.

#### Timekeeper 
> +2 Acc, +1 Aug, +6 Hp

Allows creation and usage of Timepieces. By default, they have 5 augment slots and by can only be used by their creator.

### Augments

#### Acceleration - 

>Timepiece Augment

Lets you use the timepiece as light/medium/heavy/super-heavy firearm, based on the size of the timepiece. AP cost and Damage Class are listed below and cannot be lowered by any specialties, and must be used from the Timepieces AP pool. You can split ready costs across turns, but may not split firing costs across turns (so a superheavy Timepiece cannot be used for acceleration without some modification modifying it's Pull).

You also must have some appropriately sized object to lauch as a projectile, but ask your Narrator if they want to track ammunition.

|Size|DC|AP to Use| AP to ready|Range|
|----|--|---------|------------|-----|
|Light|2|1|0|50 ft, -1 acc per 10ft past|
|Medium|4|1|1|100 ft, -1 acc per 25ft past|
|Heavy|6|2|1|200 ft, -1 acc per 25ft past|
|Super-Heavy|8|2|2|300 ft, -1 acc per 100ft past|

|Marque|Effect|
|------|------|
|1|Can be used as the firearm of the same size|
|2|Can be used as a firearm, size class below or above it|
|3|Can be used as a firearm two sizes above or below it |
|4|Can be used as a firearm of any class |

#### Stasis -

>Timepiece Augment

Lets you freeze someone or something in time for a set amount of AP, rendering it unable to move, evade or anything else. If the target is inanimate and not being acted upon by any forces stronger than gravity, the AP cost is halfed.
Modify these costs for size class of object and person. Can be tiered down with a brute or spirit resist.

|Marque|Effect|
|------|------|
|1| Freeze for 1 AP at a cost of 1 AP |
|2| Freeze for 2 AP at a cost of 1 AP |
|3| Freeze for 3 AP at a cost of 1 AP |
|3| Freeze for 4 AP at a cost of 1 AP |

#### Extra Valve 
>Timepiece Augment

>Two augment slots

Can draw additional AP from the timepiece per turn.

|Marque|Effect|
|------|------|
|1| Double Pull |
|2| Double Pull +1 AP |
|3| Double Pull +2 AP |
|4| + 2AP, Then double pull |

#### Capacity Boost
>Timepiece Augment

Increase the capacity of the timepiece, at cost of charge time.

|Marque|Effect|
|------|------|
|1| 2x Capacity, 6x Charge Time  |
|2| 2x Capacity, 4x Charge Time |
|3| 4x Capacity, 4x Charge Time |
|4| 4x Capacity, 2x Charge Time |

#### Reinforced
>Timepiece Augment

Gives the timepiece additional wounds
|Marque|Effect|
|------|------|
|1|+2 wounds|
|2|+4 wounds|
|3|2x wounds| 
|4|4x wounds|

#### Regrowth
>Timepiece Augment

Repair wounds every round.

|Marque|Effect|
|------|------|
|1|1 wounds per round|
|2|2 wounds per round|
|3|3 wounds per round, can repair self even if broken, but not shattered(12 on wounds roll)|
|4|4 wounds per round, can repair if shattered|

#### Ablative Regrowth
>Timepiece Augment

Will spend time from its pool instead of taking damage to repair itself. Can exceed pull values, but is automatic and cannot be turned off at will.

|Marque|Effect|
|-------|-----|
|1|1AP per wnd|
|2|1AP per 2 Wnd|
|3|1AP per 3 wnd|
|4|1AP per 4 wnd|

#### Bullet Time
>Timepiece Augment

You can reflexively spend AP to make sure you dodge attacks, you can spend up to your pieces Pull on yourself. You can spend this after you see both the accuracy and evade roll.

|Marque|Effect|
|-------|-----|
|1|+2 Eva 4 AP|
|2|+2 Eva 3 AP|
|3|+2 Eva 2 AP|
|4|+2 Eva 1 AP|


#### Paranoid
>Timepiece Augment

It knows when it's about to get hit and dodges. Evade bonus when targeted by a called shot.

|Marque|Effect|
|-------|-----|
|1|+1 Eva|
|2|+2 Eva|
|3|+4 Eva|
|4|+6 Eva|

#### Armored
>Timepiece Augment
(figure this out)


#### Hereidtary
>Timepiece Augment

Time stored in this piece can be transfered between timepieces whos owners are blood-bound. Ask your narrator what they consider blood-bound. Removal of this augment will clear any charge in this piece, and this cannot be used with the Super Charger augment.

#### Dynastic
>Timepiece Augment

>Requires : Hereditary

Time stored in thie piece can be used by any trained timekeepers who are blood-bound. Requires the Hereditary augment also be in this pieces, and removal of this augment will clear any charge in this piece, and this cannot be used with the Super Charger augment.

#### Super charger
>Timepiece Augment

The more time you put in at once, the more efficently you can put time in. If once you've stored a certain amount of AP in one uninterrupted charge session, the charge rate decreases significantly. You can fully charge your timepiece if you can charge it for a set, uninterriupted period of time. If you have to take a break, it's only charged at it's normal rate. It also maxes out at 144 AP.

Note: this does in fact bypass the charging rate increase from capacity, but maxes out at 144 AP.

|Marque|Effect|
|-------|-----|
|1|Fully charged after 3 Hours|
|2|Fully charged after 1.5 hours|
|3|Fully charged in 30 mins|
|4|Fully charged in 5 mins|
