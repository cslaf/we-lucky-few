
## Glamour Skill

You are an illusionist, able to manipulate Fey glamour to your will and advantage. You can change your appearance at will, confuse and confound your opponents, copy their skills and sometimes even their weapons. 

#### Masking
> Accuracy +1, Evade +2, Hit Points +8

> Glamour Specialty

> 🧘 Stance (costs 1 AP to enter)

> ⬇️ Resist: Cunning (negates)

You can use your skill in glamor to alter your appearance or the appearance of any equipment, armor, wepaon, or item you're using. You can only alter it by one size class (I.e. medium->Heavy or Medium->Light), and you can choose to use it as though it were the size of the actual object, or the illusory object, as well as what Damage or Soak class it is effectively. An observer makes a cunning roll if they want to inspect the item closely, when you enter the stance in their line of sight, or interact with it in any way (are hit by, or hit it), and can publically call you out to break your illusion.

#### Greater Masking
// Requires : Masking and 12(?) points in Glamour
// Specialty that extends the size restrictions on masking (? based on Glamour?)

#### Natural Masking
// Requires : Masking and x points in Glamour
// Lets you Mask for 0 AP

#### Imitated Engineering
//Requires Greater masking and Sincerest Form of Flattery

#### Sincerest Form of Flattery
> Accuracy +1, Strike +2, Hit Points +8

> Glamour Specialty

You can imitate any skill or action you see another person take, but you have to decide to memorize that action when you see it performed, and you can only hold one of these memories at a time. If the action is a skill/attribute check, you gain their bonus when you attempt the action, and if the action is a specialty or attack, you use their stats for the purposes of the attack. You don't copy any of the bonuses given by their equipment or augments on their equipment.

#### Eidetic Memory
// Requires : Sincerest Form of Flattery and 12(?) points in Glamour
//Specialty that lets you memorize more skills/actions based on your (Glamour)

#### Perfect Pretender
// Requires 12 points in Glamour
// Specialty that lets you use your Glamour instead of any Attribute, though any opposed rolls can be resisted with Cunning instead of the normal resist.

#### Helping Hand

Note: the following specialties are derivatives from specialties in the core system.

#### Captive Gaze
> Accuracy +1, Evade +1, Hit Points +9

> Glamour Specialty

> 🧘 Stance (costs 1 AP to enter)

> ⬇️ Resist: Cunning (negates)

You have the most facinating eyes. When entering this stance, choose a target you can make direct eye contact with. Your eyes lock, and they can't look away, and your piercing gaze holds them through any obstacle other than a foot of solid stone. Any actions they take against that aren't direct against you they must do as if blind (-4 accuracy on attacks, -4 evade on attacks made against them). Moving in a direction they're not facing can be difficult, and they must either move carefully (5ft per AP) or make a Dex save every move to avoid tripping. They can attempt to resist immediately when you enter the stance, and then at the end of their turn, when their action points refresh. If they successfully resist, you exit your stance.

#### Wingman
> Evade +1, Priority +3, Hit Points +8

> Glamour Specialty

> ❓ Requires: 2 skill points in Glamor

By adding in little snippets of information to help an argument, you add a bonus to another player’s cunning roll whenever they are attempting diplomacy, bluff, or any social interaction. The bonus is a +1 for every 2 skill points you have in Glamour.

#### Sleight of Hand
> Evade +2, Priority +2, Hit Points +6

> Glamour Specialty

When you use an item, one second it’s there, the next it’s gone. Your opponents won’t be able to take advantage of you while you use items. Activating and using items does not leave you open to reflexive attacks.

#### Unmarred Perfection
> Evade +1, Priority +2, Hit Points +8

> Glamour Specialty

> 🧘 Stance (costs 1 AP to enter)

> ⬇️ Resist: Cunning or Spirit (negates)

You radiate non-violence and tranquility. While in this stance, anybody that attempts to attack you must resist or suffer a penalty to their accuracy equal to your skill in Glamour. However, to maintain this aura, and the stance will break if you take any violent actions towards anybody.

#### Deafening Roar
> Strike +2, Defense +2, Hit Points +10

> Glamour Specialty

> ⬇️ Resist: Brute (tiers down)

> 🪙 Cost: 1 AP

You choose one adjacent opponent and roar so loudly in his ear that his eardrum bursts. The opponent is deafened (suffering a -2 to evade) for a number of turns.

|Tier| Effect|
|--|--|
|1|1 turns|
|2|2 turns|
|3|3 turns|
|4|4 turns|

#### Theater Projection
// Might be better as equipment(?)
//extend distance of voice based Glamours by 5ft per 6 points of Glamour.

#### Distract
> Evade +1, Priority +3, Hit Points +7

> Glamour Specialty

> ⬇️ Resist: Cunning (negates)

> 🪙 Cost: 2 AP reflexively

Sometimes you’re right in front of them. Other times, they’re not so sure. You can distract an opponent during their turn, momentarily thinking that you’re somewhere else. Whenever an opponent takes an action against you, you may attempt to distract them. Choose another location within 10 feet to create your distraction. If the opponent fails their Cunning resist against your Glamour, they - for the purpose of this one action - believe that you are in the location where you caused the distraction. If there is something else there (like a wall or a person), they gain a +10 on the resist.

#### Where'd They Go?
> Evade +1, Priority +3, Hit Points +7

> Showmanship Specialty

> ❓ Requires: 3 skill points in Glamour

> ⬇️ Resist: Dexterity (negates)

> 🪙 Cost: 3 AP reflexively

With the flick of your wrist, you can make an ally within your sight appear in one location when they thought they were elsewhere. When an ally has failed an evade roll, you may reflexively move your ally to an adjacent space. The assailant may resist against your Glamour with their Dexterity. If you succeed, your ally may immediately move to an adjacent space, and the assailant’s attack automatically misses.
