## Darkness Skill


### Smokescreen Specialties

#### Smokescreen
> Evade +1, Priority +3, Hit Points +8

> Showmanship Specialty

> 🪙 Cost: 1 AP

When you need to, you’ve always got a way to throw enemies off for just a second or two. You may toss down a Smokescreen during your turn that lasts until the end of your turn (when your action points refresh). This Smokescreen only envelopes you, but it hides all of your actions while in the Smokescreen. If you move, the Smokescreen does not move with you and instead disperses.

#### Walking Darkness
> Evade +1, Speed +5, Hit Points +7

> Showmanship Specialty

> 🧘 Stance (costs 1 AP to enter)

> ❓ Requires: Smokescreen specialty

More than a simple disappearing act, you walk across the battlefield shrouded by a magician’s smoke cloud. While in this stance, you gain a +4 to evade and all of your actions are considered hidden. If an opponent has some way of knowing where you are that does not rely on sight or can see through perfect darkness, you do not gain the bonus to evade against them.