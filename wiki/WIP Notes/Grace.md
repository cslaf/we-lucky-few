# Move to Grace?



### Choreographed Specialties

#### Epic Dance
> Evade +2, Priority +1, Hit Points +6

> Showmanship Specialty

> 🪙 Cost: 2 AP to begin, 1 AP to continue during subsequent turns

You bust out your best dance moves, chaining them together in a beautifully unpredictable fashion. While dancing your epic dance, you gain a +3 to evade. You gain an additional point of evade for every 5 skill points you have in Showmanship. You can only have one epic dance going at any given time.

        If tripped, your Epic Dance is cancelled and must be restarted.

#### Never Stop the Dance
> Evade +1, Defense +2, Hit Points +9

> Showmanship Specialty

> ❓ Requires: Epic Dance specialty

While performing your epic dance, you use your choreography to  block and brace your body against incoming blows. You gain an identical bonus to your defense in addition to the evade bonus you receive from your epic dance.


### Epic Music Specialties

#### Battle Theme
> Accuracy +1, Evade +1, Hit Points +7

> Showmanship Specialty

> 🪙 Cost: 2 AP to begin, 1 AP to continue during subsequent turns

Don’t worry, you’ve got this. The epic music in the air says that you can’t fail. While performing your battle theme, you gain a +3 to accuracy rolls. You gain an additional point of accuracy for every 5 skill points you have in showmanship. You can only be singing one battle theme at any given time.

        If you are singing, a successful called shot to your neck will cancel your battle theme. Alternatively, if you are using a musical instrument, a successful sunder or disarm will also cancel your battle theme.

#### Heavenly Serenade
> Evade +1, Priority +2, Hit Points +8

> Showmanship Specialty

> ❓ Requires: Battle Theme specialty

> ⬇️ Resist: Spirit (negates)

> 🪙 Cost: 2 AP reflexively

Your Battle Theme is so beautiful that it digs deep into people’s souls and prevents them from acting. While performing your Battle Theme, any time anybody within 25 feet wants to make an action, you can reflexively spend 2 action points to make them roll a resist against your Showmanship in order to take that action. If they fail the resist, they instead lose 1 action point.

#### Spotlight
> Accuracy +1, Evade +1, Hit Points +9

> Showmanship Specialty

> ❓ Requires: Battle Theme specialty

> 🪙 Cost: 1 AP reflexively

You point to an ally within 25 feet, signaling to them that it’s their time to shine. While performing your Battle Theme, you may spend 1 action point reflexively to grant your Battle Theme accuracy bonus to an ally’s accuracy, evade, strike, or defense roll.

#### Unified Chorus
> Accuracy +1, Strike +3, Hit Points +10

> Showmanship Specialty

> ❓ Requires: Battle Theme specialty & 16 skill points in Showmanship

Your beautiful song causes your allies to lend you their voices! Any bonuses to accuracy or strike you gain from your Battle Theme now apply to all allies within 25 feet of you. If multiple allies have this specialty, everyone only receives bonuses from the one with the highest amount of skill points in Showmanship.

#### Victory Theme
> Accuracy +1, Strike +2, Hit Points +9

> Showmanship Specialty

> ❓ Requires: Battle Theme specialty

You begin to weave a power ballad which drives you forward; your music crying for the utter destruction of your foes. Any bonus to accuracy you receive from your battle theme also acts as a bonus to your strike.
